//package jactor;
//
//import org.agilewiki.jactor.JAFuture;
//import org.agilewiki.jactor.JAMailboxFactory;
//import org.agilewiki.jactor.MailboxFactory;
//import org.agilewiki.jactor.RP;
//import org.agilewiki.jactor.lpc.JLPCActor;
//import org.agilewiki.jactor.simpleMachine.BooleanFunc;
//import org.agilewiki.jactor.simpleMachine.ObjectFunc;
//import org.agilewiki.jactor.simpleMachine.SimpleMachine;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/9/253:29
// */
//public class Test {
//
//    public static void main(String[] args) {
//        Test test = new Test();
//        test.test();
//    }
//
//
//    public void test() {
//        MailboxFactory mailboxFactory = JAMailboxFactory.newMailboxFactory(1);
//        try {
//            IfF actor = new IfF();
//            actor.initialize(mailboxFactory.createMailbox());
//            JAFuture future = new JAFuture();
//            System.out.println(SimpleRequest.req.send(future, actor));
//        } catch (Throwable e) {
//            e.printStackTrace();
//        } finally {
//            mailboxFactory.close();
//        }
//    }
//
//    class IfF extends JLPCActor implements SimpleRequestReceiver {
//
//        @Override
//        public void processRequest(SimpleRequest unwrappedRequest, RP rp) throws Exception {
//            SMBuilder smb = new SMBuilder();
//            smb._if(sm -> false, "skip");
//
//            smb._set(sm -> {
//                System.out.println("does not print");
//                return null;
//            });
//            smb._label("skip");
//            smb._set(sm ->{
//                System.out.println("Hello world!");
//                return null;
//            });
//            smb.call(rp);
//            //Output:
//            //Hello world!
//            //null
//        }
//    }
//}
