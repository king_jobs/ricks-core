//package jactor;
//
//import org.agilewiki.jactor.RP;
//import org.agilewiki.jactor.lpc.TargetActor;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/9/253:33
// */
//public interface SimpleRequestReceiver extends TargetActor {
//    public void processRequest(SimpleRequest request, RP rp)
//            throws Exception;
//}
//
