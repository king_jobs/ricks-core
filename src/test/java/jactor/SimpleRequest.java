//package jactor;
//
//import org.agilewiki.jactor.Actor;
//import org.agilewiki.jactor.RP;
//import org.agilewiki.jactor.lpc.JLPCActor;
//import org.agilewiki.jactor.lpc.Request;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/9/253:32
// */
//public class SimpleRequest extends Request<Object, SimpleRequestReceiver> {
//    public final static SimpleRequest req = new SimpleRequest();
//
//    @Override
//    public boolean isTargetType(Actor targetActor) {
//        return targetActor instanceof SimpleRequestReceiver;
//    }
//
//    @Override
//    public void processRequest(JLPCActor targetActor, RP rp) throws Exception {
//        SimpleRequestReceiver smDriver = (SimpleRequestReceiver) targetActor;
//        smDriver.processRequest(this, rp);
//    }
//}
//
