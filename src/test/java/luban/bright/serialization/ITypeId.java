package luban.bright.serialization;

public interface ITypeId {
    int getTypeId();
}
