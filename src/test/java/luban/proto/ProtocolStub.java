//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
package luban.proto;

import java.util.*;
import luban.bright.net.IProtocolCreator;
import luban.proto.test.Foo;
import luban.proto.test.TestProto1;
import luban.proto.test.TestRpc;
import luban.proto.test.TestRpc2;

public final class ProtocolStub {
    static final Map<Integer, IProtocolCreator> _factories = new HashMap<>();

    static {
        _factories.put(TestProto1.__ID__, TestProto1::new);
        _factories.put(Foo.__ID__, Foo::new);

        _factories.put(TestRpc.__ID__, TestRpc::new);
        _factories.put(TestRpc2.__ID__, TestRpc2::new);
    }

    public static Map<Integer, IProtocolCreator> getFactories() {
        return _factories;
    }
}