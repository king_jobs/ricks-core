package luban;

import luban.bright.serialization.AbstractBean;
import luban.bright.serialization.ByteBuf;

public class SimpleObject extends AbstractBean {

    private int c;

    private boolean g;

    private String n;

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public boolean isG() {
        return g;
    }

    public void setG(boolean g) {
        this.g = g;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    @Override
    public void serialize(ByteBuf bs) {
        bs.writeInt(c);
        bs.writeBool(g);
        bs.writeString(n);
    }

    @Override
    public void deserialize(ByteBuf bs) {
        c = bs.readInt();
        g = bs.readBool();
        n = bs.readString();
    }

    @Override
    public int getTypeId() {
        return 0;
    }
}
