package bitset;

import java.util.Arrays;
import java.util.BitSet;

public class BitSetConvert {

    public static byte[] bitSet2Bytes(BitSet bitSet) {
        byte[] bytes = new byte[bitSet.size() / 8];
        for (int i = 0; i < bitSet.size(); i++) {
            int index = i / 8;
            int offset = 7 - i % 8;
            bytes[index] |= (bitSet.get(i) ? 1 : 0) << offset;
        }
        return bytes;
    }

    public static BitSet bytes2BitSet(byte[] bytes) {
        BitSet bitSet = new BitSet(bytes.length * 8);
        int index = 0;
        for (int i = 0; i < bytes.length; i++) {
            for (int j = 7; j >= 0; j--) {
                bitSet.set(index++, (bytes[i] & (1 << j)) >> j == 1 ? true : false);
            }
        }
        return bitSet;
    }

    public static void main(String[] args) {
        BitSet bitSet = new BitSet();
        bitSet.set(0, true);
        bitSet.set(10, true);
        bitSet.set(11, false);
        bitSet.set(12, true);
        bitSet.set(13, true);
        bitSet.set(14, true);
        bitSet.set(15, false);
        bitSet.set(16, true);
//        //将BitSet对象转成byte数组
//        byte[] bytes = bitSet2Bytes(bitSet);
//        System.out.println(Arrays.toString(bytes));
//
//        //在将byte数组转回来
//        bitSet = bytes2BitSet(bytes);
        byte[] bytes  = bitSet.toByteArray();
        System.out.println(Arrays.toString(bytes));
        bitSet = BitSet.valueOf(bytes);
        System.out.println(bitSet.get(0));
        System.out.println(bitSet.get(10));
        System.out.println(bitSet.get(11));
        System.out.println(bitSet.get(12));
        System.out.println(bitSet.get(13));
        System.out.println(bitSet.get(14));
        System.out.println(bitSet.get(15));
    }
}
