//package event;
//
//import org.ricks.utils.ReflectUtil;
//
//import java.lang.reflect.Method;
//
///**
// * @author ricks
// * @Description: 动态代理被EventReceiver注解标注的方法，为了避免反射最终会用javassist字节码增强的方法去代理EventReceiverDefinition
// * @date 2022/9/2618:48
// */
//public class EventReceiverDefinition implements IEventReceiver {
//
//
//    private Object bean;
//
//    // 被EventReceiver注解标注的方法
//    private Method method;
//
//    // 接收的参数Class
//    private Class<? extends Event> eventClazz;
//
//    public EventReceiverDefinition(Object bean, Method method, Class<? extends Event> eventClazz) {
//        this.bean = bean;
//        this.method = method;
//        this.eventClazz = eventClazz;
//        ReflectUtil.makeAccessible(this.method);
//    }
//
//    @Override
//    public void invoke(Event event) {
//        ReflectUtil.invokeMethod(bean, method, event);
//    }
//
//    public Object getBean() {
//        return bean;
//    }
//
//    public void setBean(Object bean) {
//        this.bean = bean;
//    }
//
//    public Method getMethod() {
//        return method;
//    }
//
//    public void setMethod(Method method) {
//        this.method = method;
//    }
//
//    public Class<? extends Event> getEventClazz() {
//        return eventClazz;
//    }
//
//    public void setEventClazz(Class<? extends Event> eventClazz) {
//        this.eventClazz = eventClazz;
//    }
//}
