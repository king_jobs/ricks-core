package event;

import org.ricks.common.event.Event;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/12/216:28
 */
public class EventTask implements Event {

    private int age;
    private String name;

    public EventTask(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
