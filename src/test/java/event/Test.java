package event;

import app.AppContext;
import app.vo.User;
import org.ricks.common.event.EventBus;

import java.util.Set;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/3/314:48
 */
public class Test {

    public static void main(String[] args) {
        AppContext.me().run(Set.of("app","event"));
//        EventBus.publish(new EventTask(1,1));
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//
//
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//
//
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//        EventBus.me().publish(new EventTask(1,"demon"));
//
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
//        EventBus.me().publish(new EventTask(1,"demon"));
//
        long s = System.nanoTime();
        EventBus.me().publish(new MyApplicationEvent(new User()));
//        EventBus.me().publish(new LoginTask(10000,"demon1111111111111"));
        long e = System.nanoTime();
        System.err.println("事件发布耗时:" + (e -s));
    }
}
