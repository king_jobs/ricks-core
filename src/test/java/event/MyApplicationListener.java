package event;

import org.ricks.common.event.ApplicationEvent;
import org.ricks.common.event.ApplicationListener;

public class MyApplicationListener implements ApplicationListener {

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.err.println("触发容器事件");
    }
}
