package event;

import org.ricks.common.event.ApplicationEvent;

public class MyApplicationEvent extends ApplicationEvent {
    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public MyApplicationEvent(Object source) {
        super(source);
    }
}
