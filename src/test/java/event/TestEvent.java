package event;

import org.ricks.common.event.EventHandler;
import org.ricks.ioc.Bean;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/3/314:13
 */
@Bean
public class TestEvent {

    static AtomicLong l = new AtomicLong(0);

    @EventHandler
    public void test(EventTask task) {
        int age = task.getAge();
        String name = task.getName();
        long c = l.getAndIncrement();
        if(c == 1) System.err.println(System.currentTimeMillis());
        if(c == 1000000) {
            System.err.println(Thread.currentThread().getName() + "  .....test event  to do event.........." + age + "and name");
            System.err.println(System.currentTimeMillis());
        }
    }

    @EventHandler
    public void test(LoginTask task) {
        int age = task.getAge();
        String name = task.getName();
        System.err.println(Thread.currentThread().getName() + "  .....test event  to do event.........." + age + "and name" + name);
    }

}
