package event.delay;

import org.ricks.common.event.EventBus;
import org.ricks.ioc.Bean;
import org.ricks.ioc.RicksIoc;
import org.ricks.schedule.Scheduled;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/1/1618:31
 */
@Bean
public class Test {

    public static void main(String[] args) {
        RicksIoc ioc = new RicksIoc();
        EventBus.me();
    }

    @Scheduled(cron = "*/5 * * * * ?")
    public void test() {
        System.err.println(Thread.currentThread().getName() +":开始执行9i那个了" + System.currentTimeMillis());
    }
}
