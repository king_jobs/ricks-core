package expiringmap;

import app.utils.CacheExpirationListener;
import org.ricks.common.lang.ExpiringMap;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/2/2813:30
 */
public class Test {

    public static void main(String[] args) throws InterruptedException {
        // Given
        ExpiringMap<String, Integer> map = ExpiringMap.builder().build();
        map.addExpirationListener(new CacheExpirationListener());
        // When / Then
        map.put("foo", 1);
        Thread.sleep(55);
        map.forEach((k,v)-> System.err.println("test1 = "+k + " & " + v));
        Thread.sleep(55);
        map.put("foo", 2);
        map.forEach((k,v)-> System.err.println("test2 = "+k + " & " + v));
        Thread.sleep(110);
        map.forEach((k,v)-> System.err.println("test3 = "+k + " & " + v));

    }
}
