package json;

import app.data.CardVo;
import org.ricks.json.JSON;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/2/1514:34
 */
public class Test {

    public static void main(String[] args) {
        CardVo cardVo = new CardVo(1,"ricks",false);
        long s = System.currentTimeMillis();
        String jsonString = JSON.toJsonStr(cardVo);

        for (int i = 0; i < 1000000; i++) {
            CardVo cardVo1 = JSON.toBean(jsonString,CardVo.class);
//            String jsonStr = JSON.toJsonStr(cardVo1);
//            System.err.println(jsonStr);
        }
        long e = System.currentTimeMillis();
        System.err.println("序列化1000000 耗时 time:" + (e- s));
//        System.err.println(" cardvo 序列化 字节流， byte size :" + bytes.length);
//        CardVo cardVo1 = JSONUtil.toBean(new String(bytes),CardVo.class);
//        System.err.println("反序列化  cardId:" + cardVo1.getCardId() + "  , cardName:" + cardVo1.getCardName());
    }
}
