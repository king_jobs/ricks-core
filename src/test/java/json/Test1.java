package json;

import app.data.CardVo;
import org.ricks.json.JSON;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/2/1514:43
 */
public class Test1 {

    public static void main(String[] args) {
        CardVo cardVo = new CardVo(1,"ricks",false);
        long s = System.currentTimeMillis();
        byte[] bytes = JSON.toJsonStr(cardVo).getBytes();

        for (int i = 0; i < 1000000; i++) {
            bytes = JSON.toJsonStr(cardVo).getBytes();
        }
        long e = System.currentTimeMillis();
        System.err.println("序列化1000000 耗时 time:" + (e- s));
        System.err.println(" cardvo 序列化 字节流， byte size :" + bytes.length);
        CardVo cardVo1 = JSON.toBean(new String(bytes),CardVo.class);
        System.err.println("反序列化  cardId:" + cardVo1.getCardId() + "  , cardName:" + cardVo1.getCardName());
    }
}
