/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/3/1114:30
 */
public class B {
    public void executeMessage(CallBack callBack,String question){
        System.out.println(Thread.currentThread().getName() + "   and    " + callBack.getClass()+"问的问题--》"+question);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String result="答案是2";
        callBack.solve(result);
    }
}
