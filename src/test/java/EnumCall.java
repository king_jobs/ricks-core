/**
 * @author ricks
 * @date 2022/9/1416:48
 */
public enum EnumCall {

    APP_CONTROLLER_TESTCONTROLLER_TESTUSERDB(2);

    private int cmd;

    EnumCall(int cmd) {
        this.cmd = cmd;
    }

}
