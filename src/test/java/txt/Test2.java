package txt;

public class Test2 {

    public static void main(String[] args) {
        int a = -790798795;
        System.err.println("原始二进制：" + Integer.toBinaryString(a));
        a = a >> 31;  //取位移高一位，bit 0=正 1=负
        System.err.println( a);

        System.err.println((999 << 31) );

        int value = 0;
        byte b = 1;
        System.err.println(((b >>> 1) ^ -(b & 1)));

        for (int i = 0; i < 20; i++) {
            System.err.println("b = " + b + " & " +(b & 0x80));

        }
    }
}
