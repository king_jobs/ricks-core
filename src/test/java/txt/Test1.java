package txt;

import org.ricks.common.TimeUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/1/510:54
 */
public class Test1 {

    public static void main(String[] args) {
        System.err.println(between());
    }

    public static boolean between() {
        long curTime = TimeUtils.currentTimeMillis();
        Calendar cd = Calendar.getInstance();
        cd.setTimeInMillis(curTime);
        cd.set(Calendar.HOUR_OF_DAY,4);
        cd.set(Calendar.MINUTE,0);
        cd.set(Calendar.SECOND,0);
        cd.set(Calendar.MILLISECOND,0);
        System.err.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cd.getTime()));

        return curTime < cd.getTimeInMillis();

    }
}
