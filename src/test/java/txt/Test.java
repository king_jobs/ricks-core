package txt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/9/1615:56
 */
public class Test {


    public static List<String> loadTxt() throws Exception {
        List<String> ips = new ArrayList<>();
        File file = new File("D:\\数据统计\\18号注册IP.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String temp;
        while ((temp = reader.readLine()) != null) {
            temp = temp.replace("/","");
            ips.add(temp);
        }
        reader.close();
        return ips;
    }

    public static int loadLog(List<String> ips) throws Exception {
        AtomicInteger count = new AtomicInteger();
        Map<String,Boolean> login = new HashMap<>();
        File file = new File("D:\\项目文档\\08.file\\10_0819.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String temp;
        while ((temp = reader.readLine()) != null) {
            for (String ip: ips) {
                if(temp.contains("688_"+ip+"_")) {
                    login.put(ip,true);
                }
            }
        }
        reader.close();
        login.forEach((k,v) -> {
            if(v) {
                System.err.println(k);
                count.addAndGet(1);
            }
        });
        System.err.println("19号留存  " + count.get());
        return count.get();
    }
}
