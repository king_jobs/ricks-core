/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/9/1415:17
 */
public class EmployeeClassLoader  extends ClassLoader{

    public Class defineClassFromFile(String className, byte[] classFile) throws ClassFormatError {
        return defineClass(className,classFile,0,classFile.length);
    }
}
