import org.ricks.net.handler.Protocol;
import org.ricks.net.AioSession;

import java.nio.ByteBuffer;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/8/2414:19
 */
public class StringProtocolTest implements Protocol<String> {

    @Override
    public String decode(ByteBuffer readBuffer, AioSession session) {
        int remaining = readBuffer.remaining();
        if (remaining < Integer.BYTES) {
            return null;
        }
        readBuffer.mark();
        int length = readBuffer.getInt();
        if (length > readBuffer.remaining()) {
            readBuffer.reset();
            return null;
        }
        byte[] b = new byte[length];
        readBuffer.get(b);
        readBuffer.mark();
        return new String(b);
    }
}
