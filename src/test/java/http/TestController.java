package http;

import org.ricks.ioc.Autowired;
import org.ricks.net.http.Controller;
import org.ricks.net.http.RequestMapping;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/1/1216:39
 */
@Controller("ricks")
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("test")
    public String test() {
        testService.testService();
        return "ricks";
    }

    @RequestMapping("test1")
    public String test1() {
        return "ricks11111111111";
    }
}
