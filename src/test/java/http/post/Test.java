package http.post;

import app.utils.HttpUtils;
import org.ricks.json.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/3/913:16
 */
public class Test {

    public static void main(String[] args) {
        Map<String,Object> map = new HashMap<>();
        List<Long> userIds = new ArrayList<>();
        userIds.add(111L);
        userIds.add(222L);
        map.put("userIds",userIds);
        map.put("businessId",1);
//        HttpUtils.doAsyncPost("http://localhost:8080/smart/test", JSONUtil.toJsonStr(map));

        HttpUtils.doAsyncPost("http://localhost:9898/ricks/test12", JSON.toJsonStr(map));
    }
}
