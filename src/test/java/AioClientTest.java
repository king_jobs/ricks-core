import org.ricks.net.AioClient;
import org.ricks.net.buffer.WriteBuffer;
import org.ricks.net.handler.MessageProcessor;
import org.ricks.net.AioSession;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/8/2414:17
 */
public class AioClientTest {

    public static void main(String[] args) throws Exception {
        MessageProcessor<String> processor = new MessageProcessor<String>() {
            @Override
            public void process(AioSession session, String msg) {
                System.out.println("receive from server: " + msg);
            }
        };
        AioClient client = new AioClient("localhost", 8888, new StringProtocolTest(), processor);
        AioSession session = client.start();
        for (int i = 0; i < 1000; i++) {
            WriteBuffer writeBuffer = session.writeBuffer();
            byte[] data = "hello smart-socket".getBytes();
            writeBuffer.writeInt(data.length);
            writeBuffer.write(data);
            writeBuffer.flush();
            Thread.sleep(2000L);
        }
    }
}
