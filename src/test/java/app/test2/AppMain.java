package app.test2;

import app.prop.Resource;
import org.ricks.ModularContext;
import org.ricks.common.asm.ClassReader;
import org.ricks.common.asm.ClassVisitor;
import org.ricks.common.asm.ClassWriter;
import org.ricks.common.asm.Opcodes;
import org.ricks.dispatch.Dispatcher;
import org.ricks.ioc.IocHolder;
import org.ricks.ioc.Message;
import org.ricks.net.Context;
import org.ricks.common.ResourceUtil;

import java.io.FileInputStream;
import java.lang.invoke.MethodHandles;
import java.util.Set;

public class AppMain {

	private static final MethodHandles.Lookup lookup = MethodHandles.lookup();

	public static void main(String[] args) throws Exception {
		Context c = new Context(null,new Message((short)4000,new byte[0]));
//		Dispatcher.dispatcher(c);
//		Thread.currentThread().setContextClassLoader(new DispatchManager.ClassLoad());
//		FileWatcher watcher = new FileWatcher("",new DataListener()); //初始化data文件变化监听
//		LogConfig.setLevel("debug"); //设置日志等级
//		AppContext.setBannerMode(Banner.Mode.OFF); //设置banner 是否开启，默认开启
//		AppContext.run(AppMain.class);//启动容器 & 启动server
//		DispatchManager.init(ApplicationContext.getApplication());
//		byte[] data = DispatchManager.getData();
//		ClassLoad classLoad = new ClassLoad();
//		classLoad.defineClass1("Dispatcher",data,0,data.length);
//		watcher.watch();//开始监听


//		Thread.sleep(5000L);
//		Dispatcher.dispatcher(new Context<Short,Byte[]>(null,new Message((short)1,new byte[0])));

//		Dispatcher dispatcher = new Dispatcher();
//
//		Context context = new Context(null,new Message((short)4000,new byte[0]));
//		DispatchManager.init(ApplicationContext.getApplication());
//		Dispatcher.dispatcher(context);
//		Dispatcher.dispatcher(context);

//		Context context1 = new Context(null,new Message((short)4001,new byte[0]));
//		Dispatcher.dispatcher(context1);
//
//		Context context1 = new Context<Short,Byte[]>(null,new Message((short)1000,new byte[0]));
//		Dispatcher.dispatcher(context1);
//
//		Context context2 = new Context<Short,Byte[]>(null,new Message((short)101,new byte[0]));
//		Dispatcher.dispatcher(context2);
//		Class clazz = Class.forName("org.demon.dispatch.Dispatcher");
//		Object o = clazz.getConstructor().newInstance();
//		Method mm = clazz.getMethod("dispatcher",Context.class);
//
//		try {
//			mm.invoke(o,context);
//		} catch (Exception e) {
//			System.err.println("+++++++++" + Thread.currentThread().getStackTrace().length);
//			throw e;
//		}
//		System.err.println("...............");




//		ClassReader cr = new ClassReader("org.demon.dispatch.Dispatcher");
//		ClassWriter cw = new ClassWriter(cr, ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
//		DispatchClassVisitor dispatchClassVisitor = new DispatchClassVisitor(cw);
//		cr.accept(dispatchClassVisitor,0);
//		String dispatch_path = FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "org/demon/dispatch/Dispatcher.class";
////		FileOutputStream fos = new FileOutputStream(dispatch_path);
////		fos.write(cw.toByteArray());
////		fos.close();
//		DispatchManager.ClassLoad classLoad = new DispatchManager.ClassLoad();
//		classLoad.defineClassToJvm("org.demon.dispatch.Dispatcher",cw.toByteArray());
////		FileUtil.del(new File(dispatch_path));
//
//		Class clazz = Class.forName("org.demon.dispatch.Dispatcher");
//		Object o = clazz.getConstructor().newInstance();
//		Method mm = clazz.getMethod("dispatcher",Context.class);
//
//		try {
//			mm.invoke(o,context);
//		} catch (Exception e) {
//			System.err.println("+++++++++" + Thread.currentThread().getStackTrace().length);
//			throw e;
//		}



		String relative_path = "org/ricks/dispatch/Dispatcher.class";
		String path2 = ResourceUtil.getResource("").getPath() + relative_path;

		System.out.println(Resource.class.getResource("/"));

		//（1）构建Cla***eader
		ClassReader cr = new ClassReader(new FileInputStream(path2));

		//（2）构建ClassWriter
		ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

		//（3）串连ClassVisitor
		int api = Opcodes.ASM9;
		ClassVisitor cv = new Dispatcher(api, cw);

		//（4）结合Cla***eader和ClassVisitor
		int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
		cr.accept(cv, parsingOptions);

		//（5）生成byte[]
		byte[] data = cw.toByteArray();

		lookup.defineClass(data);

		ModularContext.me().scan(Set.of("app"));
		IocHolder.initIoc();
		Dispatcher dispatcher = new Dispatcher(0,null);
		dispatcher.dispatcher(c);
//		Dispatcher.dispatcher(context1);

	}

}
