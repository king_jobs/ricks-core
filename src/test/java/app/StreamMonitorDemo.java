///*******************************************************************************
// * Copyright (c) 2017-2021, org.smartboot. All rights reserved.
// * project name: smart-socket
// * file name: StreamMonitorDemo.java
// * Date: 2021-08-21
// * Author: sandao (zhengjunweimail@163.com)
// *
// ******************************************************************************/
//
//package app;
//
//
//import app.processor.ChannelListener;
//import org.ricks.ioc.Message;
//import org.ricks.net.KcpServer;
//import org.ricks.net.handler.AbstractMessageProcessor;
//import org.ricks.net.handler.DefaultHandler;
//import org.ricks.net.handler.StreamMonitorPlugin;
//import org.ricks.net.protocol.ShortCmdProtocol;
//import org.ricks.net.AioSession;
//import org.ricks.net.StateMachineEnum;
//
//import java.io.IOException;
//import java.nio.channels.AsynchronousSocketChannel;
//import java.util.function.BiConsumer;
//
///**
// * @author 三刀（zhengjunweimail@163.com）
// * @version V1.0 , 2021/8/21
// */
//public class StreamMonitorDemo {
//    public static void main(String[] args) throws IOException {
//        //服务端
//        AbstractMessageProcessor<Message> processor = new DefaultHandler() {
//            @Override
//            public void process0(AioSession session, Message msg) {
////                System.out.println("收到客户端请求消息: " + msg);
//                byte[] content = "Hi Client".getBytes();
//                try {
//                    session.writeBuffer().writeInt(content.length);
//                    session.writeBuffer().write(content);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {
//            }
//        };
//        //注册插件
////        processor.addPlugin(new StreamMonitorPlugin<>());
//        processor.addPlugin(new StreamMonitorPlugin<>(new BiConsumer<AsynchronousSocketChannel, byte[]>() {
//            @Override
//            public void accept(AsynchronousSocketChannel asynchronousSocketChannel, byte[] bytes) {
//                System.err.println("输入内容: " + new String(bytes) + " END");
//            }
//        }, new BiConsumer<AsynchronousSocketChannel, byte[]>() {
//            @Override
//            public void accept(AsynchronousSocketChannel asynchronousSocketChannel, byte[] bytes) {
//                System.err.println("输出内容: " + new String(bytes) + " END");
//            }
//        }));
//        KcpServer server =  new KcpServer(25025,new ShortCmdProtocol(), processor);
//        server.setListener(new ChannelListener());
//        server.start(25025);
//
////        //客户端
////        AioQuickClient client = new AioQuickClient("localhost", 8080, new StringProtocol(), new AbstractMessageProcessor<String>() {
////            @Override
////            public void process0(AioSession session, String msg) {
////                System.out.println("收到服务端响应消息: " + msg);
////            }
////
////            @Override
////            public void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {
////
////            }
////        });
////        AioSession session = client.start();
////        byte[] content = "HelloWorld".getBytes();
////        session.writeBuffer().writeInt(content.length);
////        session.writeBuffer().write(content);
////        session.writeBuffer().flush();
//    }
//}
