package okjson;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class DemoUserClass {
    public String				userName ;
    public String				email ;
    public UserExtInfo			userExtInfo ;
    public LinkedList<String>			interestGroupList ;
    public LinkedList<BorrowDetail>	borrowDetailList ;

    public HashMap<Integer,String> map1;

    public HashMap<String,UserExtInfo> map2;
}

