package kcp;

import org.ricks.protocol.IPacket;

public class InnerPing implements IPacket {

    public final static short PROTOCOL_ID = 51;
    @Override
    public short protocolId() {
        return PROTOCOL_ID;
    }
}
