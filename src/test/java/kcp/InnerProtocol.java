package kcp;

import org.ricks.net.AioSession;
import org.ricks.net.handler.Protocol;

import java.io.IOException;
import java.nio.ByteBuffer;


/**
 * @author ricks
 * @Description: client <->  broker 字节流解析
 * @date 2023/2/1318:14
 */
public class InnerProtocol implements Protocol<Object> {

    @Override
    public Object decode(ByteBuffer readBuffer, AioSession session) throws IOException {
        short cmd = readBuffer.getShort();
        int packId = readBuffer.getInt();
        InnerMessage message = new InnerMessage();
        message.packageId = packId;
        message.cmd = cmd;
        return message;
    }
}
