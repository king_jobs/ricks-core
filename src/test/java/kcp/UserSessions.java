package kcp;

import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户 session 管理器
 * <pre>
 *     对所有用户UserSession的管理，统计在线用户等
 *
 *     关于用户管理 UserSessions 和 UserSession 可以参考这里：
 *     https://www.yuque.com/iohao/game/wg6lk7
 * </pre>
 */
public class UserSessions {

    private final static int MAX_USER_COUNT = 5000;

    /**
     * key : 玩家 id
     * value : UserSession
     */
    private final Map<Long, UserSession> userIdMap = new ConcurrentHashMap<>();

    /**
     * key : sessionId
     * value : UserSession
     */
    private final Map<Long, UserSession> userChannelIdMap = new ConcurrentHashMap<>();

    /**
     * key: roomId
     * value: UserSession
     * 进入房间维护，退出房间解绑
     * UserSession close 销毁
     */
    private final Map<String,GroupUnit> userGroupIdMap = new ConcurrentHashMap<>();



    /**
     * 获取 UserSession
     *
     * @param sessionId
     * @return UserSession
     */
    public UserSession getUserSessionBySessionId(long sessionId) throws RuntimeException {
        UserSession userSession = userChannelIdMap.get(sessionId);

        if (Objects.isNull(userSession)) {
            throw new RuntimeException("userSession 不存在，请先加入 session 管理中，UserSessions.add(sessionId)  curr sessionId = " + sessionId);
        }

        return userSession;
    }

    /**
     * true 用户存在
     *
     * @param userId 用户id
     * @return true 用户存在
     */
    public boolean existUserSession(long userId) {
        return this.userIdMap.containsKey(userId);
    }

    /**
     * true 用户存在
     *
     * @param sessionId 用户id
     * @return true 用户存在
     */
    public boolean existUserSessionBySessionId(long sessionId) {
        return this.userChannelIdMap.containsKey(sessionId);
    }

    /**
     * 获取 UserSession
     *
     * @param userId userId
     * @return UserSession
     */
    public UserSession getUserSessionByUserId(long userId) throws RuntimeException {
        UserSession userSession = this.userIdMap.get(userId);

        if (Objects.isNull(userSession)) {
            throw new RuntimeException("userSession 不存在，请先登录在使用此方法. userId:" + userId);
        }

        return userSession;
    }

    public UserSession getUserSession(long sessionId) throws RuntimeException {
        UserSession userSession = this.userChannelIdMap.get(sessionId);

        if (Objects.isNull(userSession)) {
            // 如果你登录了，但是又报这个异常，一般是将 ExternalGlobalConfig.verifyIdentity = false 了。
            throw new RuntimeException("userSession 不存在");
        }

        return userSession;
    }

    /**
     * 加入到 session 管理
     *
     * @param session
     */
    public void add(AioSession session) {
        try {
            logIp(session);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        UserSession userSession = new UserSession(session);

        Logger.info(" sessionId:"+session.sid()+" 正在加入session 管理容器");
        this.userChannelIdMap.putIfAbsent(session.sid(), userSession);
    }


    /**
     * 当前在线人数
     *
     * @return 当前在线人数
     */
    public long countOnline() {
        return this.userIdMap.size();
    }

    /**
     * 全员消息广播
     * 消息类型 ExternalMessage
     *
     * @param msg 消息
     */
    public void broadcast(GamePack msg) {
        userIdMap.values().forEach(userSession -> userSession.writeAndFlush(msg));
    }


    private void logIp(AioSession session) throws IOException {
            InetSocketAddress socketAddress = session.getRemoteAddress();
            String remoteAddress = socketAddress.getAddress().getHostAddress();
            int remotePort = socketAddress.getPort();

            socketAddress = session.getLocalAddress();
            String localAddress = socketAddress != null ? socketAddress.getAddress().getHostAddress() : "null";
            int localPort = socketAddress.getPort();

            Logger.debug("localAddress::"+localAddress+":"+localPort+", remoteAddress::"+remoteAddress+":" + remotePort);
    }


    private UserSessions() {

    }

    public static UserSessions me() {
        return Holder.ME;
    }

    /** 通过 JVM 的类加载机制, 保证只加载一次 (singleton) */
    private static class Holder {
        static final UserSessions ME = new UserSessions();
    }



    /**
     * 将AioSession加入群组group
     *
     * @param group
     * @param session
     */
    public final synchronized void join(String group, UserSession session) {
        GroupUnit groupUnit = userGroupIdMap.computeIfAbsent(group,key -> new GroupUnit());
        groupUnit.groupList.add(session);
    }

    public final synchronized void remove(String group, UserSession session) {
        GroupUnit groupUnit = userGroupIdMap.get(group);
        if (groupUnit == null) {
            return;
        }
        groupUnit.groupList.remove(session);
        if (groupUnit.groupList.isEmpty()) {
            userGroupIdMap.remove(group);
        }
    }

    public final void remove(UserSession session) {
        for (String group : userGroupIdMap.keySet()) {
            remove(group, session);
        }
    }

    public void writeToGroup(String group, GamePack gamePack) {
        GroupUnit groupUnit = userGroupIdMap.get(group);
        for (UserSession session : groupUnit.groupList) {
            session.writeAndFlush(gamePack);
        }
    }

    private class GroupUnit {
        Set<UserSession> groupList = new HashSet<>();
    }
}
