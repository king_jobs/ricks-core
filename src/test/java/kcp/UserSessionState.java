package kcp;

public enum UserSessionState {
    /** 活跃的 */
    ACTIVE,
    /** 死的 */
    DEAD;
}
