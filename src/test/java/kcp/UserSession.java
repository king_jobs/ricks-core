package kcp;

import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
//import org.ricks.netlizh.Kcp;
//import org.ricks.net.transport.kcp.KcpSession;
import org.ricks.net.buffer.WriteBuffer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户的唯一 session 信息
 * <pre>
 *     与 channel 是 1:1 的关系，可取到对应的 userId、channel 等信息
 *
 */
public class UserSession {

    /** user channel  要么连上kcp 要么连上tcp       */
    private final AioSession aioSession;
    /** userId */
    private long userId;
    /** 用户状态 */
    private UserSessionState state;
    /**
     * 业务节点ID ,如果分配好了 直至user session 销毁,才会解绑 避免动态增删节点 路由问题
     * 如果已经绑定的节点 session 管理中心未拿到节点，说明节点被删除，重新分配节点
     *  模块绑定节点
     */
    private Map<Byte,Short> moduleNode = new ConcurrentHashMap<>();

    private boolean loginSuccess;

    public UserSession(AioSession session) {
        this.aioSession = session;
        this.state = UserSessionState.ACTIVE;
    }

    /**
     * 设置当前用户（玩家）的 id
     * <pre>
     *     当设置好玩家 id ，也表示着已经身份验证了（表示登录过了）。
     * </pre>
     *
     * @param userId userId
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserChannelId() {
        return aioSession.sid();
    }

    public short getServerId(byte moduleId) {
        return moduleNode.getOrDefault(moduleId,(short)0);
    }

    public void bindNodeId(byte moduleId,short nodeId) {
        moduleNode.put(moduleId,nodeId);
    }

    /**
     * 获取玩家ip
     *
     * @return 获取玩家ip
     */
    public String getIp() {
        if (Boolean.FALSE.equals(isActiveChannel())) {
            return "";
        }

        InetSocketAddress inetSocketAddress = null;
        try {
            inetSocketAddress = aioSession.getRemoteAddress();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return inetSocketAddress.getHostString();
    }

    public void unBind(int moduleId) {
        moduleNode.remove(moduleId);
    }

    public void clearBind() {
        moduleNode.clear();
    }

    /**
     * 是否进行身份验证
     *
     * @return true 已经身份验证了，表示登录过了。
     */
    public boolean isVerifyIdentity() {
        return this.userId > 0 && loginSuccess;
    }

    public boolean isActiveChannel() {
        return Objects.nonNull(aioSession) && !aioSession.isInvalid();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof UserSession that)) {
            return false;
        }

        return userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }

    private String getChannelId() {
        return String.valueOf(this.aioSession.sid());
    }

    public void setState(UserSessionState state) {
        this.state = state;
    }

    public void close() {
        if(aioSession != null) aioSession.close();
    }

    public void writeAndFlush(GamePack msg) {
        if(aioSession == null || aioSession.isInvalid()) return;
//        if(aioSession instanceof KcpSession kcpSession){
//            int headLength = 2 + 4 + 2;
//            //游戏默认是小端
//            ByteBuffer byteBuffer = ByteBuffer.allocate(headLength+msg.getData().length);
//            byteBuffer.putShort(msg.getCmd());
//            byteBuffer.putInt(msg.getPackageId());
//            byteBuffer.putShort(msg.responseStatus);
//            byteBuffer.put(msg.getData());
//            byteBuffer.flip();//重置 limit 和postion 值 否则 buffer 读取数据不对
//            byte[] bytes = new byte[headLength+msg.getData().length];
//            byteBuffer.get(bytes);
////            System.err.println("发送 字节长度：" + bytes.length + " & " + bytes[0] + "," + bytes[1]);
//            Kcp kcp = kcpSession.getUkcp();
//            int code;
//            try {
//                code = kcp.send(bytes);
//            } catch (Exception e) {
//                code = -9;
//            }
//            if(code == 0) kcp.update(TimeUtils.currentTimeMillis()); else close();
//        } else
            writeAndFlushTcp(msg);
    }

    private void writeAndFlushTcp(GamePack msg) {
        WriteBuffer buffer = aioSession.writeBuffer();
        try {
            buffer.writeInt(msg.getData().length);
            buffer.writeShort(msg.getCmd());
            buffer.writeInt(msg.getPackageId());
            buffer.writeShort(msg.responseStatus);
            buffer.write(msg.getData());
        } catch (Exception e) {
            try {
                Logger.error("cmd:"+msg.getCmd()+" 写回客户端:"+aioSession.getRemoteAddress()+" 失败.");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        } finally {
            buffer.flush();
        }
    }

    public long getUserId() {
        return userId;
    }

    public UserSessionState getState() {
        return state;
    }

    public AioSession getAioSession() {
        return aioSession;
    }

    public boolean isLoginSuccess() {
        return loginSuccess;
    }

    public void setLoginSuccess(boolean loginSuccess) {
        this.loginSuccess = loginSuccess;
    }
}
