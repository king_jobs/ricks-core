//package kcp;
//
//
//
//import app.AppMain;
//import org.ricks.net.transport.kcp.Kcp;
//import org.ricks.net.transport.kcp.KcpBootstrap;
//import org.ricks.net.transport.kcp.KcpSession;
//import org.ricks.net.transport.kcp.KcpChannel;
//
//import java.net.InetSocketAddress;
//import java.nio.ByteBuffer;
//import java.nio.ByteOrder;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/10/1411:43
// */
//public class Test {
//
//    private static KcpChannel channel;
//
//    public static void main(String[] args) throws InterruptedException {
////        AppContext.run(AppMain.class);
//        KcpBootstrap bootstrap = new KcpBootstrap(new InnerProtocol(),new InnerHandler());
//        channel = (KcpChannel) bootstrap.open(55556);
////        KcpSession kcpSession = createKcpSession(0);
////        writeAndFlush(kcpSession);
////        writeAndFlush(kcpSession);
////        writeAndFlush(kcpSession);
//    }
//
//    public static KcpSession createKcpSession(int conv) {
//        ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(conv);
//        InetSocketAddress socketAddress = new InetSocketAddress("192.168.18.182",55555);
//        return (KcpSession) channel.createAndCacheSession(socketAddress);
//    }
//
//
//
//    public static void writeAndFlush(KcpSession kcpSession) {
//
//            int headLength = 2 + 4 + 2;
//            //游戏默认是小端
//            ByteBuffer byteBuffer = ByteBuffer.allocate(headLength);
//            byteBuffer.putShort((short) 10001);
//            byteBuffer.putInt(0);
//            byteBuffer.putShort((short) 0);
//            byteBuffer.put(new byte[0]);
//            byteBuffer.flip();//重置 limit 和postion 值 否则 buffer 读取数据不对
//            byte[] bytes = new byte[headLength];
//            byteBuffer.get(bytes);
////            System.err.println("发送 字节长度：" + bytes.length + " & " + bytes[0] + "," + bytes[1]);
//            Kcp kcp = kcpSession.getUkcp();
//            int code;
//            try {
//                code = kcp.send(bytes);
//            } catch (Exception e) {
//                code = -9;
//            }
////            if(code == 0) kcp.update(TimeUtils.currentTimeMillis()); else kcpSession.close(true);
////        });
//    }
//}
