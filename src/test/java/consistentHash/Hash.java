package consistentHash;

/**
 * 普通hash
 *
 * @author luo
 */
public class Hash {

    public static void main(String[] args) {
        String[] clientArr = new String[]{"1.2.1.1", "2.3.3.2", "3.3.3.4", "4.3.5.4"};

        // 节点数
        int nodeNum = 5;

        for (String client : clientArr) {
            int hash = Math.abs(client.hashCode());
            int nodeIndex = hash % nodeNum;
            System.out.println("client: " + client + "\t" + "nodeIndex: " + nodeIndex);
        }
    }

}

