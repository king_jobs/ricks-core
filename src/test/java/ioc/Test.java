package ioc;

import com.sun.source.util.TaskEvent;
import event.EventTask;
import event.LoginTask;
import org.ricks.ioc.RicksIoc;
import org.ricks.ioc.manager.EventMethodManager;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/1/1214:15
 */
public class Test {

    public static void main(String[] args) {
        RicksIoc ioc = new RicksIoc();
        for (int i = 0; i < 2000000 ; i++) {
            EventMethodManager.me().asyncSubmit(new EventTask(1,"1"));
//            EventMethodManager.get().syncSubmit(new LoginTask(2,"2"));
        }
    }


}
