package rpc;

import org.ricks.net.NetContext;
import org.ricks.net.rpc.ConsumerConfig;
import org.ricks.net.rpc.ConsumerModule;
import org.ricks.net.rpc.ProtocolModule;

import java.util.List;

public class ConsumerTest {

    public static void main(String[] args) {
        // 定义2个模块：可以为服务提供者用，也可以为服务消费者用，这个仅仅是模块信息
        var protocolModule1 = new ProtocolModule((byte) 100, "aaa");
        var protocolModule2 = new ProtocolModule((byte) 120, "bbb");

        // 服务消费者模块和服务消费者配置(服务消费者模块多一个负载均衡属性)
        var consumerModules = List.of(new ConsumerModule(protocolModule1, "random", "a"), new ConsumerModule(protocolModule2, "random", "b"));
        // 服务消费者配置：这个是没Ip的
        var consumerConfig = ConsumerConfig.valueOf(consumerModules);

        NetContext.me().setConsumerConfig(consumerConfig);

    }
}
