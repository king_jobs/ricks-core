package rpc;

import org.ricks.common.lang.Assert;
import org.ricks.net.codec.DataCodec;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;

public class RpcCodec implements DataCodec {
    @Override
    public byte[] encode(Object data) {
        Assert.isTrue(data instanceof IPacket packet);
        ByteBuf buf = new ByteBuf(1024);
        ProtocolManager.write(buf,(IPacket) data);
        return buf.toArray();
    }

    @Override
    public <T> T decode(byte[] data, Class<?> dataClass) {
        ByteBuf buf = new ByteBuf(data);
        return (T) ProtocolManager.read(buf);
    }
}
