package rpc;

import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
import org.ricks.net.NetContext;
import org.ricks.net.StateMachineEnum;
import org.ricks.net.handler.AbstractMessageProcessor;
import org.ricks.protocol.IPacket;

public class RpcNetHandler extends AbstractMessageProcessor<IPacket> {

    @Override
    public void process0(AioSession session, IPacket msg) {
        NetContext.getRouter().receive(session,msg);
    }

    @Override
    public void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {
        switch (stateMachineEnum) {
            case NEW_SESSION: {
                Logger.debug(" create session.......");
                break;
            }
            case PROCESS_EXCEPTION:
                Logger.error("process exception", throwable);
//                session.close();
                break;
            case SESSION_CLOSED:
                Logger.info(" close session......." );
                break;
            case DECODE_EXCEPTION: {
                Logger.warn("aio decode exception,", throwable);
                break;
            }
        }
    }
}
