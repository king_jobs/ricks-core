package rpc;

import org.ricks.net.*;
import org.ricks.net.rpc.RpcPackage;
import org.ricks.protocol.ProtocolManager;

import java.io.IOException;
import java.util.Set;

public class RcpClient {

    public static void main(String[] args) throws IOException {
        ProtocolManager.initProtocol(Set.of(RpcPackage.class));
        RpcNetHandler handler = new RpcNetHandler();
        AioClient client = new AioClient("localhost",26891,(org.ricks.net.handler.Protocol)new RpcProtocol(),handler);
        DataCodecKit.setDataCodec(new RpcCodec());
//        client.connectTimeout(5000);
        AioSession session = client.start();
        //这种方式简单，提前知道session 所以还是要搞获取session策略 session还是得统一管理
        Router router = new Router();
        NetContext.me().setRouter(router);
        long s = System.currentTimeMillis();
        String str = "";
        for (int i = 0; i < 20000; i++) {
            byte[] data = router.syncAsk(session,(short) 5205,(" i love world ........." + i).getBytes());
            str = new String(data);
        }
        long e = System.currentTimeMillis();
        System.err.println("......................" + str + " & " + (e - s)+ "ms");
    }
}
