package rpc;

import org.ricks.protocol.IPacket;

public class RpcData implements IPacket {

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
