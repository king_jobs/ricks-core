package rpc;

import org.ricks.net.AioSession;
import org.ricks.net.protocol.FixedLengthBytesProtocol;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;

public class RpcProtocol extends FixedLengthBytesProtocol<Object> {

    @Override
    protected Object decode(byte[] bytes, AioSession session) {
        ByteBuf buf = new ByteBuf(bytes);
        var packet = ProtocolManager.read(buf);
        return packet;
    }
}
