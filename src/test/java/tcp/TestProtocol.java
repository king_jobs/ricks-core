package tcp;

import org.ricks.net.AioSession;
import org.ricks.net.handler.Protocol;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/2/816:41
 */
public class TestProtocol implements Protocol<Long> {
    @Override
    public Long decode(ByteBuffer readBuffer, AioSession session) throws IOException {
        long sessionId = readBuffer.getLong();
        System.err.println("sessionId: " + sessionId);
        return sessionId;
    }
}
