package tcp;

import app.data.codec.InnerCodec;
import app.data.codec.protocol.InnerProtocol;
import org.ricks.common.conf.RicksConf;
import org.ricks.ioc.Message;
import org.ricks.net.AioClient;
import org.ricks.net.AioSession;
import org.ricks.net.handler.DefaultHandler;
import org.ricks.net.handler.Protocol;
import org.ricks.net.DataCodecKit;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/12/1514:46
 */
public class Test2 {

    public static void main(String[] args) throws IOException {
        RicksConf.loadingProperties(); //初始化配置
        DataCodecKit.setDataCodec(new InnerCodec());
        DefaultHandler handler = new DefaultHandler();
        AioClient client = new AioClient("localhost",26891,(Protocol) new InnerProtocol(),handler);
        DataCodecKit.setDataCodec(new InnerCodec());
//        client.connectTimeout(5000);
        AioSession session = client.start();
        session.send(new Message((short)1,new byte[0]));
//        RequestMessage
//        session.writeBuffer().writeInt(2);
//        session.writeBuffer().writeShort((short) 3000);
//        session.writeBuffer().flush();

        ByteBuffer buffer = ByteBuffer.allocate(6);
        buffer.putInt(2);
        buffer.putShort((short) 1);
        buffer.put(new byte[0]);
        System.err.println(bytes2hexFirst( buffer.array()));
    }


    public static String bytes2hexFirst(byte[] bytes) {
        final String HEX = "0123456789abcdef";
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        for (byte b : bytes) {
            // 取出这个字节的高4位，然后与0x0f与运算，得到一个0-15之间的数据，通过HEX.charAt(0-15)即为16进制数
            sb.append(HEX.charAt((b >> 4) & 0x0f));
            // 取出这个字节的低位，与0x0f与运算，得到一个0-15之间的数据，通过HEX.charAt(0-15)即为16进制数
            sb.append(HEX.charAt(b & 0x0f));
        }

        return sb.toString();
    }

}
