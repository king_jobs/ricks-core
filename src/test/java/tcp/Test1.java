//package tcp;
//
//import org.ricks.common.conf.RicksConf;
//import org.ricks.ioc.Message;
//import org.ricks.log.LogModular;
//import org.ricks.net.AioClient;
//import org.ricks.net.AioSession;
//import org.ricks.net.buffer.WriteBuffer;
//import org.ricks.net.handler.DefaultHandler;
//import org.ricks.net.handler.HeartPlugin;
//import org.ricks.net.protocol.ShortCmdProtocol;
//
//import java.io.IOException;
//import java.nio.ByteBuffer;
//import java.util.concurrent.TimeUnit;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/12/1514:46
// */
//public class Test1 {
//
//    public static void main(String[] args) throws IOException {
//        LogModular.get().init();
//        RicksConf.loadingProperties(); //初始化配置
//        DefaultHandler handler = new DefaultHandler();
//        handler.addPlugin(new HeartPlugin<Message>(4000,5000, TimeUnit.MILLISECONDS) {
//            @Override
//            public void sendHeartRequest(AioSession session) throws IOException {
////                WriteBuffer writeBuffer = session.writeBuffer();
////                byte[] content = "heart message".getBytes();
////                writeBuffer.writeInt(content.length + 2);
////                writeBuffer.writeShort((short) 3000);
////                writeBuffer.write(content);
//            }
//
//            @Override
//            public boolean isHeartMessage(AioSession session, Message msg) {
//                return (int)msg.getCmd() == 0;
//            }
//        });
//        AioClient client = new AioClient("localhost",52052,new TestProtocol(), new TestHandler());
//        client.connectTimeout(5000);
//        AioSession session = client.start();
//        session.writeBuffer().writeInt(2);
//        session.writeBuffer().writeShort((short) 3000);
//        session.writeBuffer().flush();
//
//        ByteBuffer buffer = ByteBuffer.allocate(6);
//        buffer.putInt(2);
//        buffer.putShort((short) 1);
//        buffer.put(new byte[0]);
//        System.err.println(bytes2hexFirst( buffer.array()));
//    }
//
//
//    public static String bytes2hexFirst(byte[] bytes) {
//        final String HEX = "0123456789abcdef";
//        StringBuilder sb = new StringBuilder(bytes.length * 2);
//        for (byte b : bytes) {
//            // 取出这个字节的高4位，然后与0x0f与运算，得到一个0-15之间的数据，通过HEX.charAt(0-15)即为16进制数
//            sb.append(HEX.charAt((b >> 4) & 0x0f));
//            // 取出这个字节的低位，与0x0f与运算，得到一个0-15之间的数据，通过HEX.charAt(0-15)即为16进制数
//            sb.append(HEX.charAt(b & 0x0f));
//        }
//
//        return sb.toString();
//    }
//
//}
