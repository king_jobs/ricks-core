package tcp;

import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;
import org.ricks.net.handler.AbstractMessageProcessor;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2023/2/816:44
 */
public class TestHandler extends AbstractMessageProcessor {
    @Override
    public void process0(AioSession session, Object msg) {
        System.err.println("sessionId: " + msg);
    }

    @Override
    public void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {

    }
}
