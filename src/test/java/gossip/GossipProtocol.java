//package gossip;
//
//import org.ricks.gossip.GossipMessage;
//import org.ricks.net.AioSession;
//import org.ricks.net.handler.Protocol;
//import org.ricks.serializer.Fse;
//import java.io.IOException;
//import java.nio.ByteBuffer;
//
//public class GossipProtocol<T> implements Protocol<GossipMessage> {
//
//    @Override
//    public GossipMessage decode(ByteBuffer readBuffer, AioSession session) throws IOException {
//        byte[] data = new byte[readBuffer.remaining()];
//        readBuffer.get(data);
//        return (GossipMessage) Fse.deSerialize(data);
//    }
//}
