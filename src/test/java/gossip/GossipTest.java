//package gossip;
//
//import org.ricks.common.conf.RicksConf;
//import org.ricks.gossip.*;
//import org.ricks.lang.Logger;
//import org.ricks.net.protocol.StringProtocol;
//import org.ricks.serializer.Fse;
//
//import java.net.UnknownHostException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class GossipTest {
//
//    public static void main(String[] args) throws UnknownHostException, InterruptedException {
//
//        short port = Short.parseShort("10500");
//        Fse.register(GossipMessage.class);
//        Fse.register(GossipMember.class);
//
//        RicksConf.loadingProperties(); //初始化配置
//
//        GossipConf gossipSettings = new GossipConf();
//        List<StartupSettings.Member> startupMembers = new ArrayList<>();
//
//        startupMembers.add(new StartupSettings.Member((byte)1,"192.168.18.105",  50000, (short) 1));
//        startupMembers.add(new StartupSettings.Member((byte)2,"192.168.18.105", 50001, (short) 1));
//        GossipManager.me().start((byte)3,"192.168.18.105",  10200, (short) 1,startupMembers,(member, state) -> {
//            Logger.info( "Member:"+member.getModuleId()+" [address="+member.getAddress()+", id="+member.getId()+", heartbeat="+member.getHeartbeat()+"] " + state.name());
//        },new GossipProtocol(),(session, obj) -> {
//            if(obj instanceof GossipMessage gossipMessage) {
//                String host = session.getRemoteAddress().getHostString();
//                int port1 = session.getRemoteAddress().getPort();
//                GossipMember senderMember = new GossipMember(gossipMessage.getModuleId(), host, port1, gossipMessage.getId(), gossipMessage.getHeartbeat());
//                GossipManager.me().handler(senderMember, gossipMessage);
//            }
//        });
//    }
//}
