//package protocol;
//
//import java.lang.reflect.Constructor;
//import java.util.*;
//
//import org.ricks.protocol.ByteBuf;
//import org.ricks.protocol.ByteBufUtils;
//import org.ricks.protocol.IPacket;
//import org.ricks.protocol.ProtocolManager;
//import org.ricks.protocol.collection.ArrayUtils;
//import org.ricks.protocol.registration.IProtocolRegistration;
//import protocol.NormalObject;
//import protocol.ObjectA;
//
//public class ProtocolRegistration101 implements IProtocolRegistration {
//
//    private Constructor constructor;
//    private IProtocolRegistration protocolRegistration102;
//    private IProtocolRegistration protocolRegistration103;
//
//    public ProtocolRegistration101(Constructor var1) {
//        this.constructor = var1;
//    }
//
//    public final short protocolId() {
//        return 101;
//    }
//
//    public final Constructor protocolConstructor() {
//        return this.constructor;
//    }
//
//    public final byte module() {
//        return 0;
//    }
//
//    public final void write(ByteBuf var1, IPacket var2) {
//        NormalObject var3 = (NormalObject)var2;
//        if (!ByteBufUtils.writePacketFlag(var1, var3)) {
//            ByteBufUtils.writeByte(var1, var3.getA());
//            ByteBufUtils.writeByteArray(var1, var3.getAaa());
//            ByteBufUtils.writeShort(var1, var3.getB());
//            ByteBufUtils.writeInt(var1, var3.getC());
//            ByteBufUtils.writeLong(var1, var3.getD());
//            ByteBufUtils.writeFloat(var1, var3.getE());
//            ByteBufUtils.writeDouble(var1, var3.getF());
//            ByteBufUtils.writeBoolean(var1, var3.isG());
//            ByteBufUtils.writeString(var1, var3.getJj());
//            this.protocolRegistration102.write(var1, (IPacket)var3.getKk());
//            ByteBufUtils.writeIntList(var1, var3.getL());
//            ByteBufUtils.writeLongList(var1, var3.getLl());
////            ByteBufUtils.writePacketList(var1, var3.getLll(), this.protocolRegistration102);
//            ByteBufUtils.writeStringList(var1, var3.getLlll());
//            ByteBufUtils.writeIntStringMap(var1, var3.getM());
////            ByteBufUtils.writeIntPacketMap(var1, var3.getMm(), this.protocolRegistration102);
//            ByteBufUtils.writeIntSet(var1, var3.getS());
//            ByteBufUtils.writeStringSet(var1, var3.getSsss());
//        }
//    }
//
//    public final Object read(ByteBuf var1) {
//        if (!ByteBufUtils.readBoolean(var1)) {
//            return null;
//        } else {
//            NormalObject var2 = new NormalObject();
//            var2.setA(ByteBufUtils.readByte(var1));
//            var2.setAaa(ByteBufUtils.readByteArray(var1));
//            var2.setB(ByteBufUtils.readShort(var1));
//            var2.setC(ByteBufUtils.readInt(var1));
//            var2.setD(ByteBufUtils.readLong(var1));
//            var2.setE(ByteBufUtils.readFloat(var1));
//            var2.setF(ByteBufUtils.readDouble(var1));
//            var2.setG(ByteBufUtils.readBoolean(var1));
//            var2.setJj(ByteBufUtils.readString(var1));
//            var2.setKk((ObjectA)this.protocolRegistration102.read(var1));
//            var2.setL(ByteBufUtils.readIntList(var1));
//            var2.setLl(ByteBufUtils.readLongList(var1));
//            List list1 = ByteBufUtils.readPacketList(var1, this.protocolRegistration102);
//            var2.setLll(list1);
//            var2.setLlll(ByteBufUtils.readStringList(var1));
//            var2.setM(ByteBufUtils.readIntStringMap(var1));
//            Map m = ByteBufUtils.readIntPacketMap(var1, this.protocolRegistration102);
//            var2.setMm(m);
//            var2.setS(ByteBufUtils.readIntSet(var1));
//            var2.setSsss(ByteBufUtils.readStringSet(var1));
//            return var2;
//        }
//    }
//    public static void main(String[] args) throws NoSuchMethodException {
//
//        ByteBuf buffer = ByteBuf.allocate();
//        ProtocolRegistration100 registration100 = new ProtocolRegistration100(ComplexObject.class.getConstructor());
//
//        ProtocolRegistration101 registration101 = new ProtocolRegistration101(NormalObject.class.getConstructor());
//        ProtocolRegistration102 registration102 = new ProtocolRegistration102(ObjectA.class.getConstructor());
//        ProtocolRegistration103 registration103 = new ProtocolRegistration103(ObjectB.class.getConstructor());
//        registration100.protocolRegistration102 = registration102;
//        registration102.protocolRegistration103 = registration103;
//        registration101.protocolRegistration102 = registration102;
//        registration101.protocolRegistration103 = registration103;
//        registration100.write(buffer,complexObject);
//        var a = registration100.read(buffer);
//
//        System.err.println("");
//    }
//
//
//
//    // -------------------------------------------以下为测试用例---------------------------------------------------------------
//    // 简单类型
//    public static final byte byteValue = 99;
//    public static final short shortValue = 9999;
//    public static final int intValue = 99999999;
//    public static final long longValue = 9999999999999999L;
//    public static final float floatValue = 99999999.9F;
//    public static final double doubleValue = 99999999.9D;
//    public static final char charValue = 'c';
//    public static final String charValueString = "c";
//    public static final String stringValue = "hello";
//
//    // 数组类型
//    public static final boolean[] booleanArray = new boolean[]{true, false, true, false, true};
//    public static final byte[] byteArray = new byte[]{Byte.MIN_VALUE, -99, 0, 99, Byte.MAX_VALUE};
//    public static final short[] shortArray = new short[]{Short.MIN_VALUE, -99, 0, 99, Short.MAX_VALUE};
//    public static final int[] intArray = new int[]{Integer.MIN_VALUE, -99999999, -99, 0, 99, 99999999, Integer.MAX_VALUE};
//    public static final int[] intArray1 = new int[]{Integer.MIN_VALUE, -99999999, -99, 0, 99, 99999999, Integer.MAX_VALUE - 1};
//    public static final int[] intArray2 = new int[]{Integer.MIN_VALUE, -99999999, -99, 0, 99, 99999999, Integer.MAX_VALUE - 2};
//    public static final long[] longArray = new long[]{Long.MIN_VALUE, -9999999999999999L, -99999999L, -99L, 0L, 99L, 99999999L, 9999999999999999L, Long.MAX_VALUE};
//    public static final float[] floatArray = new float[]{Float.MIN_VALUE, -99999999.9F, -99.9F, 0F, 99.9F, 99999999.9F, Float.MAX_VALUE};
//    public static final double[] doubleArray = new double[]{Double.MIN_VALUE, -99999999.9F, -99.9D, 0D, 99.9D, 99999999.9F, Double.MAX_VALUE};
//    public static final char[] charArray = new char[]{'a', 'b', 'c', 'd', 'e'};
//    public static final String[] stringArray = new String[]{"a", "b", "c", "d", "e"};
//
//    public static final ObjectA objectA = new ObjectA();
//    public static final ObjectB objectB = new ObjectB();
//    public static final Map<Integer, String> mapWithInteger = new HashMap<>(Map.of(Integer.MIN_VALUE, "a", -99, "b", 0, "c", 99, "d", Integer.MAX_VALUE, "e"));
//    public static final List<Integer> listWithInteger = new ArrayList<>(ArrayUtils.toList(intArray));
//    public static final List<Integer> listWithInteger1 = new ArrayList<>(ArrayUtils.toList(intArray1));
//    public static final List<Integer> listWithInteger2 = new ArrayList<>(ArrayUtils.toList(intArray2));
//    public static final List<ObjectA> listWithObject = new ArrayList<>(List.of(objectA, objectA, objectA));
//    public static final List<List<ObjectA>> listListWithObject = new ArrayList<>(List.of(listWithObject, listWithObject, listWithObject));
//    public static final List<List<Integer>> listListWithInteger = new ArrayList<>(List.of(listWithInteger, listWithInteger, listWithInteger));
//    public static final List<List<List<Integer>>> listListListWithInteger = new ArrayList<>(List.of(listListWithInteger, listListWithInteger, listListWithInteger));
//    public static final List<String> listWithString = new ArrayList<>(ArrayUtils.toList(stringArray));
//    public static final Set<Integer> setWithInteger = new HashSet<>(ArrayUtils.toList(intArray));
//    public static final Set<Set<List<Integer>>> setSetListWithInteger = new HashSet<>(Set.of(new HashSet<>(Set.of(listWithInteger)), new HashSet<>(Set.of(listWithInteger1)), new HashSet<>(Set.of(listWithInteger2))));
//    public static final Set<Set<ObjectA>> setSetWithObject = new HashSet<>(Set.of(new HashSet<>(Set.of(objectA))));
//    public static final Set<String> setWithString = new HashSet<>(ArrayUtils.toList(stringArray));
//    public static final Map<Integer, ObjectA> mapWithObject = new HashMap<>(Map.of(1, objectA, 2, objectA, 3, objectA));
//    public static final Map<ObjectA, List<Integer>> mapWithList = new HashMap<>(Map.of(objectA, listWithInteger));
//    public static final Map<List<List<ObjectA>>, List<List<List<Integer>>>> mapWithListList = new HashMap<>(Map.of(new ArrayList<>(List.of(listWithObject, listWithObject, listWithObject)), listListListWithInteger));
//    public static final List<Map<Integer, String>> listMap = new ArrayList<>(List.of(mapWithInteger, mapWithInteger, mapWithInteger));
//    public static final Set<Map<Integer, String>> setMapWithInteger = new HashSet<>(Set.of(mapWithInteger));
//    public static final Map<List<Map<Integer, String>>, Set<Map<Integer, String>>> mapListSet = new HashMap<>(Map.of(listMap, setMapWithInteger));
//    public static final Byte[] byteBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(byteArray), Byte.class);
//    public static final Short[] shortBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(shortArray), Short.class);
//    public static final Integer[] integerArray = ArrayUtils.listToArray(ArrayUtils.toList(intArray), Integer.class);
//    public static final Long[] longBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(longArray), Long.class);
//    public static final List<Long> listWithLong = ArrayUtils.toList(longArray);
//    public static final Float[] floatBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(floatArray), Float.class);
//    public static final List<Float> listWithFloat = ArrayUtils.toList(floatArray);
//    public static final Double[] doubleBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(doubleArray), Double.class);
//    public static final List<Double> listWithDouble = ArrayUtils.toList(doubleArray);
//    public static final Boolean[] booleanBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(booleanArray), Boolean.class);
//    public static final List<Boolean> listWithBoolean = ArrayUtils.toList(booleanArray);
//    public static final Character[] charBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(charArray), Character.class);
//    public static final ComplexObject complexObject = new ComplexObject();
//    public static final NormalObject normalObject = new NormalObject();
//    public static final SimpleObject simpleObject = new SimpleObject();
//
//    static {
//        objectA.a = 999999990;
////        objectA.objectB = objectB;
//        objectA.m = mapWithInteger;
////        objectA.setA(Integer.MAX_VALUE);
////        objectA.setM(mapWithInteger);
////        objectA.setObjectB(objectB);
//        objectB.setFlag(false);
//    }
//
//    static {
//        complexObject.setA(byteValue);
////        complexObject.setAa(byteValue);
//        complexObject.setAaa(byteArray);
//        complexObject.setAaaa(byteBoxArray);
//        complexObject.setB(shortValue);
//        complexObject.setBb(shortValue);
//        complexObject.setBbb(shortArray);
//        complexObject.setBbbb(shortBoxArray);
//        complexObject.setC(intValue);
//        complexObject.setCc(intValue);
//        complexObject.setCcc(intArray);
//        complexObject.setCccc(integerArray);
//        complexObject.setD(longValue);
//        complexObject.setDd(longValue);
//        complexObject.setDdd(longArray);
//        complexObject.setDddd(longBoxArray);
//        complexObject.setE(floatValue);
//        complexObject.setEe(floatValue);
//        complexObject.setEee(floatArray);
//        complexObject.setEeee(floatBoxArray);
//        complexObject.setF(doubleValue);
//        complexObject.setFf(doubleValue);
//        complexObject.setFff(doubleArray);
//        complexObject.setFfff(doubleBoxArray);
//        complexObject.setG(true);
//        complexObject.setGg(true);
//        complexObject.setGgg(booleanArray);
//        complexObject.setGggg(booleanBoxArray);
////        complexObject.setH(charValue);
////        complexObject.setHh(charValue);
////        complexObject.setHhh(charArray);
////        complexObject.setHhhh(charBoxArray);
//        complexObject.setJj(stringValue);
//        complexObject.setJjj(stringArray);
//        complexObject.setKk(objectA);
//        complexObject.setKkk(new ObjectA[]{objectA, objectA});
//
//        complexObject.setL(listWithInteger);
////        complexObject.setLl(listListListWithInteger);
////        complexObject.setLll(listListWithObject);
//        complexObject.setLlll(listWithString);
//        complexObject.setLllll(listMap);
//
//        complexObject.setM(mapWithInteger);
//        complexObject.setMm(mapWithObject);
////        complexObject.setMmm(mapWithList);
////        complexObject.setMmmm(mapWithListList);
////        complexObject.setMmmmm(mapListSet);
//
//        complexObject.setS(setWithInteger);
////        complexObject.setSs(setSetListWithInteger);
////        complexObject.setSss(setSetWithObject);
//        complexObject.setSsss(setWithString);
////        complexObject.setSssss(setMapWithInteger);
//
//        normalObject.setA(byteValue);
//        normalObject.setAaa(byteArray);
//        normalObject.setB(shortValue);
//        normalObject.setC(intValue);
//        normalObject.setD(longValue);
//        normalObject.setE(floatValue);
//        normalObject.setF(doubleValue);
//        normalObject.setG(true);
//        normalObject.setJj(stringValue);
//        normalObject.setKk(objectA);
//
//        normalObject.setL(listWithInteger);
//        normalObject.setLl(listWithLong);
//        normalObject.setLll(listWithObject);
//        normalObject.setLlll(listWithString);
//
//        normalObject.setM(mapWithInteger);
//        normalObject.setMm(mapWithObject);
//
//        normalObject.setS(setWithInteger);
//        normalObject.setSsss(setWithString);
//
//        simpleObject.setC(intValue);
//        simpleObject.setG(true);
//
//
//    }
//}
