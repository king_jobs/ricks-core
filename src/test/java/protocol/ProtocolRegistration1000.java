package protocol;

import org.ricks.net.rpc.RpcPackage;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.ByteBufUtils;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.registration.IProtocolRegistration;

import java.lang.reflect.Constructor;

public class ProtocolRegistration1000 implements IProtocolRegistration {
    private Constructor constructor;

    public ProtocolRegistration1000(Constructor var1) {
        this.constructor = var1;
    }

    public final short protocolId() {
        return 1000;
    }

    public final Constructor protocolConstructor() {
        return this.constructor;
    }

    public final byte module() {
        return 0;
    }

    public final void write(ByteBuf var1, Object var2) {
        RpcPackage var3 = (RpcPackage)var2;
        if (!ByteBufUtils.writePacketFlag(var1, var3)) {
            ByteBufUtils.writeShort(var1, var3.getCmd());
            ByteBufUtils.writeByteArray(var1, var3.getData());
            ByteBufUtils.writeInt(var1, var3.getReqId());
        }
    }

    public final Object read(ByteBuf var1) {
        if (!ByteBufUtils.readBoolean(var1)) {
            return null;
        } else {
            RpcPackage var2 = new RpcPackage();
            var2.setCmd(ByteBufUtils.readShort(var1));
            var2.setData(ByteBufUtils.readByteArray(var1));
            var2.setReqId(ByteBufUtils.readInt(var1));
            return var2;
        }
    }
}
