package protocol;

import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.ByteBufUtils;
import org.ricks.protocol.registration.IProtocolRegistration;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

public class ProtocolRegistration11111 implements IProtocolRegistration {
    @Override
    public short protocolId() {
        return 0;
    }

    @Override
    public byte module() {
        return 0;
    }

    @Override
    public Constructor<?> protocolConstructor() {
        return null;
    }

    @Override
    public void write(ByteBuf buffer, Object packet) {

    }

    @Override
    public Object read(ByteBuf buffer) {
        int a = ByteBufUtils.readInt(buffer);
        String c = ByteBufUtils.readString(buffer);
        List e = ByteBufUtils.readStringList(buffer);
        double g = ByteBufUtils.readDouble(buffer);
        int[] h = ByteBufUtils.readIntArray(buffer);
        long b = ByteBufUtils.readLong(buffer);
        Map f = ByteBufUtils.readStringLongMap(buffer);
        short d = ByteBufUtils.readShort(buffer);

        return new ObjectC(a,b,c,d,e,f,g,h);
    }
}
