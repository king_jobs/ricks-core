package protocol;

import org.ricks.protocol.Protocol;

import java.util.List;
import java.util.Map;

@Protocol(id = 11111)
public record ObjectC(
        int a,
        long l,
        String b,
        short s,
        List<String> c,
        Map<String,Long> map1,
        double d,
        int[] intArr
) {
}
