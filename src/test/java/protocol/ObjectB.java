

package protocol;

import org.ricks.protocol.IPacket;
import org.ricks.protocol.Protocol;

import java.util.Objects;

@Protocol(id = 103)
public class ObjectB implements IPacket {

    public static final transient short PROTOCOL_ID = 103;

    private boolean flag;

//    private byte a;
//
//    private String b;
//
//    private int c;
//
//    private long d;
//
//    private float e;
//
//    private double f;
//
//    private byte[] g;
//
//    private int[] h;
//
//    private String[] i;


    @Override
    public short protocolId() {
        return PROTOCOL_ID;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }


//    public byte[] getG() {
//        return g;
//    }
//
//    public void setG(byte[] g) {
//        this.g = g;
//    }
//
//    public int[] getH() {
//        return h;
//    }
//
//    public void setH(int[] h) {
//        this.h = h;
//    }
//
//    public String[] getI() {
//        return i;
//    }
//
//    public void setI(String[] i) {
//        this.i = i;
//    }
//
//    public byte getA() {
//        return a;
//    }
//
//    public int getC() {
//        return c;
//    }
//
//    public void setC(int c) {
//        this.c = c;
//    }
//
//    public long getD() {
//        return d;
//    }
//
//    public void setD(long d) {
//        this.d = d;
//    }
//
//    public float getE() {
//        return e;
//    }
//
//    public void setE(float e) {
//        this.e = e;
//    }
//
//    public double getF() {
//        return f;
//    }
//
//    public void setF(double f) {
//        this.f = f;
//    }
//
//    public void setA(byte a) {
//        this.a = a;
//    }
//
//    public String getB() {
//        return b;
//    }
//
//    public void setB(String b) {
//        this.b = b;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectB objectB = (ObjectB) o;
        return flag == objectB.flag;
    }

    @Override
    public int hashCode() {
        return Objects.hash(flag);
    }
}

