//package protocol;
//
//import java.lang.reflect.Constructor;
//
//import org.ricks.lang.Logger;
//import org.ricks.protocol.ByteBuf;
//import org.ricks.protocol.ByteBufUtils;
//import org.ricks.protocol.IPacket;
//import org.ricks.protocol.registration.IProtocolRegistration;
//import org.ricks.utils.ClassUtil;
//import protocol.SimpleObject;
//
//public class ProtocolRegistration104 implements IProtocolRegistration {
//    private Constructor constructor;
//
//    private ProtocolRegistration100 protocolRegistration100;
//
//    public ProtocolRegistration104(Constructor var1) {
//        this.constructor = var1;
//    }
//
//    public final short protocolId() {
//        return 104;
//    }
//
//    public final Constructor protocolConstructor() {
//        return this.constructor;
//    }
//
//    public final byte module() {
//        return 0;
//    }
//
//    public final void write(ByteBuf var1, Object var2) {
//        SimpleObject var3 = (SimpleObject)var2;
//        if (!ByteBufUtils.writePacketFlag(var1, var3)) {
//            ByteBufUtils.writeInt(var1, var3.getC());
//            ByteBufUtils.writeBoolean(var1, var3.isG());
//
//            Object a = var3.getC();
//            if(a instanceof String s) {
//                ByteBufUtils.writeString(var1,s);
//            } else if(a instanceof IPacket packet) {
//                protocolRegistration100.write(var1,packet);
//            } else {
//                Logger.error("Object 只允许基础数据 和 IPacket");
//            }
//        }
//    }
//
//    public final Object read(ByteBuf var1) {
//        if (!ByteBufUtils.readBoolean(var1)) {
//            return null;
//        } else {
//            SimpleObject var2 = new SimpleObject();
//            var2.setC(ByteBufUtils.readInt(var1));
//            var2.setG(ByteBufUtils.readBoolean(var1));
//            return var2;
//        }
//    }
//}