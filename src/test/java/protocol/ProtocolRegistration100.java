//package protocol;
//
//
//import java.lang.reflect.Constructor;
//import java.util.Map;
//
//import org.ricks.protocol.ByteBuf;
//import org.ricks.protocol.ByteBufUtils;
//import org.ricks.protocol.IPacket;
//import org.ricks.protocol.collection.CollectionUtils;
//import org.ricks.protocol.registration.IProtocolRegistration;
//import protocol.ComplexObject;
//import protocol.ObjectA;
//
//public class ProtocolRegistration100 implements IProtocolRegistration {
//    private Constructor constructor;
//    public IProtocolRegistration protocolRegistration102;
//
//    public ProtocolRegistration100(Constructor var1) {
//        this.constructor = var1;
//    }
//
//    public final short protocolId() {
//        return 100;
//    }
//
//    public final Constructor protocolConstructor() {
//        return this.constructor;
//    }
//
//    public final byte module() {
//        return 0;
//    }
//
//    public final void write(ByteBuf var1, Object var2) {
//        ComplexObject var3 = (ComplexObject)var2;
//        if (!ByteBufUtils.writePacketFlag(var1, var3)) {
//            ByteBufUtils.writeByte(var1, var3.getA());
//            ByteBufUtils.writeByteBox(var1, var3.getAa());
//            ByteBufUtils.writeByteArray(var1, var3.getAaa());
//            ByteBufUtils.writeByteBoxArray(var1, var3.getAaaa());
//            ByteBufUtils.writeShort(var1, var3.getB());
//            ByteBufUtils.writeShortBox(var1, var3.getBb());
//            ByteBufUtils.writeShortArray(var1, var3.getBbb());
//            ByteBufUtils.writeShortBoxArray(var1, var3.getBbbb());
//            ByteBufUtils.writeInt(var1, var3.getC());
//            ByteBufUtils.writeIntBox(var1, var3.getCc());
//            ByteBufUtils.writeIntArray(var1, var3.getCcc());
//            ByteBufUtils.writeIntBoxArray(var1, var3.getCccc());
//            ByteBufUtils.writeLong(var1, var3.getD());
//            ByteBufUtils.writeLongBox(var1, var3.getDd());
//            ByteBufUtils.writeLongArray(var1, var3.getDdd());
//            ByteBufUtils.writeLongBoxArray(var1, var3.getDddd());
//            ByteBufUtils.writeFloat(var1, var3.getE());
//            ByteBufUtils.writeFloatBox(var1, var3.getEe());
//            ByteBufUtils.writeFloatArray(var1, var3.getEee());
//            ByteBufUtils.writeFloatBoxArray(var1, var3.getEeee());
//            ByteBufUtils.writeDouble(var1, var3.getF());
//            ByteBufUtils.writeDoubleBox(var1, var3.getFf());
//            ByteBufUtils.writeDoubleArray(var1, var3.getFff());
//            ByteBufUtils.writeDoubleBoxArray(var1, var3.getFfff());
//            ByteBufUtils.writeBoolean(var1, var3.isG());
//            ByteBufUtils.writeBooleanBox(var1, var3.getGg());
//            ByteBufUtils.writeBooleanArray(var1, var3.getGgg());
//            ByteBufUtils.writeBooleanBoxArray(var1, var3.getGggg());
//            ByteBufUtils.writeString(var1, var3.getJj());
//            ByteBufUtils.writeStringArray(var1, var3.getJjj());
//            this.protocolRegistration102.write(var1, (IPacket)var3.getKk());
////            ByteBufUtils.writePacketArray(var1, var3.getKkk(), this.protocolRegistration102);
//            ByteBufUtils.writeIntList(var1, var3.getL());
////            var3.getLl();
////            var3.getLll();
//            ByteBufUtils.writeStringList(var1, var3.getLlll());
//            var3.getLllll();
//            ByteBufUtils.writeIntStringMap(var1, var3.getM());
////            ByteBufUtils.writeIntPacketMap(var1, var3.getMm(), this.protocolRegistration102);
////            var3.getMmm();
////            var3.getMmmm();
////            var3.getMmmmm();
//            ByteBufUtils.writeIntSet(var1, var3.getS());
////            var3.getSs();
////            var3.getSss();
//            ByteBufUtils.writeStringSet(var1, var3.getSsss());
////            var3.getSssss();
//            ByteBufUtils.writeInt(var1, var3.getMyCompatible());
//            this.protocolRegistration102.write(var1, (IPacket)var3.getMyObject());
//        }
//    }
//
//    public final Object read(ByteBuf var1) {
//        if (!ByteBufUtils.readBoolean(var1)) {
//            return null;
//        } else {
//            ComplexObject var2 = new ComplexObject();
//            var2.setA(ByteBufUtils.readByte(var1));
//            var2.setAa(ByteBufUtils.readByteBox(var1));
//            var2.setAaa(ByteBufUtils.readByteArray(var1));
//            var2.setAaaa(ByteBufUtils.readByteBoxArray(var1));
//            var2.setB(ByteBufUtils.readShort(var1));
//            var2.setBb(ByteBufUtils.readShortBox(var1));
//            var2.setBbb(ByteBufUtils.readShortArray(var1));
//            var2.setBbbb(ByteBufUtils.readShortBoxArray(var1));
//            var2.setC(ByteBufUtils.readInt(var1));
//            var2.setCc(ByteBufUtils.readIntBox(var1));
//            var2.setCcc(ByteBufUtils.readIntArray(var1));
//            var2.setCccc(ByteBufUtils.readIntBoxArray(var1));
//            var2.setD(ByteBufUtils.readLong(var1));
//            var2.setDd(ByteBufUtils.readLongBox(var1));
//            var2.setDdd(ByteBufUtils.readLongArray(var1));
//            var2.setDddd(ByteBufUtils.readLongBoxArray(var1));
//            var2.setE(ByteBufUtils.readFloat(var1));
//            var2.setEe(ByteBufUtils.readFloatBox(var1));
//            var2.setEee(ByteBufUtils.readFloatArray(var1));
//            var2.setEeee(ByteBufUtils.readFloatBoxArray(var1));
//            var2.setF(ByteBufUtils.readDouble(var1));
//            var2.setFf(ByteBufUtils.readDoubleBox(var1));
//            var2.setFff(ByteBufUtils.readDoubleArray(var1));
//            var2.setFfff(ByteBufUtils.readDoubleBoxArray(var1));
//            var2.setG(ByteBufUtils.readBoolean(var1));
//            var2.setGg(ByteBufUtils.readBooleanBox(var1));
//            var2.setGgg(ByteBufUtils.readBooleanArray(var1));
//            var2.setGggg(ByteBufUtils.readBooleanBoxArray(var1));
//            var2.setJj(ByteBufUtils.readString(var1));
//            var2.setJjj(ByteBufUtils.readStringArray(var1));
//            var2.setKk((ObjectA)this.protocolRegistration102.read(var1));
//            System.err.println("111");
//            int var40 = ByteBufUtils.readInt(var1);
//            ObjectA[] var41 = new ObjectA[CollectionUtils.comfortableLength(var40)];
//
//            for(int var42 = 0; var42 < var40; ++var42) {
//                ObjectA var43 = (ObjectA)this.protocolRegistration102.read(var1);
//                var41[var42] = var43;
//            }
//
//            var2.setKkk(var41);
//            System.err.println("222");
//            var2.setL(ByteBufUtils.readIntList(var1));
//            var2.setLlll(ByteBufUtils.readStringList(var1));
//            var2.setM(ByteBufUtils.readIntStringMap(var1));
//            Map map = ByteBufUtils.readIntPacketMap(var1, this.protocolRegistration102);
//            System.err.println("333");
//            var2.setMm(map);
//            var2.setS(ByteBufUtils.readIntSet(var1));
//            var2.setSsss(ByteBufUtils.readStringSet(var1));
//            var2.setMyCompatible(ByteBufUtils.readInt(var1));
//            var2.setMyObject((ObjectA)this.protocolRegistration102.read(var1));
//            System.err.println("444");
//            return var2;
//        }
//    }
//}
