//package protocol;
//
//import org.ricks.protocol.ByteBuf;
//import org.ricks.protocol.ByteBufUtils;
//import org.ricks.protocol.IPacket;
//import org.ricks.protocol.registration.IProtocolRegistration;
//import org.ricks.protocol.serializer.IntSerializer;
//import org.ricks.protocol.serializer.MapSerializer;
//import org.ricks.protocol.serializer.ObjectProtocolSerializer;
//
//import java.lang.reflect.Constructor;
//import java.util.Map;
//
//public class ProtocolRegistration102 implements IProtocolRegistration {
//    private Constructor constructor;
//    public IProtocolRegistration protocolRegistration103;
//
//    public ProtocolRegistration102(Constructor var1) {
//        this.constructor = var1;
//    }
//
//    public final short protocolId() {
//        return 102;
//    }
//
//    public final Constructor protocolConstructor() {
//        return this.constructor;
//    }
//
//    public final byte module() {
//        return 0;
//    }
//
//    public final void write(ByteBuf var1, IPacket var2) {
//        ObjectA var3 = (ObjectA)var2;
//        if (!ByteBufUtils.writePacketFlag(var1, var3)) {
//            ByteBufUtils.writeInt(var1, var3.getA());
//            ByteBufUtils.writeIntStringMap(var1, var3.getM());
//            this.protocolRegistration103.write(var1, (IPacket)var3.getObjectB());
//        }
//    }
//
//    public final Object read(ByteBuf var1) {
//        if (!ByteBufUtils.readBoolean(var1)) {
//            return null;
//        } else {
//            ObjectA var2 = new ObjectA();
//            var2.setA(ByteBufUtils.readInt(var1));
//            var2.setM(ByteBufUtils.readIntStringMap(var1));
//            var2.setObjectB((ObjectB)this.protocolRegistration103.read(var1));
//            return var2;
//        }
//    }
//}