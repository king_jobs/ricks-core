//
//
//package protocol;
//
//
//import org.ricks.protocol.ByteBuf;
//import org.ricks.protocol.ProtocolManager;
//import org.ricks.protocol.collection.ArrayUtils;
//import org.ricks.utils.StrUtil;
//
//import java.util.*;
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//
//public class SpeedTest {
//
//    public static int benchmark = 100_0000;
//
//    static {
//
//        // 这行加上，会在protocol目录下，生成jsProtocol文件夹及其对应的js协议文件
////        op.getGenerateLanguages().add(CodeLanguage.Cpp);
////        op.getGenerateLanguages().add(CodeLanguage.JavaScript);
////        op.getGenerateLanguages().add(CodeLanguage.TypeScript);
////        op.getGenerateLanguages().add(CodeLanguage.Lua);
////        op.getGenerateLanguages().add(CodeLanguage.CSharp);
////        op.getGenerateLanguages().add(CodeLanguage.GdScript);
//
//        // 需要protocol协议的字段里面都加上JProtobuf注解才能用
////        op.setProtocolParam("protobuf=protobuf.xml");
////        op.getGenerateLanguages().add(CodeLanguage.Protobuf);
//
//        // zfoo协议注册(其实就是：将Set里面的协议号和对应的类注册好，这样子就可以根据协议号知道是反序列化为哪个类)
//        ProtocolManager.initProtocolAuto(Set.of( SimpleObject.class, NormalObject.class,ObjectA.class,ObjectB.class, ComplexObject.class));
//
//    }
//
//
//    @Ignore
//    @Test
//    public void zfooTest() {
//
//        // netty的ByteBuf做了更多的安全检测，java自带的ByteBuffer并没有做安全检测，为了公平，把不需要的检测去掉
//        // java通过ByteBuffer.allocate(1024 * 8)构造出来的是使用了unsafe的HeapByteBuffer，为了公平，使用netty中带有unsafe操作的UnpooledUnsafeHeapByteBuf
//
//        ByteBuf buffer = ByteBuf.allocate();
//
//        // 序列化和反序列化简单对象
//        long startTime = System.currentTimeMillis();
//        for (int i = 0; i < benchmark; i++) {
//            buffer.clear();
//            // 把对象序列化到buffer中
//            ProtocolManager.write(buffer, simpleObject);
//
//            // 从buffer中反序列化出对象
//            var packet = ProtocolManager.read(buffer);
//
//        }
//
//        System.out.println(StrUtil.format("[ricks]     [简单对象] [thread:{}] [size:{}] [time:{}]", Thread.currentThread().getName(), buffer.getwriteIndex(), System.currentTimeMillis() - startTime));
//
//        // 序列化和反序列化常规对象
//        startTime = System.currentTimeMillis();
//        for (int i = 0; i < benchmark; i++) {
//            buffer.clear();
//            ProtocolManager.write(buffer, normalObject);
//            var packet = ProtocolManager.read(buffer);
//
//
//        }
//
//        System.out.println(StrUtil.format("[ricks]     [常规对象] [thread:{}] [size:{}] [time:{}]", Thread.currentThread().getName(), buffer.getwriteIndex(), System.currentTimeMillis() - startTime));
//
//        // 序列化和反序列化复杂对象
//        startTime = System.currentTimeMillis();
//        for (int i = 0; i < benchmark; i++) {
//            buffer.clear();
//            ProtocolManager.write(buffer, complexObject);
//            var packet = ProtocolManager.read(buffer);
//
//        }
//
//        System.out.println(StrUtil.format("[ricks]     [复杂对象] [thread:{}] [size:{}] [time:{}]", Thread.currentThread().getName(), buffer.getwriteIndex(), System.currentTimeMillis() - startTime));
//
//    }
//
//
//
//    // -------------------------------------------以下为测试用例---------------------------------------------------------------
//    // 简单类型/////////////
//    public static final byte byteValue = 99;
//    public static final short shortValue = 9999;
//    public static final int intValue = 99999999;
//    public static final long longValue = 9999999999999999L;
//    public static final float floatValue = 99999999.9F;
//    public static final double doubleValue = 99999999.9D;
//    public static final char charValue = 'c';
//    public static final String charValueString = "c";
//    public static final String stringValue = "hello";
//
//    // 数组类型
//    public static final boolean[] booleanArray = new boolean[]{true, false, true, false, true};
//    public static final byte[] byteArray = new byte[]{Byte.MIN_VALUE, -99, 0, 99, Byte.MAX_VALUE};
//    public static final short[] shortArray = new short[]{Short.MIN_VALUE, -99, 0, 99, Short.MAX_VALUE};
//    public static final int[] intArray = new int[]{Integer.MIN_VALUE, -99999999, -99, 0, 99, 99999999, Integer.MAX_VALUE};
//    public static final int[] intArray1 = new int[]{Integer.MIN_VALUE, -99999999, -99, 0, 99, 99999999, Integer.MAX_VALUE - 1};
//    public static final int[] intArray2 = new int[]{Integer.MIN_VALUE, -99999999, -99, 0, 99, 99999999, Integer.MAX_VALUE - 2};
//    public static final long[] longArray = new long[]{Long.MIN_VALUE, -9999999999999999L, -99999999L, -99L, 0L, 99L, 99999999L, 9999999999999999L, Long.MAX_VALUE};
//    public static final float[] floatArray = new float[]{Float.MIN_VALUE, -99999999.9F, -99.9F, 0F, 99.9F, 99999999.9F, Float.MAX_VALUE};
//    public static final double[] doubleArray = new double[]{Double.MIN_VALUE, -99999999.9F, -99.9D, 0D, 99.9D, 99999999.9F, Double.MAX_VALUE};
//    public static final char[] charArray = new char[]{'a', 'b', 'c', 'd', 'e'};
//    public static final String[] stringArray = new String[]{"a", "b", "c", "d", "e"};
//
//    public static final ObjectA objectA = new ObjectA();
//    public static final ObjectB objectB = new ObjectB();
//    public static final Map<Integer, String> mapWithInteger = new HashMap<>(Map.of(Integer.MIN_VALUE, "a", -99, "b", 0, "c", 99, "d", Integer.MAX_VALUE, "e"));
//    public static final List<Integer> listWithInteger = new ArrayList<>(ArrayUtils.toList(intArray));
//    public static final List<Integer> listWithInteger1 = new ArrayList<>(ArrayUtils.toList(intArray1));
//    public static final List<Integer> listWithInteger2 = new ArrayList<>(ArrayUtils.toList(intArray2));
//    public static final List<ObjectA> listWithObject = new ArrayList<>(List.of(objectA, objectA, objectA));
//    public static final List<List<ObjectA>> listListWithObject = new ArrayList<>(List.of(listWithObject, listWithObject, listWithObject));
//    public static final List<List<Integer>> listListWithInteger = new ArrayList<>(List.of(listWithInteger, listWithInteger, listWithInteger));
//    public static final List<List<List<Integer>>> listListListWithInteger = new ArrayList<>(List.of(listListWithInteger, listListWithInteger, listListWithInteger));
//    public static final List<String> listWithString = new ArrayList<>(ArrayUtils.toList(stringArray));
//    public static final Set<Integer> setWithInteger = new HashSet<>(ArrayUtils.toList(intArray));
//    public static final Set<Set<List<Integer>>> setSetListWithInteger = new HashSet<>(Set.of(new HashSet<>(Set.of(listWithInteger)), new HashSet<>(Set.of(listWithInteger1)), new HashSet<>(Set.of(listWithInteger2))));
//    public static final Set<Set<ObjectA>> setSetWithObject = new HashSet<>(Set.of(new HashSet<>(Set.of(objectA))));
//    public static final Set<String> setWithString = new HashSet<>(ArrayUtils.toList(stringArray));
//    public static final Map<Integer, ObjectA> mapWithObject = new HashMap<>(Map.of(1, objectA, 2, objectA, 3, objectA));
//    public static final Map<ObjectA, List<Integer>> mapWithList = new HashMap<>(Map.of(objectA, listWithInteger));
//    public static final Map<List<List<ObjectA>>, List<List<List<Integer>>>> mapWithListList = new HashMap<>(Map.of(new ArrayList<>(List.of(listWithObject, listWithObject, listWithObject)), listListListWithInteger));
//    public static final List<Map<Integer, String>> listMap = new ArrayList<>(List.of(mapWithInteger, mapWithInteger, mapWithInteger));
//    public static final Set<Map<Integer, String>> setMapWithInteger = new HashSet<>(Set.of(mapWithInteger));
//    public static final Map<List<Map<Integer, String>>, Set<Map<Integer, String>>> mapListSet = new HashMap<>(Map.of(listMap, setMapWithInteger));
//    public static final Byte[] byteBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(byteArray), Byte.class);
//    public static final Short[] shortBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(shortArray), Short.class);
//    public static final Integer[] integerArray = ArrayUtils.listToArray(ArrayUtils.toList(intArray), Integer.class);
//    public static final Long[] longBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(longArray), Long.class);
//    public static final List<Long> listWithLong = ArrayUtils.toList(longArray);
//    public static final Float[] floatBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(floatArray), Float.class);
//    public static final List<Float> listWithFloat = ArrayUtils.toList(floatArray);
//    public static final Double[] doubleBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(doubleArray), Double.class);
//    public static final List<Double> listWithDouble = ArrayUtils.toList(doubleArray);
//    public static final Boolean[] booleanBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(booleanArray), Boolean.class);
//    public static final List<Boolean> listWithBoolean = ArrayUtils.toList(booleanArray);
//    public static final Character[] charBoxArray = ArrayUtils.listToArray(ArrayUtils.toList(charArray), Character.class);
//    public static final ComplexObject complexObject = new ComplexObject();
//    public static final NormalObject normalObject = new NormalObject();
//    public static final SimpleObject simpleObject = new SimpleObject();
//
//    static {
//        objectA.a = 999999990;
//        objectA.objectB = objectB;
//        objectA.m = mapWithInteger;
//        objectA.setA(Integer.MAX_VALUE);
//        objectA.setM(mapWithInteger);
//        objectA.setObjectB(objectB);
//        objectB.setFlag(false);
//    }
//
//    static {
//        complexObject.setA(byteValue);
//        complexObject.setAa(byteValue);
//        complexObject.setAaa(byteArray);
//        complexObject.setAaaa(byteBoxArray);
//        complexObject.setB(shortValue);
//        complexObject.setBb(shortValue);
//        complexObject.setBbb(shortArray);
//        complexObject.setBbbb(shortBoxArray);
//        complexObject.setC(intValue);
//        complexObject.setCc(intValue);
//        complexObject.setCcc(intArray);
//        complexObject.setCccc(integerArray);
//        complexObject.setD(longValue);
//        complexObject.setDd(longValue);
//        complexObject.setDdd(longArray);
//        complexObject.setDddd(longBoxArray);
//        complexObject.setE(floatValue);
//        complexObject.setEe(floatValue);
//        complexObject.setEee(floatArray);
//        complexObject.setEeee(floatBoxArray);
//        complexObject.setF(doubleValue);
//        complexObject.setFf(doubleValue);
//        complexObject.setFff(doubleArray);
//        complexObject.setFfff(doubleBoxArray);
//        complexObject.setG(true);
//        complexObject.setGg(true);
//        complexObject.setGgg(booleanArray);
//        complexObject.setGggg(booleanBoxArray);
////        complexObject.setH(charValue);
////        complexObject.setHh(charValue);
////        complexObject.setHhh(charArray);
////        complexObject.setHhhh(charBoxArray);
//        complexObject.setJj(stringValue);
//        complexObject.setJjj(stringArray);
//        complexObject.setKk(objectA);
//        complexObject.setKkk(new ObjectA[]{objectA, objectA});
//
//        complexObject.setL(listWithInteger);
////        complexObject.setLl(listListListWithInteger);
////        complexObject.setLll(listListWithObject);
//        complexObject.setLlll(listWithString);
//        complexObject.setLllll(listMap);
//
//        complexObject.setM(mapWithInteger);
//        complexObject.setMm(mapWithObject);
////        complexObject.setMmm(mapWithList);
////        complexObject.setMmmm(mapWithListList);
////        complexObject.setMmmmm(mapListSet);
//
//        complexObject.setS(setWithInteger);
////        complexObject.setSs(setSetListWithInteger);
////        complexObject.setSss(setSetWithObject);
//        complexObject.setSsss(setWithString);
////        complexObject.setSssss(setMapWithInteger);
//
//        normalObject.setA(byteValue);
//        normalObject.setAaa(byteArray);
//        normalObject.setB(shortValue);
//        normalObject.setC(intValue);
//        normalObject.setD(longValue);
//        normalObject.setE(floatValue);
//        normalObject.setF(doubleValue);
//        normalObject.setG(true);
//        normalObject.setJj(stringValue);
//        normalObject.setKk(objectA);
//
//        normalObject.setL(listWithInteger);
//        normalObject.setLl(listWithLong);
//        normalObject.setLll(listWithObject);
//        normalObject.setLlll(listWithString);
//
//        normalObject.setM(mapWithInteger);
//        normalObject.setMm(mapWithObject);
//
//        normalObject.setS(setWithInteger);
//        normalObject.setSsss(setWithString);
//
//        simpleObject.setC(intValue);
//        simpleObject.setG(true);
//
//
//    }
//
//
//}
