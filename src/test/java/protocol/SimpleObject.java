

package protocol;


import org.ricks.protocol.IPacket;
import org.ricks.protocol.Protocol;


@Protocol(id = 104)
public class SimpleObject implements IPacket {

    private boolean g;
    private int c;


    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public boolean isG() {
        return g;
    }

    public void setG(boolean g) {
        this.g = g;
    }

}
