

package protocol;

import org.ricks.protocol.IPacket;
import org.ricks.protocol.Protocol;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Protocol(id = 102)

public class ObjectA  {

//    public static final transient short PROTOCOL_ID = 102;

    public int a;

    public Map<Integer, String> m;
//
////    private byte[] e;
//
//    public ObjectB objectB;

//    private List<Integer> h;
//
//    private Set<Integer> f;

//    @Override
//    public short protocolId() {
//        return PROTOCOL_ID;
//    }

//    public Set<Integer> getF() {
//        return f;
//    }
//
//    public void setF(Set<Integer> f) {
//        this.f = f;
//    }
//
//    public List<Integer> getH() {
//        return h;
//    }
//
//    public void setH(List<Integer> h) {
//        this.h = h;
//    }
//
//    public byte[] getE() {
//        return e;
//    }
//
//    public void setE(byte[] e) {
//        this.e = e;
//    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public Map<Integer, String> getM() {
        return m;
    }

    public void setM(Map<Integer, String> m) {
        this.m = m;
    }

//    public ObjectB getObjectB() {
//        return objectB;
//    }
//
//    public void setObjectB(ObjectB objectB) {
//        this.objectB = objectB;
//    }

}
