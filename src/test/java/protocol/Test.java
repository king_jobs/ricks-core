package protocol;

import app.vo.User;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.ByteBufUtils;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;
import org.ricks.protocol.registration.ProtocolAnalysis;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.BaseField;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.serializer.*;

import java.lang.reflect.Constructor;
import java.util.*;

public class Test {


    public static void main(String[] args) throws NoSuchMethodException {
        ProtocolAnalysis.analyze(Set.of(ObjectA.class, ObjectB.class, SimpleObject.class, NormalObject.class, ObjectC.class, ComplexObject.class));
//        System.err.println(     (ProtocolRegistration.class.getCanonicalName() + 100).replace(".","/"));
//        System.err.println( ProtocolRegistration.class.getSimpleName() + 100);

//        ProtocolRegistration registration = new ProtocolRegistration();
//        var fieldRegistrations = registration.getFieldRegistrations();
//        for (int i = 0; i < 100_0000 ; i++) {
//            ProtocolManager.
//        }

        Map<String,Long> map = Map.of("1",11L,"2",222L);
        ByteBuf byteBuf = new ByteBuf(1024);
        ObjectC objectC = new ObjectC(1,  9999L,"ricks",(short) 520,List.of("ricks","is","god"),map,0.1111,new int[]{1,4,9});
        ProtocolManager.write(byteBuf,objectC);
        var aa = ProtocolManager.read(byteBuf);
        System.err.println(aa);
    }


    public final void write(ByteBuf var1, Object var2) {
        NormalObject var3 = (NormalObject) var2;
        if (!ByteBufUtils.writePacketFlag(var1, var3)) {
            ByteSerializer.INSTANCE.writeObject(var1, var3.getA(), null);
        }
    }

}
