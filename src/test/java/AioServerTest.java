import org.ricks.net.handler.MessageProcessor;
import org.ricks.net.AioSession;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/8/2413:45
 */
public class AioServerTest {

    /**
     * 读回调处理线程池,可用于业务处理
     */
    private static final ExecutorService readExecutorService = Executors.newFixedThreadPool(10);

    /**
     * read工作组
     */
    static Work[] work  = new Work[5];;

    public static void main(String[] args) throws IOException {
        for (int i = 0; i < 5; i++) {
            work[i] = new Work();
            readExecutorService.execute(work[i]);
        }



        MessageProcessor<String> processor = new MessageProcessor<String>() {
            @Override
            public void process(AioSession session, String msg) {
//                System.out.println("receive from client: " + msg);
//                WriteBuffer outputStream = session.writeBuffer();
//                try {
//                    byte[] bytes = msg.getBytes();
//                    outputStream.writeInt(bytes.length);
//                    outputStream.write(bytes);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                work[0].add("aaaaaa");
            }
        };
//        AioServer server = new AioServer(8888, new StringProtocolTest(), processor); //需要自定义work线程 和  自定义处理业务机制
//        server.start();
    }
}
