//package asm;
//
//import app.command.EnumCall;
//import app.controller.TestController;
//import app.controller.UserController;
//import org.ricks.net.Context;
//import org.ricks.ioc.AppContext;
//
///**
// * ASM直接生成单例 ，协议解析直接调用
// * @author ricks
// * @date 2022/9/1415:32
// */
//public class Dispacher1 {
//
////    private Dispacher() {}
////
////    private static class SingleCase {
////        public static final Dispacher INSTANCE = new Dispacher();
////    }
////
////    public static Dispacher get() {
////        return Dispacher.SingleCase.INSTANCE;
////    }
////
////    public void dispacher(Context<Short,Byte[]> context) {
////        switch (context.getCmd()) {
////            case EnumCall.APP_CONTROLLER_TESTCONTROLLER_TESTUSERDB1:
////                ApplicationContext.getBean(TestController.class).test(context);
////                break;
////            case EnumCall.APP_CONTROLLER_TESTCONTROLLER_TESTUSERDB2:
////                ApplicationContext.getBean(TestController.class).testUserDb(context);
////                break;
////            default: System.err.println(context.getCmd() + " 未知指令");
////        }
////    }
//
//
//    public static void dispatcher(Context<Short,byte[]> context) {
//        switch (context.getCmd()) {
//            case EnumCall.APP_CONTROLLER_TESTCONTROLLER_TESTUSERDB1: AppContext.getBean(TestController.class).test(context);break;
//            case EnumCall.APP_CONTROLLER_TESTCONTROLLER_TESTUSERDB2: AppContext.getBean(TestController.class).testUserDb(context);break;
//            case 3: AppContext.getBean(UserController.class).testUserDb(context);break;
//            case 4: AppContext.getBean(UserController.class).testUserDb(context);break;
//            case 10: AppContext.getBean(UserController.class).testUserDb(context);break;
//            case 102: AppContext.getBean(UserController.class).testUserDb(context);break;
//
//            case 9: AppContext.getBean(UserController.class).testUserDb(context);break;
//            default: System.err.println(context.getCmd() + " 未知指令");
//        }
//    }
//}
