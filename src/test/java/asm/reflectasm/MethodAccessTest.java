/*
 * Copyright © 2018 www.noark.xyz All Rights Reserved.
 * 
 * 感谢您选择Noark框架，希望我们的努力能为您提供一个简单、易用、稳定的服务器端框架 ！
 * 除非符合Noark许可协议，否则不得使用该文件，您可以下载许可协议文件：
 * 
 * 		http://www.noark.xyz/LICENSE
 *
 * 1.未经许可，任何公司及个人不得以任何方式或理由对本框架进行修改、使用和传播;
 * 2.禁止在本项目或任何子项目的基础上发展任何派生版本、修改版本或第三方版本;
 * 3.无论你对源代码做出任何修改和改进，版权都归Noark研发团队所有，我们保留所有权利;
 * 4.凡侵犯Noark版权等知识产权的，必依法追究其法律责任，特此郑重法律声明！
 */
package asm.reflectasm;

import org.ricks.common.asm.MethodAccess;
import org.ricks.common.TimeUtils;

import java.lang.reflect.Method;

/**
 * 调用方法测试用例.
 *
 * @since 3.0
 * @author 小流氓[176543888@qq.com]
 */
public class MethodAccessTest {
	private final TestBean bean = new TestBean();
	private Method method;// JDK的反射.
	private MethodAccess access;// ReflectAsm
	private int methodIndex;

	{
		try {
			method = TestBean.class.getMethod("setId", int.class);
			access = MethodAccess.get(TestBean.class);
			methodIndex = access.getIndex("setId");
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}


	public static void main(String[] args) throws Exception {
		MethodAccessTest test = new MethodAccessTest();
		test.test();
	}


	public void test() throws Exception {
		long startTime1 = TimeUtils.currentTimeMillis();
		for (int i = 1; i < 1000000000; i++) {
			method.invoke(bean, 1);
		}
		long endTime1 = TimeUtils.currentTimeMillis();
		long time1 = endTime1 - startTime1;
		System.err.println("jdk反射耗时：" + time1 + "ms");

		long startTime2 = TimeUtils.currentTimeMillis();
		for (int i = 0; i < 1000000000; i++) {
			access.invoke(bean, methodIndex, 1);
		}
		long endTime2 = TimeUtils.currentTimeMillis();
		long time2 = endTime2 - startTime2;
		System.err.println("asm反射耗时：" + time2 + "ms");

		long startTime3 = TimeUtils.currentTimeMillis();
		TestBean testBean = new TestBean();
		for (int i = 0; i < 1000000000; i++) {
			testBean.setId(1);
		}
		long endTime3 = TimeUtils.currentTimeMillis();
		long time3 = endTime3 - startTime3;
		System.err.println("对象直接调用：" + time3 + "ms");

	}

}