package asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/11/2511:12
 */
public class Test1 {

    public static void main(String[] args) {

//        ClassWriter classWriter = new ClassWriter(0);
//
//        classWriter.visit(V17, ACC_PUBLIC | ACC_SUPER, "org/demon/dispatch/Dispacher", null, "java/lang/Object", null);
//        classWriter.visitSource("Dispacher.java", null);
//
////        FieldVisitor fieldVisitor = classWriter.visitField(ACC_PUBLIC | ACC_FINAL | ACC_STATIC, "dispacher", "Lorg/demon/dispatch/Dispacher;", null, null);
////        fieldVisitor.visitEnd();
//
//        classWriter.visitInnerClass("java/lang/invoke/MethodHandles$Lookup", "java/lang/invoke/MethodHandles", "Lookup", ACC_PUBLIC | ACC_FINAL | ACC_STATIC);
//
//        MethodVisitor methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
//        methodVisitor.visitCode();
////        Label label0 = new Label();
////        methodVisitor.visitLabel(label0);
////        methodVisitor.visitLineNumber(14, label0);
//        methodVisitor.visitVarInsn(ALOAD, 0);
//        methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
//        methodVisitor.visitInsn(RETURN);
////        Label label1 = new Label();
////        methodVisitor.visitLabel(label1);
////        methodVisitor.visitLocalVariable("this", "Lorg/demon/dispatch/Dispacher;", null, label0, label1, 0);
//        methodVisitor.visitMaxs(0, 0);
//        methodVisitor.visitEnd();
//
//        methodVisitor = classWriter.visitMethod(ACC_PUBLIC + ACC_STATIC, "dispacher", "(Lorg/demon/common/Context;)V", "(Lorg/demon/common/Context<Ljava/lang/Short;[Ljava/lang/Byte;>;)V", null);
//        methodVisitor.visitParameter("context", 0);
//        methodVisitor.visitCode();
//
////        Label start_label = new Label();
////        methodVisitor.visitLabel(start_label);
////        methodVisitor.visitLineNumber(19, start_label);
//        methodVisitor.visitVarInsn(ALOAD, 1);
//        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "org/demon/common/Context", "getCmd", "()Ljava/lang/Object;", false);
//        methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/Short");
//        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S", false);
//
//        int[] messageIds = new int[tupleList.size()];
//        Label[] labels = new Label[tupleList.size()];
//        for (int i = 0; i < tupleList.size(); i++) {
//            labels[i] = new Label();
//            messageIds[i] = Integer.valueOf(tupleList.get(i).get(0).toString());
//        }
//        Label defaultLabel  = new Label();
//        Label end_label = new Label();
//        methodVisitor.visitTableSwitchInsn(1,labels.length,defaultLabel, labels);
//        int currLineNum = 19;
//        for (int i = 0; i < tupleList.size(); i++) {
//            currLineNum += 2;
//            createMethodFunction(methodVisitor,tupleList.get(i), labels[i],currLineNum,end_label);
//        }
//
////        methodVisitor.visitLabel(defaultLabel);
////        methodVisitor.visitLineNumber(currLineNum, defaultLabel);
////        methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
////        methodVisitor.visitInsn(RETURN);
//
//
//        methodVisitor.visitLabel(end_label);
////        methodVisitor.visitLocalVariable("this", "Lorg/demon/dispatch/Dispacher;", null, start_label, end_label, 0);
////        methodVisitor.visitLocalVariable("context", "Lorg/demon/common/Context;", "Lorg/demon/common/Context<Ljava/lang/Short;[Ljava/lang/Byte;>;", start_label, end_label, 1);
//        methodVisitor.visitMaxs(2, 1);
//        methodVisitor.visitEnd();
//
//
//
////        methodVisitor = classWriter.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
////        methodVisitor.visitCode();
////        methodVisitor.visitLabel(new Label());
////        methodVisitor.visitLineNumber(14, label0);
////        methodVisitor.visitTypeInsn(NEW, "org/demon/dispatch/Dispacher");
////        methodVisitor.visitInsn(DUP);
////        methodVisitor.visitMethodInsn(INVOKESPECIAL, "org/demon/dispatch/Dispacher", "<init>", "()V", false);
////        methodVisitor.visitFieldInsn(PUTSTATIC, "org/demon/dispatch/Dispacher", "dispacher", "Lorg/demon/dispatch/Dispacher;");
////        methodVisitor.visitInsn(RETURN);
////        methodVisitor.visitMaxs(2, 0);
////        methodVisitor.visitEnd();
//        classWriter.visitEnd();
//
//        return classWriter.toByteArray();


        //bad
//        ClassWriter classWriter = new ClassWriter(0);
//        classWriter.visit(V17, ACC_PUBLIC | ACC_SUPER, "org/demon/dispatch/Dispacher", null, "java/lang/Object", null);
//        MethodVisitor methodVisitor = classWriter.visitMethod(ACC_PUBLIC + ACC_STATIC, "dispacher",  "(Lorg/demon/common/Context;)V", "(Lorg/demon/common/Context<Ljava/lang/Short;[Ljava/lang/Byte;>;)V", null);
//        methodVisitor.visitFieldInsn(GETFIELD,"java/lang/System","out","Ljava/io/PrintStream");
//        methodVisitor.visitLdcInsn("HelloWorld!");
//        methodVisitor.visitMethodInsn(INVOKESPECIAL,"java/io/PrintStream","println","(Ljava/lang/String;)V",false);
//        methodVisitor.visitInsn(RETURN);
//        methodVisitor.visitMaxs(2,1);
//        methodVisitor.visitEnd();
//        classWriter.visitEnd();
//        return classWriter.toByteArray();


        /**   ok    */
//        ClassWriter classWriter = new ClassWriter(0);
//        FieldVisitor fieldVisitor;
//        RecordComponentVisitor recordComponentVisitor;
//        MethodVisitor methodVisitor;
//        AnnotationVisitor annotationVisitor0;
//
//        classWriter.visit(V17, ACC_PUBLIC | ACC_SUPER, "org/demon/dispatch/Dispacher", null, "java/lang/Object", null);
//
//        classWriter.visitSource("Dispacher.java", null);
//
//        {
//            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
//            methodVisitor.visitCode();
//            Label label0 = new Label();
//            methodVisitor.visitLabel(label0);
//            methodVisitor.visitLineNumber(5, label0);
//            methodVisitor.visitVarInsn(ALOAD, 0);
//            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
//            methodVisitor.visitInsn(RETURN);
//            Label label1 = new Label();
//            methodVisitor.visitLabel(label1);
//            methodVisitor.visitLocalVariable("this", "Lorg/demon/dispatch/Dispacher;", null, label0, label1, 0);
//            methodVisitor.visitMaxs(1, 1);
//            methodVisitor.visitEnd();
//        }
//        {
//            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_STATIC, "dispacher", "(Lorg/demon/common/Context;)V", "(Lorg/demon/common/Context<Ljava/lang/Short;[Ljava/lang/Byte;>;)V", null);
//            methodVisitor.visitParameter("context", 0);
//            methodVisitor.visitCode();
//            Label label0 = new Label();
//            methodVisitor.visitLabel(label0);
//            methodVisitor.visitLineNumber(9, label0);
//            methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;");
//            methodVisitor.visitLdcInsn("HelloWorld!");
//            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
//            Label label1 = new Label();
//            methodVisitor.visitLabel(label1);
//            methodVisitor.visitLineNumber(11, label1);
//            methodVisitor.visitInsn(RETURN);
//            Label label2 = new Label();
//            methodVisitor.visitLabel(label2);
//            methodVisitor.visitLocalVariable("context", "Lorg/demon/common/Context;", "Lorg/demon/common/Context<Ljava/lang/Short;[Ljava/lang/Byte;>;", label0, label2, 0);
//            methodVisitor.visitMaxs(2, 1);
//            methodVisitor.visitEnd();
//        }
//        classWriter.visitEnd();
//
//        return classWriter.toByteArray();

    }
}
