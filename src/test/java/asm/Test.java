package asm;

import org.ricks.common.IoUtil;
import org.ricks.common.PathUtil;
import org.ricks.common.asm.*;

import java.io.FileOutputStream;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/9/1615:54
 */
public class Test implements Opcodes {

    public static void asm() throws Exception {
        //新建一个类生成器 Compute_frames COMPUTE_MAXS 这两个参数能让asm 自动更新操作数据
        ClassWriter classWriter = new ClassWriter(0);

        classWriter.visit (V17, ACC_PUBLIC, "app/Human", null, "java/lang/Object", null);


        MethodVisitor mv = classWriter.visitMethod (ACC_PUBLIC, "<init>", "()V", null, null);
        mv.visitCode();
        Label label0 = new Label();
        mv.visitLabel(label0);
        mv.visitLineNumber(10,label0);
        mv.visitVarInsn(ALOAD,0);
        mv.visitMethodInsn(INVOKESPECIAL,"java/lang/Object","<init>","()V",false);
        Label label1 = new Label();
        mv.visitLabel(label1);
        mv.visitLineNumber(11,label1);
        mv.visitFieldInsn(GETSTATIC,"java/lang/System","out","Ljava/io/PrintStream;");
        mv.visitLdcInsn("demon is god ......");
        mv.visitMethodInsn(INVOKEVIRTUAL,"java/io/PrintStream","println","(Ljava/lang/String;)V",false);
        Label label2 = new Label();
        mv.visitLabel(label2);
        mv.visitLineNumber(12, label2);
        mv.visitInsn(RETURN);
        Label label3 = new Label();
        mv.visitLocalVariable("this","LHuman;",null,label0,label3,0);
        mv.visitMaxs(2,1);
        mv.visitEnd ();

        //生成String 类型成员变量 private string name
        FieldVisitor fv = classWriter.visitField(ACC_PRIVATE,"name","Ljava/lang/String",null,null);
        fv.visitEnd();
        //生成long 类型成员 private long age
        fv = classWriter.visitField(ACC_PRIVATE,"age","J",null,null);
        fv.visitEnd();
        //生成int类型成员 protected int no
        fv = classWriter.visitField(ACC_PROTECTED,"no","I",null,null);
        fv.visitEnd();
        //生成静态成员 public static long score
        fv = classWriter.visitField(ACC_PUBLIC + ACC_STATIC,"score","J",null,null);
        fv.visitEnd();
        //生成常量 public static final String real_name = "demon is god"
        fv = classWriter.visitField(ACC_PUBLIC + ACC_STATIC + ACC_FINAL,"real_name","Ljava/lang/String",null,"demon is god");
        fv.visitEnd();

        classWriter.visitEnd();

        byte[] data = classWriter.toByteArray();
        /**  生成class文件 */
        String path = PathUtil.currPath() + "\\Human.class";
        IoUtil.write(new FileOutputStream(path),true,data);
        System.err.println("类生成的位置:" + path);

        /**  直接加载进jvm  **/
//        Method defineClass =  ClassLoader.class.getDeclaredMethod("defineClass", String.class,byte[].class, int.class, int.class);
//        defineClass.setAccessible(true);
//        defineClass.invoke(ClassLoader.getSystemClassLoader(),"Human",data,0,data.length);

    }
}
