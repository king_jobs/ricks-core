package asm;

import org.ricks.common.asm.Label;
import org.ricks.common.asm.MethodVisitor;
import org.ricks.common.asm.Opcodes;
import org.ricks.common.asm.Type;
import org.ricks.common.lang.Tuple;

import java.util.List;

/**
 * @author ricks
 * @Description:ASM 构建指令调用管理
 * @date 2022/9/1515:01
 */
public class DispacherTest implements Opcodes {

    public static byte[] dump(List<Tuple> tupleList) throws Exception {

        return null;
    }

    private static void  createMethodFunction(MethodVisitor methodVisitor,Tuple tuple, Label label,int lineNum,Label gotoLabel) {

        methodVisitor.visitLabel(label);
        methodVisitor.visitLineNumber(lineNum, label);
        methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
        methodVisitor.visitLdcInsn(Type.getType("L"+tuple.get(1)+";"));
        methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/ioc/ApplicationContext", "getBean", "(Ljava/lang/Class;)Ljava/lang/Object;", false);
        methodVisitor.visitTypeInsn(CHECKCAST, tuple.get(1));
        methodVisitor.visitVarInsn(ALOAD, 0);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, tuple.get(1), tuple.get(2), "(Lorg/demon/common/Context;)V", false);
        methodVisitor.visitJumpInsn(GOTO, gotoLabel);
    }
}