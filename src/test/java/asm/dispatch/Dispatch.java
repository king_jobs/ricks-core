package org.ricks.dispatch;

import org.ricks.net.Context;

/**
 * @author ricks
 * AMS 生成class二进制 直接加载进jvm 浪费时间太长了，暂时用 JavaLangAccess  来解决这个 classlaoder问题
 * 或者就直接生成class文件，这样都在同一个类加载器中加载
 * 但是工程如果打成jar包，生成的这个class 是在jar内还是 jar外。如果jar外生成还不如使用 openJdk 创建出来的 JavaLangAccess
 * @date 2022/12/210:17
 */
public abstract class Dispatch {

    public Dispatch() {}

    abstract void dispatcher(Context context);
}
