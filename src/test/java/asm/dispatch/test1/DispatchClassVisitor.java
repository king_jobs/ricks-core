//package asm.dispatch.test1;
//
//import org.ricks.asm.ClassVisitor;
//import org.ricks.asm.ClassWriter;
//import org.ricks.asm.FieldVisitor;
//import org.ricks.asm.MethodVisitor;
//
//import java.util.Locale;
//import java.util.Set;
//
//import static org.ricks.asm.Opcodes.*;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/11/2915:34
// */
//public class DispatchClassVisitor extends ClassVisitor {
//
////    private static final List<Tuple> tupleList = DispatchManager.initPairs(ApplicationContext.getApplication());
//    public DispatchClassVisitor(ClassVisitor cv) {
//        super(ASM7, cv);
//    }
//
//    @Override
//    public final void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
//        super.visit(version,access,name,signature,superName,interfaces);
//    }
//
//    @Override
//    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
//        MethodVisitor methodVisitor = super.visitMethod(access, name, desc, signature, exceptions);
//        System.err.println("访问方法的名称：" + name);
//        if("dispatcher".equals(name.trim())) return new DispatchMethodVisitor(methodVisitor); else if("<client>".equals(name.trim())) return new DispatchClinetVisitor(methodVisitor);
//        return methodVisitor;
//    }
//
//    @Override
//    public void visitEnd() {
//        super.visitEnd();
//    }
//
//    private static FieldVisitor initFieldVisitor(Set<String> classNames, ClassWriter classWriter) {
//        FieldVisitor fieldVisitor = null;
//        for (String className : classNames) {
//            String objName = className.substring(className.lastIndexOf(".") + 1,className.length()).toLowerCase(Locale.ROOT);
//            String cName = className.replace(".","/");
//            fieldVisitor = classWriter.visitField(ACC_PRIVATE | ACC_FINAL | ACC_STATIC, objName, "L"+cName+";", null, null);
//            fieldVisitor.visitEnd();
//        }
//        return fieldVisitor;
//    }
//}
