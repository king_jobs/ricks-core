package asm.dispatch.test;

/**
 * @author ricks
 * @Description: asm 生成switch（string） 直接调用方式
 * @date 2023/1/611:53
 */
public class Test1 {

    public static void main(String[] args) {
        switch ("ssss") {
            case "aaa": System.err.println("aaa");break;
            case "ssss": System.err.println("ssss"); break;
            default: System.err.println("default");
        }
    }
}
