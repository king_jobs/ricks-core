//package asm.dispatch.test;
//
//import app.controller.UserController;
//import org.ricks.asm.reflectasm.MethodAccess;
//import org.ricks.net.Context;
//import org.ricks.dispatch.Dispatcher;
//import org.ricks.ioc.AppContext;
//import org.ricks.ioc.Message;
//import org.ricks.utils.TimeUtils;
//
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//
///**
// * @author ricks
// * @Description:不同调用方式测试
// * @date 2023/1/611:01
// */
//public class Test {
//
//    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
//        long s = TimeUtils.currentTimeMillis();
//        MethodAccess access = MethodAccess.get(UserController.class);
//        UserController t = AppContext.getBean(UserController.class);
//        for (int i = 0; i < 100000000; i++) {
//            int index = access.getIndex("test", Context.class);
//            access.invoke(t,index,new Context(null,new Message(1,new byte[0])));
//        }
//        long e = TimeUtils.currentTimeMillis();
//        System.err.println("reflectasm调用1一亿次 耗时：" + (e -s));
//
//
//        long s1 = TimeUtils.currentTimeMillis();
//        for (int i = 0; i < 100000000; i++) {
//            t.test(new Context(null,new Message(1,new byte[0])));
//        }
//        long e1 = TimeUtils.currentTimeMillis();
//        System.err.println("对象直接调用1一亿次 耗时：" + (e1 -s1));
//
//
//        long s2 = TimeUtils.currentTimeMillis();
//        for (int i = 0; i < 100000000; i++) {
//            Method m = UserController.class.getMethod("test", Context.class);
//            m.invoke(t,new Context(null,new Message(1,new byte[0])));
//        }
//        long e2 = TimeUtils.currentTimeMillis();
//        System.err.println("jdk反射调用1一亿次 耗时：" + (e2 -s2));
//
//        long s3 = TimeUtils.currentTimeMillis();
//        for (int i = 0; i < 100000000; i++) {
//            Dispatcher.dispatcher(new Context(null,new Message((short)10,new byte[0])));
//        }
//        long e3 = TimeUtils.currentTimeMillis();
//        System.err.println("dispatch调用1一亿次 耗时：" + (e3 -s3));
//    }
//}
