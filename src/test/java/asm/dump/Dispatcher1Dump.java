package asm.dump;

import org.ricks.common.asm.*;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/11/3013:14
 */
public class Dispatcher1Dump implements Opcodes {

    public static byte[] dump() throws Exception {

        ClassWriter classWriter = new ClassWriter(0);
        FieldVisitor fieldVisitor;
        RecordComponentVisitor recordComponentVisitor;
        MethodVisitor methodVisitor;
        AnnotationVisitor annotationVisitor0;

        classWriter.visit(V17, ACC_PUBLIC | ACC_SUPER, "app/Dispatcher1", null, "java/lang/Object", null);

        classWriter.visitSource("Dispatcher1.java", null);

        classWriter.visitInnerClass("java/lang/invoke/MethodHandles$Lookup", "java/lang/invoke/MethodHandles", "Lookup", ACC_PUBLIC | ACC_FINAL | ACC_STATIC);

            fieldVisitor = classWriter.visitField(ACC_PRIVATE | ACC_FINAL | ACC_STATIC, "userController", "Lapp/controller/UserController;", null, null);
            fieldVisitor.visitEnd();

            fieldVisitor = classWriter.visitField(ACC_PRIVATE | ACC_FINAL | ACC_STATIC, "testController", "Lapp/controller/TestController;", null, null);
            fieldVisitor.visitEnd();

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(17, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            methodVisitor.visitInsn(RETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "Lapp/Dispatcher1;", null, label0, label1, 0);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_STATIC, "dispatcher", "(Lorg/demon/common/Context;)V", "(Lorg/demon/common/Context<Ljava/lang/Short;[Ljava/lang/Byte;>;)V", null);
            methodVisitor.visitParameter("context", 0);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(23, label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "org/ricks/common/Context", "getCmd", "()Ljava/lang/Object;", false);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/Short");
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Short", "shortValue", "()S", false);

            methodVisitor.visitVarInsn(ISTORE, 1);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(24, label1);
            methodVisitor.visitVarInsn(ILOAD, 1);
            Label label2 = new Label();
            Label label3 = new Label();
            Label label4 = new Label();
            Label label5 = new Label();
            Label label6 = new Label();
            Label label7 = new Label();
            Label label8 = new Label();
            Label label9 = new Label();
            Label label10 = new Label();
            Label label11 = new Label();
            Label label12 = new Label();
            methodVisitor.visitLookupSwitchInsn(label12, new int[]{1, 2, 3, 4, 10, 11, 12, 13, 100, 101}, new Label[]{label2, label3, label4, label5, label6, label7, label8, label9, label10, label11});
            methodVisitor.visitLabel(label6);
            methodVisitor.visitLineNumber(25, label6);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{Opcodes.INTEGER}, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "userController", "Lapp/controller/UserController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/UserController", "test", "(Lorg/demon/common/Context;)V", false);
            Label label13 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label9);
            methodVisitor.visitLineNumber(26, label9);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "userController", "Lapp/controller/UserController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/UserController", "demon", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label8);
            methodVisitor.visitLineNumber(27, label8);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "userController", "Lapp/controller/UserController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/UserController", "tesGachaDb", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label7);
            methodVisitor.visitLineNumber(28, label7);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "userController", "Lapp/controller/UserController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/UserController", "testUserDb", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLineNumber(29, label2);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "testController", "Lapp/controller/TestController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/TestController", "test", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label5);
            methodVisitor.visitLineNumber(30, label5);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "testController", "Lapp/controller/TestController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/TestController", "demon", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label4);
            methodVisitor.visitLineNumber(31, label4);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "testController", "Lapp/controller/TestController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/TestController", "tesGachaDb", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label10);
            methodVisitor.visitLineNumber(32, label10);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "testController", "Lapp/controller/TestController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/TestController", "test00", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label11);
            methodVisitor.visitLineNumber(33, label11);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "testController", "Lapp/controller/TestController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/TestController", "test001", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label3);
            methodVisitor.visitLineNumber(34, label3);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "app/Dispatcher1", "testController", "Lapp/controller/TestController;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "app/controller/TestController", "testUserDb", "(Lorg/demon/common/Context;)V", false);
            methodVisitor.visitJumpInsn(GOTO, label13);
            methodVisitor.visitLabel(label12);
            methodVisitor.visitLineNumber(35, label12);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "err", "Ljava/io/PrintStream;");
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "org/ricks/common/Context", "getCmd", "()Ljava/lang/Object;", false);
            methodVisitor.visitInvokeDynamicInsn("makeConcatWithConstants", "(Ljava/lang/Object;)Ljava/lang/String;", new Handle(Opcodes.H_INVOKESTATIC, "java/lang/invoke/StringConcatFactory", "makeConcatWithConstants", "(Ljava/lang/invoke/MethodHandles$Lookup;Ljava/lang/String;Ljava/lang/invoke/MethodType;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/invoke/CallSite;", false), new Object[]{"\u0001 \u672a\u77e5\u6307\u4ee4"});
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
            methodVisitor.visitLabel(label13);
            methodVisitor.visitLineNumber(39, label13);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
            methodVisitor.visitInsn(RETURN);
            Label label14 = new Label();
            methodVisitor.visitLabel(label14);
            methodVisitor.visitLocalVariable("context", "Lorg/demon/common/Context;", "Lorg/demon/common/Context<Ljava/lang/Short;[Ljava/lang/Byte;>;", label0, label14, 0);
            methodVisitor.visitLocalVariable("cmd", "S", null, label1, label14, 1);
            methodVisitor.visitMaxs(2, 2);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_STATIC, "<clinit>", "()V", null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(19, label0);
            methodVisitor.visitLdcInsn(Type.getType("Lapp/controller/UserController;"));
            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/ioc/ApplicationContext", "getBean", "(Ljava/lang/Class;)Ljava/lang/Object;", false);
            methodVisitor.visitTypeInsn(CHECKCAST, "app/controller/UserController");
            methodVisitor.visitFieldInsn(PUTSTATIC, "app/Dispatcher1", "userController", "Lapp/controller/UserController;");
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(20, label1);
            methodVisitor.visitLdcInsn(Type.getType("Lapp/controller/TestController;"));
            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/ioc/ApplicationContext", "getBean", "(Ljava/lang/Class;)Ljava/lang/Object;", false);
            methodVisitor.visitTypeInsn(CHECKCAST, "app/controller/TestController");
            methodVisitor.visitFieldInsn(PUTSTATIC, "app/Dispatcher1", "testController", "Lapp/controller/TestController;");
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitMaxs(1, 0);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        return classWriter.toByteArray();
    }
}
