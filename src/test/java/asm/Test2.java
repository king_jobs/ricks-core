//package asm;
//
//import app.controller.TestController;
//import org.ricks.net.Context;
//import org.ricks.lang.Tuple;
//import org.ricks.ioc.App;
//import org.ricks.ioc.Bean;
//import org.ricks.net.ActionMethod;
//import org.ricks.ioc.bean.factory.AbstractBeanFactory;
//import org.ricks.lang.Logger;
//import org.ricks.utils.FileUtil;
//
//import java.lang.reflect.Method;
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/11/2813:33
// */
//public class Test2 {
//
//    private static byte[] data;
//
//    public static void init(App context) {
//        try {
////            CreateDispatchJava.createJava(initPairs(context));
//
////            bytes = DispacherDump.dump(initPairs(context));
////            DispatchManager loader = new DispatchManager();
////            System.err.println("指令生成 byte length:" + bytes.length);
////            Class clazz = ClassUtil.loadClass("org.demon.dispatch.Dispatcher");
////            Dispacher.dispacher(new Context<>(null,null));
////            String path = PathUtil.currPath() + "\\Dispatcher.class";
////            IoUtil.write(new FileOutputStream(path),true,bytes);
////            System.err.println("类生成的位置:" + path);
//
//
//            String dispatch_path = FileUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "org/ricks/dispatch/Dispatcher.class";
//            System.err.println(dispatch_path);
////            InputStream is = new FileInputStream(new File(dispatch_path));
////            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
////            byte[] bytes = new byte[1024];
////            int temp;
////            while ((temp = is.read(bytes)) != -1) {
////                outputStream.write(bytes, 0, temp);
////            }
////            //转换后的byte[]
////            byte[] finalBytes = outputStream.toByteArray();
//
//
////            ClassWriter cw = DispacherDump.dump(initPairs(context));
////            byte[] data =  cw.toByteArray();
////            ClassReader cr = new ClassReader(Dispatcher.class.getName());
////            //（4）结合ClassReader和ClassVisitor
////            int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
////            cr.accept(cw, parsingOptions);
////            FileOutputStream fos = new FileOutputStream(dispatch_path);
////            fos.write(data);
////            ClassLoad classLoad = new ClassLoad();
////            classLoad.defineClassToJvm("org.demon.dispatch.Dispatcher",data,0,data.length);
////            AppMain.class.getClassLoader().loadClass("org.demon.dispatch.Dispatcher");
//            System.err.println(Dispatcher.class.getClassLoader());
//            System.err.println(TestController.class.getClassLoader());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static byte[] getData() {
//        return data;
//    }
//
//    private static List<Tuple> initPairs(App context) {
//        List<Tuple> pairs = new ArrayList<>();
//        AbstractBeanFactory beanFactory = context.getBeanFactory();
//        if(beanFactory != null) {
//            List<Object> beans = beanFactory.getBeansForAnnotation(Bean.class);
//            if (!beans.isEmpty()) {
//                for (Object obj : beans) {
//                    try {
//                        Method[] methods = obj.getClass().getMethods();
//                        for (Method method : methods) {
//                            ActionMethod messageHandler = method.getAnnotation(ActionMethod.class);
//                            if (messageHandler == null) {
//                                continue;
//                            }
//                            Class<?>[] parameterClazzes = method.getParameterTypes();
//                            if (parameterClazzes.length != 1) {
//                                throw new IllegalArgumentException("消息处理方法的参数不正确，参数必须是一个。");
//                            }
//                            Class<?> commandClass = parameterClazzes[0];
//                            if (!Context.class.isAssignableFrom(commandClass)) {
//                                continue;
//                            }
//                            pairs.add(new Tuple(messageHandler.messageId(),getClassName(obj).replace("/","."),method.getName()));
//                        }
//                    } catch (Exception e) {
//                        Logger.error("", e);
//                    }
//                }
//            }
//        }
//        return pairs;
//    }
//
//    private static String getClassName(Object obj) {
//        String beanName = obj.getClass().getName();
//        return beanName.replace(".","/");
//    }
//
//
//    private static String getMessageHandlerName(String beanName, String methodName){
//        String name = beanName.replace(".","_");
//        StringBuilder builder = new StringBuilder(name);
//        builder.append("_");
//        builder.append(methodName);
//        return builder.toString().toUpperCase();
//    }
//
//}
