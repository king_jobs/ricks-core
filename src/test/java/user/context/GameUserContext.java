//package user.context;
//
//
//import inutan.data.Tables;
//import org.ricks.lang.Assert;
//
///**
// * 玩家上下文容器
// * 本来计划策划数据类似OrmContext 上下文方式取数据
// * OrmContext.me().get()
// * 但是luban excel是可以配置多个key联合引用index ,就意味数据不是唯一性
// * 所以luban生成得数据是没办法装载进 StorageContext 进行，那就使用第二种方式 用户上下文得方式进行 业务获取策划数据
// */
//public final class GameUserContext extends UserContext {
//
//    private static Tables tables;
//
//    /**
//     * 策划数据装载进 用户上下文中
//     * 容器启动，data热更，都需要调用 GameUserContext。loadTables(Tables) 策划数据装载进用户上下文中，以便业务使用
//     * @param tables
//     */
//    public static void loadTables(Tables tables) {
//        GameUserContext.tables = tables;
//    }
//
//    @Override
//    public Tables tables() {
//        Assert.notNull(tables,"警告容器没有装载进策划配置数据,");
//        return tables;
//    }
//}
