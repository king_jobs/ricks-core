package chatgpt.gossip;

import java.util.List;
import java.util.Random;

public class Test {

    public static void main(String[] args) {
        // 创建5个节点
        GossipProtocol node1 = new GossipProtocol(1);
        GossipProtocol node2 = new GossipProtocol(2);
        GossipProtocol node3 = new GossipProtocol(3);
        GossipProtocol node4 = new GossipProtocol(4);
        GossipProtocol node5 = new GossipProtocol(5);

        // 将节点连接成一个环形拓扑结构
        node1.addNeighbor(node2);
        node2.addNeighbor(node1);
        node2.addNeighbor(node3);
        node3.addNeighbor(node2);
        node3.addNeighbor(node4);
        node4.addNeighbor(node3);
        node4.addNeighbor(node5);
        node5.addNeighbor(node4);
        node5.addNeighbor(node1);
        node1.addNeighbor(node5);

        // 初始化每个节点的状态
        node1.receiveState(100, 1);
        node2.receiveState(200, 1);
        node3.receiveState(300, 1);
        node4.receiveState(400, 1);
        node5.receiveState(500, 1);

        // 随机选取一个节点开始 gossip 协议
        Random random = new Random();
        GossipProtocol starter = List.of(node1, node2, node3, node4, node5).get(random.nextInt(5));
        starter.gossip();

        // 输出每个节点的状态信息
        System.out.println("Node 1: " + node1.getState());
        System.out.println("Node 2: " + node2.getState());
        System.out.println("Node 3: " + node3.getState());
        System.out.println("Node 4: " + node4.getState());
        System.out.println("Node 5: " + node5.getState());
    }
}
