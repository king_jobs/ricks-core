package chatgpt.gossip;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GossipProtocol {

    private int id; // 节点编号
    private int version; // 状态版本号
    private int state; // 节点状态

    private List<GossipProtocol> neighbors; // 邻居节点列表

    public GossipProtocol(int id) {
        this.id = id;
        this.version = 1;
        this.state = 0;
        this.neighbors = new ArrayList<>();
    }

    public void addNeighbor(GossipProtocol neighbor) {
        this.neighbors.add(neighbor);
    }

    public void gossip() {
        Random random = new Random();
        GossipProtocol neighbor = neighbors.get(random.nextInt(neighbors.size()));

        // 向随机邻居请求状态信息
        int neighborState = neighbor.getState();
        int neighborVersion = neighbor.getVersion();

        // 如果邻居节点的状态版本号比当前节点的版本号更高，则更新当前节点的状态
        if (neighborVersion > this.version) {
            this.state = neighborState;
            this.version = neighborVersion + 1;

            // 广播新状态到所有邻居节点并重复执行 gossip()
            broadcast();
            gossip();
        }
    }

    public void broadcast() {
        for (GossipProtocol neighbor : neighbors) {
            neighbor.receiveState(this.state, this.version);
        }
    }

    public void receiveState(int state, int version) {
        // 如果接收到的状态版本号比当前节点的版本号更高，则更新当前节点的状态
        if (version > this.version) {
            this.state = state;
            this.version = version + 1;

            // 广播新状态到所有邻居节点并重复执行 gossip()
            broadcast();
            gossip();
        }
    }

    public int getId() {
        return id;
    }

    public int getVersion() {
        return version;
    }

    public int getState() {
        return state;
    }
}
