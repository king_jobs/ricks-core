package app.role;

import app.dao.RoleRepository;

import java.util.List;

public abstract class UserContext {

    long userId;

    abstract RoleInfo roleInfo();

    abstract List<MailInfo> mailInfo();

    void login() {
        Thread.ofVirtual().name("login",userId).start(() -> init());
    }

    private void init() {
        roleInfo();
        mailInfo();
    }
}

