package app.role;

import app.dao.MailRepository;
import app.dao.RoleRepository;
import org.ricks.ioc.Autowired;

public class RoleEntity implements Role{

    @Autowired
    public RoleRepository roleRepository;

    @Autowired
    private MailRepository mailRepository;

    @Override
    public RoleInfo roleInfo(long id) {
        return roleRepository.load(id);
    }

    @Override
    public MailInfo mailInfo(long roleId) {
        return mailRepository.load(roleId);
    }


}
