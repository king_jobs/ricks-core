package app.role;

import app.dao.MailRepository;
import app.dao.RoleRepository;
import org.ricks.ioc.IocHolder;
import org.ricks.orm.OrmContext;

import java.util.List;

public final class GameUserContext extends UserContext {

    private static Tables tables;

    /**
     * 策划数据装载进 用户上下文中
     * @param tables
     */
    public static void loadTables(Tables tables) {
        GameUserContext.tables = tables;
    }

    @Override
    RoleInfo roleInfo() {
        return OrmContext.me().uniqueCacheLoad(RoleInfo.class,userId).get();
//        RoleRepository repository = IocHolder.getBean(RoleRepository.class);
//        return repository.load(userId);
    }

    @Override
    List<MailInfo> mailInfo() {
        return OrmContext.me().multiCacheLoad(MailInfo.class,userId);
//        MailRepository repository = IocHolder.getBean(MailRepository.class);
//        return repository.load(userId);
    }

}
