package app.role;

public interface Role {

    RoleInfo roleInfo(long roleId);

    MailInfo mailInfo(long roleId);
}
