package app;

import org.ricks.agent.JavaAgent;
import org.ricks.common.event.EventBus;
import org.ricks.ioc.DefaultBootstrap;
import org.ricks.common.lang.StopWatch;
import org.ricks.common.FileUtil;
import org.ricks.common.watch.DelayWatcher;
import org.ricks.common.watch.WatchMonitor;

/**
 * @author ricks
 * @Description: 注解方式启动server 弊端 只能启动一个服务，但是项目需求可能启动多个server
 * 2022年12月2日 18:37:31 后面找时间这部分进行优化
 * @date 2020/7/914:12
 */
public class AppContext extends DefaultBootstrap {

    private static final AppContext INSTANCE = new AppContext();
    private AppContext(){}
    public static AppContext me() {
        return INSTANCE;
    }


    /**
     * 仅仅是简单的容器启动, server如果需要启动则写代码
     * 启动容器，默认初始化分发器
     * @return
     */
    @Override
    protected void onStart() {
        StopWatch stopWatch = StopWatch.create("app-start");
        stopWatch.start();
        try {
//            initWatcherModular(ModularContext.me().projectClasses()); //初始化热更机制
//            DispatchManager.init(); //初始化网络消息分发器
            EventBus.me().init();//初始化事件
            DEFAULT_BANNER.printBanner(); //初始化banner
        } finally {
            stopWatch.stop();
            System.out.println("============ app started on time:"+ stopWatch.getTotalTimeMillis() + " ms ===========");
        }
    }

    @Override
    protected void onStop() {
        EventBus.me().destroy();
    }

    private static void initWatcherModular(Class clazz) {
        JavaAgent javaAgent = new JavaAgent();
        FileUtil.del("ricks/class"); //启动先删除目录下所有文件夹 文件。
        WatchMonitor monitor = WatchMonitor.createAll("ricks/class", new DelayWatcher(javaAgent, 500));
        monitor.start();
//            FileWatcher watcher = new FileWatcher("",newInstance);
//            watcher.start();
    }
}
