package app.config;

//import app.utils.CacheExpirationListener;
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
//import org.ricks.db.ConnectionSourceHelper;
//import org.ricks.db.DataAccessor;
//import org.ricks.db.MysqlDataAccessor;
//import org.ricks.common.conf.RicksConf;
//import org.ricks.db.write.AsyncWriteMgr;
//import org.ricks.db.write.AsyncWriteService;
import app.utils.CacheExpirationListener;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.ricks.common.conf.RicksConf;
import org.ricks.ioc.Bean;
import org.ricks.ioc.Configuration;
import org.ricks.ioc.Value;
import org.ricks.common.lang.ExpirationListener;
import org.ricks.net.IConsumer;
import org.ricks.net.IRouter;
import org.ricks.net.Router;
import org.ricks.net.rpc.Consumer;
import org.ricks.net.rpc.ConsumerConfig;
import org.ricks.net.rpc.ConsumerModule;
import org.ricks.net.rpc.ProtocolModule;
import org.ricks.orm.ConnectionSourceHelper;
import org.ricks.orm.DataAccessor;
import org.ricks.orm.MysqlDataAccessor;

import javax.sql.DataSource;
import java.util.List;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/12/714:33
 */
@Configuration
public class AppConfig {

    @Value("jdbc.url")
    public String url;

    @Bean("dataAccessor")
    public DataAccessor initDataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(RicksConf.get("jdbc.url"));//mysql
        hikariConfig.setDriverClassName(RicksConf.get("jdbc.driverClassName"));
        hikariConfig.setUsername(RicksConf.get("jdbc.username"));
        hikariConfig.setPassword(RicksConf.get("jdbc.password"));
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
//            hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setIdleTimeout(5000);
        hikariConfig.setMaximumPoolSize(20);
        hikariConfig.setKeepaliveTime(5000);
        hikariConfig.setValidationTimeout(3000);
        hikariConfig.setMaxLifetime(1000);
        HikariDataSource masterDataSource = new HikariDataSource(hikariConfig);
        return new MysqlDataAccessor(ConnectionSourceHelper.getSingle(masterDataSource));
    }

    private DataSource createData(String url) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(url);//mysql
        hikariConfig.setDriverClassName(RicksConf.get("jdbc.driverClassName"));
        hikariConfig.setUsername(RicksConf.get("jdbc.username"));
        hikariConfig.setPassword(RicksConf.get("jdbc.password"));
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
//            hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setIdleTimeout(5000);
        hikariConfig.setMaximumPoolSize(20);
        hikariConfig.setKeepaliveTime(5000);
        hikariConfig.setValidationTimeout(3000);
        hikariConfig.setMaxLifetime(1000);
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public ExpirationListener initCacheExpirationListener() {
        return new CacheExpirationListener();
    }

    @Bean
    public IRouter initRouter() {
        return new Router();
    }


    @Bean
    public IConsumer initConsumer() {
        return new Consumer();
    }

    @Bean
    public ConsumerConfig initConsumerConfig() {
        // 定义2个模块：可以为服务提供者用，也可以为服务消费者用，这个仅仅是模块信息
        var protocolModule1 = new ProtocolModule((byte) 1, "game");
        var protocolModule2 = new ProtocolModule((byte) 2, "bbb");

        // 服务消费者模块和服务消费者配置(服务消费者模块多一个负载均衡属性)
        var consumerModules = List.of(new ConsumerModule(protocolModule1, "random", "a"), new ConsumerModule(protocolModule2, "random", "b"));
        // 服务消费者配置：这个是没Ip的
        var consumerConfig = ConsumerConfig.valueOf(consumerModules);
        return consumerConfig;
    }
}
