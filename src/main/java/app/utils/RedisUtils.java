//package app.utils;
//
//import org.ricks.common.conf.RicksConf;
//import redis.clients.jedis.*;
//import java.util.Set;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2020/8/1019:58
// */
//public class RedisUtils {
//
//    private static JedisPool pool;
//
//    static {
//        //设置配置信息
//        JedisPoolConfig jedisPool = new JedisPoolConfig();
//        jedisPool.setMaxTotal(RicksConf.toInt("redis.maxTotal"));
//        jedisPool.setMaxIdle(RicksConf.toInt("redis.minIdle"));
//        jedisPool.setMaxWaitMillis(RicksConf.toInt("redis.maxWaitMillis"));
//        jedisPool.setMinIdle(RicksConf.toInt("redis.minIdle"));
//        jedisPool.setTestOnBorrow(RicksConf.toBool("redis.testOnBorrow"));
//        jedisPool.setTestOnReturn(RicksConf.toBool("redis.testOnReturn"));
//        //根据配置信息创建Jedis连接连接池
//        pool = new JedisPool(jedisPool, RicksConf.get("redis.host"), RicksConf.toInt("redis.port"), RicksConf.toInt("redis.timeout"),RicksConf.get("redis.password"),RicksConf.toInt("redis.db"));
//    }
//
//    public static Jedis getJedis(){
//        return pool.getResource();
//    }
//
//    /**
//     * 返回hash中指定存储位置的值
//     * @param  key
//     * @return 存储对应的值
//     * */
//    public static String get(String key) {
//        Jedis sjedis = getJedis();
//        try {
//            String s = sjedis.get(key);
//            return s;
//        }finally {
//            sjedis.close();
//        }
//    }
//
//    public static byte[] get(byte[] key) {
//        Jedis sjedis = getJedis();
//        try {
//            byte[] s = sjedis.get(key);
//            return s;
//        } finally {
//            sjedis.close();
//        }
//    }
//
//    /**
//     * 返回hash中指定存储位置的值
//     * @param  key
//     * @return 存储对应的值
//     * */
//    public static String set(String key,String value) {
//        Jedis sjedis = getJedis();
//        try {
//            String s = sjedis.set(key,value);
//            return s;
//        }finally {
//            sjedis.close();
//        }
//    }
//
//    public static void expire(String key,int s) {
//        Jedis sjedis = getJedis();
//        try {
//           sjedis.expire(key,s);
//        }finally {
//            sjedis.close();
//        }
//    }
//
//    public static void p(String key) {
//        Jedis sjedis = getJedis();
//        try {
//            sjedis.persist(key);
//        }finally {
//            sjedis.close();
//        }
//    }
//
//    /**
//     * 返回hash中指定存储位置的值
//     * @param  key
//     * @param  fieid 存储的名字
//     * @return 存储对应的值
//     * */
//    public static String hget(String key, String fieid) {
//        Jedis sjedis = getJedis();
//        try {
//            String s = sjedis.hget(key, fieid);
//            return s;
//        }finally {
//            sjedis.close();
//        }
//    }
//
//     public static byte[] hget(byte[] key, byte[] fieid) {
//        Jedis sjedis = getJedis();
//        try {
//            byte[] s = sjedis.hget(key, fieid);
//            return s;
//        } finally {
//            sjedis.close();
//        }
//    }
//
//    public static long hset(byte[] key, byte[] fieid, byte[] value) {
//        Jedis sjedis = getJedis();
//        try {
//            long s = sjedis.hset(key, fieid,value);
//            return s;
//        } finally {
//            sjedis.close();
//        }
//    }
//
//
//    /**
//     * 返回指定hash中的所有存储名字,类似Map中的keySet方法
//     *
//     * @param key
//     * @return Set<String> 存储名称的集合
//     * */
//    public static Set<String> hkeys(String key) {
//        Jedis sjedis = getJedis();
//        try {
//            Set<String> set = sjedis.hkeys(key);
//            return set;
//        }finally {
//            sjedis.close();
//        }
//    }
//
//    public static Set<byte[]> hkeys(byte[] key) {
//        Jedis sjedis = getJedis();
//        try {
//            Set<byte[]> set = sjedis.hkeys(key);
//            return set;
//        }finally {
//            sjedis.close();
//        }
//    }
//
//    public static void subscribe(final BinaryJedisPubSub subscriber, final byte[]... channels) {
//        Jedis sjedis = getJedis();
//        try {
//            sjedis.subscribe(subscriber, channels);
//        }finally {
//            sjedis.close();
//        }
//    }
//
//    public static void subscribe(final JedisPubSub subscriber, final String... channels) {
//        Jedis sjedis = getJedis();
//        try {
//            sjedis.subscribe(subscriber, channels);
//        }finally {
//            sjedis.close();
//        }
//    }
//}
//
