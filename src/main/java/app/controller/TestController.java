//package app.controller;
//
//import app.command.EnumCall;
//import app.dao.UserRepository;
//import app.data.codec.message.FlowContext;
//import app.vo.GachaVo;
//import app.vo.User;
//import org.ricks.utils.actor.Actor;
//import org.ricks.ioc.Autowired;
//import org.ricks.lang.Logger;
//import org.ricks.net.*;
//import org.ricks.ioc.Bean;
//import org.ricks.utils.RandomUtil;
//import org.ricks.utils.TimeUtils;
//
//import java.nio.ByteBuffer;
//import java.util.concurrent.atomic.AtomicBoolean;
//import java.util.concurrent.atomic.AtomicLong;
//
///**
// * @author ：demon-chen
// * @date ：Created in 2019/12/18 下午3:16
// * @description：
// * @modified By：
// * @version: $
// */
//@Bean
//public class TestController {
//
////    public static final TestController testController = new TestController();
//
//    private static final AtomicLong a = new AtomicLong();
//
//    @Autowired
//    private UserRepository userDao;
//
//
//     private AtomicLong count = new AtomicLong(0);
//     private volatile long startTime =  0;
//     private AtomicBoolean start = new AtomicBoolean(true);
////    @HttpMapping(value = "test")
////    public void test(HttpResponse response){
////        Logger.info(" test.......");
////        response.redirect("www.baidu.com");
////    }
//
//    @ActionMethod(messageId = 1)
//    public void test(Context context) {
////        System.err.println("进入  test  ,,,,,,,,1000......");
//        long num = count.incrementAndGet();
////        CardVo o = DataCodecKit.decode((byte[]) context.getMessage(),CardVo.class);
////        System.err.println("进入  test  ,,,,,,,,"+ o.getCardId()+"......" + o.getCardName() + "  a" + o.isMe());
////        if(num == 1) {
////            startTime = TimeUtils.currentTimeMillis();
////            Logger.info("jdk aio  test....... time:{}",startTime);
////        }
////        if(TimeUtils.currentTimeMillis() - startTime > 1000  && start.get()) {
////            start.set(false);
////            Logger.info("1秒承载  count:{}",count);
////        }
////        if(num == 1000000) {
////            Logger.info("jdk aio  test....... time:{}",TimeUtils.currentTimeMillis());
////        }
////        CardData cardData = Optional.of(DataContext.getCard(10010000)).orElse(new CardData());
////        System.err.println(" cardId: " + cardData.getCardId() + " and AdvanceID: "+ cardData.getAdvanceID() + " and AdvanceValue: " + cardData.getAdvanceValue() );
////        context.send();
////        KcpSeesion kcpSeesion = (KcpSeesion)context.getSession();
////        System.err.println("conv:" + kcpSeesion.getKcp().getConv() + " and 请求次数 num:" + num);
////        System.err.println("进入请求，。。。。。");
////        context.send();
////        userDao.cacheLoadAll(user -> user.getRoleId());
//
//    }
//
//    @ActionMethod(messageId = 100)
//    public void test00(Context context) {
//        System.err.println("进入cmd = 100  test  ,,,,,,,,,");
//        long num = count.incrementAndGet();
////        if(num == 1) {
////            startTime = TimeUtils.currentTimeMillis();
////            Logger.info("jdk aio  test....... time:{}",startTime);
////        }
////        if(TimeUtils.currentTimeMillis() - startTime > 1000  && start.get()) {
////            start.set(false);
////            Logger.info("1秒承载  count:{}",count);
////        }
////        if(num == 1000000) {
////            Logger.info("jdk aio  test....... time:{}",TimeUtils.currentTimeMillis());
////        }
////        CardData cardData = Optional.of(DataContext.getCard(10010000)).orElse(new CardData());
////        System.err.println(" cardId: " + cardData.getCardId() + " and AdvanceID: "+ cardData.getAdvanceID() + " and AdvanceValue: " + cardData.getAdvanceValue() );
////        context.send();
////        KcpSeesion kcpSeesion = (KcpSeesion)context.getSession();
////        System.err.println("conv:" + kcpSeesion.getKcp().getConv() + " and 请求次数 num:" + num);
////        context.send();
//
////        context.send(RoleMessage.createRoleInfoMsg());
////        context.send(CommonMessage.getSimpleBoolValue());
//    }
//
//    @ActionMethod(messageId = 900)
//    public void test001(Context context) {
////        System.err.println("进入cmd = 101  test  ,,,,,,,,,");
////        long num = count.incrementAndGet();
////        if(num == 1) {
////            startTime = TimeUtils.currentTimeMillis();
////            Logger.info("jdk aio  test....... time:{}",startTime);
////        }
////        if(TimeUtils.currentTimeMillis() - startTime > 1000  && start.get()) {
////            start.set(false);
////            Logger.info("1秒承载  count:{}",count);
////        }
////        if(num == 1000000) {
////            Logger.info("jdk aio  test....... time:{}",TimeUtils.currentTimeMillis());
////        }
////        CardData cardData = Optional.of(DataContext.getCard(10010000)).orElse(new CardData());
////        System.err.println(" cardId: " + cardData.getCardId() + " and AdvanceID: "+ cardData.getAdvanceID() + " and AdvanceValue: " + cardData.getAdvanceValue() );
////        context.send();
////        KcpSeesion kcpSeesion = (KcpSeesion)context.getSession();
////        System.err.println("conv:" + kcpSeesion.getKcp().getConv() + " and 请求次数 num:" + num);
//    }
//
//    @ActionMethod(messageId = EnumCall.APP_CONTROLLER_TESTCONTROLLER_TESTUSERDB2)
//    public void testUserDb(Context context) {
//        long num = count.incrementAndGet();
//        Logger.info("开始保存数据。。。。。。。。" + num);
//        User user = new User();
//        user.setId(3312034310411780130L);
//        user.setAge(18);
//        user.setBindDiamond(10000);
//        user.setCookingExp(0);
//        user.setCookingLv(1);
//        user.setDuration(1);
//        user.setCreateTime(TimeUtils.currentTimeMillis());
//        user.setFinalCityId(1000);
//        user.setGold(1000);
//        user.setGreetings("你好，demon !");
//        user.setHeadIcon(10000);
//        user.setLv(1);
//        user.setOpenId("demon");
//        user.setRoleId(RandomUtil.randomLong());
//        user.setRoleName("demon_is_god");
//        user.setSendGift(111);
//        userDao.update(user);
////        context.send();
//    }
//
//
//    @ActionMethod(messageId = 3)
//    public void tesGachaDb(Context context) {
//        GachaVo gachaVo = new GachaVo();
//        gachaVo.setGachaId(18);
//        gachaVo.setGachaNum(10000);
//        gachaVo.setGachaName("新手卡池");
//        gachaVo.setGachaType(1);
//        gachaVo.setRoleId(RandomUtil.randomLong());
////        EntityMgr.save(gachaVo);
////        context.send();
//    }
//
//    @ActionMethod(messageId = 4)
//    public void demon(Context context) {
//        GachaVo gachaVo = new GachaVo();
//        gachaVo.setGachaId(18);
//        gachaVo.setGachaNum(10000);
//        gachaVo.setGachaName("新手卡池");
//        gachaVo.setGachaType(1);
//        gachaVo.setRoleId(RandomUtil.randomLong());
////        EntityMgr.save(gachaVo);
////        context.send();
//    }
//
//
////    @HttpMapping(value = "a")
////    public void a(HttpResponse response){
////        Logger.info(" test.......");
////        response.sendHtml("https://www.baidu.com");
////    }
//
//    @ActionMethod(messageId = 10001)
//    public void enterRoom(FlowContext context) {
//        Logger.info(context.getSession()+" enter room  packId"+context.getPackageId()+".................................  count:" + a.incrementAndGet());
//
//        writeAndFlush((KcpSession) context.getSession(),context.getPackageId());
//    }
//
//
//    public void writeAndFlush(KcpSession kcpSession,int packId) {
//
////        Actors.me().getKcpActor(kcpSession.getSessionID()).execute("kcp",() -> {
//            int headLength = 2 + 4 + 2;
//            //游戏默认是小端
//            ByteBuffer byteBuffer = ByteBuffer.allocate(headLength);
//            byteBuffer.putShort((short) 10001);
//            byteBuffer.putInt(packId);
//            byteBuffer.putShort((short) 0);
//            byteBuffer.put(new byte[0]);
//            byteBuffer.flip();//重置 limit 和postion 值 否则 buffer 读取数据不对
//            byte[] bytes = new byte[headLength];
//            byteBuffer.get(bytes);
////            System.err.println("发送 字节长度：" + bytes.length + " & " + bytes[0] + "," + bytes[1]);
//        Ukcp kcp = kcpSession.getUkcp();
//        Actor actor = kcp.getActor();
//        actor.execute("write",() -> {
//            int code;
//            try {
//                code = kcp.send(bytes);
//            } catch (Exception e) {
//                code = -9;
//            }
//            if (code != 0) kcpSession.close(true);
//        });
//
////            if(code == 0) kcp.update(TimeUtils.currentTimeMillis()); else kcpSession.close(true);
////        });
//    }
//}
