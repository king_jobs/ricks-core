package app.controller;

import org.ricks.net.Context;
import org.ricks.ioc.Bean;
import org.ricks.net.ActionMethod;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/12/1618:26
 */
@Bean
public class CommonAction {

    @ActionMethod(messageId = 901)
    public void getConv(Context context) {
        context.getSession().close(true);
    }
}
