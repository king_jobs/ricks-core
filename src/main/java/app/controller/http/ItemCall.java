package app.controller.http;

import app.vo.Card;

import java.util.Set;
import java.util.stream.IntStream;

@FunctionalInterface
public interface ItemCall {

    int itemCall(Set<Card> cards);

//    default int call(Set<Card> cards) {
//        return cards.stream().flatMapToInt(card -> IntStream.of(card.getCardId())).sum();
//    }
}
