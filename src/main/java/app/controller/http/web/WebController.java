package app.controller.http.web;

import org.ricks.ioc.Autowired;
import org.ricks.common.lang.Logger;
import org.ricks.net.http.Controller;
import org.ricks.net.http.HttpResponse;
import org.ricks.net.http.RequestMapping;
import org.ricks.net.http.web.HtmlConst;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ricks
 * @Description:
 * @date 2022/12/511:18
 */
@Controller("w")
public class WebController {

    @Autowired("dataSource")
    private DataSource hikariDataSource;

    @RequestMapping(value = "test")
    public String test(HttpResponse response) {
        Logger.info("11111111111111" );
        return HtmlConst.HEADER;
    }


    @RequestMapping(value = "test1")
    public void test1(HttpResponse response) {
        Map m = new HashMap();
        m.put("test","ricks");
        m.put("ricks","ricks is god!");
//        response.to("index.html",m);
    }

}
