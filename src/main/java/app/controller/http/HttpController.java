package app.controller.http;

import app.dao.UserRepository;
import app.http.vo.UserVo;
import app.vo.Card;
import app.vo.User;
import org.ricks.common.lang.Assert;
import org.ricks.net.http.Param;
import org.ricks.orm.OrmContext;
import org.ricks.ioc.Autowired;
import org.ricks.common.lang.ExpirationListener;
import org.ricks.common.lang.Logger;
import org.ricks.net.http.Controller;
import org.ricks.net.http.RequestMapping;
import org.ricks.common.MapUtils;
import org.ricks.common.TimeUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/12/511:18
 */
@Controller("ricks")
public class HttpController {

    @Autowired
    private UserRepository userDao;

    private static final AtomicLong counter = new AtomicLong(0);

    @Autowired
    private ExpirationListener expirationListener;

    static Map<Integer, UserVo>  users = new ConcurrentHashMap<>();

    @RequestMapping(value = "testlog")
    public Card testLog(@Param(value = "a") int a,@Param(value = "b") short b) {

        long count = counter.incrementAndGet();

        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
//        UserVo user = OrmContext.me().load(UserVo.class,1);
//        Logger.info("test: " + count);
//        System.err.println(Thread.currentThread() + "     test" + count + "- ricks is  god - demon is  ricks");
//        return "test" + count + "- ricks is  god - demon is  ricks .........." ;
        Call call = (num1, num2) -> num1 + num2;

        int aa = call.call(1, 2);
//        System.err.println(aa + ".....................");
        ItemCall itemCall = (cards) -> cards.stream().flatMapToInt(card -> IntStream.of(card.getCardId())).sum();
        float aaa = itemCall.itemCall(Set.of(new Card(11, "ricsk", 111), new Card(222, "is", 333), new Card(4234, "god", 3242)));
//        System.err.println(aaa + ".....................");
//        CallAll callAll =  () -> 1;
//        callAll.callAll((Set.of(new Card(11,"ricsk",111), new Card(222,"is", 333) , new Card(4234,"god",3242));

        UserVo userVo = OrmContext.me().uniqueCacheLoad(UserVo.class, 1L).get();
        Assert.notNull(userVo, "user 无法查到数据");
//        System.err.println(userVo.toString());
        return new Card(1,"test" + count + "- ricks is  god - demon is  ricks ..........",2222);
    }

    @RequestMapping(value = "test")
    public String test(int a) {
        Logger.info("dedeededed" );
        String d = TimeUtils.formatYMDHMSTime(TimeUtils.currentTimeMillis());
        User user = new User();
        List<Card> cards = new ArrayList<>();
        Card card = new Card(1,"ricks",12);
        cards.add(card);
        user.setFriends(cards);
        BitSet bitSet = new BitSet();
        bitSet.set(100000,true);
        bitSet.set(100001,true);
//        user.setRoleId(user.getId());
        user.setBitSet(bitSet);
//        userDao.update(user);

//        byte[] data = Fse.serialize(user.getFriends());
//        System.err.println("序列化字节长度："+ data);
//        var f = Fse.deSerialize(data);
//        userDao.insert(user);
//        userDao.update("update god set bitset = ? , roleName = ? where id = ? ",bitSet,"ricks1",-9045986273804850679L);
//        User user1 = userDao.load(user.getId());
//        BitSet bitSet1 = user1.getBitSet();
//        if(bitSet1 == null) bitSet1 = new BitSet();

        return "";
//        return "demon is god  进入，，，，，    凯子是哈皮，， 小汪  小汪  小汪" + d + " and p = " + a + "  bitset 100000=" + bitSet1.get(100000) + " & 100001="+ bitSet1.get(100001) + " 100002" + bitSet1.get(100002);
    }

    @RequestMapping(value = "test1")
    public User test1() {
//        User user = new User();
//        user.setAge(18);
//        user.setRoleName("demon-"+ System.nanoTime());
//        user.setGold(9999);
//        user.setSendGift(1);
//        user.setGreetings("哈哈哈11111111111111111111");
//        user.setLv(99);
//        user.setFinalCityId(1000);
//        user.setRoleId(RandomUtil.randomLong());
//        user.setId(RandomUtil.randomLong());
//        EntityMgr.save(user);


        Map map = MapUtils.newHashMap();
        map.put("age",18);
        map.put("lv",99);
        User user = userDao.load(map);
        return user;
//        user.update();
//
//
//        User user1 = new User();
//        user1.setAge(18);
//        user1.setRoleName("demon-"+ System.nanoTime());
//        user1.setGold(9999);
//        user1.setSendGift(1);
//        user1.setGreetings("哈哈哈222222222222222222222222");
//        user1.setLv(99);
//        user1.setFinalCityId(1000);
//        user1.setRoleId(4369497559225200411l);
//        user1.setId(3533337058539995136L);
//        user1.update();
//
//
////        User user3 = new User();
////        user3.setAge(18);
////        user3.setRoleName("demon-"+ System.nanoTime());
////        user3.setGold(9999);
////        user3.setSendGift(1);
////        user3.setGreetings("哈哈哈222222222222222222222222");
////        user3.setLv(99);
////        user3.setFinalCityId(1000);
////        user3.setRoleId(RandomUtil.randomLong());
////        Map<Integer,String> map = new HashMap<>();
////        map.put(1,"demon");
////        map.put(2,"is");
////        map.put(3,"god");
////        user3.setResource(map);
//        List<User> addUsers = new ArrayList<>();
//        for (int i = 0; i < 3000; i++) {
//            User user3 = new User();
//            user3.setAge(18);
//            user3.setRoleName("demon-"+ System.nanoTime());
//            user3.setGold(9999);
//            user3.setSendGift(1);
//            user3.setGreetings("哈哈哈222222222222222222222222");
//            user3.setLv(99);
//            user3.setFinalCityId(1000);
//            user3.setRoleId(RandomUtil.randomLong());
//            Map<Integer,String> map = new HashMap<>();
//            map.put(1,"demon");
//            map.put(2,"is");
//            map.put(3,"god");
//            user3.setResource(map);
//            addUsers.add(user3);
//
//        }
//        EntityMgr.save(User.class,addUsers);
////        user3.setGreetings("开始更新，，，，，，");
////
////        List<Card> friends = new ArrayList<>();
////        Card card1 = new Card(1,"展示",1);
////        Card card2 = new Card(2,"法师",1);
////        List<String> friendStr = new ArrayList<>();
////        friendStr.add("汪婷");
////        friendStr.add("晓涵");
////        friends.add(card1);
////        friends.add(card2);
////        Map<Integer,Card> map1 = new HashMap<>();
////        map1.put(1,card1);
////        map1.put(2, card2);
////        user3.setResource1(map1);
////        user3.setFriends(friends);
////        user3.setFriendStr(friendStr);
////        user3.setBytes("hello fuck you".getBytes());
////        user3.update();
//        User user4 = new User();
//        user4.setId(3550779548532473856L);
//        user4 = user4.get();
////        String stt = new String(user4.getBytes());
////        System.err.println(stt);
//        user1.save();
//        user1.delete();
//        user1.update();

//        return user;
    }

    @RequestMapping(value = "test2")
    public User test2() {
        User user = new User();
        user.setAge(18);
        user.setRoleName("demon");
        user.setGold(9999);
        user.setSendGift(1);
        user.setGreetings("哈哈哈111");
        user.setLv(99);
        user.setFinalCityId(1000);
        user.setId(3533336842617225216L);
        List<User> users =  userDao.loadAll(3533336842617225216L);
        if(user != null) {
            users.forEach(u -> System.err.println(u.getId()));

            System.err.println(" size : " + users.size());
        }
        return user;
    }

    @RequestMapping(value = "test3")
    public void test3() {
//        Class<?> cardClass = new ArrayList<Card>().getClass();
//        JSONObject jsonObject = new JSONObject("{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}");
//        System.err.println(jsonObject.toMap());
//        JSONWriter.valueToString("{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}").
//        Card cards =  JSON.toBean("{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}",Card.class);
//        System.err.println("......");
    }

    @RequestMapping(value = "test4")
    public void test4(User user) {
//        List<Card> cards =  JSON.toBean("[{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}]",ArrayList.class);
//        System.err.println("......" + user.getRoleName());
    }

    @RequestMapping(value = "testList")
    public void testList(List<Boolean> intList) {
//        JSONArray jsonArray = JSONUtil.parseArray("[{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}]");
//        List list = jsonArray.toList(Card.class);
//        System.err.println("......" + user.getRoleName());
        intList.forEach(i -> System.err.println(i));
    }

    @RequestMapping(value = "testMap")
    public void testMap(Map<String,String> map) {
//        JSONArray jsonArray = JSONUtil.parseArray("[{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}]");
//        List list = jsonArray.toList(Card.class);
//        System.err.println("......" + user.getRoleName());
        map.forEach((k,v) -> System.err.println(k + " and " + v));
    }

    @RequestMapping(value = "testListBean")
    public void testListBean(List<User> users) {
//        JSONArray jsonArray = JSONUtil.parseArray("[{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}]");
//        List list = jsonArray.toList(Card.class);
//        System.err.println("......" + user.getRoleName());
        users.forEach(u -> System.err.println(u.getRoleName()));
    }


    @RequestMapping(value = "test10")
    public User test10() {
        return userDao.load(3637105179720941820L);
    }

//    @RequestMapping(value = "test11")
//    public User test11() {
//        User user = new User();
//        user.setAge(18);
//        user.setRoleName("demon-"+ System.nanoTime());
//        user.setGold(9999);
//        user.setSendGift(1);
//        user.setGreetings("哈哈哈11111111111111111111");
//        user.setLv(99);
//        user.setFinalCityId(1000);
//        user.setRoleId(10000000L);
//        user.setId(RandomUtil.randomLong());
//        List<Card> cardList = new ArrayList<>();
//        Card card = new Card(1,"ricks",1);
//        Map<Integer,Card> map = new HashMap<>();
//        map.put(3,card);
//        user.setResource1(map);
//        cardList.add(card);
//        user.setFriends(cardList);
//        user.setBytes(new byte[]{1});
//        userDao.insert(user);
//        Logger.info("正在保存的 user id:"+user.getId());
//        long dbt = System.nanoTime();
//        userDao.load(user.getId());
//        long dbe = System.nanoTime();
//        Logger.info("db get 耗时："+(dbe -dbt));
//        MultiCacheRepository cacheRepository = (MultiCacheRepository)IocHolder.getIoc().get("User");
//        long dbt1 = System.nanoTime();
//
//        List<User> user1 = cacheRepository.cacheLoadAll(10000000L);
//        long dbe1 = System.nanoTime();
//
//        Logger.info("cache get 耗时："+(dbe1 -dbt1));
//        user1.forEach(u -> Logger.info("u.getId()"+","+u.getRoleId()+","+u.getRoleName()+","+u.getAge()));
//
//        users.put(10000000,user);
//
//        long dbt2 = System.nanoTime();
//
//        users.get(10000000);
//        long dbe2 = System.nanoTime();
//
//        Logger.info("map get 耗时："+(dbe2 -dbt2));
//        return  user1.get(0);
//    }

    @RequestMapping(value = "test12")
    public void test12(List<Long> userIds,int businessId) {
        System.err.println(businessId);
//        System.err.println("热更成功，。，，，，");
    }

}
