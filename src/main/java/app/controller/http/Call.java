package app.controller.http;

@FunctionalInterface
public interface Call {

    int call(int a,int b);

}
