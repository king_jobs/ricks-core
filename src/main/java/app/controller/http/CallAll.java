package app.controller.http;


import app.vo.Card;

import java.util.Set;

public interface CallAll extends ItemCall,Call{

    int all();

    default int callAll(Set<Card> cards ) {
        return call(itemCall(cards),100);
    };
}
