package app.controller;

import app.context.ActionErrorEnum;
import app.data.codec.message.InnerMessage;
import org.ricks.ioc.Action;
import org.ricks.net.Context;
import org.ricks.ioc.Bean;
import org.ricks.net.ActionMethod;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/11/3015:34
 */
@Action
public class RoleController extends Object{

    private static AtomicLong counter = new AtomicLong(0);
    @ActionMethod(messageId = 3000)
    public void test(Context context) {
//        System.err.println(" + into role controller 3000.......");
        ActionErrorEnum.dataNotExist.assertTrue(false);
    }

    @ActionMethod(messageId = 11111)
    public void testKcp(Context context) {
//        System.err.println(Thread.currentThread().getName() + " and sessionId: "+ context.getSession().sid()+ " into role controller " + counter.incrementAndGet());
//        ActionErrorEnum.dataNotExist.assertTrue(false);
    }

    @ActionMethod(messageId = 3001)
    public void test1(Context context) {
        System.err.println("into role controller 3001.......");

        context.send(new byte[1]);
    }
}
