//package app.controller;
//
//import app.dao.UserRepository;
//import app.data.CardData;
//import app.data.context.DataContext;
//import app.vo.GachaVo;
//import app.vo.User;
//import org.ricks.ioc.Autowired;
//import org.ricks.lang.Logger;
//import org.ricks.net.Context;
//import org.ricks.ioc.Bean;
//import org.ricks.net.ActionMethod;
//import org.ricks.utils.RandomUtil;
//import org.ricks.utils.TimeUtils;
//
//import java.util.Optional;
//import java.util.concurrent.atomic.AtomicBoolean;
//import java.util.concurrent.atomic.AtomicLong;
//
///**
// * @author ：demon-chen
// * @date ：Created in 2019/12/18 下午3:16
// * @description：
// * @modified By：
// * @version: $
// */
//@Bean
//public class UserController {
//
//
//    @Autowired
//    private UserRepository userDao;
////    public static final TestController testController = new TestController();
//
//
//     private AtomicLong count = new AtomicLong(0);
//     private volatile long startTime =  0;
//     private AtomicBoolean start = new AtomicBoolean(true);
////    @HttpMapping(value = "test")
////    public void test(HttpResponse response){
////        Logger.info(" test.......");
////        response.redirect("www.baidu.com");
////    }
//
//    @ActionMethod(messageId = 10)
//    public void test(Context context) {
//        long num = count.incrementAndGet();
//        if(num == 1) {
//            startTime = TimeUtils.currentTimeMillis();
//            Logger.info("jdk aio  test....... time:"+startTime);
//        }
//        if(TimeUtils.currentTimeMillis() - startTime > 1000  && start.get()) {
//            start.set(false);
//            Logger.info("1秒承载  count:"+count);
//        }
//        if(num == 1000000) {
//            Logger.info("jdk aio  test....... time:"+TimeUtils.currentTimeMillis());
//        }
//        CardData cardData = Optional.of(DataContext.getCard(10010000)).orElse(new CardData());
////        System.err.println(" cardId: " + cardData.getCardId() + " and AdvanceID: "+ cardData.getAdvanceID() + " and AdvanceValue: " + cardData.getAdvanceValue() );
////        context.send();
//    }
//
//    @ActionMethod(messageId = 11)
//    public void testUserDb(Context context) {
//        System.err.println("开始保存数据。。。。。。。。");
//        User user = new User();
//        user.setAge(18);
//        user.setBindDiamond(10000);
//        user.setCookingExp(0);
//        user.setCookingLv(1);
//        user.setDuration(1);
//        user.setCreateTime(TimeUtils.currentTimeMillis());
//        user.setFinalCityId(1000);
//        user.setGold(1000);
//        user.setGreetings("你好，demon !");
//        user.setHeadIcon(10000);
//        user.setLv(1);
//        user.setOpenId("demon");
//        user.setRoleId(RandomUtil.randomLong());
//        user.setRoleName("demon_is_god");
//        user.setSendGift(111);
//        userDao.insert(user);
////        context.send();
//    }
//
//
//    @ActionMethod(messageId = 12)
//    public void tesGachaDb(Context context) {
//        GachaVo gachaVo = new GachaVo();
//        gachaVo.setGachaId(18);
//        gachaVo.setGachaNum(10000);
//        gachaVo.setGachaName("新手卡池");
//        gachaVo.setGachaType(1);
//        gachaVo.setRoleId(RandomUtil.randomLong());
////        userDao.insert(gachaVo);
//        //        context.send();
//    }
//
//    @ActionMethod(messageId = 13)
//    public void demon(Context context) {
//        GachaVo gachaVo = new GachaVo();
//        gachaVo.setGachaId(18);
//        gachaVo.setGachaNum(10000);
//        gachaVo.setGachaName("新手卡池");
//        gachaVo.setGachaType(1);
//        gachaVo.setRoleId(RandomUtil.randomLong());
////        EntityMgr.save(gachaVo);
////        context.send();
//    }
//
//
////    @HttpMapping(value = "a")
////    public void a(HttpResponse response){
////        Logger.info(" test.......");
////        response.sendHtml("https://www.baidu.com");
////    }
//
//}
