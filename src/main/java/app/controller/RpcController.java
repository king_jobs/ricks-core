package app.controller;

import org.ricks.net.rpc.*;

@RpcAction
public class RpcController {


    @RpcMethod(rpcCmd = 5205)
    public byte[] testRpc(RpcPackage rpcPackage) {
        String str = new String(rpcPackage.getData());
        System.err.println("into rpc server " + str);
        return ("hi ,RPC . i am ricks ! == " + str).getBytes();
    }
}
