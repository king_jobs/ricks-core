package app.controller;

import org.ricks.net.Context;
import org.ricks.ioc.Bean;
import org.ricks.net.ActionMethod;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/11/3015:34
 */
@Bean
public class SystemController {

    @ActionMethod(messageId = 4000)
    public void test(Context context) {
        System.err.println("into role controller 4000.......");
    }

    @ActionMethod(messageId = 4001)
    public void test1(Context context) {
        System.err.println("into role controller 4001.......");
    }
}
