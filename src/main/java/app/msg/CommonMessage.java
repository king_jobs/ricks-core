//package app.msg;
//
//import app.fb.SimpleBoolValue;
//import com.google.flatbuffers.FlatBufferBuilder;
//import org.ricks.ioc.Message;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/12/1618:55
// */
//public class CommonMessage {
//
//    public static Message getSimpleBoolValue() {
//        FlatBufferBuilder builder = new FlatBufferBuilder(0);
//
//        builder.finish(SimpleBoolValue.createSimpleBoolValue(builder,true));
//        return new Message(100,builder.sizedByteArray());
//    }
//}
