//package app.msg;
//
//import app.fb.GetRoleInfo;
//import com.google.flatbuffers.FlatBufferBuilder;
//import org.ricks.ioc.Message;
//
///**
// * @author ricks
// * @Title:
// * @Package
// * @Description:
// * @date 2022/12/1618:51
// */
//public class RoleMessage {
//
//    public static Message createRoleInfoMsg() {
//        FlatBufferBuilder builder = new FlatBufferBuilder(0);
//        int roleName = builder.createString("ricks");
//
//        int guildNameOffset = builder.createString("");
//
//
//        GetRoleInfo.startGetRoleInfo(builder);
//
//        GetRoleInfo.addRoleid(builder, 1111);
//        GetRoleInfo.addRolename(builder, roleName);
//        GetRoleInfo.addGender(builder, 1);
//        GetRoleInfo.addLevel(builder, 100);
//        GetRoleInfo.addExp(builder, 100);
//        GetRoleInfo.addGold(builder, 100);
//        GetRoleInfo.addGuildName(builder, guildNameOffset);
//
//        // 生日相关得数据
//
//        int states = GetRoleInfo.endGetRoleInfo(builder);
//        builder.finish(states);
//
//        return new Message<>(104, builder.sizedByteArray());
//    }
//}
