package app.prop;

public class PropAddHead {

    private final AddPropEnum addPropEnum;

    /**
     * 策划表 获得道具ID
     */
    private final int id;

    public AddPropEnum getAddPropEnum() {
        return addPropEnum;
    }

    public int getId() {
        return id;
    }

    public PropAddHead(AddPropEnum addPropEnum, int id) {
        this.addPropEnum = addPropEnum;
        this.id = id;
    }

    public static PropAddHead valueOf(AddPropEnum addPropEnum, int id) {
        PropAddHead propAddHead = new PropAddHead(addPropEnum, id);
        return propAddHead;
    }
}
