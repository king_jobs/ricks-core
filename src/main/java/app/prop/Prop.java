package app.prop;

import java.util.ArrayList;
import java.util.List;

public abstract class Prop implements IProp{

    protected List getAddProps(PropAddHead propAddHead) {
        switch (propAddHead.getAddPropEnum()) {
            case RANDOM_ADD_PROP -> System.err.println("从策划配置随机奖励表拿到增加道具");
            case REGULAR_ADD_PROP -> System.err.println("从策划配置固定奖励表拿到增加道具");
            default -> System.err.println("未知类型无法拿到数据");
        }
        return new ArrayList();
    }
}
