package app.prop;

public interface Resource {

    default int type() {
        return 0;
    };

    default int id() {
        return 0;
    };

    default int num() {
        return 0;
    };

}
