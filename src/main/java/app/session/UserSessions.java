package app.session;

import app.session.hook.UserHook;
import app.session.hook.UserHookDefault;
import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户 session 管理器
 * <pre>
 *     对所有用户UserSession的管理，统计在线用户等
 *
 *     关于用户管理 UserSessions 和 UserSession 可以参考这里：
 *     https://www.yuque.com/iohao/game/wg6lk7
 * </pre>
 *
 * @author 渔民小镇
 * @date 2022-01-11
 */
public class UserSessions {

    /**
     * key : 玩家 id
     * value : UserSession
     */
    private final Map<Long, UserSession> userIdMap = new ConcurrentHashMap<>();

    /**
     * key : sessionId
     * value : UserSession
     */
    private final Map<Long, UserSession> userChannelIdMap = new ConcurrentHashMap<>();

    /** UserHook 上线时、下线时会触发 */
    private UserHook userHook = new UserHookDefault();


    /**
     * 获取 UserSession
     *
     * @param sessionId
     * @return UserSession
     */
    public UserSession getUserSessionBySessionId(long sessionId) throws RuntimeException {
        UserSession userSession = userChannelIdMap.get(sessionId);

        if (Objects.isNull(userSession)) {
            throw new RuntimeException("userSession 不存在，请先加入 session 管理中，UserSessions.add(sessionId)  curr sessionId = " + sessionId);
        }

        return userSession;
    }

    /**
     * true 用户存在
     *
     * @param userId 用户id
     * @return true 用户存在
     */
    public boolean existUserSession(long userId) {
        return this.userIdMap.containsKey(userId);
    }

    /**
     * 获取 UserSession
     *
     * @param userId userId
     * @return UserSession
     */
    public UserSession getUserSessionByUserId(long userId) throws RuntimeException {
        UserSession userSession = this.userIdMap.get(userId);

        if (Objects.isNull(userSession)) {
            throw new RuntimeException("userSession 不存在，请先登录在使用此方法");
        }

        return userSession;
    }

    public UserSession getUserSession(long sessionId) throws RuntimeException {
        UserSession userSession = this.userChannelIdMap.get(sessionId);

        if (Objects.isNull(userSession)) {
            // 如果你登录了，但是又报这个异常，一般是将 ExternalGlobalConfig.verifyIdentity = false 了。
            throw new RuntimeException("userSession 不存在");
        }

        return userSession;
    }

    /**
     * 加入到 session 管理
     *
     * @param session
     */
    public void add(AioSession session) {
        try {
            logIp(session);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        UserSession userSession = new UserSession(session);


        this.userChannelIdMap.putIfAbsent(session.sid(), userSession);
    }

    /**
     * 设置 channel 的 userId
     *
     * @param sessionId userChannelId
     * @param userId        userId
     * @return true 设置成功
     */
    public boolean settingUserId(long sessionId, long userId) {

        UserSession userSession = this.getUserSession(sessionId);
        if (Objects.isNull(userSession)) {
            return false;
        }

        if (Boolean.FALSE.equals(userSession.isActiveChannel())) {
            removeUserSession(userSession);
            return false;
        }

        userSession.setUserId(userId);

        this.userIdMap.put(userId, userSession);

        // 上线通知
        if (userSession.isVerifyIdentity()) {
            userHookInto(userSession);
        }

        return true;
    }

    /**
     * 移除 UserSession
     *
     * @param userSession userSession
     */
    public void removeUserSession(UserSession userSession) {
        Objects.requireNonNull(userSession);

        if (userSession.getState() == UserSessionState.DEAD) {
            return;
        }

        long userId = userSession.getUserId();
        this.userIdMap.remove(userId);
        this.userChannelIdMap.remove(userSession.getAioSession().sid());

        if (userSession.getState() == UserSessionState.ACTIVE && userSession.isVerifyIdentity()) {
            userSession.setState(UserSessionState.DEAD);
            this.userHookQuit(userSession);
        }

        // 关闭用户的连接
        userSession.close();
    }

    /**
     * 当前在线人数
     *
     * @return 当前在线人数
     */
    public long countOnline() {
        return this.userIdMap.size();
    }

    /**
     * 全员消息广播
     * 消息类型 ExternalMessage
     *
     * @param msg 消息
     */
    public void broadcast(Object msg) {
        userIdMap.values().forEach(userSession -> userSession.writeAndFlush(msg));
    }

    /**
     * 上线通知。注意：只有进行身份验证通过的，才会触发此方法
     *
     * @param userSession userSession
     */
    private void userHookInto(UserSession userSession) {
        if (Objects.isNull(userHook) || !userSession.isVerifyIdentity()) {
            return;
        }

        this.userHook.into(userSession);
    }

    /**
     * 离线通知。注意：只有进行身份验证通过的，才会触发此方法
     *
     * @param userSession userSession
     */
    private void userHookQuit(UserSession userSession) {
        if (Objects.isNull(userHook) || !userSession.isVerifyIdentity()) {
            return;
        }

        this.userHook.quit(userSession);
    }

    private void logIp(AioSession session) throws IOException {
        InetSocketAddress socketAddress = session.getRemoteAddress();
        String remoteAddress = socketAddress.getAddress().getHostAddress();
        int remotePort = socketAddress.getPort();

        socketAddress = session.getLocalAddress();
        String localAddress = socketAddress != null ? socketAddress.getAddress().getHostAddress() : "null";
        int localPort = socketAddress.getPort();

        Logger.debug("localAddress::"+localAddress+":"+localPort+", remoteAddress::"+ remoteAddress+":"+remotePort);
    }


    private UserSessions() {

    }

    public static UserSessions me() {
        return Holder.ME;
    }

    /** 通过 JVM 的类加载机制, 保证只加载一次 (singleton) */
    private static class Holder {
        static final UserSessions ME = new UserSessions();
    }

    public void setUserHook(UserHook userHook) {
        this.userHook = userHook;
    }


}
