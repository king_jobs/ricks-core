/*
 * # iohao.com . 渔民小镇
 * Copyright (C) 2021 - 2022 double joker （262610965@qq.com） . All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.session;

import org.ricks.common.exception.ServerBootstrapException;
import org.ricks.net.AioSession;

import java.net.InetSocketAddress;
import java.util.Objects;

/**
 * 用户的唯一 session 信息
 * <pre>
 *     与 channel 是 1:1 的关系，可取到对应的 userId、channel 等信息
 *
 */
public class UserSession {

    /** user channel  要么连上kcp 要么连上tcp       */
    private final AioSession aioSession;
    /** userId */
    private long userId;
    /** 用户状态 */
    private UserSessionState state;

    public UserSession(AioSession session) {
        this.aioSession = session;
        this.state = UserSessionState.ACTIVE;
    }

    /**
     * 设置当前用户（玩家）的 id
     * <pre>
     *     当设置好玩家 id ，也表示着已经身份验证了（表示登录过了）。
     * </pre>
     *
     * @param userId userId
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserChannelId() {
        return aioSession.sid();
    }

    /**
     * 获取玩家ip
     *
     * @return 获取玩家ip
     */
    public String getIp() {
        if (Boolean.FALSE.equals(isActiveChannel())) {
            return "";
        }
        try {
            InetSocketAddress inetSocketAddress =  aioSession.getRemoteAddress();
            return inetSocketAddress.getHostString();
        } catch (Exception e) {
           throw new ServerBootstrapException(e.getMessage());
        }
    }


    /**
     * 是否进行身份验证
     *
     * @return true 已经身份验证了，表示登录过了。
     */
    public boolean isVerifyIdentity() {
        return this.userId > 0;
    }

    public boolean isActiveChannel() {
        return Objects.nonNull(aioSession) && !aioSession.isInvalid();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof UserSession that)) {
            return false;
        }

        return userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }

    private String getChannelId() {
        return String.valueOf(this.aioSession.sid());
    }

    public void setState(UserSessionState state) {
        this.state = state;
    }

    public void close() {
        aioSession.close();
    }

    public void writeAndFlush(Object msg) {
//        aioSession.sendResponse(msg);
    }

    public AioSession getAioSession() {
        return aioSession;
    }

    public long getUserId() {
        return userId;
    }

    public UserSessionState getState() {
        return state;
    }
}
