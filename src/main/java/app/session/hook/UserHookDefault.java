package app.session.hook;

import app.session.UserSession;
import app.session.UserSessions;
import org.ricks.common.lang.Logger;

/**
 * UserHookDefault
 * 改造成抽象的钩子，quit逻辑要销毁掉自己的 cache db回写容器
 * 异步回写还是用回以 table-name 机制，玩家退出不用销毁回写容器，只用clear玩家缓存
 */
public class UserHookDefault implements UserHook {

    @Override
    public void into(UserSession userSession) {
        long userId = userSession.getUserId();
        Logger.info("玩家上线 userId: "+ userId+" -- "+userSession.getUserChannelId());
        Logger.info("当前在线玩家数量： "+ UserSessions.me().countOnline());
    }

    @Override
    public void quit(UserSession userSession) {
        long userId = userSession.getUserId();
        Logger.info("玩家退出 userId: "+ userId+" -- "+userSession.getUserChannelId());
        Logger.info("当前在线玩家数量： "+ UserSessions.me().countOnline());
    }
}
