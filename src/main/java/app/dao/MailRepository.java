package app.dao;

import app.role.MailInfo;
import org.ricks.orm.BaseRepository;
import org.ricks.orm.anno.Repository;

/**
 * jdk 环境可以使用 Asm 字节增强生成
 *
 * GraalVM 环境只能手写
 */
@Repository
public class MailRepository extends BaseRepository<MailInfo,Long> {

//    @Sql("update user_vo set name = ? ")
//    public void updateName(String name){}


}
