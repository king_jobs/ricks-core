package app.vo;
import org.ricks.orm.BaseLongIDEntity;
import org.ricks.orm.anno.Column;
import org.ricks.orm.anno.Table;
import org.ricks.orm.repository.UniqueCacheRepository;

import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Map;

/**
 * @author ricks
 * @date 2022/9/717:28
 */
@Table(name = "user", repository = UniqueCacheRepository.class)
public class User extends BaseLongIDEntity<User>{

    @Column
    private int age;

    @Column
    private String roleName;

    @Column
    private long gold;

    @Column
    private int lv;

    @Column
    private long exp;

    @Column
    private long createTime;

    @Column
    private String greetings;

    @Column
    private int headIcon;

    @Column
    private String openId;

    @Column
    private int finalCityId;

    @Column
    private int sendGift;

    @Column
    private int cookingLv;

    @Column
    private long cookingExp;

    @Column
    private int bindDiamond;

    @Column
    private int duration;

    @Column
    private Map<Integer,String > resource;

    @Column
    private Map<Integer,Card> resource1;

    @Column
    private List<Card> friends;

    @Column
    private List<String> friendStr;

    @Column
    private  byte[] bytes;

    @Column
    private BitSet bitSet;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public long getGold() {
        return gold;
    }

    public void setGold(long gold) {
        this.gold = gold;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        support.firePropertyChange("lv", this.lv, lv);
        this.lv = lv;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getGreetings() {
        return greetings;
    }

    public void setGreetings(String greetings) {
        this.greetings = greetings;
    }

    public int getHeadIcon() {
        return headIcon;
    }

    public void setHeadIcon(int headIcon) {
        this.headIcon = headIcon;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public int getFinalCityId() {
        return finalCityId;
    }

    public void setFinalCityId(int finalCityId) {
        this.finalCityId = finalCityId;
    }

    public int getSendGift() {
        return sendGift;
    }

    public void setSendGift(int sendGift) {
        this.sendGift = sendGift;
    }

    public int getCookingLv() {
        return cookingLv;
    }

    public void setCookingLv(int cookingLv) {
        this.cookingLv = cookingLv;
    }

    public long getCookingExp() {
        return cookingExp;
    }

    public void setCookingExp(long cookingExp) {
        this.cookingExp = cookingExp;
    }

    public int getBindDiamond() {
        return bindDiamond;
    }

    public void setBindDiamond(int bindDiamond) {
        this.bindDiamond = bindDiamond;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Map<Integer, String> getResource() {
        return resource;
    }

    public void setResource(Map<Integer, String> resource) {
        this.resource = resource;
    }

    public List<Card> getFriends() {
        return friends;
    }

    public void setFriends(List<Card> friends) {
        this.friends = friends;
    }

    public List<String> getFriendStr() {
        return friendStr;
    }

    public void setFriendStr(List<String> friendStr) {
        this.friendStr = friendStr;
    }

    public Map<Integer, Card> getResource1() {
        return resource1;
    }

    public void setResource1(Map<Integer, Card> resource1) {
        this.resource1 = resource1;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public BitSet getBitSet() {
        return bitSet;
    }

    public void setBitSet(BitSet bitSet) {
        this.bitSet = bitSet;
    }

    @Override
    public String toString() {
        return "User{" +
                "age=" + age +
                ", roleName='" + roleName + '\'' +
                ", gold=" + gold +
                ", lv=" + lv +
                ", exp=" + exp +
                ", createTime=" + createTime +
                ", greetings='" + greetings + '\'' +
                ", headIcon=" + headIcon +
                ", openId='" + openId + '\'' +
                ", finalCityId=" + finalCityId +
                ", sendGift=" + sendGift +
                ", cookingLv=" + cookingLv +
                ", cookingExp=" + cookingExp +
                ", bindDiamond=" + bindDiamond +
                ", duration=" + duration +
                ", resource=" + resource +
                ", resource1=" + resource1 +
                ", friends=" + friends +
                ", friendStr=" + friendStr +
                ", bytes=" + Arrays.toString(bytes) +
                ", bitSet=" + bitSet +
                '}';
    }
}
