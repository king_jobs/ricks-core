package app.vo;

/**
 * @author ricks
 * @date 2022/12/1213:52
 */
public class Card {

    private int cardId;
    private String cardName;
    private int count;

    public Card() {
    }

    public Card(int cardId, String cardName, int count) {
        this.cardId = cardId;
        this.cardName = cardName;
        this.count = count;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Card{" +
                "cardId=" + cardId +
                ", cardName='" + cardName + '\'' +
                ", count=" + count +
                '}';
    }
}
