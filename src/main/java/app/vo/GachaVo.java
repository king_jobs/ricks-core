package app.vo;

import org.ricks.orm.BaseLongIDEntity;
import org.ricks.orm.anno.Column;

/**
 * @author ricks
 * @date 2022/9/717:28
 */
//@Table(repository = MultiCacheRepository.class)
public class GachaVo extends BaseLongIDEntity {

    @Column
    private int gachaId;

    @Column
    private String gachaName;

    @Column
    private long gachaNum;

    @Column
    private int gachaType;


    public int getGachaId() {
        return gachaId;
    }

    public void setGachaId(int gachaId) {
        this.gachaId = gachaId;
    }

    public String getGachaName() {
        return gachaName;
    }

    public void setGachaName(String gachaName) {
        this.gachaName = gachaName;
    }

    public long getGachaNum() {
        return gachaNum;
    }

    public void setGachaNum(long gachaNum) {
        this.gachaNum = gachaNum;
    }

    public int getGachaType() {
        return gachaType;
    }

    public void setGachaType(int gachaType) {
        this.gachaType = gachaType;
    }
}
