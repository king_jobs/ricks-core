package app.context;

/**
 * action 错误码
 * <pre>
 *     关于异常机制的解释可以参考这里:
 *     https://www.yuque.com/iohao/game/avlo99
 * </pre>
 *
 * @author 渔民小镇
 * @date 2022-01-14
 */
public enum ActionErrorEnum implements MsgExceptionInfo {
    /**
     * 系统其它错误
     * <pre>
     *      一般不是用户自定义的异常，很可能是用户引入的第三方包抛出的异常
     * </pre>
     */
    systemOtherErrCode((short) -1000., "系统其它错误"),
    /** 参数验错误码 */
    validateErrCode((short) -1001, "参数验错误"),
    /** 路由错误码，一般是客户端请求了不存在的路由引起的 */
    cmdInfoErrorCode((short) -1002, "路由错误"),
    /** 心跳错误码 */
    idleErrorCode((short) -1003, "心跳超时相关"),
    /** 需要登录后才能调用业务方法 */
    verifyIdentity((short) -1004, "请先登录"),
    /** class 不存在 */
    classNotExist((short) -1005, "class 不存在"),
    /** 数据不存在 */
    dataNotExist((short) 1006, "数据不存在"),
    /** 强制玩家下线 */
    forcedOffline((short) -1007, "强制玩家下线"),
    ;

    /** 消息码 */
    final short code;
    /** 消息模板 */
    final String msg;

    ActionErrorEnum(short code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    @Override
    public short getCode() {
        return code;
    }

    @Override
    public String getMsg() {
        return msg;
    }
}
