package app.context;

import org.ricks.agent.FileListener;
import org.ricks.common.lang.Logger;
import org.ricks.common.PathUtil;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;


public class BasicData implements FileListener {
    private static Path path =  Paths.get(PathUtil.currPath() + "/data.dat");

    public static Map<String, byte[]> map =new HashMap<>();

    public BasicData(){
        loadBasicData();
    }
    public static void loadData(Path file){
        map = readData(file.toFile());
    }
    public static void loadBasicData(){
        loadData(path);
        try {
            System.out.println("== load succ ==");
        }catch (Exception e){
            System.out.println(e);
        }
    }


    private static Map<String,byte[]> readData(File file){
        ObjectInputStream input = null;
        Object obj = null;
        try{
            input = new ObjectInputStream(new FileInputStream(file));
            obj = input.readObject();
            if (obj == null) {
                System.err.println("Object can't been reload by ObjectInputStreamMethod");
                input.close();
                return null;
            }
            if (obj != null && obj instanceof Map) {
                Map<String,byte[]> readMap = (Map<String,byte[]>) obj;
                input.close();
                return readMap;
            }
        } catch(FileNotFoundException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        } catch(ClassNotFoundException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onCreate(Path file) {
        loadBasicData();
    }

    @Override
    public void onModify(Path file) {
        File dataFile = path.toFile();
        System.err.println("into basic data ,...");
        if(dataFile.getName().endsWith("data.dat")){
            Logger.info("==========================  data change  ========================");
            loadBasicData();
        }
    }

    @Override
    public void onDelete(Path file) {
        System.err.println("data delete ........");
    }
}
