package app;

import app.controller.http.HttpController;
import org.ricks.ModularContext;
import org.ricks.common.conf.RicksConf;
import org.ricks.ioc.IocHolder;
import org.ricks.net.http.*;
import org.ricks.orm.OrmContext;

import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class AppMain {

	static AtomicLong atomicLong = new AtomicLong(0);
	public static void main(String[] args) throws Exception {
//		AppContext.setBannerMode(Banner.Mode.OFF); //设置banner 是否开启，默认开启
		ModularContext.me().scan(Set.of("app"));
		RicksConf.loadingProperties(); //初始化配置
		IocHolder.initIoc();
		OrmContext.me().init();
//		AppContext.me().run(Set.of("app"));//启动容器 & 启动server
//		ServerSSLContextFactory serverFactory = new ServerSSLContextFactory(AppMain.class.getClassLoader().getResourceAsStream("server.keystore"), "123456", "123456");
//		SslPlugin<Integer> sslServerPlugin = new SslPlugin<>(serverFactory, ClientAuth.OPTIONAL);
//		RestfulBootstrap.get().start(9000);

//		HttpBootstrap bootstrap = new HttpBootstrap().httpHandler(new HttpServerHandler() {
//			byte[] bytes = "hello world".getBytes();
//
//			@Override
//			public void handle(HttpRequest request, HttpResponse response) throws IOException {
////                System.out.println("url:"+request.getRequestURL());
////                System.out.println("param:"+request.getParameters());
////                System.out.println("name: "+request.getParameter("name"));
////				long count = atomicLong.incrementAndGet();
////				System.err.println(count);
//				response.setContentLength(bytes.length);
//				response.setContentType("text/plain; charset=UTF-8");
//				response.write(bytes);
//			}
//		}).setPort(8080);


		RestfulHandler restfulHandler = new RestfulHandler(null);
		restfulHandler.addController(new HttpController());
		HttpBootstrap bootstrap = new HttpBootstrap().setPort(9000);
		bootstrap.httpHandler(restfulHandler);
		bootstrap.configuration()
				.threadNum(Runtime.getRuntime().availableProcessors())
				.readBufferSize(1024 * 4)
				.writeBufferSize(1024 * 4)
				.readMemoryPool(16384 * 1024 * 4)
				.writeMemoryPool(10 * 1024 * 1024 * Runtime.getRuntime().availableProcessors(), Runtime.getRuntime().availableProcessors()).debug(false);
		bootstrap.start();
//
//		NetContext.me().init();

//		DataCodecKit.setDataCodec(new GameCodec());
//		InnerProtocol protocol = new InnerProtocol();
//		InnerHandler handler = new InnerHandler();
//		KcpBootstrap kcpBootstrap = new KcpBootstrap((Protocol)protocol,handler);
//		kcpBootstrap.open("127.0.0.1",55555);


//
//		HttpBootstrap httpBootstrap = new HttpBootstrap();
//		httpBootstrap.httpHandler(new WebHandler());
//		httpBootstrap.setPort(9999);
//		httpBootstrap.start();
//		StaticHandler staticHandler = new StaticHandler();
//		String classPath = StaticHandler.class.getResource("/static").getPath();
//		System.err.println(classPath);
//		AioServer aioServer = new AioServer();
//		aioServer.start(6666);
//		HttpBootstrap httpBootstrap = new HttpBootstrap();
//		httpBootstrap.httpHandler(new HttpStaticResourceHandler("static"));
//		httpBootstrap.start();
//
//		Engine engine = Engine.use();
//
//		engine.setDevMode(true);
//		engine.setToClassPathSourceFactory();
//
//		engine.getTemplate("index.html");
//
//		Kv kv = Kv.by("key", 123);
//		Template template = engine.getTemplate("index.html");
//
//		// 字节流模式输出到 OutputStream
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		template.render(kv, baos);
//
//		// 字符流模式输出到 StringWriter
//		StringWriter sw = new StringWriter();
//		template.render(kv, sw);
//
//		// 直接输出到 String 变量
//		String str = template.renderToString(kv);
//		System.err.println(str);
//		IocHolder.getIoc().get(DataContext.class).init();

//		AppContext.run(AppMain.class);//启动容器 & 启动server
////		DataCodecKit.setDataCodec(new InnerCodec());
//		AioServer aioServer = new AioServer(26891, (Protocol) new TcpInnerProtocol(),new InnerHandler());
//		aioServer.start();
//		DataCodecKit.setDataCodec(new InnerCodec());
//		aioServer.start(26891);
//		KcpServer kcpServer = new KcpServer();
//		kcpServer.start(26891);

//		long s = System.currentTimeMillis();
//		for (int i = 0; i < 1000000; i++) {
//			byte[] data = ObjectUtil.serialize(1);
//			Object obj = ObjectUtil.deserialize(data);
//		}
//		long e = System.currentTimeMillis();
//		System.err.println("耗时：" + (e -s));

//		LogModular.get().init();
//		DefaultHandler handler = new DefaultHandler();
//		AioClient client = new AioClient("192.168.18.105",10500,(Protocol)new StringProtocol(),handler);
//		ReconnectPlugin.get().addReconnectPlugin(client);
//		client.start();






//		String relative_path = "org/ricks/dispatch/Dispatcher.class";
//		String path2 = ResourceUtil.getResource("").getPath() + relative_path;
//
//		System.out.println(Resource.class.getResource("/"));
//
//		//（1）构建Cla***eader
//		ClassReader cr = new ClassReader(new FileInputStream(path2));
//
//		//（2）构建ClassWriter
//		ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
//
//		//（3）串连ClassVisitor
//		int api = Opcodes.ASM9;
//		ClassVisitor cv = new Dispatcher(api, cw);
//
//		//（4）结合Cla***eader和ClassVisitor
//		int parsingOptions = ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES;
//		cr.accept(cv, parsingOptions);
//
//		//（5）生成byte[]
//		byte[] data = cw.toByteArray();
//
//		Dispatcher.load(data);
//
//		ModularContext.me().scan(Set.of("app"));
//		IocHolder.initIoc();
//		Dispatcher dispatcher = new Dispatcher(0,null);
//		dispatcher.dispatcher(null);

//		ByteBuffer buffer = ByteBuffer.allocate(1024);
//		buffer.putShort((short) 3001);
//		buffer.putInt(1);
//		System.err.println(StrUtil.toHexString(buffer.array()));
	}

}
