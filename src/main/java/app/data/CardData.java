package app.data;

public class CardData{

/**
 *角色ID
 */
 private int cardId;

/**
 *角色base编号
 */
 private int cardBaseId;

/**
 *稀有度
 */
 private int rare;

/**
 *被动能力
 */
 private int[] passiveSkill;

/**
 *同名获得突破道具ID
 */
 private int AdvanceID;

/**
 *同名获得突破道具数量
 */
 private int AdvanceValue;

/**
 * 设置角色ID
 * @param cardId 角色ID
 */
 public void setCardId(int cardId){
     this.cardId = cardId;
 }

/**
 * 获得角色ID
 * @return cardId 角色ID
 */
 public int getCardId(){
     return cardId;
 }

/**
 * 设置角色base编号
 * @param cardBaseId 角色base编号
 */
 public void setCardBaseId(int cardBaseId){
     this.cardBaseId = cardBaseId;
 }

/**
 * 获得角色base编号
 * @return cardBaseId 角色base编号
 */
 public int getCardBaseId(){
     return cardBaseId;
 }

/**
 * 设置稀有度
 * @param rare 稀有度
 */
 public void setRare(int rare){
     this.rare = rare;
 }

/**
 * 获得稀有度
 * @return rare 稀有度
 */
 public int getRare(){
     return rare;
 }

/**
 * 设置被动能力
 * @param passiveSkill 被动能力
 */
 public void setPassiveSkill(int[] passiveSkill){
     this.passiveSkill = passiveSkill;
 }

/**
 * 获得被动能力
 * @return passiveSkill 被动能力
 */
 public int[] getPassiveSkill(){
     return passiveSkill;
 }

/**
 * 设置同名获得突破道具ID
 * @param AdvanceID 同名获得突破道具ID
 */
 public void setAdvanceID(int AdvanceID){
     this.AdvanceID = AdvanceID;
 }

/**
 * 获得同名获得突破道具ID
 * @return AdvanceID 同名获得突破道具ID
 */
 public int getAdvanceID(){
     return AdvanceID;
 }

/**
 * 设置同名获得突破道具数量
 * @param AdvanceValue 同名获得突破道具数量
 */
 public void setAdvanceValue(int AdvanceValue){
     this.AdvanceValue = AdvanceValue;
 }

/**
 * 获得同名获得突破道具数量
 * @return AdvanceValue 同名获得突破道具数量
 */
 public int getAdvanceValue(){
     return AdvanceValue;
 }

}