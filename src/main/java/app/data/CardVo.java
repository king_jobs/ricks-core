package app.data;

import java.io.Serializable;

public class CardVo implements Serializable {

    private int cardId;

    private String cardName;

    private boolean isMe;

    public CardVo() {
    }

    public CardVo(int cardId, String cardName, boolean isMe) {
        this.cardId = cardId;
        this.cardName = cardName;
        this.isMe = isMe;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getCardName() {
        return cardName;
    }

    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    public boolean isMe() {
        return isMe;
    }

    public void setMe(boolean me) {
        isMe = me;
    }
}
