package app.data.context;

import app.data.CardData;
import org.ricks.ioc.Autowired;
import org.ricks.ioc.Bean;
import org.ricks.ioc.Init;
import org.ricks.common.lang.Logger;
import org.ricks.net.http.web.TemplateManager;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ricks
 * @date 2022/9/616:55
 */
@Bean
public class DataContext {

    @Autowired
    private TemplateManager webTemplate;

    private static Map<Integer, CardData> cardContext = new HashMap();

    @Init
    public void init() {
//        cardContext = DataAgent.getBeanMap(CardData.class);
        cardContext.forEach((k,v) -> Logger.info("cardId: "+v.getCardId()+" and cardBaseId: "+ v.getCardBaseId()));
    }

    public static CardData getCard(int cardId) {
        return cardContext.get(cardId);
    }

}
