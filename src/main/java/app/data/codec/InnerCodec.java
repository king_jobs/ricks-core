package app.data.codec;

import org.ricks.net.codec.DataCodec;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;

/**
 * @author ricks
 * @Description:
 * @date 2023/2/1317:48
 */
public class InnerCodec implements DataCodec {
    @Override
    public byte[] encode(Object data) {
        if(data instanceof IPacket packet) {
            ByteBuf buf = new ByteBuf(1024);
            ProtocolManager.write(buf,packet);
            return buf.toArray();
        }
        return null;
    }

    @Override
    public <T> T decode(byte[] data, Class<?> dataClass) {
        ByteBuf buf = new ByteBuf(data);
        return (T) ProtocolManager.read(buf);
    }
}
