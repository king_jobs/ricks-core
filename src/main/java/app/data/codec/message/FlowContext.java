package app.data.codec.message;

import org.ricks.ioc.Message;
import org.ricks.net.AioSession;
import org.ricks.net.Context;

import java.util.HashSet;

/**
 * @author ricks
 * @Description:这种结构体，cmd & byte[] 太过简单
 * IO-Game 返回客户端会带上状态码，错误信息（简化网络消息传输，不用带上错误信息，知道错误码就可以定位错误原因）。心跳包 业务包 区分，pb 直接解析所有信息。
 * 还有一种方式，自定义协议头。包体
 * @date 2021/5/2720:23
 */
public class FlowContext<T> extends Context<Short,byte[]> {

    private AioSession session;

    /** userId */
    private long userSessionId;
    private long userId;
    private short responseStatus;
    /** true 业务方法有异常 */
    private boolean error;
    private int packageId;
    private String groupId;
    private HashSet<Long> toUserIds;
    public FlowContext(AioSession session, short cmdId, byte[] data) {
        super(session,new Message(cmdId,data));
        this.session = session;
    }

    @Override
    public byte[] getMessage() {
        return super.getMessage();
    }

    @Override
    public void send (Message message) {
        session.send(message);
    }

    public AioSession getSession() {
        return session;
    }

    public long getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(long userSessionId) {
        this.userSessionId = userSessionId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public short getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(short responseStatus) {
        this.responseStatus = responseStatus;
    }

    public HashSet<Long> getToUserIds() {
        return toUserIds;
    }

    public void setToUserIds(HashSet<Long> toUserIds) {
        this.toUserIds = toUserIds;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * 如果登录成功绑定 userId
     * 登录失败 使用userSessionId ，拿到userSession 进行推送消息
     * @return
     */
    public boolean checkBindUserId() {
        return userId > 0;
    }
}
