package app.data.codec.message;

/**
 * 消息类型
 */
public class DataConstant {
    /**
     * 内部路由
     */
    public static byte RUTE_MESSAGE = 0;
    /**
     * 玩家列表空，推送个人
     */
    public static byte BROADCAST_MESSAGE = 1;
    /**
     * 房间推送
     */
    public static byte BROADCAST_GROUP_MESSAGE = 2;
    /**
     * 全服推送
     */
    public static byte BROADCAST_ALL_MESSAGE = 3;

}
