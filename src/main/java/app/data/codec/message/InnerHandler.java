package app.data.codec.message;

import org.ricks.common.exception.ServerBootstrapException;
import org.ricks.ioc.manager.ActionMethodManager;
import org.ricks.ioc.wrap.ActionMethodWrapper;
import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;
import org.ricks.net.handler.AbstractMessageProcessor;

/**
 * @author chenwei
 * @Description: 玩家消息 处理handler
 * 1、维护玩家session
 * 2、维护不同模块节点session
 * 3、转发给其它模块节点
 * 4、转发给玩家 （to 1, to N, to all ）
 * @date 2023/2/1510:40
 */
public class InnerHandler extends AbstractMessageProcessor<Object> {

    @Override
    public void process0(AioSession session, Object obj) {
        /**
         * broker玩家消息 处理handler 不需要玩家路由 action
         * 直接cmd 进行转发模块，避免功能开发繁琐
         * 需要根据cmd 做不同的处理
         * 尽量简化 broker 指令 职责，做好一次开发以后尽量不用动
         */
        if(obj instanceof InnerMessage msg) {
//            if (msg.state == DataConstant.RUTE_MESSAGE) {
//                ActionMethodWrapper methodDefinition = ActionMethodManager.getActionMethod(msg.cmd);
//
//                FlowContext flowContext = new FlowContext(session, msg.cmd, msg.data);
//                flowContext.setUserId(msg.userId);
//                flowContext.setPackageId(msg.packageId);
//                flowContext.setUserSessionId(msg.userSessionId);
//                flowContext.setGroupId(msg.groupId);
//                flowContext.setToUserIds(msg.toUserIds);
//                methodDefinition.invoke(flowContext);
//            } else {
//                ExternalKit.broadcast(msg); //广播消息
//            }

            ActionMethodWrapper methodDefinition = ActionMethodManager.getActionMethod(msg.cmd);

            FlowContext flowContext = new FlowContext(session, msg.cmd, msg.data);
            flowContext.setUserId(msg.userId);
            flowContext.setPackageId(msg.packageId);
            flowContext.setUserSessionId(msg.userSessionId);
            flowContext.setGroupId(msg.groupId);
            flowContext.setToUserIds(msg.toUserIds);
            methodDefinition.invoke(flowContext);
        } else {
            throw new ServerBootstrapException("未知消息类型，obj:" + obj);
        }
    }


    @Override
    public void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {
        switch (stateMachineEnum) {
            case NEW_SESSION: {
                Logger.info(" create session.......");
                break;
            }
            case PROCESS_EXCEPTION:
                Logger.error("process exception", throwable);
//                session.close();
                break;
            case SESSION_CLOSED:
                Logger.info(" close session......." );
                break;
            case DECODE_EXCEPTION: {
                Logger.warn("aio decode exception,", throwable);
                break;
            }
        }
    }
}
