package app.data.codec.message;

import org.ricks.ioc.Message;
import org.ricks.net.AioSession;
import org.ricks.net.Context;
import org.ricks.protocol.Protocol;
import java.util.HashSet;


@Protocol(id = 50)
public final class InnerMessage {
    public byte state;
    public short cmd;
    public long userId;
    public long userSessionId;
    public int packageId;
    public short responseStatus;
    public byte[] data;
    public String groupId;
    public HashSet<Long> toUserIds;


    public byte getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public Short getCmd() {
        return cmd;
    }

    public void setCmd(short cmd) {
        this.cmd = cmd;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserSessionId() {
        return userSessionId;
    }

    public void setUserSessionId(long userSessionId) {
        this.userSessionId = userSessionId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public short getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(short responseStatus) {
        this.responseStatus = responseStatus;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public HashSet<Long> getToUserIds() {
        return toUserIds;
    }

    public void setToUserIds(HashSet<Long> toUserIds) {
        this.toUserIds = toUserIds;
    }

    @Override
    public String toString() {
        return "{ "
                + "state:" + state + ","
                + "cmd:" + cmd + ","
                + "userId:" + userId + ","
                + "userSessionId:" + userSessionId + ","
                + "packageId:" + packageId + ","
                + "responseStatus:" + responseStatus + ","
                + "data:" + data + ","
                + "groupId:" + groupId + ","
                + "toUserIds:" + toUserIds + ","
                + "}";
    }
}
