package app.data.codec.message;

import java.io.Serial;

/**
 * 请求参数
 *
 * @author 渔民小镇
 * @date 2021-12-20
 */
public sealed class RequestMessage extends BarMessage permits SyncRequestMessage {
    @Serial
    private static final long serialVersionUID = 8564408386704453534L;

    /** 客户端对应收发包用的 */
    private int packageId;

    public ResponseMessage createResponseMessage() {
        ResponseMessage responseMessage = new ResponseMessage();

        this.settingCommonAttr(responseMessage);

        return responseMessage;
    }

    public void settingCommonAttr(final ResponseMessage responseMessage) {
        // response 与 request 使用的 headMetadata 为同一引用
        responseMessage.setHeadMetadata(this.headMetadata);
    }

    /**
     * 设置自身属性到 request 中
     *
     * @param requestMessage request
     */
    public void copyTo(RequestMessage requestMessage) {
        requestMessage.responseStatus = this.responseStatus;
        requestMessage.validatorMsg = this.validatorMsg;
        requestMessage.headMetadata = this.headMetadata;
        requestMessage.dataClass = this.dataClass;
        requestMessage.data = this.data;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }
}
