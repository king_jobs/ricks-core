package app.data.codec.message;


/**
 * 对外服数据协议
 */
public class ExternalMessage {

    /** 业务路由  默认0：心跳指令*/
    private short cmd;

    /** 客户端对应收发包用的 */
    private int packageId;

    /**
     * 响应码。
     * <pre>
     *     从字段精简的角度，我们不可能每次响应都带上完整的异常信息给客户端排查问题，
     *     因此，我们会定义一些响应码，通过编号进行网络传输，方便客户端定位问题。
     *
     *     0:成功
     *     !=0: 表示有错误
     * </pre>
     */
    private short responseStatus;

    private byte[] data;

    public ExternalMessage() {
    }

    public ExternalMessage(short cmd, byte[] data) {
        this.cmd = cmd;
        this.data = data;
    }

    /**
     * 业务数据
     *
     * @param data 业务数据
     */
    public void setData(byte[] data) {
        if (data != null) {
            this.data = data;
        }
    }

    public short getCmd() {
        return cmd;
    }

    public void setCmd(short cmd) {
        this.cmd = cmd;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public short getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(short responseStatus) {
        this.responseStatus = responseStatus;
    }

    public byte[] getData() {
        return data;
    }
}
