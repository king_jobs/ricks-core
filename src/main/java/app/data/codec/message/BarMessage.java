package app.data.codec.message;

import app.context.MsgExceptionInfo;
import org.ricks.net.DataCodecKit;

import java.io.Serial;
import java.io.Serializable;

/**
 * 消息基类
 *
 * @author 渔民小镇
 * @date 2021-12-20
 */
public abstract sealed class BarMessage implements Serializable permits RequestMessage, ResponseMessage {
    @Serial
    private static final long serialVersionUID = 562068269463876111L;

    /** 响应码: 0:成功, 其他表示有错误 */
    protected short responseStatus;
    /** 异常信息、JSR380 验证信息 */
    protected String validatorMsg;

    /** 元信息 */
    protected HeadMetadata headMetadata;

    /**
     * 业务数据的 class 信息
     */
    protected String dataClass;
    /** 实际请求的业务参数 byte[] */
    protected byte[] data;

    public BarMessage setData(byte[] data) {
        this.data = data;
        return this;
    }

    public BarMessage setData(Object data) {
        // 保存一下业务数据的 class
        this.dataClass = data.getClass().getName();

        byte[] bytes = DataCodecKit.encode(data);
        return this.setData(bytes);
    }

    /**
     * 设置验证的错误信息
     *
     * @param validatorMsg 错误信息
     * @return this
     */
    public BarMessage setValidatorMsg(String validatorMsg) {
        if (validatorMsg != null) {
            this.validatorMsg = validatorMsg;
        }

        return this;
    }

    public BarMessage setError(MsgExceptionInfo msgExceptionInfo) {
        this.responseStatus = msgExceptionInfo.getCode();
        this.validatorMsg = msgExceptionInfo.getMsg();
        return this;
    }

    /**
     * 是否有错误
     * <pre>
     *     this.errorCode != 0 表示有错误
     * </pre>
     *
     * @return true 有错误码
     */
    public boolean hasError() {
        return this.responseStatus != 0;
    }

    public short getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(short responseStatus) {
        this.responseStatus = responseStatus;
    }

    public String getValidatorMsg() {
        return validatorMsg;
    }

    public HeadMetadata getHeadMetadata() {
        return headMetadata;
    }

    public void setHeadMetadata(HeadMetadata headMetadata) {
        this.headMetadata = headMetadata;
    }

    public String getDataClass() {
        return dataClass;
    }

    public void setDataClass(String dataClass) {
        this.dataClass = dataClass;
    }

    public byte[] getData() {
        return data;
    }
}
