package app.data.codec.message;

import org.ricks.net.DataCodecKit;

import java.util.HashSet;


/**
 * @author chenwei
 * @Description: 对外
 * @date 2023/2/1015:49
 */
public class ExternalKit {

    public static InnerMessage convertInnerMessage(GamePack message) {

        return createInnerMessage(message.getCmd(), message.getData());
    }

    /**
     * 创建请求消息
     *
     * @param data     业务数据
     * @return 请求消息
     */
    public static InnerMessage createInnerMessage(short cmd, Object data) {
        byte[] bytes = null;

        if (data != null) {
            bytes = DataCodecKit.encode(data);
        }

        return createInnerMessage(cmd, bytes);
    }

    /**
     * 创建请求消息
     *
     * @return 请求消息
     */
    public static InnerMessage createInnerMessage(short cmd) {
        return createInnerMessage(cmd, new byte[0]);
    }

    /**
     * 创建请求消息
     *
     * @param data     业务数据 byte[]
     * @return 请求消息
     */
    public static InnerMessage createInnerMessage(short cmd, byte[] data) {

        // 请求
        InnerMessage innerMessage = new InnerMessage();
        innerMessage.cmd = cmd;
        innerMessage.data = data;
        innerMessage.state = DataConstant.RUTE_MESSAGE;
        innerMessage.toUserIds = new HashSet<>();
        innerMessage.groupId = "";
        return innerMessage;
    }

    public static GamePack convertNetPack(InnerMessage innerMessage) {

        // 游戏框架内置的协议， 与游戏前端相互通讯的协议
        GamePack gamePack = new GamePack(innerMessage.cmd,innerMessage.data);
        // 状态码
        gamePack.setResponseStatus(innerMessage.responseStatus);
        gamePack.setPackageId(innerMessage.packageId);
        return gamePack;
    }

    public static GamePack createNetPack(short cmd) {
       return new GamePack(cmd,new byte[0]);
    }

    public static GamePack createNetPack(short cmd, Object object) {
        byte[] data = null;

        if (object != null) {
            data = DataCodecKit.encode(object);
        }
        return createNetPack(cmd,data);
    }

    public static GamePack createNetPack(short cmd, byte[] data) {
        // 游戏框架内置的协议， 与游戏前端相互通讯的协议
        return new GamePack(cmd,data);
    }




    /**
     * 全服推送 指定玩家推送 个人推送
     * 三种推送给玩家的方式
     * @param message
     */
    public static void broadcast(InnerMessage message) {
        GamePack gamePack = ExternalKit.convertNetPack(message);


        // 推送消息给单个真实用户
        long userSessionId = message.userSessionId;
//        ExternalKit.writeAndFlush(userSessionId, gamePack);
    }


}
