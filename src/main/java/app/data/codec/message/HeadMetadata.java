package app.data.codec.message;

import java.io.Serializable;

/**
 * 元信息
 * 内部转发
 */
public final class HeadMetadata implements Serializable {

    /** userId */
    protected long userId;

    protected short cmdId;

    /**
     * 来源逻辑服 client id
     * <pre>
     *     比如是对外服发起的请求，这个来源就是对外服的 clientId
     *     clientId 指的是 服务器的唯一id
     * </pre>
     */
    protected int sourceClientId;

    /**
     * 目标逻辑服 endPointClientId
     * 比如 小游戏打完发奖励，通知具体的逻辑服进行处理
     * 玩家逻辑服登录 就会和 userId 就会和 逻辑服的serverId 进行绑定，玩家退出游戏之后就会解除绑定
     */
    protected int endPointClientId;


    /**
     * 扩展字段
     * <pre>
     *     开发者有特殊业务可以通过这个字段来扩展元信息，该字段的信息会跟随每一个请求；
     * </pre>
     */
    protected byte[] attachmentData;


    public HeadMetadata(short cmdId) {
        this.cmdId = cmdId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public short getCmdId() {
        return cmdId;
    }

    public void setCmdId(short cmdId) {
        this.cmdId = cmdId;
    }

    public int getEndPointClientId() {
        return endPointClientId;
    }

    public void setEndPointClientId(int endPointClientId) {
        this.endPointClientId = endPointClientId;
    }

    public byte[] getAttachmentData() {
        return attachmentData;
    }

    public void setAttachmentData(byte[] attachmentData) {
        this.attachmentData = attachmentData;
    }

    public int getSourceClientId() {
        return sourceClientId;
    }

    public void setSourceClientId(int sourceClientId) {
        this.sourceClientId = sourceClientId;
    }
}
