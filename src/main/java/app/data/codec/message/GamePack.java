package app.data.codec.message;

import org.ricks.ioc.Message;

/**
 * 协议包体
 */
public class GamePack extends Message<Short,byte[]> {

    private int packageId;

    public short responseStatus;

    public GamePack(Short cmd, byte[] data, int packageId) {
        super(cmd, data);
        this.packageId = packageId;
    }

    public GamePack(Short cmd, byte[] data, int packageId, short responseStatus) {
        super(cmd, data);
        this.packageId = packageId;
        this.responseStatus = responseStatus;
    }

    public GamePack(Short cmd, byte[] data) {
        super(cmd, data);
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public short getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(short responseStatus) {
        this.responseStatus = responseStatus;
    }
}
