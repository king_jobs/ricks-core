//package app.data.codec;
//
//import app.data.codec.external.ExternalKit;
//import app.data.codec.message.BarMessage;
//import app.data.codec.message.FlowContext;
//import app.data.codec.message.RequestMessage;
//import app.data.codec.message.ResponseMessage;
//import org.ricks.ioc.manager.ActionMethodManager;
//import org.ricks.ioc.wrap.ActionMethodWrapper;
//import org.ricks.lang.Logger;
//import org.ricks.net.AioSession;
//import org.ricks.net.Context;
//import org.ricks.net.StateMachineEnum;
//import org.ricks.net.action.ActionErrorEnum;
//import org.ricks.net.action.MsgException;
//import org.ricks.net.buffer.WriteBuffer;
//import org.ricks.net.handler.AbstractMessageProcessor;
//import org.ricks.net.handler.TcpAioSession;
//
//import java.io.IOException;
//import java.util.Objects;
//
///**
// * @author ricks
// * @Description:默认消息处理器
// * 目前方面asm动态生成class switch(cmd) case ....  case ....
// * todo 调整为asm 反射 和 直接调用区别不大。
// * 默认handler asm反射机制
// * @date 2022/12/2913:29
// */
//public class DefaultHandler extends AbstractMessageProcessor<BarMessage> {
//
//    @Override
//    public void process0(AioSession session, BarMessage msg) {
//        if(msg instanceof ResponseMessage responseMessage) {
//            /**
//             * broker 收到其它业务节点ResponseMessage，直接写回玩家
//             */
//            long userId = responseMessage.getHeadMetadata().getUserId();
//            ExternalKit.writeAndFlush(userId, ExternalKit.convertExternalMessage(responseMessage));
//        } else if(msg instanceof RequestMessage requestMessage){
//            /**
//             * 其它业务节点收到 broker 请求
//             */
//            ActionMethodWrapper methodDefinition = ActionMethodManager.getActionMethod(msg.getHeadMetadata().getCmdId());
//            FlowContext context = new FlowContext(session, requestMessage);
//            Object methodResult;
//            if (Objects.isNull(methodDefinition)) {
//                context.setError(true);
//                methodResult = new MsgException(ActionErrorEnum.cmdInfoErrorCode);
//            } else {
//                methodResult = invoke(context, methodDefinition);
//            }
//            context.setMethodResult(methodResult);
//            wrap(context);
//            actionAfter(context, methodDefinition);
//        }
//    }
//
//    /**
//     * 执行action method ，铺抓异常
//     * @param context
//     * @param methodDefinition
//     * @return
//     */
//    private Object invoke(final FlowContext context,ActionMethodWrapper methodDefinition) {
//        try {
//            return methodDefinition.invoke(context);
//        } catch (Throwable e) {
//            context.setError(true);
//            if (e instanceof MsgException msgException) {
//                Logger.error("路由出现内部错误 cmd:{} ,内部错误 error code:{} and case:{}",context.getCmd(),msgException.getMsgCode(),msgException.getMessage());
//                return msgException;
//            }
//
//            // 到这里，一般不是用户自定义的错误，很可能是开发者引入的第三方包或自身未捕获的错误等情况
//            Logger.error(e.getMessage(), e);
//
//            return new MsgException(ActionErrorEnum.systemOtherErrCode);
//        }
//    }
//
//    private void wrap(final FlowContext context) {
//        final ResponseMessage responseMessage = context.getResponse();
//        // 业务方法的返回值
//        final Object result = context.getMethodResult();
//
//        // 如果有异常错误，异常处理
//        if (context.isError()) {
//
//            MsgException msgException = (MsgException) result;
//            short code = msgException.getMsgCode();
//            responseMessage.setResponseStatus(code);
//            return;
//        }
//
//        // 业务方法返回值
//        if (Objects.nonNull(result)) {
//            responseMessage.setData(result);
//        }
//    }
//
//    /**
//     * 仅作用于 broker <-->  server  单通信
//     * 比如 聊天是需要广播给所有的网关节点
//     * 比如 网关 需要使用 ExternalKit.broadcast(BroadcastMessage) 进行推送给玩家
//     * @param context
//     * @param methodDefinition
//     */
//    private void actionAfter(FlowContext context, ActionMethodWrapper methodDefinition) {
//        final ResponseMessage response = context.getResponse();
//
//        AioSession session = context.getSession();
//
//        if (Objects.isNull(session)) {
//            return;
//        }
//
//        // 有错误就响应给调用方
//        if (response.hasError()) {
//            session.sendResponse(response);
//            return;
//        }
//
//        // action 方法返回值是 void 的，不做处理
//        if (methodDefinition.isVoid()) {
//            return;
//        }
//
//        // 将数据回传给调用方
//        session.sendResponse(response);
//    }
//
//    @Override
//    public void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {
//        switch (stateMachineEnum) {
//            case NEW_SESSION: {
//                //如果是tcp session 写回客户端，唯一标识给kcp做convId
////                if(session instanceof TcpAioSession tcpAioSession) {
////                    sessionIdToClient(tcpAioSession);
////                }
//                Logger.debug(" create session.......");
//                break;
//            }
//            case PROCESS_EXCEPTION:
//                Logger.error("process exception", throwable);
////                session.close();
//                break;
//            case SESSION_CLOSED:
//                Logger.debug(" close session.......");
//                break;
//            case DECODE_EXCEPTION: {
//                Logger.warn("http decode exception,", throwable);
//                break;
//            }
//        }
//    }
//
//    private void sessionIdToClient(TcpAioSession session) {
//        WriteBuffer writeBuffer = session.writeBuffer();
//        try {
//            writeBuffer.writeLong(session.getSessionID());
//            writeBuffer.flush();
//        } catch (IOException e) {
//            Logger.error(" SessionId 写回客户端异常, case:{}",e.getMessage());
//        }
//    }
//}
