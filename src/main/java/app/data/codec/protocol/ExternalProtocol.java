package app.data.codec.protocol;

import app.data.codec.external.ExternalKit;
import app.data.codec.message.ExternalMessage;
import app.data.codec.message.RequestMessage;
import org.ricks.net.AioSession;
import org.ricks.net.DataCodecKit;
import org.ricks.net.protocol.FixedLengthBytesProtocol;


/**
 * @author ricks
 * @Description: client <->  broker 字节流解析 ，broker解析
 * @date 2023/2/1318:14
 */
public class ExternalProtocol extends FixedLengthBytesProtocol<RequestMessage> {

    @Override
    protected RequestMessage decode(byte[] bytes, AioSession session) {
        ExternalMessage message =  DataCodecKit.decode(bytes, ExternalMessage.class);
        // 是否可以访问业务方法（action），true 表示可以访问该路由对应的业务方法

        // 当访问验证没通过，通知玩家

        // 将 message 转换成 RequestMessage
        return ExternalKit.convertRequestMessage(message);
    }
}
