package app.data.codec.protocol;

import app.data.codec.message.InnerMessage;
import org.ricks.net.AioSession;
import org.ricks.net.protocol.FixedLengthBytesProtocol;

import java.nio.ByteBuffer;


/**
 * @author chenwei
 * @Description: client <->  broker 字节流解析
 * @date 2023/2/1318:14
 */
public class TcpInnerProtocol extends FixedLengthBytesProtocol<Object> {


//    @Override
//    public Object decode(ByteBuffer readBuffer, AioSession session) throws IOException {
//        short cmd = readBuffer.getShort();
//        int packageId = readBuffer.getInt();
////        Logger.info("KCP 解析出来的包序号 packageId:" + packageId + " 收到包总数量：" + innerMessages.size());
//        byte[] data = new byte[readBuffer.remaining()];
//        readBuffer.get(data);
//        InnerMessage message = new InnerMessage();
//        message.cmd = cmd;
//        message.packageId = packageId;
//
//        return message;
//    }

    @Override
    protected Object decode(byte[] bytes, AioSession session) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        short cmd = buffer.getShort();
        int packageId = buffer.getInt();
//        Logger.info("KCP 解析出来的包序号 packageId:" + packageId + " 收到包总数量：" + innerMessages.size());
        byte[] data = new byte[buffer.remaining()];
        buffer.get(data);
        InnerMessage message = new InnerMessage();
        message.cmd = cmd;
        message.packageId = packageId;

        return message;
    }
}
