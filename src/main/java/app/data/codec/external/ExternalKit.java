package app.data.codec.external;

import app.data.codec.message.ExternalMessage;
import app.data.codec.message.HeadMetadata;
import app.data.codec.message.RequestMessage;
import app.data.codec.message.ResponseMessage;
import app.session.UserSession;
import app.session.UserSessions;
import org.ricks.net.AioSession;
import org.ricks.net.DataCodecKit;

import java.util.Collection;
import java.util.Objects;

/**
 * @author ricks
 * @Description: 对外
 * @date 2023/2/1015:49
 */
public class ExternalKit {

    public static RequestMessage convertRequestMessage(ExternalMessage message) {

        short cmd = message.getCmd();
        byte[] data = message.getData();

        return createRequestMessage(cmd, data);
    }

    /**
     * 创建请求消息
     *
     * @param data     业务数据
     * @return 请求消息
     */
    public static RequestMessage createRequestMessage(short cmd, Object data) {
        byte[] bytes = null;

        if (data != null) {
            bytes = DataCodecKit.encode(data);
        }

        return createRequestMessage(cmd, bytes);
    }

    /**
     * 创建请求消息
     *
     * @return 请求消息
     */
    public static RequestMessage createRequestMessage(short cmd) {
        return createRequestMessage(cmd, null);
    }

    /**
     * 创建请求消息
     *
     * @param data     业务数据 byte[]
     * @return 请求消息
     */
    public static RequestMessage createRequestMessage(short cmd, byte[] data) {

        // 元信息
        HeadMetadata headMetadata = new HeadMetadata(cmd);

        // 请求
        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setHeadMetadata(headMetadata);

        requestMessage.setData(data);

        return requestMessage;
    }

    public static ExternalMessage convertExternalMessage(ResponseMessage responseMessage) {
        HeadMetadata headMetadata = responseMessage.getHeadMetadata();

        // 路由
        short cmdId = headMetadata.getCmdId();
        // 业务数据
        byte[] data = responseMessage.getData();

        // 游戏框架内置的协议， 与游戏前端相互通讯的协议
        ExternalMessage externalMessage = new ExternalMessage();
        externalMessage.setCmd(cmdId);
        externalMessage.setData(data);
        // 状态码
        externalMessage.setResponseStatus(responseMessage.getResponseStatus());

        return externalMessage;
    }

    public static ExternalMessage createExternalMessage(short cmd) {
        ExternalMessage externalMessage = new ExternalMessage();
        externalMessage.setCmd(cmd);
        return externalMessage;
    }

    public static ExternalMessage createExternalMessage(short cmd, Object object) {
        byte[] data = null;

        if (object != null) {
            data = DataCodecKit.encode(object);
        }

        return new ExternalMessage(cmd, data);
    }

    public static ExternalMessage createExternalMessage(short cmd, byte[] data) {
        // 游戏框架内置的协议， 与游戏前端相互通讯的协议
        ExternalMessage externalMessage = ExternalKit.createExternalMessage(cmd);

        // 业务数据
        externalMessage.setData(data);

        return externalMessage;
    }

    public static void writeAndFlush(long userId, ExternalMessage message) {

        if (!UserSessions.me().existUserSession(userId)) {
            return;
        }

        UserSession userSession = UserSessions.me().getUserSession(userId);

        AioSession channel = userSession.getAioSession();

//        channel.sendResponse(message);
    }

    /**
     * 全服推送 指定玩家推送 个人推送
     * 三种推送给玩家的方式
     * @param message
     */
    public static void broadcast(BroadcastMessage message) {
        ResponseMessage responseMessage = message.getResponseMessage();
        ExternalMessage externalMessage = ExternalKit.convertExternalMessage(responseMessage);
        // 推送消息给全服真实用户
        if (message.isBroadcastAll()) {
            // 给全体推送
            UserSessions.me().broadcast(externalMessage);
            return;
        }
        // 推送消息给指定的真实用户列表
        Collection<Long> userIdList = message.getUserIdList();
        if (Objects.nonNull(userIdList)) {
            for (Long userId : userIdList) {
                ExternalKit.writeAndFlush(userId, externalMessage);
            }

            return;
        }

        // 推送消息给单个真实用户
        HeadMetadata headMetadata = responseMessage.getHeadMetadata();
        long userId = headMetadata.getUserId();
        ExternalKit.writeAndFlush(userId, externalMessage);
    }
}
