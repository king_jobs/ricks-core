package app.data.codec.external;

import app.data.codec.message.ResponseMessage;
import org.ricks.common.lang.Assert;
import org.ricks.common.CollUtil;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;

/**
 * 广播消息
 */
public class BroadcastMessage implements Serializable {
    @Serial
    private static final long serialVersionUID = -8781053474740658678L;

    /** 广播的消息 */
    private ResponseMessage responseMessage;
    /** 接收广播的用户列表 */
    private Collection<Long> userIdList;
    /** true 给全体广播 */
    private boolean broadcastAll;

    public ResponseMessage getResponseMessage() {
        return responseMessage;
    }

    public BroadcastMessage setResponseMessage(ResponseMessage responseMessage) {
        this.responseMessage = responseMessage;
        return this;
    }

    public Collection<Long> getUserIdList() {
        return userIdList;
    }

    public BroadcastMessage setUserIdList(Collection<Long> userIdList) {
        Assert.isFalse(broadcastAll," Broadcast to ALL ");
        this.userIdList = userIdList;
        return this;
    }

    public boolean isBroadcastAll() {
        return broadcastAll;
    }

    public BroadcastMessage setBroadcastAll(boolean broadcastAll) {
        Assert.isFalse(CollUtil.isEmpty(userIdList)," Broadcast to userIds ", userIdList.toString());
        this.broadcastAll = broadcastAll;
        return this;
    }
}
