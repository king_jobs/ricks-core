package app.http.vo;

import org.ricks.orm.BaseLongIDEntity;
import org.ricks.orm.anno.Column;
import org.ricks.orm.anno.IndexKey;
import org.ricks.orm.anno.Table;
import org.ricks.orm.repository.UniqueCacheRepository;

@Table(name = "test_user_vo",repository = UniqueCacheRepository.class)
public class UserVo extends BaseLongIDEntity<UserVo> {

    @Column
    @IndexKey
    private String username;

    @Column
    @IndexKey
    private String password;

    @Column
    private long createTime;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "UserVo{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
