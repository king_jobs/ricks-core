package app.protocol;

import java.util.Map;

public record ObjectA(

    int a,

    Map<Integer, String> m,

    String s
){}

