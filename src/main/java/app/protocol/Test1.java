package app.protocol;

import org.ricks.common.MapUtils;

import java.util.HashMap;

public class Test1 {

    public static void main(String[] args) {
        Test test = new Test(1,"ricks");

        User user = new User(2, "ricks is god !");

        System.err.println(test.age());
        System.err.println(test.name());

        System.err.println(user);

        HashMap map = MapUtils.newHashMap();

        map.put("1111", 11111);
        map.put(2222,"22222");
        map.put(true, "ricks");

        ObjectA objectA = new ObjectA(99,map,"AAAAA");
        System.err.println(objectA);
    }
}

record User(int a, String b) {
}