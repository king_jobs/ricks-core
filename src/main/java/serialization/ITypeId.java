package serialization;

public interface ITypeId {
    int getTypeId();
}
