package org.ricks;

import org.ricks.common.actor.Actors;
import org.ricks.ioc.Ioc;

/**
 * @author ricks
 * @Description:
 * @date 2023/1/1113:59
 */
public class StopHook extends Thread{

    private final Ioc ioc;

    public StopHook(Ioc ioc) {
        // 修正停服逻辑线程名称
        super("stop");
        this.ioc = ioc;
    }

    @Override
    public void run() {
        this.ioc.stop();
        Actors.me().destroy();
    }
}
