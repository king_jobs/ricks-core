package org.ricks.schedule;

import org.ricks.ioc.RicksMethod;
import org.ricks.common.StrUtil;
import java.lang.annotation.*;

/**
 * Scheduled注解用来标识一个可延迟执行的方法.
 * <p>
 * 1. 延迟多久，间隔多久执行指定方法<br>
 * 2. CRON表达式的调度执行 <br>
 * 如果当前Controller线程组为玩家，当前标识的方法应该具有一个PlayerId的参数
 */
@Documented
@RicksMethod
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Scheduled {

    /**
     * 一种类似CRON的表达式。
     * <p>
     * 例cron="1/10 * * * * *" <br>
     * 如果设计表达式，下面两个参数将不在生效
     *
     * @return 可以解析为CRON调度的表达式
     */
    String cron() default StrUtil.EMPTY;

    /**
     * 首次延迟多长时间后再执行.
     *
     * @return 以毫秒为单位的周期
     */
    long initialDelay() default -1;

    /**
     * 以毫秒为单位的固定周期延迟执行，即执行开始时间为延迟开始时间.
     * <p>
     * 比如，5秒，10秒，15秒，20秒...
     *
     * @return 以毫秒为单位的周期
     */
    long fixedRate() default -1;
}