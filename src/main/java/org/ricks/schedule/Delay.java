package org.ricks.schedule;

import org.ricks.ioc.RicksMethod;

import java.lang.annotation.*;

@Documented
@RicksMethod
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Delay {

    long delay() default -1;
}
