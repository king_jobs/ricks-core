package org.ricks;

/**
 * 一个模块接口.
 *
 */
public interface Modular {


    /**
     * 模块初始化.
     */
    void init();

    /**
     * 模块销毁.
     */
    void destroy();
}