
package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * 用来标识一个配置文件类.
 * 可与{@link Value}来实现注入配置参数. <br>
 * 这种类也是只在启动时有用，相当于启动配置文件的作用.
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Configuration {

}