package org.ricks.ioc;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 用来标识初始化方法
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface RicksMethod {

}