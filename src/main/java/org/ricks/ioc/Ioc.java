package org.ricks.ioc;

import java.util.List;

/**
 * @author ricks
 * @Description:重构IOC 接口，class获取ioc对象
 * 工程全部都是事件
 * 日志 自定义网络协议 http协议 业务事件任务 定时任务 都可以包装成事件 投递给任务执行者
 * 任务执行者实际上就是 asm反射机制
 * IOC容器 初始化自定义bean注解，进行包装 以便事件使用
 * IOC 改版思路解析
 * @date 2023/1/1114:20
 */
public interface Ioc {
    /**
     * 从容器中获取一个对象。
     * <ul>
     * <li>如果定义了注解中name()属性，采用其值为注入名
     * <li>否则采用类型 simpleName 的首字母小写形式作为注入名
     * </ul>
     *
     * @param <T>   IOC对象类型
     * @param clazz IOC对象类型的Class对象
     * @return IOC对象
     */
    <T> T get(Class<T> clazz);

    <T> List<T> getBeans(Class<T> clazz);

    <T> T get(String beanId);

    <T> List<T> allBeans();


    void stop();

}
