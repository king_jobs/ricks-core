package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2020/6/2611:35
 */
@Documented
@RicksMethod
@Retention(RetentionPolicy.RUNTIME)
public @interface Bean {

    String value() default "";
}
