package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * @author ricks
 * @Description:游戏入口
 * @date 2020/6/2611:35
 */
@Bean
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Action {

}
