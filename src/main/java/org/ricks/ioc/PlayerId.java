
package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * RoleId注解表示此属性是否为游戏中的角色ID字段.
 * <p>
 * 可联合Id或Column一起使用.<br>
 * 网络层，就是注入角色ID,等于@Autowired("playerId")
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface PlayerId {
}