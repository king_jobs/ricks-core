package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * 用来标识初始化方法
 */
@Documented
@RicksMethod
@Retention(RetentionPolicy.RUNTIME)
public @interface Init {

}