
package org.ricks.ioc;

/**
 * 一个被IOC容器所管理的JavaBean定义描述类.
 */
public interface BeanDefinition {

    /**
     * 获取Bean的名称，默认为类全名.
     * @return Bean的名称
     */
    String getName();

    /**
     * 注入属性.
     * @param ioc 装配对象缓存
     */
    void injection(RicksIoc ioc);
}