
package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * Json注解用来标注需要把对象转成Json格式的属性.
 * <p>
 * 可配合Column一起使用. <br>
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Json {
    /**
     * 是否写入类名.
     * <p>
     * 默认不写入 <br>
     * <b>如果需要写入类名，注意重构时包目录和类名</b>
     *
     * @return 如果需要写入类名返回true, 否则返回false.
     */
    JsonStyle style() default JsonStyle.DefaultStyle;

    /**
     * @author 小流氓[176543888@qq.com]
     */
    public enum JsonStyle {
        /**
         * 这个没有任何意义，只是个默认值.
         */
        DefaultStyle,
        /**
         * 写入Class类名
         */
        WriteClassName;
    }
}