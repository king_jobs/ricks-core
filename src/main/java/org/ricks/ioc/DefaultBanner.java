package org.ricks.ioc;

import org.ricks.common.FileUtil;
import org.ricks.common.StrUtil;

/**
 * @author ricks
 * @Description:
 * @date 2021/3/815:15
 */
public class DefaultBanner implements Banner{

    private DefaultBootstrap bootstrap;
    public DefaultBanner(DefaultBootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public static String DEFAULT_BANNER_FILE = "banner.txt";

    @Override
    public void printBanner() {
        if(bootstrap.bannerMode == Banner.Mode.ON) {
            String bannerInfo = FileUtil.readFile(DEFAULT_BANNER_FILE);
            bannerInfo = StrUtil.isNotBlank(bannerInfo) ? bannerInfo : "not found banner.txt , load banner fail ！ ";
            System.out.println(bannerInfo);
        }
    }
}
