package org.ricks.ioc;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 用来标识销毁方法
 */
@Documented
@RicksMethod
@Retention(RetentionPolicy.RUNTIME)
public @interface Destroy {

}