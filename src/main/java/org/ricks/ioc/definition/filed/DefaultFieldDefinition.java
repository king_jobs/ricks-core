package org.ricks.ioc.definition.filed;

import org.ricks.ioc.FieldDefinition;
import org.ricks.ioc.RicksIoc;
import org.ricks.common.lang.Logger;

import java.lang.reflect.Field;

/**
 * 一个被IOC容器所管理的JavaBean定义描述类.
 */
public class DefaultFieldDefinition implements FieldDefinition {

    protected final Field field;
    protected final String beanName;
    protected final Class<?> fieldClass;

    public DefaultFieldDefinition(Field field, String beanName) {
        this(field, field.getType(), beanName);
    }

    protected DefaultFieldDefinition(Field field, Class<?> fieldClass, String beanName) {
        this.field = field;
        this.beanName = beanName;
        this.fieldClass = fieldClass;
        this.field.setAccessible(true);
    }

    @Override
    public void injection(Object single, RicksIoc ioc) {
        //获取属性的beanid
        Object bean = beanName.isBlank() ? ioc.get(fieldClass) : ioc.get(beanName);;
        try {
            field.set(single,bean);
        } catch (Exception e) {
            Logger.error("Bean "+ single +" IOC属性注入失败,fieldClass["+fieldClass+"] beanId["+beanName+"]");
        }
    }

}