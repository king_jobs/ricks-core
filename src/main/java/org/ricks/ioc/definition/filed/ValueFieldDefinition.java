
package org.ricks.ioc.definition.filed;

import org.ricks.common.conf.RicksConf;
import org.ricks.common.exception.ServerBootstrapException;
import org.ricks.ioc.RicksIoc;
import org.ricks.common.StrUtil;

import java.lang.reflect.Field;

/**
 * Value方式注入配置参数.
 */
public class ValueFieldDefinition extends DefaultFieldDefinition {
    /**
     * 对应配置文件中的Key...
     */
    private final String key;

    public ValueFieldDefinition(Field field, String key) {
        super(field,key);
        this.key = key;
    }

    /**
     * 配置参数注入，如果没找到则忽略，使用默认值...
     */
    @Override
    public void injection(Object single, RicksIoc ioc) {
        String value = RicksConf.get(key);
        if (StrUtil.isNotEmpty(value)) {
            try {
                field.setAccessible(true);
                field.set(single, conver(value,field.getType()));
            } catch (Exception e) {
                // 静态属性写入时Target会为null
                Class<?> klass = single == null ? field.getDeclaringClass() : single.getClass();
                throw new ServerBootstrapException(klass + " 的 " + field.getName() + " 属性无法注入.", e);
            }
        }
    }

    private Object conver(String value,Class clazz){
        Object object;
        switch (clazz.getName()) {
            case "byte" :
                object = Byte.valueOf(value);
                break;
            case "short" :
                object = Short.parseShort(value);
                break;
            case "int" :
                object = Integer.valueOf(value);
                break;
            case "float" :
                object = Float.parseFloat(value);
                break;
            case "long" :
                object = Long.parseLong(value);
                break;
            case "double" :
                object = Double.parseDouble(value);
                break;
            case "boolean" :
                object = Boolean.parseBoolean(value);
                break;
            default :
                object = String.valueOf(value);
                break;
        }
        return object;
    }
}