
package org.ricks.ioc.definition;

import org.ricks.common.asm.MethodAccess;
import org.ricks.ioc.Autowired;
import org.ricks.ioc.*;
import org.ricks.ioc.definition.filed.DefaultFieldDefinition;
import org.ricks.ioc.definition.filed.ValueFieldDefinition;
import org.ricks.ioc.definition.method.SimpleMethodDefinition;
import org.ricks.ioc.wrap.BaseMethodWrapper;
import org.ricks.common.AnnotationUtil;
import org.ricks.common.ClassUtil;
import org.ricks.common.StrUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * 默认的Bean定义描述类.
 * 准备反射使用
 */
public class DefaultBeanDefinition implements BeanDefinition {

    /**
     * 缓存那个单例对象
     */
    protected final Object bean;
    protected final MethodAccess methodAccess;
    protected final HashMap<Class<? extends Annotation>, List<MethodDefinition>> customMethods = new HashMap<>();

    private final Class<?> beanClass;

    private String beanName = StrUtil.EMPTY;

    private Annotation annotation;
    private Class<? extends Annotation> annotationType;
    

    /**
     * 所有需要注入的属性
     */
    private final ArrayList<FieldDefinition> autowiredFields = new ArrayList<>();

    public DefaultBeanDefinition(Class<?> klass,String defBeanId) {
        this(klass, ClassUtil.newInstance(klass),defBeanId);
    }

    public DefaultBeanDefinition(String beanName, Object object) {
        this(object.getClass(), object,beanName);
        this.beanName = beanName;
    }

    public DefaultBeanDefinition(Class<?> klass, Object object,String defBeanId) {
        this.bean = object;
        this.beanClass = klass;
        this.beanName = defBeanId.isBlank() ? StrUtil.lowerFirst(beanClass.getSimpleName()) : defBeanId;
        this.methodAccess = MethodAccess.get(beanClass);
    }

    public DefaultBeanDefinition init() {
        this.analysisField();
        this.analysisMethod();
        return this;
    }



    @Override
    public String getName() {
        return beanName;
    }

    /**
     * 获取这个Bean的单例缓存对象.
     *
     * @return 实例对象.
     */
    public Object getBean() {
        return bean;
    }

    /**
     * 获取当前Bean的Class
     *
     * @return 当前Bean的Class
     */
    public Class<?> getBeanClass() {
        return beanClass;
    }


    private void analysisMethod() {
        Method[] methods = beanClass.getMethods();
        for (Method method : methods) {
            Annotation[] annotations = method.getAnnotations();
            // 没有注解的忽略掉
            Arrays.stream(annotations).filter(a -> AnnotationUtil.hasAnnotation(a.annotationType(), RicksMethod.class)).forEach(annotation -> {
                final Class<? extends Annotation> annotationType = annotation.annotationType();
                analysisMethodByAnnotation(annotationType, annotation, method);
            });
        }
    }

    /**
     * 分析方法上的注解.
     *
     * @param annotationType 注解类型
     * @param annotation     注解对象
     * @param method         方法体
     */
    protected void analysisMethodByAnnotation(Class<? extends Annotation> annotationType, Annotation annotation, Method method) {
        customMethods.computeIfAbsent(annotationType, key -> new ArrayList<>(64)).add(new SimpleMethodDefinition(methodAccess, method));
    }

    private void analysisField() {
        List<Field> fieldList = new ArrayList<>();
        Class tempClass = beanClass;
        while (tempClass != null) {
            fieldList.addAll(Arrays.asList(tempClass.getDeclaredFields()));
            tempClass = tempClass.getSuperclass();
        }
        fieldList.stream().filter(v -> v.isAnnotationPresent(Autowired.class) || v.isAnnotationPresent(Value.class)).forEach(this::analysisAutowiredOrValue);
    }

    /**
     * 分析注入的类型
     */
    private void analysisAutowiredOrValue(Field field) {
        Value value = field.getAnnotation(Value.class);
        if (value == null) {
            Autowired autowired = field.getAnnotation(Autowired.class);
            // 其他就当普通Bean处理...
            autowiredFields.add(new DefaultFieldDefinition(field, autowired.value()));
        } else {
            autowiredFields.add(new ValueFieldDefinition(field, value.value())); // @Value注入配置属性.
        }
    }

    @Override
    public void injection(RicksIoc ioc) {
        this.autowiredFields.forEach((v) -> v.injection(bean, ioc));
    }

    /**
     * 分析此用的功能用途.
     *
     * @param ioc 容器
     */
    public void doAnalysisFunction(RicksIoc ioc) {
        // 有自定义的注解需要送回来IOC容器中.
        customMethods.forEach((k, list) -> list.forEach(v -> ioc.addCustomMethod(k, new BaseMethodWrapper(bean, v.getMethodAccess(), v.getMethodIndex()))));
    }

}