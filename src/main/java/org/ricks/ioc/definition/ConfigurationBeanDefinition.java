
package org.ricks.ioc.definition;

import org.ricks.ioc.*;
import org.ricks.ioc.definition.method.BeanMethodDefinition;
import org.ricks.common.AnnotationUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 启动配置Bean的定义.
 *
 */
public class ConfigurationBeanDefinition extends DefaultBeanDefinition {

    private final List<BeanMethodDefinition> beans;

    public ConfigurationBeanDefinition( Class<?> klass) {
        super( klass,"");
        this.beans = new ArrayList<>();
    }

    @Override
    public void injection(RicksIoc ioc) {
        super.injection(ioc);

        // 注入完属性，还要建构相关Bean.
        for (BeanMethodDefinition beanMethodDefinition : beans) {
            Object obj = beanMethodDefinition.getMethodAccess().invoke(bean, beanMethodDefinition.getMethodIndex());
            DefaultBeanDefinition beanDefinition = new DefaultBeanDefinition(obj.getClass(), obj,beanMethodDefinition.getBeanName()).init();
            Class[] returnClasses = beanMethodDefinition.getMethodAccess().getReturnTypes();
            ioc.add(beanDefinition, returnClasses[beanMethodDefinition.getMethodIndex()]);
        }
    }

    @Override
    protected void analysisMethodByAnnotation(Class<? extends Annotation> annotationType, Annotation annotation, Method method) {
        // 配置类中，只关心@Bean的注解方法，其他都忽略掉吧，没有什么意义...
        if (annotationType == Bean.class) {
            beans.add(new BeanMethodDefinition(methodAccess, method, (Bean) annotation));
        }
        // 其他的交给父类去处理
        else if(AnnotationUtil.hasAnnotation(method, RicksMethod.class)) {
            super.analysisMethodByAnnotation(annotationType, annotation, method);
        }
    }
}