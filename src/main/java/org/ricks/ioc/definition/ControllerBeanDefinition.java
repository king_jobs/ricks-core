package org.ricks.ioc.definition;

import org.ricks.common.event.EventHandler;
import org.ricks.ioc.Bean;
import org.ricks.ioc.RicksIoc;
import org.ricks.ioc.RicksMethod;
import org.ricks.ioc.definition.method.*;
import org.ricks.ioc.manager.*;
import org.ricks.ioc.wrap.*;
import org.ricks.net.ActionMethod;
import org.ricks.net.http.Controller;
import org.ricks.net.http.RequestMapping;
import org.ricks.net.rpc.RpcMethod;
import org.ricks.schedule.Scheduled;
import org.ricks.common.AnnotationUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * 控制器的Bean定义描述类.
 * IOC准备解析事件
 */
public class ControllerBeanDefinition extends DefaultBeanDefinition {


    private final ArrayList<SimpleMethodDefinition> pmds = new ArrayList<>();
    private final ArrayList<EventMethodDefinition> emds = new ArrayList<>();
    private final ArrayList<HttpMethodDefinition> hmds = new ArrayList<>();
    private final ArrayList<ScheduledMethodDefinition> smds = new ArrayList<>();
    private final ArrayList<ActionMethodDefinition> actions = new ArrayList<>(); //action
    private final ArrayList<RpcMethodDefinition> rmds = new ArrayList<>(); //rpc

    public ControllerBeanDefinition(Class<?> klass,Annotation annotation) {
        super(klass,  annotation instanceof Bean bean ? bean.value() : "");
    }

    @Override
    protected void analysisMethodByAnnotation(Class<? extends Annotation> annotationType, Annotation annotation, Method method) {
        // 客户端过来的协议入口.
        if (annotationType == ActionMethod.class) {
            actions.add(new ActionMethodDefinition(methodAccess, method ,(ActionMethod)annotation));
        }
        //RPC
        if (annotationType == RpcMethod.class) {
            rmds.add(new RpcMethodDefinition(methodAccess, method ,(RpcMethod) annotation));
        }

        // 事件监听
        else if (annotationType == EventHandler.class) {
            emds.add(new EventMethodDefinition(methodAccess, method, (EventHandler) annotation, this));
        }
        // HTTP服务
        else if (annotation instanceof RequestMapping requestMapping) {
            Controller controller = bean.getClass().getAnnotation(Controller.class);
            hmds.add(new HttpMethodDefinition(methodAccess, method,controller, requestMapping));
        }
        // 延迟任务
        else if (annotationType == Scheduled.class) {
            smds.add(new ScheduledMethodDefinition(methodAccess, method, (Scheduled) annotation));
        }
        // 其他的交给父类去处理
        else if(AnnotationUtil.hasAnnotation(method, RicksMethod.class)){
            super.analysisMethodByAnnotation(annotationType, annotation, method);
        }
    }

    @Override
    public void doAnalysisFunction(RicksIoc ioc) {
        super.doAnalysisFunction(ioc);

        this.doAnalysisEventHandler();

        this.doAnalysisHttpHandler();

        this.doAnalysisScheduledHandler();

        this.doAnalysisActionHandler(); //action 注入管理器

        this.doAnalysisRpcHandler(); //RPC 注入管理器
    }

    /**
     * 分析延迟任务处理入口.
     */
    private void doAnalysisScheduledHandler() {
        final ScheduledMethodManager manager = ScheduledMethodManager.get();
        smds.forEach(smd -> manager.resetScheduledHandler(new ScheduledMethodWrapper(bean, smd)));
    }

    /**
     * 分析事件处理入口.
     * actor 在真正执行业务上面获取
     */
    private void doAnalysisEventHandler() {
        emds.forEach(emd -> EventMethodManager.me().resetEventHandler(new EventMethodWrapper(bean, emd)));
    }

    /**
     * 分析HTTP处理入口.
     */
    private void doAnalysisHttpHandler() {
        hmds.forEach(hmd -> HttpMethodManager.registerHandler(new HttpMethodWrapper(bean,hmd)));
    }

    /**
     * 分析action处理入口.
     */
    private void doAnalysisActionHandler() {
        actions.forEach(action -> ActionMethodManager.registerAction(new ActionMethodWrapper(bean,action)));
    }

    /**
     * 分析action处理入口.
     */
    private void doAnalysisRpcHandler() {
        rmds.forEach(rmd -> RpcMethodManager.registerHandler(new RpcMethodWrapper(bean,rmd)));
    }
}