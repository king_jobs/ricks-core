package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.ioc.Bean;

import java.lang.reflect.Method;

/**
 * Bean申明的方法定义.
 */
public class BeanMethodDefinition extends SimpleMethodDefinition {
    /**
     * 如果这个方法是申明一个JavaBean的情况指定的Bean名称
     */
    protected final String beanName;

    public BeanMethodDefinition(MethodAccess methodAccess, Method method, Bean bean) {
        super(methodAccess, method);
        this.beanName = bean.value();
    }

    public String getBeanName() {
        return beanName;
    }
}
