package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.orm.Sql;

import java.lang.reflect.Method;

/**
 * dao层 sql解析
 */
public class DaoMethodDefinition extends SimpleMethodDefinition {
    private final String sql;
    private final Class[] parameterClazz;

    public DaoMethodDefinition(MethodAccess methodAccess, Method method, Sql sqlMethod) {
        super(methodAccess, method);
        this.sql = sqlMethod.value();
        this.parameterClazz = method.getParameterTypes();
    }

}