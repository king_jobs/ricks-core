
package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.schedule.Scheduled;
import java.lang.reflect.Method;

/**
 * 延迟任务入口的定义.
 */
public class ScheduledMethodDefinition extends SimpleMethodDefinition {
    private final Scheduled scheduled;
    private final String methodName;

    public ScheduledMethodDefinition(MethodAccess methodAccess, Method method, Scheduled scheduled) {
        super(methodAccess, method);
        this.scheduled = scheduled;
        this.methodName = method.getName();
    }

    public Scheduled getScheduled() {
        return scheduled;
    }

    public String getMethodName() {
        return methodName;
    }
}