
package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.net.http.Controller;
import org.ricks.net.http.RequestMapping;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * HTTP请求方法的定义.
 */
public class HttpMethodDefinition extends SimpleMethodDefinition {
    private final String path;
    private final Parameter[] parameters;

    public HttpMethodDefinition(MethodAccess methodAccess, Method method, Controller controller, RequestMapping mapping) {
        super(methodAccess, method);
        this.path = getMappingUrl(controller,mapping);
        this.parameters = method.getParameters();
    }

    @Override
    public Parameter[] getParameters() {
        return parameters;
    }

    public String getPath() {
        return path;
    }

    private String getMappingUrl(Controller controller, RequestMapping requestMapping) {
        String rootPath = controller.value();
        StringBuilder sb = new StringBuilder("/");
        if (rootPath.length() > 0) {
            if (rootPath.charAt(0) == '/') {
                sb.append(rootPath, 1, rootPath.length());
            } else {
                sb.append(rootPath);
            }
        }
        if (requestMapping.value().length() > 0) {
            char sbChar = sb.charAt(sb.length() - 1);
            if (requestMapping.value().charAt(0) == '/') {
                if (sbChar == '/') {
                    sb.append(requestMapping.value(), 1, requestMapping.value().length());
                } else {
                    sb.append(requestMapping.value());
                }
            } else {
                if (sbChar != '/') {
                    sb.append('/');
                }
                sb.append(requestMapping.value());
            }
        }
        return sb.toString();
    }
}