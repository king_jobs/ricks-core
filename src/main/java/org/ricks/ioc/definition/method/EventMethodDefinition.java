
package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.common.event.EventHandler;
import org.ricks.common.event.Event;
import org.ricks.ioc.definition.ControllerBeanDefinition;
import org.ricks.common.StrUtil;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * 事件处理入口的定义.
 */
public class EventMethodDefinition extends SimpleMethodDefinition {
    private final EventHandler eventListener;
    private final Class<? extends Event> eventClass; //事件参数

    @SuppressWarnings("unchecked")
    public EventMethodDefinition(MethodAccess methodAccess, Method method, EventHandler eventListener, ControllerBeanDefinition beanDefinition) {
        super(methodAccess, method);
        this.eventListener = eventListener;

        final String className = beanDefinition.getBeanClass().getName();

        var paramClazzs = method.getParameterTypes();
        if (paramClazzs.length != 1) {
            throw new IllegalArgumentException(StrUtil.format("[class:{}] [method:{}] must have one parameter!", className, method.getName()));
        }
        if (!Event.class.isAssignableFrom(paramClazzs[0])) {
            throw new IllegalArgumentException(StrUtil.format("[class:{}] [method:{}] must have one [IEvent] type parameter!", className, method.getName()));
        }

        this.eventClass = (Class<? extends Event>) paramClazzs[0];
        var eventName = eventClass.getCanonicalName();
        var methodName = method.getName();

        if (!Modifier.isPublic(method.getModifiers())) {
            throw new IllegalArgumentException(StrUtil.format("[class:{}] [method:{}] [event:{}] must use 'public' as modifier!", className, methodName, eventName));
        }

        if (Modifier.isStatic(method.getModifiers())) {
            throw new IllegalArgumentException(StrUtil.format("[class:{}] [method:{}] [event:{}] can not use 'static' as modifier!", className, methodName, eventName));
        }
    }

    public Class<? extends Event> getEventClass() {
        return eventClass;
    }

    public boolean isAsync() {
        return eventListener.async();
    }

}