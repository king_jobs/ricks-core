package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.net.rpc.RpcMethod;

import java.lang.reflect.Method;

/**
 * 自定义协议方法解析
 */
public class RpcMethodDefinition extends SimpleMethodDefinition {
    private final short rpcCmd;
    private final Class<?>[] parameterClasses;

    public RpcMethodDefinition(MethodAccess methodAccess, Method method, RpcMethod rpcMethod) {
        super(methodAccess, method);
        this.rpcCmd = rpcMethod.rpcCmd();
        this.parameterClasses = method.getParameterTypes();
    }

    public short getRpcCmd() {
        return rpcCmd;
    }
}