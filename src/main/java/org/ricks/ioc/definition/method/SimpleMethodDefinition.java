
package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.ioc.MethodDefinition;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

/**
 * 一个简单的方法定义.
 */
public class SimpleMethodDefinition implements MethodDefinition {
    protected final Method method;
    protected final Parameter[] parameters;
    protected final MethodAccess methodAccess;

    public SimpleMethodDefinition(MethodAccess methodAccess, Method method) {
        this.method = method;
        this.methodAccess = methodAccess;
        this.parameters = method.getParameters();
    }

    @Override
    public MethodAccess getMethodAccess() {
        return methodAccess;
    }

    @Override
    public int getMethodIndex() {
        return methodAccess.getIndex(method.getName(), method.getParameterTypes());
    }

    @Override
    public Parameter[] getParameters() {
        return parameters;
    }

    /**
     * 获取这个JDK方法引用.
     *
     * @return 获取方法引用
     */
    public Method getMethod() {
        return method;
    }
}