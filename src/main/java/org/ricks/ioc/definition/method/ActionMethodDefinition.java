package org.ricks.ioc.definition.method;

import org.ricks.common.asm.MethodAccess;
import org.ricks.net.Context;
import org.ricks.net.ActionMethod;
import java.lang.reflect.Method;

/**
 * 自定义协议方法解析
 */
public class ActionMethodDefinition extends SimpleMethodDefinition {
    private final short cmd;
    private final Class clazz;

    public ActionMethodDefinition(MethodAccess methodAccess, Method method, ActionMethod actionMethod) {
        super(methodAccess, method);
        this.cmd = actionMethod.messageId();
        Class<?>[] parameterClazzes = method.getParameterTypes();
        if (parameterClazzes.length != 1) {
            throw new IllegalArgumentException("消息处理方法的参数不正确，参数必须是一个。");
        }
        Class<?> commandClass = parameterClazzes[0];
        if (!Context.class.isAssignableFrom(commandClass)) {
            throw new IllegalArgumentException("消息处理方法的参数不正确，参数必须是Context ! ");
        }
        this.clazz = commandClass;
    }

    public short getCmd() {
        return cmd;
    }
}