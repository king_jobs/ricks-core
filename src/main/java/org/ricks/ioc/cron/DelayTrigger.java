
package org.ricks.ioc.cron;

import java.util.Date;

/**
 * 延迟触发器.
 */
public interface DelayTrigger {
    /**
     * 推算下一次执行时间。
     *
     * @return 下一次执行时间
     */
    Date nextExecutionTime();
}