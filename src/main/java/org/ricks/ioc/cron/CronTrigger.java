
package org.ricks.ioc.cron;

import java.util.Date;

/**
 * CRON表达式类型的触发器.
 */
public class CronTrigger implements DelayTrigger {
    private final CronExpression expression;
    /**
     * 上次执行计算时间.
     */
    private Date lastScheduledExecutionTime;

    public CronTrigger(String expression) {
        this.expression = new CronExpression(expression);
        this.lastScheduledExecutionTime = new Date();
    }

    @Override
    public Date nextExecutionTime() {
        return lastScheduledExecutionTime = expression.next(lastScheduledExecutionTime);
    }
}