package org.ricks.ioc.cron;

import java.util.Calendar;
import java.util.Date;

/**
 * 固定速率的延迟触发器.
 */
public class FixedDelayTrigger implements DelayTrigger {
    private final int initialDelay;
    private final int fixedRate;

    /**
     * 上次执行计算时间.
     */
    private Date lastScheduledExecutionTime;

    public FixedDelayTrigger(long initialDelay, long fixedRate) {
        this.initialDelay = (int) initialDelay;
        this.fixedRate = (int) fixedRate;
    }

    @Override
    public Date nextExecutionTime() {
        return lastScheduledExecutionTime = ((lastScheduledExecutionTime == null) ? addMilliseconds(new Date(), initialDelay) : addMilliseconds(lastScheduledExecutionTime, fixedRate));
    }

    public static Date addMilliseconds(final Date date, final int amount) {
        return add(date, Calendar.MILLISECOND, amount);
    }

    private static Date add(final Date date, final int calendarField, final int amount) {
        // 如果加上0的话，那就是不变，不要再计算啦
        if (amount == 0) {
            return date;
        }

        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(calendarField, amount);
        return c.getTime();
    }
}
