package org.ricks.ioc;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/3/814:40
 */
public interface Banner {

    void printBanner();


    enum Mode {

        /**
         * Disable printing of the banner.
         */
        OFF,

        /**
         *  printing of the banner.
         */
        ON,

    }

}
