package org.ricks.ioc;

/**
 * 一个属性的定义.
 */
public interface FieldDefinition {

    /**
     * 注入对象.
     *
     * @param single 宿主对象
     */
    void injection(Object single, RicksIoc ioc);
}