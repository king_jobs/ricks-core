package org.ricks.ioc;

import org.ricks.ModularContext;
import org.ricks.common.event.ContextStartedEvent;
import org.ricks.common.event.ContextStoppedEvent;
import org.ricks.common.event.EventBus;
import org.ricks.ioc.definition.ConfigurationBeanDefinition;
import org.ricks.ioc.definition.ControllerBeanDefinition;
import org.ricks.ioc.definition.DefaultBeanDefinition;
import org.ricks.ioc.wrap.MethodWrapper;
import org.ricks.common.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @author ricks
 * @date 2023/1/1114:25
 */
public class RicksIoc implements Ioc{


    /**
     * 容器中所有托管的Bean对象.
     */
    private final ConcurrentHashMap<String, Object> nameToBean = new ConcurrentHashMap<>(512);
    private final ConcurrentHashMap<Class<? extends Annotation>, List<MethodWrapper>> customMethods = new ConcurrentHashMap<>();

    private final HashMap<Class<?>, DefaultBeanDefinition> clazzToBean = MapUtils.newHashMap(1024);
    private final List<BeanDefinition> configurations = new ArrayList<>(); //配置Bean

    public RicksIoc() {
        IocHolder.setIoc(this);
        Set<Class<?>> classes = ModularContext.me().projectClasses();
        classes.forEach(this::analysisClass); //解析class,实例化
        finishBeanInitialization(configurations);
        finishBeanInitialization(clazzToBean.values());
        clazzToBean.values().forEach(b->b.doAnalysisFunction(this));

        // 3、初始化方法...
        invokeCustomAnnotationMethod(Init.class); //初始化Init
        EventBus.me().publish(new ContextStartedEvent(this));//发布容器初始化事件
    }

    @Override
    public <T> T get(Class<T> clazz) {
        if(clazzToBean.containsKey(clazz)) {
            return (T)clazzToBean.get(clazz).getBean();
        }
        return null;
    }

    @Override
    public <T> List<T> getBeans(Class<T> clazz) {
        Object o = clazzToBean.get(clazz).getBean();
        if (o instanceof List beans) {
            return beans;
        }
        return null;
    }

    @Override
    public <T> T get(String beanId) {
        return (T)nameToBean.get(beanId);
    }


    @Override
    public <T> List<T> allBeans() {
        return (List<T>)nameToBean.values().stream().collect(Collectors.toList());
    }


    /**
     * 分析Class
     *
     * @param klass Class
     */
    private void analysisClass(Class<?> klass) {
        // 接口、内部类、枚举、注解和匿名类 直接忽略
        if (klass.isInterface() || klass.isMemberClass() || klass.isEnum() || klass.isAnnotation() || klass.isAnonymousClass()) {
            return;
        }

        // 抽象类和非Public的也忽略
        int modify = klass.getModifiers();
        if (Modifier.isAbstract(modify) || (!Modifier.isPublic(modify))) {
            return;
        }
/**
 *      如果IOC支持配置注入，这一坨解析就可以去掉
        if (ClassUtil.isAssignable(DaoSourceManage.class,klass)) {
            PooledMgr.init((Class<? extends DaoSourceManage>) klass);
        }

        if (ClassUtil.isAssignable(TemplateManager.class,klass)) {
            try {
                //web http 渲染模板
                TemplateManager templateManager = (TemplateManager) klass.getConstructor().newInstance();
                this.templateManager = templateManager;
                Logger.info("WEB 渲染模板初始化成功 !");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
*/
        //初始化table 容器
        Annotation annotation = AnnotationUtil.getAnnotation(klass,Bean.class);
        // 查找带有@Bean注解或其他注解上有@Bean注解的类
        if(annotation != null) {
            analytical(klass,annotation); //只要是Bean都进行解析
        } else
        // 目标类上实际注解类型    实现ApplicationListener 自动注入IOC
        if (AnnotationUtil.hasAnnotation(klass,Configuration.class)) {
            analyticalConfiguration(klass);
        }
    }

    /**
     * 协议入口控制类(模块化)
     */
    private void analytical(Class<?> klass,Annotation bean) {
        DefaultBeanDefinition definition = new ControllerBeanDefinition(klass,bean).init();
        add(definition,klass);
    }

    private void analyticalConfiguration(Class<?> klass) {
        DefaultBeanDefinition definition = new ConfigurationBeanDefinition(klass).init();
        configurations.add(definition);
    }

    public void add(DefaultBeanDefinition definition,Class klass){
        clazzToBean.put(klass, definition);
        nameToBean.put(definition.getName(),definition.getBean());
    }

    public void addConfigBeanDefinition(DefaultBeanDefinition beanDefinition) {
        configurations.add(beanDefinition);
    }

    /**
     * 为有注解的方法添加方法执行入口.
     *
     * @param klass 注解类
     * @param mw    方法执行对象
     */
    public void addCustomMethod(Class<? extends Annotation> klass, MethodWrapper mw) {
        customMethods.computeIfAbsent(klass, key -> new ArrayList<>()).add(mw);
    }

    /**
     * 初始化和依赖注入的关系
     */
    private void finishBeanInitialization(Collection<? extends BeanDefinition> beans) {
        beans.forEach(bean -> bean.injection(this));
    }

    /**
     * 调用自定义注解方法.
     *
     * @param klass 注解类
     */
    public void invokeCustomAnnotationMethod(Class<? extends Annotation> klass) {
        customMethods.getOrDefault(klass, Collections.emptyList()).forEach(MethodWrapper::invoke);
    }

    @Override
    public void stop() {
        // 3、初始化方法...
        invokeCustomAnnotationMethod(Destroy.class); //销毁
        EventBus.me().publish(new ContextStoppedEvent(this)); //容器销毁事件
    }
}
