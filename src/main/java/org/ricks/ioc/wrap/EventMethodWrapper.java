
package org.ricks.ioc.wrap;

import org.ricks.common.event.Event;
import org.ricks.ioc.definition.method.EventMethodDefinition;

/**
 * 事件处理方法的包装类.
 *  Event 线程ID目的，同一玩家事件异步执行在 同一个事件线程处理
 */
public class EventMethodWrapper extends AbstractControllerMethodWrapper {

    private final Class<? extends Event> eventClass;
    private final boolean async;

    public EventMethodWrapper(Object bean, EventMethodDefinition emd) {
        super(bean, emd);
        this.async = emd.isAsync();
        this.eventClass = emd.getEventClass();
    }

    /**
     * 构建这个事件日志的编码
     *
     * @param single 处理类入口
     * @param emd    事件处理方法的定义
     * @return 返回日志编码
     */
    private static String buildLogCode(Object single, EventMethodDefinition emd) {
        StringBuilder sb = new StringBuilder(128);
        // 事件
        sb.append("event[");
        // 类名
        sb.append(single.getClass().getSimpleName()).append('#');
        // 方法名称
        sb.append(emd.getMethod().getName()).append('(');
        // 参数名称
        sb.append(emd.getEventClass().getSimpleName()).append(')');
        // 最终结果
        return sb.append(']').toString();
    }


    public Class<? extends Event> getEventClass() {
        return eventClass;
    }

    public boolean isAsync() {
        return async;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "EventMethodWrapper [async=" + async + ", method=" + methodAccess.getMethodNames()[methodIndex] + "]";
    }
}