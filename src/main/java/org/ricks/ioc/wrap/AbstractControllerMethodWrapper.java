
package org.ricks.ioc.wrap;

import org.ricks.common.actor.Actor;
import org.ricks.common.actor.Actors;
import org.ricks.ioc.MethodDefinition;

/**
 * Controller类中的可执行方法.
 */
public abstract class AbstractControllerMethodWrapper extends BaseMethodWrapper {
    
    /**
     * 执行的actor,默认actor 随机
     */
    protected Actor actor = Actors.me().getRandom();



    public AbstractControllerMethodWrapper(Object single,MethodDefinition md) {
        super(single, md.getMethodAccess(), md.getMethodIndex());
    }

}