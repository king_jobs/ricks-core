package org.ricks.ioc.wrap;

import org.ricks.ioc.cron.CronTrigger;
import org.ricks.ioc.cron.DelayTrigger;
import org.ricks.ioc.cron.FixedDelayTrigger;
import org.ricks.ioc.definition.method.ScheduledMethodDefinition;
import org.ricks.schedule.Scheduled;
import org.ricks.common.StrUtil;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 延迟任务处理方法的包装类.
 */
public class ScheduledMethodWrapper extends AbstractControllerMethodWrapper {
    /**
     * 自动增的ID编号，用于唯一调度
     */
    private static final AtomicLong AUTO_ID = new AtomicLong(0);
    private final Long id;
    private final DelayTrigger trigger;

    public ScheduledMethodWrapper(Object bean, ScheduledMethodDefinition smd) {
        super(bean, smd);
        // 生成一个唯一ID编号.
        this.id = AUTO_ID.incrementAndGet();

        final Scheduled scheduled = smd.getScheduled();
        // 固定速率不能为0，就是有人会忘了写参数，给个明确的提示
        if (scheduled.fixedRate() <= 0 && StrUtil.isEmpty(scheduled.cron())) {
            String position = bean.getClass().getName() + "[" + smd.getMethodName() + "]";
            throw new RuntimeException("@Scheduled没有配置参数：" + position + "，请火速处理...");
        }
        this.trigger = StrUtil.isNotEmpty(scheduled.cron()) ? new CronTrigger(scheduled.cron()) : new FixedDelayTrigger(scheduled.initialDelay(), scheduled.fixedRate());
    }

    public Long getId() {
        return id;
    }

    public Date nextExecutionTime() {
        return trigger.nextExecutionTime();
    }
}