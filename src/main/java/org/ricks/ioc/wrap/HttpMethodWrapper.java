package org.ricks.ioc.wrap;

import org.ricks.ioc.definition.method.HttpMethodDefinition;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;

/**
 * 封包处理方法包装类.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public class HttpMethodWrapper extends AbstractControllerMethodWrapper {
    private final String path;
    private final Method method;
    private final Type[] types;
    private final Parameter[] parameters;


    public HttpMethodWrapper(Object single, HttpMethodDefinition methodDefinition) {
        super(single, methodDefinition);
        this.path = methodDefinition.getPath();
        // 这里的方法缓存着，拦截器里可能会有获取注解的需求
        this.method = methodDefinition.getMethod();
        this.types = method.getGenericParameterTypes();
        this.parameters = method.getParameters();
    }


    public String getPath() {
        return path;
    }

    public Type[] getTypes() {
        return types;
    }

    public Parameter[] getParameters() {
        return parameters;
    }
}