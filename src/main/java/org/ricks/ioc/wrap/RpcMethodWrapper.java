package org.ricks.ioc.wrap;

import org.ricks.ioc.definition.method.RpcMethodDefinition;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;

/**
 * 封包处理方法包装类.
 */
public class RpcMethodWrapper extends AbstractControllerMethodWrapper {
    private final short rpcCmd;
    private final Method method;
    private final Type[] types;
    private final Parameter[] parameters;


    public RpcMethodWrapper(Object single, RpcMethodDefinition rpcMethodDefinition) {
        super(single, rpcMethodDefinition);
        this.rpcCmd = rpcMethodDefinition.getRpcCmd();
        // 这里的方法缓存着，拦截器里可能会有获取注解的需求
        this.method = rpcMethodDefinition.getMethod();
        this.types = method.getGenericParameterTypes();
        this.parameters = method.getParameters();
    }


    public short getRpcCmd() {
        return rpcCmd;
    }

    public Type[] getTypes() {
        return types;
    }

    public Parameter[] getParameters() {
        return parameters;
    }
}