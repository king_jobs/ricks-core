package org.ricks.ioc.wrap;

/**
 * 可执行的方法.
 */
public interface MethodWrapper {

    /**
     * 执行.
     *
     * @param args 参数列表
     * @return 返回值
     */
    public Object invoke(Object... args);
}