package org.ricks.ioc.wrap;

import org.ricks.ioc.definition.method.ActionMethodDefinition;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/**
 * 封包处理方法包装类.
 */
public class ActionMethodWrapper extends AbstractControllerMethodWrapper {
    private final short cmd;
    private final Method method;
    private final Type type;
    /** 返回类型 */
    final Class<?> returnTypeClazz;


    public ActionMethodWrapper(Object single, ActionMethodDefinition methodDefinition) {
        super(single, methodDefinition);
        this.cmd = methodDefinition.getCmd();
        // 这里的方法缓存着，拦截器里可能会有获取注解的需求
        this.method = methodDefinition.getMethod();
        this.type = method.getGenericParameterTypes()[0];
        // 方法返回值类型
        returnTypeClazz = methodAccess.getReturnTypes()[methodIndex];
    }


    @Override
    public Object invoke(Object... args) {
        if (args.length == 0) {
            return methodAccess.invoke(bean, methodIndex);
        } else {
            return methodAccess.invoke(bean, methodIndex, args);
        }
    }

    /**
     * 方法返回值类型是否 void
     *
     * @return true 是 void
     */
    public boolean isVoid() {
        return Void.TYPE == this.returnTypeClazz;
    }

    public short getCmd() {
        return cmd;
    }

    public Type getType() {
        return type;
    }


}