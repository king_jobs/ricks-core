
package org.ricks.ioc.wrap;


import org.ricks.common.asm.MethodAccess;

/**
 * 一个可执行的方法.
 */
public class BaseMethodWrapper implements MethodWrapper {
    /**
     * 缓存那个单例对象
     */
    protected final Object bean;
    protected final int methodIndex;
    protected final MethodAccess methodAccess;

    public BaseMethodWrapper(Object bean, MethodAccess methodAccess, int methodIndex) {
        this.bean = bean;
        this.methodIndex = methodIndex;
        this.methodAccess = methodAccess;
    }

    @Override
    public Object invoke(Object... args) {
        if (args.length == 0) {
            return methodAccess.invoke(bean, methodIndex);
        } else {
            return methodAccess.invoke(bean, methodIndex, args);
        }
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + methodIndex;
        result = prime * result + ((bean == null) ? 0 : bean.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BaseMethodWrapper other = (BaseMethodWrapper) obj;
        if (methodIndex != other.methodIndex) {
            return false;
        }
        if (bean == null) {
            if (other.bean != null) {
                return false;
            }
        } else if (!bean.equals(other.bean)) {
            return false;
        }
        return true;
    }
}