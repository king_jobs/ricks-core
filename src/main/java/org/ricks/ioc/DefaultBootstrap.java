package org.ricks.ioc;

import org.ricks.ModularContext;
import org.ricks.common.conf.RicksConf;
import org.ricks.common.event.EventBus;
import java.util.Set;

public abstract class DefaultBootstrap {

    protected Banner.Mode bannerMode = Banner.Mode.ON;;
    protected final Banner DEFAULT_BANNER = new DefaultBanner(this);

    public DefaultBootstrap() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            onStop();
        }));
    }

    public void run(Set<String> scanPackageNames) {
        ModularContext.me().scan(scanPackageNames); //核心 项目启动优先扫描包体
        EventBus.me().initApplicationEvent(); //启动容器事件机制
        RicksConf.loadingProperties(); //初始化配置
        IocHolder.initIoc(); //初始化IOC容器
        onStart();
    }

    protected abstract void onStart();

    protected abstract void onStop();

}
