package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * @author ricks
 * @Description:
 * @date 2020/8/615:36
 */
@Bean
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Value {

    String value() default "";
}
