package org.ricks.ioc.manager;

import org.ricks.ioc.wrap.ScheduledMethodWrapper;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 延迟任务管理类.
 */
public class ScheduledMethodManager {
    private static final ScheduledMethodManager INSTANCE = new ScheduledMethodManager();
    private final Map<Long, ScheduledMethodWrapper> handlers = new ConcurrentHashMap<>(128);

    private ScheduledMethodManager() {
    }

    public static ScheduledMethodManager get() {
        return INSTANCE;
    }

    public Collection<ScheduledMethodWrapper> getHandlers() {
        return handlers.values();
    }

    public ScheduledMethodWrapper getHandler(Long id) {
        return handlers.get(id);
    }

    /**
     * 注册延迟任务处理方法.
     *
     * @param scheduledWrapper 延迟任务处理方法包装对象
     */
    public void resetScheduledHandler(ScheduledMethodWrapper scheduledWrapper) {
        handlers.put(scheduledWrapper.getId(), scheduledWrapper);
    }
}