package org.ricks.ioc.manager;

import org.ricks.ioc.wrap.ActionMethodWrapper;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * action方法管理类.
 */
public class ActionMethodManager {

    private static final Map<Short, ActionMethodWrapper> actionMap = new ConcurrentHashMap<>();

    private ActionMethodManager() {
    }

    /**
     * 注册action请求处理器.
     *
     * @param action cmd请求消息
     */
    public static void registerAction(ActionMethodWrapper action) {
        short cmd = action.getCmd();
        if(actionMap.containsKey(cmd)) {
            throw new RuntimeException("重复定义的cmd：" + cmd);
        }
        // 存档备用
        actionMap.put(cmd, action);
    }

    /**
     * 获取指定访问方式的路径处理器.
     * @param cmd   指令
     * @return 获取指定访问方式的路径处理器
     */
    public static ActionMethodWrapper getActionMethod(short cmd) {
        return actionMap.get(cmd);
    }

    public static Set<Short> allCmd() {
        return actionMap.keySet();
    }
}