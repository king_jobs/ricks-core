package org.ricks.ioc.manager;

import org.ricks.ioc.wrap.HttpMethodWrapper;
import org.ricks.common.StrUtil;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * HTTP方法管理类.
 */
public class HttpMethodManager {

    private static final Map<String, HttpMethodWrapper> handlerMap = new ConcurrentHashMap<>();

    private HttpMethodManager() {
    }

    /**
     * 注册HTTP请求处理器.
     *
     * @param handler HTTP请求处理器
     */
    public static void registerHandler(HttpMethodWrapper handler) {
        // URI不能有空格...
        if (handler.getPath().indexOf(StrUtil.SPACE) != -1) {
            throw new RuntimeException("URI中发现空格：" + handler.getPath());
        }
        // 重复定义的URI
        if (handlerMap.containsKey(handler.getPath())) {
            throw new RuntimeException("重复定义的URI：" + handler.getPath());
        }
        // 存档备用
        handlerMap.put(handler.getPath(), handler);
    }

    /**
     * 获取指定访问方式的路径处理器.
     * @param path   路径
     * @return 获取指定访问方式的路径处理器
     */
    public static HttpMethodWrapper getHttpHandler(String path) {
        return handlerMap.get(path);
    }
}