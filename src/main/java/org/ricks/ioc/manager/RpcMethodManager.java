package org.ricks.ioc.manager;

import org.ricks.ioc.wrap.RpcMethodWrapper;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * HTTP方法管理类.
 */
public class RpcMethodManager {

    private static final Map<Short, RpcMethodWrapper> handlerMap = new ConcurrentHashMap<>();

    private RpcMethodManager() {
    }

    /**
     * 注册rpc请求处理器.
     *
     * @param handler Rpc请求处理器
     */
    public static void registerHandler(RpcMethodWrapper handler) {
        short rpcCmd = handler.getRpcCmd();
        if(handlerMap.containsKey(rpcCmd)) {
            throw new RuntimeException("重复定义的Rpccmd：" + rpcCmd);
        }
        // 存档备用
        handlerMap.put(rpcCmd, handler);
    }

    /**
     * @param rpcCmd   rpc路由
     * @return 获取指定访问方式的路径处理器
     */
    public static RpcMethodWrapper getRpcHandler(short rpcCmd) {
        return handlerMap.get(rpcCmd);
    }
}