package org.ricks.ioc.manager;


import org.ricks.common.actor.Actor;
import org.ricks.common.actor.Actors;
import org.ricks.common.actor.ActorRunnable;
import org.ricks.common.event.Event;
import org.ricks.ioc.wrap.EventMethodWrapper;
import org.ricks.common.lang.Logger;
import org.ricks.common.CollUtil;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 事件处理管理类.
 * 事件模块依赖于 IOC ，统一管理
 * @since 3.0
 */
public class EventMethodManager {

    private static final EventMethodManager INSTANCE = new EventMethodManager();
    private final Map<Class<? extends Event>, List<EventMethodWrapper>> handlers = new ConcurrentHashMap<>();

    private EventMethodManager() {
    }

    /**
     * 获取事件管理器单例对象
     * @return
     */
    public static EventMethodManager me() {
        return INSTANCE;
    }


    /**
     * 获取事件订阅列表.
     *
     * @param eventClass 事件源类型
     * @return 订阅列表
     */
    public List<EventMethodWrapper> getEventMethodWrappers(Class<? extends Event> eventClass) {
        return handlers.getOrDefault(eventClass, Collections.emptyList());
    }

    /**
     * 注册事件处理方法.
     *
     * @param eventWrapper 事件处理方法包装对象
     */
    public void resetEventHandler(EventMethodWrapper eventWrapper) {
        handlers.computeIfAbsent(eventWrapper.getEventClass(), key -> new ArrayList<>()).add(eventWrapper);
    }

    /**
     * 同步抛出一个事件，会在当前线程中运行
     *
     * @param event 需要抛出的事件
     */
    public void syncSubmit(Event event) {
        var list = handlers.get(event.getClass());
        if (CollUtil.isEmpty(list)) {
            Logger.error("没有找到可以执行的事件，key:"+ event.getClass().getName());
            return;
        }
        doSubmit(event, list);
    }


    /**
     * 异步抛出一个事件，事件不在同一个线程中处理
     *
     * @param event 需要抛出的事件
     */
    public void asyncSubmit(Event event) {
        var list = handlers.get(event.getClass());
        if (CollUtil.isEmpty(list)) {
            Logger.error("没有找到可以执行的事件，key:"+  event.getClass().getName());
            return;
        }
        Actor actor =  Actors.me().getEventActor(event.threadId());
        actor.execute(new ActorRunnable("async-event",() -> doSubmit(event, list)));
    }


    /**
     * 执行方法调用
     *
     * @param event        事件
     * @param receiverList 所有的观察者
     */
    private void doSubmit(Event event, List<EventMethodWrapper> receiverList) {
        for (var receiver : receiverList) {
            try {
                receiver.invoke(event);
            } catch (Exception e) {
                Logger.error("eventBus未知exception异常", e);
            } catch (Throwable t) {
                Logger.error("eventBus未知error异常", t);
            }
        }
    }

}