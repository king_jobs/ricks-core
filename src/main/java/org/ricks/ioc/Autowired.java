package org.ricks.ioc;

import java.lang.annotation.*;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2020/8/615:36
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface Autowired {

    String value() default "";
}
