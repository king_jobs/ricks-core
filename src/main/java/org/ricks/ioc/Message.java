package org.ricks.ioc;

import org.ricks.protocol.IPacket;


/**
 * @author ricks
 * @date 2020/8/2411:54
 */
public class Message<K,T> implements IPacket {
    K cmd;
    T data;

    public Message() {
    }

    public Message(K cmd, T data) {
        this.cmd = cmd;
        this.data = data;
    }

    public K getCmd() {
        return cmd;
    }

    public void setCmd(K cmd) {
        this.cmd = cmd;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
