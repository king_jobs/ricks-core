package org.ricks.ioc;

import org.ricks.net.http.web.TemplateManager;
import org.ricks.orm.BaseRepository;

import java.util.List;

/**
 * IOC容器持有类.
 */
public class IocHolder {
    private static Ioc IOC;

    private IocHolder() {
    }

    /**
     * 获取IOC容器.
     * <p>
     * 常规用不上这个方法，可以方便脚本获取一些服务
     *
     * @return IOC容器
     */
    public static Ioc getIoc() {
        return IOC;
    }

    static void setIoc(Ioc ioc) {
        IocHolder.IOC = ioc;
    }

    public static void initIoc() {
        IOC = new RicksIoc();
    }

    public static <T> List<T> getBeans(Class<T> clazz) {
        return IOC.getBeans(clazz);
    }

    public static <T> T getBean(Class<T> clazz) {
        return IOC.get(clazz);
    }
}