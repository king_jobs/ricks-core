
package org.ricks.ioc;

import org.ricks.common.asm.MethodAccess;

import java.lang.reflect.Parameter;

/**
 * 一个方法的定义.
 *
 */
public interface MethodDefinition {

    /**
     * 获取此方法的访问入口.
     *
     * @return 访问入口
     */
    MethodAccess getMethodAccess();

    /**
     * 获取此方法的访问入口所对应的Index.
     *
     * @return 访问入口所对应的Index.
     */
    int getMethodIndex();

    /**
     * 获取参数列表
     *
     * @return 参数
     */
    Parameter[] getParameters();

}