package org.ricks.orm.write;

public interface OrmWriteContainerLifeListener<K> {

    /**
     * 异步回写容器创建
     */
    void create(K key);

    /**
     * 异步回写容器销毁
     */
    void destroy(K key);
}
