package org.ricks.orm.write;

/**
 * 对数据的操作类型.
 */
public enum OperateType {
    // MYSQL
    INSERT, DELETE, UPDATE, SELECT;
}
