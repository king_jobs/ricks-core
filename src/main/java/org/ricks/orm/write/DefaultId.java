
package org.ricks.orm.write;

import java.io.Serializable;

/**
 * 默认的系统ID.
 *
 */
public class DefaultId implements Serializable {
    public static final DefaultId INSTANCE = new DefaultId();
    private static final long serialVersionUID = -6529401819014260134L;

    private DefaultId() {
    }

    @Override
    public String toString() {
        return DefaultId.class.getSimpleName();
    }
}