package org.ricks.orm.write;

import org.ricks.common.actor.Actor;
import org.ricks.common.actor.Actors;
import org.ricks.common.conf.RicksConf;
import org.ricks.orm.*;
import org.ricks.common.lang.ExpirationListener;
import org.ricks.common.lang.ThreadNameFactory;
import org.ricks.orm.cache.*;
import org.ricks.common.lang.Logger;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;

import static org.ricks.orm.DataModular.*;


/**
 * 抽象的异步回写中心.
 * 和小流氓作者确认  RemovalListener 目的就是玩家退出进而销毁回写容器
 * 但其实可以在 网络心跳超时 手动进行销毁
 * 就是用table name 进行回写容器 init
 * table - AsyncWriteContainer  一一对应，
 *
 * 异步写管理中心
 */
public class AsyncWriteManager {

    /**
     * 1小时没有写操作，回写容器销毁
     */
    private int offlineInterval = 3600;
    protected DataAccessor dataAccessor;

    protected OrmWriteContainerLifeListener writeContainerLifeListener;
    /**
     * 定时存档间隔，单位：秒
     */
    protected int saveInterval = 300;
    /**
     * 批量存档数量
     */
    protected int batchOperateNum = 64;
    /**
     * 这个定时任务，有空就处理一下数据保存和缓存清理功能
     */
    protected ScheduledExecutorService scheduledExecutorService;
    private int threadPoolSize = 4;
    private String threadNamePrefix = "async-write-data";
    /**
     * 异步回写容器缓存
     *
     * 滚服游戏 可以用时间驱动 cache db数据驱动容器 销毁机制，因为滚服 玩家永远在一个服务器上面。缓存 db回写容器 也是固定服务器。所以即使网络通信断掉也不用立即销毁玩家缓存
     * 可以设置缓存 db回写容器失效时间 一个小时，一个小时之后自动回收玩家所有得资源。滚服游戏 玩家分配服务器是固定得也不需要考虑 玩家数据命中问题
     *
     * 全球服不一样，玩家可以分配任意节点。比如玩家开始在gameA 上面 创建了回写容器 和 load cache .玩家重新登录 如果无状态情况下则会分配在gameB .db回写容器5分钟间隔 db一次策划，那就会出现5分钟数据丢失情况。所以全球服模式 不能使用cache时间策略来驱动失效，而是手动控制 由网关层 kcp tcp ws 链接驱动 cache db回写容器 销毁
     * 网关网络触发了close session事件后 ，RPC同步调用game exitUser 来驱动数据db cache销毁
     *
     * 由网关 网络通信进行驱动容器销毁机制，网关网络触发close session事件， 同步RPC路由逻辑服 销毁ORM db回写容器 和 cache缓存机制
     */
    private LoadingCache<Serializable, AsyncWriteContainer> containers;

    private AsyncWriteManager(){}
    private static AsyncWriteManager INSTANCE = new AsyncWriteManager();

    public static AsyncWriteManager me(){
        return INSTANCE;
    }

    private Actor flushActor = Actors.me().createActor("flush-db");

    /**
     * 销毁机制
     */
    private ExpirationListener<Serializable, AsyncWriteContainer> listener;

    public void init() {
        /**
         * ORM基础配置
         */
        offlineInterval = RicksConf.toIntOrDefault(DATA_OFFLINE_INTERVAL,3600);
        saveInterval = RicksConf.toIntOrDefault(DATA_SAVE_INTERVAL,300);
        batchOperateNum = RicksConf.toIntOrDefault(DATA_BATCH_NUM,300);
        threadPoolSize = RicksConf.toIntOrDefault(DATA_THREAD_POOL_SIZE,4);
        threadNamePrefix = RicksConf.getOrDefault(DATA_THREAD_NAME_PREFIX,"db-write");


        dataAccessor.judgeAccessType();
        Logger.info("初始化数据存储模块，批量存档数量为"+batchOperateNum+",定时存档的时间间隔为"+ saveInterval + "秒");
        // 初始化存档异步线程池
        scheduledExecutorService = new ScheduledThreadPoolExecutor(threadPoolSize, new ThreadNameFactory(threadNamePrefix,1,false));

        CacheLoader<Serializable, AsyncWriteContainer> loader = groupId -> {
            Logger.info("ORM创建异步回写容器 groupId="+ groupId);
            if(!Objects.isNull(writeContainerLifeListener)) {
                writeContainerLifeListener.create(groupId);
            }
            return new AsyncWriteContainer(groupId, saveInterval, scheduledExecutorService, dataAccessor, batchOperateNum);
        };

        listener = (key, value) -> {
            Logger.info("ORM销毁"+offlineInterval+"秒都没有读写操作的异步回写容器 groupId=" + key);
            value.syncFlush();
            value.close();
            if(!Objects.isNull(writeContainerLifeListener)) writeContainerLifeListener.destroy(key);
        };

        /**
         * Caffeine 缓存过期清除机制优化 & 缓存过期后db flush
         * groupId=tableName ,就意味一个异步容器对应一个表。那它的生命周期应该是进程从启动到停止
         * 所以不用RemovalListener进行一个小时刷新
         *
         * 其实最重要就两点
         * 1.初始化表的回写容器
         * 2.进程停止全部刷入db
         *
         * 回写容器需要销毁，1个小时没有写操作 销毁容器（防止内存泄露）
         */
        this.containers = Caffeine.newBuilder().build(loader);
    }


    public <T> void insert(TableMeta<T> tm, T entity) {
        this.operation(tm, entity, OperateType.INSERT);
    }

    public <T> void delete(TableMeta<T> tm, T entity) {
        this.operation(tm, entity, OperateType.DELETE);
    }

    public <T> void deleteAll(TableMeta<T> tm, List<T> result) {
        for (T entity : result) {
            this.operation(tm, entity, OperateType.DELETE);
        }
    }

    public <T> void update(TableMeta<T> tm, T entity) {
        this.operation(tm, entity, OperateType.UPDATE);
    }


    /**
     * 实体类对象的操作.
     *
     * @param tm     实体对象描述类
     * @param entity 实体类对象
     * @param type   操作类型
     * @param <T>    实体类
     */
    protected <T> void operation(TableMeta<T> tm, T entity, OperateType type) {
        Serializable groupId = this.analysisGroupIdByEntity(tm, entity);
        AsyncWriteContainer container = containers.get(groupId);
        switch (type) {
            case INSERT:
                container.insert(tm, entity);
                break;
            case DELETE:
                container.delete(tm, entity);
                break;
            case UPDATE:
                container.update(tm, entity);
                break;
            default:
                Logger.warn("这是要干嘛？ type="+type+",entity="+ entity);
                break;
        }
    }

    /**
     * 异步回写容器数量
     * 8小时没用写操作，默认销毁 .销毁回写容器时间考虑清楚。
     * 目前以回写容器作为 玩家与游戏节点 之间的绑定关系。如果销毁时间 过长。玩家退出游戏 长时间内不会与游戏节点解绑
     * 再加上回写容器 准备作为玩家分配游戏节点的基础，如果销毁时间过长 游戏节点分配5000 ，可能实际只能分配3000-4000
     *
     * 如果使用userSession 作为游戏节点分配的基础，就意味需要每个网关节点 把自己维护的所有userSession 同步到一个收集服务上。实际网关也需要探测存活
     *
     * io-game作者 也只说两个常规方式 redis or http
     * 然后他自定义了一种 脉冲通信方式
     *
     * 还是由异步回写驱动 游戏节点分配机制，弊端就是如果玩家在游戏1个小时没有写操作 ，其它玩家登录 还会分配在这个节点。可以会超时游戏节点设置的人数上线
     * @return
     */
    public int size() {
        return containers.size();
    }

    /**
     * 智能分析这个实体类的角色Id是多少.
     * <p>
     * @param <T>    实体类型
     * @param tm     实体类的描述对象.
     * @param entity 实体对象.
     * @return 如果这个实体对象中有@PlayerId则返回此属性的值，否则返回默认的（系统）角色ID
     */
    private  <T> Serializable analysisGroupIdByEntity(TableMeta<T> tm, T entity) {
        // 拥有@PlayerId的必属性角色的数据
        if (tm.getPlayerId() != null) {
            return tm.getPlayerIdValue(entity);
        }
        return DefaultId.INSTANCE;
    };

    public void shutdown() {
        Logger.info("开始通知数据保存任务线程池关闭.");
        this.syncFlushAll();
        scheduledExecutorService.shutdown();
        flushActor.shutdown();
        try {
            // 尝试等待10分钟回写操作，10分钟都没写完就全停掉吧，不写了
            if (!scheduledExecutorService.awaitTermination(DataConstant.SHUTDOWN_MAX_TIME, TimeUnit.MINUTES)) {
                scheduledExecutorService.shutdownNow();
            }
            Logger.info("数据保存任务线程池已全部回写完，关闭成功.");
        } catch (InterruptedException ie) {
            Logger.error("数据保存任务线程池停机时发生异常.", ie);
            scheduledExecutorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    public void syncFlushAll() {
        for (AsyncWriteContainer container : containers.asMap().values()) {
            container.syncFlush();
        }
    }

    /**
     * 销毁回写容器
     *
     *           Thread.ofVirtual().name("write-container",id).start(() -> {
     *               //触发容器销毁监听
     *               listener.expired(playerId,container);
     *               //触发生命周期监听 注意此时在虚拟线程依附的 forkjoinpool 平台线程中
     *               if(Objects.nonNull(writeContainerLifeListener)) writeContainerLifeListener.destroy(playerId);
     *           });
     *
     *   由最外层创建虚拟线程，执行玩家缓存 db容器 flush db数据  机制
     * @param playerId
     */
    public void destroy(Serializable playerId) {
        AsyncWriteContainer container = containers.invalidate(playerId);
        if(Objects.isNull(container)) return;
        //触发容器销毁监听
        listener.expired(playerId,container);
        //触发生命周期监听 注意此时在虚拟线程依附的 forkjoinpool 平台线程中
        if(Objects.nonNull(writeContainerLifeListener)) writeContainerLifeListener.destroy(playerId);
        /**
         *         Thread.ofVirtual().name("write-container",id).start(() -> {
         *             //触发容器销毁监听
         *             listener.expired(playerId,container);
         *             //触发生命周期监听 注意此时在虚拟线程依附的 forkjoinpool 平台线程中
         *             if(Objects.nonNull(writeContainerLifeListener)) writeContainerLifeListener.destroy(playerId);
         *         });
         *
         *         由最外层创建虚拟线程，执行玩家缓存 db容器 flush db数据  机制
         *
         */
    }

//    /**
//     * 清除缓存机制 ＆ 立刻刷入db
//     * 建议使用单独的线程进行落地数据，避免业务线程IO阻塞，落地数据可以使用队列方式 即使慢一些也不会影响业务
//     * @param groupId 玩家ID
//     * @param consumer 函数返回参数，玩家数据落地结果 是否成功
//     * @return
//     */
//    public void asyncFlushByGroupId(Serializable groupId, Consumer<Boolean> consumer) {
//        flushActor.execute("flush",() -> consumer.accept(doFlushByGroupId(groupId)));
//    }
//
//    public boolean doFlushByGroupId(Serializable groupId) {
//        if(containers == null) {
//            return true;
//        }
//        AsyncWriteContainer container = containers.get(groupId);
//        if(Objects.isNull(container)) {
//            Logger.warn("【警告】 groupId:"+ groupId+" not found write container ");
//            return false;
//        }
//        container.syncFlush();
//        return true;
//    }
//
//    /**
//     * 异步刷新玩家数据 demo
//     * @param args
//     */
//    private static void main(String[] args) {
//        AsyncWriteManager.me().asyncFlushByGroupId(10000000, flush -> doTask(flush));
//    }

    private static void doTask(boolean a) {
        System.err.println(a);
    }


    public void setDataAccessor(DataAccessor dataAccessor) {
        this.dataAccessor = dataAccessor;
    }

    public void setWriteContainerLifeListener(OrmWriteContainerLifeListener writeContainerLifeListener) {
        this.writeContainerLifeListener = writeContainerLifeListener;
    }
}