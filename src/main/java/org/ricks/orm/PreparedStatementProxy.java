package org.ricks.orm;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * PreparedStatement代理对象.
 *
 */
public class PreparedStatementProxy {
    private final PreparedStatement pstmt;
    private final boolean statementParameterSetLogEnable;

    /**
     * 批量级别的参数列表
     */
    private final List<List<Object>> batchParameterList = new LinkedList<>();
    /**
     * 记录本次存档每个字段的长度
     */
    private final Map<String, Integer> columnMaxLenMap;
    /**
     * 非批量的参数列表
     */
    private List<Object> parameters = new LinkedList<>();

    public PreparedStatementProxy(PreparedStatement pstmt, boolean statementParameterSetLogEnable, Map<String, Integer> columnMaxLenMap) {
        this.pstmt = pstmt;
        this.statementParameterSetLogEnable = statementParameterSetLogEnable;
        this.columnMaxLenMap = columnMaxLenMap;
    }

    public int executeUpdate() throws SQLException {
        return pstmt.executeUpdate();
    }

    public void addBatch() throws SQLException {
        pstmt.addBatch();
        batchParameterList.add(parameters);
        this.parameters = new LinkedList<>();
    }

    public int[] executeBatch() throws SQLException {
        return pstmt.executeBatch();
    }

    public ResultSet executeQuery() throws SQLException {
        return pstmt.executeQuery();
    }

    public void setObject(int parameterIndex, Object x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add(x);
        }
        pstmt.setObject(parameterIndex, x);
    }

    public void setByteArray(int parameterIndex, byte[] array) throws SQLException {
        this.setObject(parameterIndex, array);
    }

    public void setString(int parameterIndex, String x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add("'" + x + "'");
        }
        pstmt.setString(parameterIndex, x);
    }

    public void setLong(int parameterIndex, Long x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add(x);
        }
        pstmt.setLong(parameterIndex, x);
    }

    public void setInt(int parameterIndex, Integer x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add(x);
        }
        pstmt.setInt(parameterIndex, x);
    }

    public void setBoolean(int parameterIndex, Boolean x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add(x);
        }
        pstmt.setBoolean(parameterIndex, x);
    }

    public void setFloat(int parameterIndex, Float x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add(x);
        }
        pstmt.setFloat(parameterIndex, x);
    }

    public void setDouble(int parameterIndex, Double x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add(x);
        }
        pstmt.setDouble(parameterIndex, x);
    }

    public void setNull(int parameterIndex, int sqlType) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add(null);
        }
        pstmt.setNull(parameterIndex, sqlType);
    }

    public void setTimestamp(int parameterIndex, Timestamp x) throws SQLException {
        if (statementParameterSetLogEnable) {
            parameters.add("'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(x) + "'");
        }
        pstmt.setTimestamp(parameterIndex, x);
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public List<List<Object>> getBatchParameterList() {
        return batchParameterList;
    }
}