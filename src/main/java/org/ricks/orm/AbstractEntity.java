package org.ricks.orm;

import org.ricks.common.asm.MethodAccess;
import org.ricks.common.lang.Logger;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * 字段变化监听
 * 单条数据操作
 * @date 2020/12/1417:32
 */
public abstract class AbstractEntity<K,ID extends Serializable> implements IEntity<ID> {

    protected PropertyChangeSupport support = new PropertyChangeSupport(this);

    private MethodAccess access = MethodAccess.get(this.getClass());
    private Class<K> childClazz;

    {
        Class<?> c = this.getClass(); //子类创建 会创建父类 子类调用时 此处的this是子类
        Type t = c.getGenericSuperclass(); //获得带有泛型的父类
        if (t instanceof ParameterizedType) {
            Type[] p = ((ParameterizedType) t).getActualTypeArguments();  //取得所有泛型
            this.childClazz= (Class<K>) p[0];
        } else Logger.error("无法获得子类class please check BaseLongIDEntity<?> 父类是否加了泛型 ！");
        support.addPropertyChangeListener(new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                System.err.println("变化的name="+ evt.getPropertyName() + " & old="+ evt.getOldValue() + " & new=" + evt.getNewValue());
            }
        });
    }

    //那些字段存在变化
    private Map<String, Object> changeParamSet;

    private long autoIncrement() {
        return 0;
    }


}