package org.ricks.orm;

import java.io.Serializable;

/**
 * @author ricks
 * @Description:基本的数据存储对象
 * @date 2020/12/720:59
 */
public interface IEntity<ID extends Serializable> extends Serializable {
    public ID getId();
    public void setId(ID id);
}
