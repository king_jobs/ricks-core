package org.ricks.orm;

/**
 * 数据存储模块.
 */
public class DataModular {

    /**
     * 服务器数据存档间隔，单位：秒，默认值：5分钟
     */
    public static final String DATA_SAVE_INTERVAL = "data.save.interval";
    /**
     * 服务器数据缓存间隔，单位：秒，默认值：1小时
     */
    public static final String DATA_OFFLINE_INTERVAL = "data.offline.interval";
    /**
     * 服务器数据每次批量操作的最大数量，默认：64
     */
    public static final String DATA_BATCH_NUM = "data.batch.num";
    /**
     * 服务器数据存档SQL是否记录日志，默认值：false
     */
    public static final String DATA_SQL_LOG_ENABLE = "data.sql.log.enable";
    /**
     * 服务器数据存档SQL占位符参数是否记录日志（前提取决于{@link DataModular#DATA_SQL_LOG_ENABLE}）
     */
    public static final String DATA_SQL_LOG_PARAMETER_ENABLE = "data.sql.log.parameter.enable";
    /**
     * 服务器数据开启慢查询的时间（单位：ms）,默认为0，不开启
     */
    public static final String DATA_SLOW_QUERY_SQL_MILLIS = "data.slow.query.sql.millis";
    /**
     * 服务器数据是否智能删除表中多的字段，默认：false
     */
    public static final String DATA_AUTO_ALTER_TABLE_DROP_COLUMN = "data.auto.alter.table.drop.column";
    /**
     * 服务器数据是否智能修正文本字段的长度，默认：true
     */
    public static final String DATA_AUTO_ALTER_COLUMN_LENGTH = "data.auto.alter.column.length";
    /**
     * 服务器数据是否智能转化EMOJI的字段，默认：true
     */
    public static final String DATA_AUTO_ALTER_EMOJI_COLUMN = "data.auto.alter.emoji.column";
    /**
     * ORM data db 线程数量
     */
    public static final String DATA_THREAD_POOL_SIZE = "data.thread.pool.size";
    /**
     * ORM data db 线程名称前缀
     */
    public static final String DATA_THREAD_NAME_PREFIX = "data.thread.name.prefix";


    public static final String DATA_JDBC_URL = "data.jdbc.url";
    public static final String DATA_JDBC_DRIVER_CLASS_NAME = "data.jdbc.driverClassName";
    public static final String DATA_JDBC_USER_NAME = "data.jdbc.username";
    public static final String DATA_JDBC_PASSWORD = "data.jdbc.password";


//    public static String JDBC_URL;
//    public static String JDBC_DRIVER_CLASS_NAME;
//    public static String JDBC_USER_NAME;
//    public static String JDBC_PASSWORD;
//
//    /**
//     * ORM 装载配置文件
//     */
//    public static void load(){
//        JDBC_URL = RicksConf.getOrDefault(DATA_JDBC_URL,"");
//        JDBC_DRIVER_CLASS_NAME = RicksConf.getOrDefault(DATA_JDBC_DRIVER_CLASS_NAME,"");
//        JDBC_USER_NAME = RicksConf.getOrDefault(DATA_JDBC_USER_NAME,"");
//        JDBC_PASSWORD = RicksConf.getOrDefault(DATA_JDBC_PASSWORD,"");
//    }

}