package org.ricks.orm;

import org.ricks.ioc.Autowired;
import org.ricks.ioc.Init;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * 实时db操作
 */
public class BaseRepository<T ,K extends Serializable> {

    @Autowired
    protected DataAccessor dataAccessor;

    private volatile boolean pmdKnownBroken = false;

    public TableMeta<T> tableMeta;

    public BaseRepository() {
        Type mySuperClass = this.getClass().getGenericSuperclass();
        Type type = ((ParameterizedType) mySuperClass).getActualTypeArguments()[0];
        this.tableMeta = TableMetaBuilder.make((Class<T>) type);
    }

    @Init
    public void checkEntityAndInitCache() {
        if(dataAccessor != null) {
            dataAccessor.checkupEntityFieldsWithDatabase(tableMeta); //检查实体类与DB映射关系并初始化缓存
        }
    }

    /**
     * 向存储策略接口插入一个实体对象.
     *
     * @param entity 实体类对象.
     */
    public void insert(T entity) {
        dataAccessor.insert(tableMeta, entity);
    }

    /**
     * 向存储策略接口插入一批实体对象.
     *
     * @param entitys 一批实体对象
     */
    public void batchInsert(List<T> entitys) {
        dataAccessor.batchInsert(tableMeta, entitys);
    }

    /**
     * 向存储策略接口删除一个实体对象.
     *
     * @param entity 实体类对象.
     */
    public void delete(T entity) {
        dataAccessor.delete(tableMeta, entity);
    }

    /**
     * 向存储策略接口删除一批实体对象.
     *
     * @param entitys 一批实体对象
     */
    public void batchDelete(List<T> entitys) {
        dataAccessor.batchDelete(tableMeta, entitys);
    }

    /**
     * 向存储策略接口修改一个实体对象.
     *
     * @param entity 实体类对象.
     */
    public void update(T entity) {
        dataAccessor.update(tableMeta, entity);
    }

    /**
     * 向存储策略接口修改一批实体对象.
     *
     * @param entitys 一批实体对象
     */
    public void batchUpdate(List<T> entitys) {
        dataAccessor.batchUpdate(tableMeta, entitys);
    }

    /**
     * 根据角色ID和实体Id从存储策略层加载数据.
     *
     * @param entityId 实体ID.
     * @return 如果存在此ID的对象，则返回此对象，否则返回 null
     */
    public T load(K entityId) {
        return (T) dataAccessor.load(tableMeta, entityId);
    }

    /**
     * 根据角色ID和实体Id从存储策略层加载数据.
     *
     * @param parameterMap 参数 k-字段名称 v-字段值
     * @return 如果存在此ID的对象，则返回此对象，否则返回 null
     */
    public T load(Map<String,Object> parameterMap) {
        return (T) dataAccessor.load(tableMeta, parameterMap);
    }


    /**
     * 从存储策略层加载数据.
     * <p>
     * 业内替规则：返回集合时，不要返回null 如果为空也要返回空列表
     *
     * @return 如果存在此类对象，则返回对象列表，否则返回 空列表.
     */
    public List<T> loadAll() {
        return dataAccessor.loadAll(tableMeta);
    }

    /**
     * 根据playerId从存储策略层加载数据.
     * <p>
     * 如果是系统的就直接调用LoadAll
     *
     * @param playerId 角色ID.
     * @return 如果存在此角色ID的对象，则返回对象列表，否则返回 空列表.
     */
    public List<T> loadAll(Serializable playerId) {
        return dataAccessor.loadAll(tableMeta, playerId);
    }

    /**
     * sql 增删改 ，
     * @param sql
     * @param <T>
     * @return
     */
    public <T> int execute(String sql,Object... parameters) {
        return dataAccessor.execute(tableMeta,sql,parameters);
    }

    public <T> T executeLoad(String sql,Object... parameters) {
        return (T) dataAccessor.executeLoad(tableMeta,sql,parameters);
    }

    public <T> List<T> executeLoadAll(String sql,Object... parameters) {
        return (List<T>) dataAccessor.executeLoadAll(tableMeta,sql,parameters);
    }

}
