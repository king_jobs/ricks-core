package org.ricks.orm;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 */
public class TableSupport {


    private static final Set<Type> notJavaBeanSet = new HashSet();

    static {
        notJavaBeanSet.add( boolean.class);
        notJavaBeanSet.add( Boolean.class);
        notJavaBeanSet.add( byte.class);
        notJavaBeanSet.add( Byte.class);
        notJavaBeanSet.add( short.class);
        notJavaBeanSet.add( Short.class);
        notJavaBeanSet.add( int.class);
        notJavaBeanSet.add( Integer.class);
        notJavaBeanSet.add( long.class);
        notJavaBeanSet.add( Long.class);
        notJavaBeanSet.add( float.class);
        notJavaBeanSet.add( Float.class);
        notJavaBeanSet.add( double.class);
        notJavaBeanSet.add( Double.class);
        notJavaBeanSet.add( String.class);
        notJavaBeanSet.add( Date.class);

    }

    public static boolean isJavaBean(Type type) {
        return !notJavaBeanSet.contains(type);
    }

    public static boolean isJavaBean(ColumnMeta columnMeta) {
        return isJavaBean(columnMeta.getField().getGenericType());
    }
}
