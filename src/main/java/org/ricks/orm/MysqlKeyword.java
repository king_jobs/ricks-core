package org.ricks.orm;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Mysql关键字.
 *
 */
public class MysqlKeyword {
    private static final HashSet<String> KEYWORD = new HashSet<>(Arrays.asList(
            // A开头的.
            "add", "all", "alter", "analyze", "and", "as", "asc", "asensitive",
            // B开头的.
            "before", "between", "bigint", "binary", "blob", "both", "by",
            // C开头的.
            "call", "cascade", "case", "change", "char", "character", "check", "collate", "column", "condition", "connection", "constraint", "continue", "convert", "create", "cross", "current_date", "current_time", "current_timestamp", "current_user",
            "cursor",
            // D开头的.
            "database", "databases", "day_hour", "day_microsecond", "day_minute", "day_second", "dec", "decimal", "declare", "default", "delayed", "delete", "desc", "describe", "deterministic", "distinct", "distinctrow", "div", "double", "drop", "dual",
            // E开头的.
            "each", "else", "elseif", "enclosed", "escaped", "exists", "exit", "explain",
            // F开头的.
            "false", "fetch", "float", "float4", "float8", "for", "force", "foreign", "from", "fulltext",
            // G开头的.
            "goto", "grant", "group",
            // H开头的.
            "having", "high_priority", "hour_microsecond", "hour_minute", "hour_second",
            // I开头的.
            "if", "ignore", "in", "index", "infile", "inner", "inout", "insensitive", "insert", "int", "int1", "int2", "int3", "int4", "int8", "integer", "interval", "into", "is", "iterate",
            // J开头的.
            "join", "key", "keys", "kill",
            // L开头的.
            "label", "leading", "leave", "left", "like", "limit", "linear", "lines", "load", "localtime", "localtimestamp", "lock", "long", "longblob", "longtext", "loop", "low_priority",
            // M开头的.
            "match", "mediumblob", "mediumint", "mediumtext", "middleint", "minute_microsecond", "minute_second", "mod", "modifies",
            // N开头的.
            "natural", "not", "no_write_to_binlog", "null", "numeric",
            // O开头的.
            "on", "optimize", "option", "optionally", "or", "order", "out", "outer", "outfile",
            // P开头的.
            "precision", "primary", "procedure", "purge",
            // R开头的.
            "raid0", "range", "read", "reads", "real", "references", "regexp", "release", "rename", "repeat", "replace", "require", "restrict", "return", "revoke", "right", "rlike",
            // S开头的.
            "schema", "schemas", "second_microsecond", "select", "sensitive", "separator", "set", "show", "smallint", "spatial", "specific", "sql", "sqlexception", "sqlstate", "sqlwarning", "sql_big_result", "sql_calc_found_rows", "sql_small_result",
            "ssl", "starting", "straight_join",
            // T开头的.
            "table", "terminated", "then", "tinyblob", "tinyint", "tinytext", "to", "trailing", "trigger", "true",
            // U开头的.
            "undo", "union", "unique", "unlock", "unsigned", "update", "usage", "use", "using", "utc_date", "utc_time", "utc_timestamp",
            // V开头的.
            "values", "varbinary", "varchar", "varcharacter", "varying",
            // WXYZ开头的.
            "when", "where", "while", "with", "write", "x509", "xor", "year_month", "zerofill"));

    /**
     * 判定指定名称是否为关键字.
     *
     * @param name 指定名称
     * @return 如果是关键字的返回true
     */
    public static boolean isKeyword(String name) {
        return KEYWORD.contains(name);
    }
}