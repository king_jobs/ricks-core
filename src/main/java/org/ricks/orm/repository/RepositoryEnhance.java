package org.ricks.orm.repository;

import org.ricks.common.asm.*;
import org.ricks.orm.anno.Table;

/**
 * Repository Asm字节增强
 */
public class RepositoryEnhance implements Opcodes {

    public static byte[] enhance(Class<?> entityClazz) throws Exception {

        var simpleName = entityClazz.getSimpleName() + "Repository";
        Table table = entityClazz.getAnnotation(Table.class);
        Class<?> superClazz = table.repository();
        var packetName = (entityClazz.getCanonicalName() + "Repository").replace(".","/");
        var superPacketName = superClazz.getCanonicalName().replace(".","/");
        ClassWriter classWriter = new ClassWriter(0);
        FieldVisitor fieldVisitor;
        RecordComponentVisitor recordComponentVisitor;
        MethodVisitor methodVisitor;
        AnnotationVisitor annotationVisitor0;

        classWriter.visit(V17, ACC_PUBLIC | ACC_SUPER, "org/ricks/orm/" + simpleName
                , "L" +superPacketName+ "<L" +entityClazz.getCanonicalName().replace(".","/")+ ";Ljava/lang/Long;>;", superPacketName, null);

        classWriter.visitSource(simpleName + ".java", null);

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, superPacketName, "<init>", "()V", false);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitInsn(RETURN);
            Label label2 = new Label();
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLocalVariable("this", "Lorg/ricks/orm/" + simpleName+";", null, label0, label2, 0);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        return classWriter.toByteArray();
    }
}
