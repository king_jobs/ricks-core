package org.ricks.orm.repository;

import org.ricks.common.lang.ExpirationListener;
import org.ricks.orm.TableMeta;

import java.io.Serializable;
import java.util.List;

/**
 * 一种带有缓存类型的数据存储接口.
 *
 */
public interface CacheRepository<T, K extends Serializable> {

    /**
     * 获取实体映射描述对象.
     *
     * @return 实体映射描述对象.
     */
    TableMeta<T> getEntityMapping();

    /**
     * 根据实体ID载入实体对象
     *
     * @param entityId 实体ID
     * @return 实体对象
     */
    T load(K entityId);

    /**
     * 载入全部实体对象.
     *
     * @return 全部实体对象
     */
    List<T> loadAll();

    /**
     * 根据玩家ID载入全部对象.
     *
     * @param playerId 玩家ID
     * @return 全部实体对象
     */
    List<T> loadAll(Serializable playerId);

    /**
     * 缓存清除机制
     * 缓存失效
     * @param k
     */
    void cacheInvalidate(K k);

    /**
     * 缓存过期监听
     * @return
     */
    ExpirationListener getCacheExpirationListener();
}