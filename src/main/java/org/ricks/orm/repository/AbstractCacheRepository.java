package org.ricks.orm.repository;

import org.ricks.orm.BaseRepository;
import org.ricks.orm.anno.Table;
import org.ricks.orm.cache.CacheMgr;
import org.ricks.orm.cache.DataCache;
import org.ricks.orm.write.AsyncWriteManager;
import org.ricks.ioc.Autowired;
import org.ricks.common.lang.ExpirationListener;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * 以玩家ID作为异步db
 */
public abstract class AbstractCacheRepository<T,K extends Serializable> extends BaseRepository<T,K> implements CacheRepository<T, K> {
    protected DataCache<T, K> dataCache;

    @Autowired
    protected ExpirationListener expirationListener;

    public void checkEntityAndInitCache() {
        super.checkEntityAndInitCache();
        // 缓存抓取策略
        this.dataCache = buildDataCache();

        if (tableMeta.getFetchType() == Table.FetchType.START) {
            dataCache.initCacheData();
        }
        CacheMgr.get().addCacheRepositories(this); //缓存维护起来，以便玩家退出统一销毁
    }

    @Override
    public ExpirationListener getCacheExpirationListener() {
        return expirationListener;
    }

    /**
     * 创建数据缓存.
     *
     * @return 数据缓存实现
     */
    protected abstract DataCache<T, K> buildDataCache();


    /**
     * 保存一个新增对象到缓存.
     *
     * @param entity 新增对象.
     */
    public void insert(T entity) {

        dataCache.insert(entity);

        AsyncWriteManager.me().insert(tableMeta, entity);
    }

    /**
     * 删除缓存一个对象.
     *
     * @param entity 实体对象.
     */
    public void delete(T entity) {
        dataCache.delete(entity);

        AsyncWriteManager.me().delete(tableMeta, entity);
    }

    /**
     * 删除当前模块全部缓存对象.
     * <p>
     * <b>这是删除全部，调用时，别犯2</b>
     */
    public void deleteAll() {
        List<T> result = dataCache.deleteAll();

        AsyncWriteManager.me().deleteAll(tableMeta, result);
    }

    /**
     * 修改缓存中的数据.
     *
     * @param entity 实体对象.
     */
    public void update(T entity) {

        dataCache.update(entity);

        AsyncWriteManager.me().update(tableMeta, entity);
    }

    /**
     * 从缓存中Load指定ID的对象
     *
     * @param entityId 缓存对象的Id
     * @return 对象
     */
    public Optional<T> cacheLoad(K entityId) {
        return Optional.ofNullable(dataCache.load(entityId));
    }

    /**
     * 从缓存中Get指定ID的对象
     *
     * @param entityId 缓存对象的Id
     * @return 对象
     */
    public T cacheGet(K entityId) {
        return dataCache.load(entityId);
    }

    /**
     * 从缓存中获取所有缓存数据.
     *
     * @return 所有缓存数据
     */
    public List<T> cacheLoadAll() {
        return dataCache.loadAll();
    }

    public void setExpirationListener(ExpirationListener expirationListener) {
        this.expirationListener = expirationListener;
    }
}
