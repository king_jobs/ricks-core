package org.ricks.orm.repository;

import org.ricks.orm.TableMeta;
import org.ricks.orm.cache.DataCache;
import org.ricks.orm.cache.UniqueDataCacheImpl;
import org.ricks.orm.write.AsyncWriteManager;

import java.io.Serializable;
import java.util.List;
import java.util.function.Predicate;

/**
 * 封装了一套缓存机制的ORM数据访问层.
 * <p>
 * 应用于两种情况：<br>
 * 可以理解为，一个角色只有一条记录或不属于任何角色的数据的类.<br>
 *
 */
public class UniqueCacheRepository<T, K extends Serializable> extends AbstractCacheRepository<T, K> {

    /**
     * 从缓存中删除指定Id的对象.
     * <p>
     *
     * @param entityId 缓存对象的Id
     */
    public void cacheDelete(K entityId) {
        T result = dataCache.delete(entityId);

        AsyncWriteManager.me().delete(tableMeta, result);
    }

    /**
     * 根据条件从缓存中获取所有缓存数据.
     *
     * @param filter 条件
     * @return 所有缓存数据
     */
    public List<T> cacheLoadAll(Predicate<T> filter) {
        return dataCache.loadAll(filter);
    }

    /**
     * 直接统计缓存中数据的数量
     *
     * @return 缓存中数据的数量
     */
    public long cacheCount() {
        return dataCache.count();
    }

    @Override
    protected DataCache<T, K> buildDataCache() {
        return new UniqueDataCacheImpl<>(this);
    }

    @Override
    public TableMeta<T> getEntityMapping() {
        return tableMeta;
    }

    @Override
    public void cacheInvalidate(K k) {
        dataCache.invalidate(k);
    }

    public List<K> cacheKeys() {
        return dataCache.cacheKeys();
    }
}