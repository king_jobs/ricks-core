package org.ricks.orm.repository;

import org.ricks.orm.TableMeta;
import org.ricks.orm.cache.DataCache;
import org.ricks.orm.cache.MultiDataCacheImpl;
import org.ricks.orm.write.AsyncWriteManager;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * 封装了一套缓存机制的ORM数据访问层.
 * <p>
 * 应用于一种情况：<br>
 * 可以理解为，一个角色可以有多条记录数据的类.<br>
 *
 * @param <T> 实体类
 * @param <K> 实体类的主键
 */
public class MultiCacheRepository<T, K extends Serializable> extends AbstractCacheRepository<T, K> {

    /**
     * 从缓存中删除指定Id的对象.
     *
     * @param playerId 玩家Id
     * @param entityId 缓存对象的Id
     */
    public void cacheDelete(Serializable playerId, K entityId) {
        T result = dataCache.load(playerId, entityId);
        dataCache.delete(result);
        AsyncWriteManager.me().delete(tableMeta, result);
    }

    /**
     * 删除角色模块缓存里的所有数据.
     *
     * @param playerId 角色Id
     */
    public void cacheDeleteAll(Serializable playerId) {
        List<T> result = dataCache.deleteAll(playerId);

        AsyncWriteManager.me().deleteAll(tableMeta, result);
    }


    /**
     * 从角色缓存中根据实体Id获取对象.
     *
     * @param playerId 角色Id
     * @param entityId 实体Id
     * @return 实体对象.
     */
    public Optional<T> cacheLoad(Serializable playerId, K entityId) {
        return Optional.of(dataCache.load(playerId, entityId));
    }

    /**
     * 从角色缓存中根据实体Id获取对象.
     *
     * @param playerId 角色Id
     * @param entityId 实体Id
     * @return 实体对象.
     */
    public T cacheGet(Serializable playerId, K entityId) {
        return dataCache.load(playerId, entityId);
    }

    /**
     * 从角色缓存中根据过滤器获取对象.
     *
     * @param playerId 角色Id
     * @param filter   条件过滤器
     * @return 实体对象.
     */
    public T cacheGet(Serializable playerId, Predicate<T> filter) {
        return this.cacheLoad(playerId, filter).orElse(null);
    }

    /**
     * 从角色缓存中根据过滤器获取对象.
     *
     * @param playerId 角色Id
     * @param filter   条件过滤器
     * @return 实体对象.
     */
    public Optional<T> cacheLoad(Serializable playerId, Predicate<T> filter) {
        return Optional.ofNullable(dataCache.load(playerId, filter));
    }

    /**
     * 统计角色缓存中符合过滤条件的对象总数。
     *
     * @param playerId 角色Id
     * @param filter   条件过滤器
     * @return 符合过滤条件的对象总数。
     */
    public long cacheCount(Serializable playerId, Predicate<T> filter) {
        return dataCache.count(playerId, filter);
    }

    /**
     * 从角色缓存中获取一个模块所有缓存数据.
     *
     * @param playerId 角色Id
     * @return 一个模块所有缓存数据.
     */
    public List<T> cacheLoadAll(Serializable playerId) {
        return dataCache.loadAll(playerId);
    }

    /**
     * 从缓存中获取符合过虑器的需求的对象.
     *
     * @param playerId 角色Id
     * @param filter   过虑器
     * @return 符合过虑器的需求的对象列表.
     */
    public List<T> cacheLoadAll(Serializable playerId, Predicate<T> filter) {
        return dataCache.loadAll(playerId, filter);
    }

    @Override
    protected DataCache<T, K> buildDataCache() {
        return new MultiDataCacheImpl<>(this);
    }

    @Override
    public TableMeta<T> getEntityMapping() {
        return tableMeta;
    }

    @Override
    public void cacheInvalidate(K k) {
        dataCache.invalidate(k);
    }
}