package org.ricks.orm;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 数据访问策略接口.
 * DataSource 已经在配置Bean set进来，可以直接使用
 *
 * 其实可以直接new MysqlDataAccessor 对象，但是考虑后期需要替换mongodb，所以还是用IOC注入方式
 */
public interface DataAccessor {

    /**
     * 猜测一下用的DB信息
     */
    public void judgeAccessType();

    /**
     * 检查实体类对应的数据库中的表结构.
     * <p>
     * 如果是关系型数据库，没有表则创建表，属性不一样就修改成一样的.
     *
     * @param <T> 实体对象类型
     * @param tm  对象实体描述类.
     */
    public <T> void checkupEntityFieldsWithDatabase(TableMeta<T> tm);

    /**
     * 插入一条数据.
     *
     * @param <T>    实体对象类型
     * @param  tm     对象实体描述类.
     * @param entity 对象数据.
     * @return 返回插入所受影响行数.
     */
    public <T> int insert(TableMeta<T> tm, T entity);

    /**
     * 插入一批数据.
     *
     * @param <T>     实体对象类型
     * @param  tm      对象实体描述类.
     * @param entitys 一批对象数据
     * @return 返回插入所受影响行数
     */
    public <T> int[] batchInsert(TableMeta<T> tm, List<T> entitys);

    /**
     * 删除一条数据.
     *
     * @param <T>    实体对象类型
     * @param  tm     对象实体描述类.
     * @param entity 对象数据.
     * @return 返回删除所受影响行数.
     */
    public <T> int delete(TableMeta<T> tm, T entity);

    /**
     * 删除一批数据.
     *
     * @param <T>     实体对象类型
     * @param  tm      对象实体描述类.
     * @param entitys 一批对象数据
     * @return 返回删除所受影响行数.
     */
    public <T> int[] batchDelete(TableMeta<T> tm, List<T> entitys);

    /**
     * 修改一条数据.
     *
     * @param <T>    实体对象类型
     * @param  tm     对象实体描述类.
     * @param entity 对象数据.
     * @return 返回修改所受影响行数.
     */
    public <T> int update(TableMeta<T> tm, T entity);

    /**
     * 修改一批数据.
     *
     * @param <T>     实体对象类型
     * @param  tm      对象实体描述类.
     * @param entitys 一批对象数据
     * @return 返回修改所受影响行数.
     */
    public <T> int[] batchUpdate(TableMeta<T> tm, List<T> entitys);

    /**
     * 加载一个指定ID的数据.
     *
     * @param <T> 实体对象类型
     * @param <K> 实体主键类型
     * @param  tm  对象实体描述类.
     * @param id  对象Id.
     * @return 返回对象数据.
     */
    public <T, K extends Serializable> T load(TableMeta<T> tm, K id);

    /**
     * 加载一个指定ID的数据.
     *
     * @param <T> 实体对象类型
     * @param  tm  对象实体描述类.
     * @param parameterMap  字段名称.
     * @return 返回对象数据.
     */
    public <T> T load(TableMeta<T> tm, Map<String,Object> parameterMap);

    /**
     * 加载表里所有的数据.
     *
     * @param <T> 实体对象类型
     * @param  tm  对象实体描述类.
     * @return 返回对象数据列表，就算没有数据，也会返回空列表.
     */
    public <T> List<T> loadAll(TableMeta<T> tm);

    /**
     * 加载指定角色Id对应模块数据.
     *
     * @param <T>      实体对象类型
     * @param  tm       对象实体描述类
     * @param playerId 玩家Id
     * @return 返回这个角色Id的模块数据，就算没有数据，也会返回空列表.
     */
    public <T> List<T> loadAll(TableMeta<T> tm, Serializable playerId);

    /**
     * sql 增删改
     * @param tm
     * @param sql
     * @param <T>
     * @return
     */
    public <T> int execute(final TableMeta<T> tm, String sql,Object[] parameters);

    /**
     * sql load单条数据
     * @param tm
     * @param sql
     * @param <T>
     * @return
     */
    public <T> T executeLoad(final TableMeta<T> tm, String sql,Object[] parameters);

    /**
     * sql load多条数据
     * @param tm
     * @param sql
     * @param <T>
     * @return
     */
    public <T> List<T> executeLoadAll(final TableMeta<T> tm, String sql,Object[] parameters);
}