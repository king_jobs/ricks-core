package org.ricks.orm;

import org.ricks.common.asm.MethodAccess;
import org.ricks.orm.anno.Column;
import org.ricks.orm.anno.IndexKey;
import org.ricks.orm.anno.PrimaryKey;
import org.ricks.orm.anno.Table;
import org.ricks.common.exception.DataException;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

/**
 * @author ricks
 * @Description:
 * @date 2020/12/211:30
 */
public class TableMetaBuilder {

    public static TableMeta make(Class<?> clazz) {
        TableMeta tableMeta = new TableMeta(clazz);
        Table table = clazz.getAnnotation(Table.class);
        Objects.requireNonNull(table, String.format("entity need use @Table - %s", clazz.getName()));
        tableMeta.setName(table.name().isBlank() ? clazz.getSimpleName() : table.name());
        if (table.charset().length() > 0) {
            tableMeta.setCharset(table.charset());
            if (table.collate().length() > 0) {
                tableMeta.setCollate(table.collate());
            }
        }
        tableMeta.setComment(table.comment());
        tableMeta.setColumns(new LinkedList());
        tableMeta.setIndexKeys(new HashSet());
        Class tempClass = clazz;
        while (tempClass !=null && !tempClass.getName().toLowerCase().equals("java.lang.object")) {
            Field[] fields = tempClass.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                Column column = field.getAnnotation(Column.class);
                if (column != null) {
                    PrimaryKey primaryKey = field.getAnnotation(PrimaryKey.class);
                    ColumnMeta columnMeta = makeColumnMate(field,tableMeta.getMethodAccess());
                    if(columnMeta.isPlayerId()) {
                        if (tableMeta.getPlayerIdColumn() != null) {
                            throw new DataException(clazz.getName() + "一个实体中最多只能有一个@PlayerId ≡ (^(OO)^) ≡");
                        }
                        tableMeta.setPlayerIdColumn(columnMeta);
                    }
                    if (primaryKey != null) {
                        if (tableMeta.getPrimaryColumn() != null) {
                            throw new DataException(String.format("only one primary key - %s", clazz.getName()));
                        }
                        tableMeta.setPrimaryColumn(columnMeta);
                        tableMeta.getColumns().add(columnMeta);
                        if (TableSupport.isJavaBean( tableMeta.getPrimaryColumn())) {
                            throw new DataException("primary key type not be java bean");
                        }
                    } else {
                        tableMeta.getColumns().add(columnMeta);
                        IndexKey indexKey = field.getAnnotation(IndexKey.class);
                        if (indexKey != null) {
                            tableMeta.getIndexKeys().add(columnMeta);
                        }
                    }
                }
            }
            tempClass = tempClass.getSuperclass(); //得到父类,然后赋给自己
        }
        Map<String, ColumnMeta> columnMetaMap = new HashMap();
        columnMetaMap.put(tableMeta.getPrimaryColumn().getColumnName(), tableMeta.getPrimaryColumn());
        List<ColumnMeta> columnMetas = tableMeta.getColumns();
        columnMetas.forEach(columnMeta -> columnMetaMap.put(columnMeta.getColumnName(), columnMeta));
        tableMeta.setColumnMetaMap(columnMetaMap);
        return tableMeta;
    }


    /**
     * 字段名称 userName 自动转化下划线  user_name
     * @param field
     * @param methodAccess
     * @return
     */
    private static ColumnMeta makeColumnMate(Field field, MethodAccess methodAccess) {
        ColumnMeta columnMeta = new ColumnMeta(field,methodAccess);
        Column column = field.getAnnotation(Column.class);
        String columnName = column.columnName().isBlank() ? field.getName() : column.columnName();
        columnMeta.setColumnName(columnName);
        guessEntityFieldColumnType(columnMeta);
        columnMeta.setReadOnly(column.readOnly());
        columnMeta.setField(field);
        if(List.class.isAssignableFrom(field.getType())) {
            Type type = field.getGenericType();
            Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
            if (actualTypeArguments != null && actualTypeArguments.length > 0) {
                 columnMeta.setFieldGenericityClazz((Class<?>) actualTypeArguments[0]);
            }
        }

        columnMeta.setNonNull(column.nonNull());
        columnMeta.setUnsigned(column.unsigned());
        PrimaryKey primaryKey = field.getAnnotation(PrimaryKey.class);
        if (primaryKey != null) {
            if (columnMeta.isIntNumber()) {
                columnMeta.setAuto(primaryKey.auto());
            }
            columnMeta.setPrimaryKey(true);
            columnMeta.setNonNull(true);
        }
        IndexKey indexKey = field.getAnnotation(IndexKey.class);
        if (indexKey != null) {
            String indexName = indexKey.index();
            columnMeta.setIndex(indexName.isBlank() ? "index_"+ columnName : indexName);
        }
        columnMeta.setComment(column.comment());

        return columnMeta;
    }

    /**
     * 根据字段现有的信息，尽可能猜测一下字段的数据库类型
     *
     * @param fm 映射字段
     */
    public static void guessEntityFieldColumnType(ColumnMeta fm) {
        Type type = fm.getField().getGenericType();
        // 明确标识为时间类型的属性
        if (type == Date.class) {
            fm.setType(FieldType.AsDate);
        }
        // JDK8的时间
        else if (type == LocalDateTime.class) {
            fm.setType(FieldType.AsLocalDateTime);
        }
        // 明确标识为JSON类型的属性
        else if (fm.isJson()) {
            fm.setType(FieldType.AsJson);
            fm.setLength(fm.getColumn() == null ? 1024 : fm.getLength());
        }
        // Blob或byte[]
        else if (fm.isBlob()) {
            fm.setType(FieldType.AsBlob);
        }
        // 整型
        else if (fm.isInt()) {
            fm.setLength(8);
            fm.setType(FieldType.AsInteger);
        }
        // 字符串
        else if (fm.isString()) {
            fm.setType(FieldType.AsString);
            fm.setLength(fm.getColumn() == null ? 255 : fm.getColumn().length());
        }
        // 长整型
        else if (fm.isLong()) {
            fm.setLength(16);
            fm.setType(FieldType.AsLong);
        }
        // 时间
        else if (Instant.class == type) {
            fm.setType(FieldType.AsInstant);
        }
        // 布尔
        else if (fm.isBoolean()) {
            fm.setType(FieldType.AsBoolean);
        }
        // Float
        else if (fm.isFloat()) {
            fm.setType(FieldType.AsFloat);
        }
        // Double
        else if (fm.isDouble()) {
            fm.setType(FieldType.AsDouble);
        }
        // AtomicInteger
        else if (type == AtomicInteger.class) {
            fm.setLength(8);
            fm.setType(FieldType.AsAtomicInteger);
        }
        // AtomicLong
        else if (type == AtomicLong.class) {
            fm.setLength(16);
            fm.setType(FieldType.AsAtomicLong);
        }
        // LongAdder
        else if (type == LongAdder.class) {
            fm.setLength(16);
            fm.setType(FieldType.AsLongAdder);
        }
        // 其他就是Json类型的.
        else {
            fm.setType(FieldType.AsJson);
            fm.setLength(fm.getColumn() == null ? 1024 : fm.getColumn().length());
        }
    }

}
