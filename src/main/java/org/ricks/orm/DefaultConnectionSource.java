package org.ricks.orm;

import org.ricks.common.exception.DaoRuntimeException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

/**
 * 一个默认的，框架只需要提供{@code DataSource}既可以集成到beetlsql里
 * @author xiandafu
 */
public class DefaultConnectionSource implements ConnectionSource {
    protected DataSource master = null;
    protected DataSource[] slaves = null;

    public DefaultConnectionSource() {
    }

    public DefaultConnectionSource(DataSource master, DataSource[] slaves) {
        this.master = master;
        this.slaves = slaves;

    }

    @Override
    public Connection getConn(boolean isUpdate) {
        if(getForceDataSource()!=null){
            try {
                return getForceDataSource().getConnection();
            } catch (SQLException e) {
                throw new DaoRuntimeException(DaoRuntimeException.CANNOT_GET_CONNECTION, e);
            }
        }

        if (this.slaves == null || this.slaves.length == 0) {
            return this.getWriteConn();
        }
        if (isUpdate) {
            return this.getWriteConn();
        } else {
            return this.getReadConn();
        }

    }

    @Override
    public Connection getMasterConn() {
        return doGetConnection(master);

    }

    protected Connection getReadConn() {
        if (slaves == null || slaves.length == 0) {
            return getWriteConn();
        } else {

            return nextSlaveConn();
        }
    }

    protected Connection getWriteConn() {

        return doGetConnection(master);

    }

    protected Connection nextSlaveConn() {
        //随机，todo，换成顺序
        DataSource ds = slaves[new Random().nextInt(slaves.length)];
        return doGetConnection( ds);
    }

    protected Connection doGetConnection( DataSource ds) {
        try {
            return ds.getConnection();

        } catch (SQLException e) {
            throw new DaoRuntimeException(DaoRuntimeException.CANNOT_GET_CONNECTION, e);
        }
    }
    @Override
    public DataSource getMasterSource() {
        return master;
    }

    public void setMasterSource(DataSource master) {
        this.master = master;
    }


    @Override
    public Connection getMetaData() {
        return this.getMasterConn();
    }

    @Override
    public DataSource[] getSlaves() {
        return slaves;
    }

}
