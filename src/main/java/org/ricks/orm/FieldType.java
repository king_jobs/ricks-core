package org.ricks.orm;

/**
 * 属性类型.
 *
 */
public enum FieldType {
    /**
     * 字符串
     */
    AsString,
    /**
     * Long类型
     */
    AsLong,
    /**
     * Integer类型
     */
    AsInteger,
    /**
     * AtomicInteger类型
     */
    AsAtomicInteger,
    /**
     * AtomicLong类型
     */
    AsAtomicLong,
    /**
     * LongAdder类型
     */
    AsLongAdder,
    /**
     * Boolean类型
     */
    AsBoolean,
    /**
     * Float类型
     */
    AsFloat,
    /**
     * Double类型
     */
    AsDouble,
    /**
     * Instant类型
     */
    AsInstant,
    /**
     * Date类型
     */
    AsDate,
    /**
     * LocalDateTime类型
     */
    AsLocalDateTime,
    /**
     * JSON类型
     */
    AsJson,
    /**
     * Blob类型
     */
    AsBlob;
}