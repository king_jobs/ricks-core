package org.ricks.orm.cache;

/**
 * K是userId ，约定不允许getAll
 * @param <K>
 * @param <V>
 */
public interface LoadingCache<K, V> extends Cache<K, V> {
    V get(K var1);

}