package org.ricks.orm.cache;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public interface AsyncCacheLoader<K, V> {
    
    CompletableFuture<V> asyncLoad( K var1,  Executor var2);

    
    default CompletableFuture<Map<K, V>> asyncLoadAll( Iterable<? extends K> keys,  Executor executor) {
        throw new UnsupportedOperationException();
    }

    
    default CompletableFuture<V> asyncReload( K key,  V oldValue,  Executor executor) {
        return this.asyncLoad(key, executor);
    }
}
