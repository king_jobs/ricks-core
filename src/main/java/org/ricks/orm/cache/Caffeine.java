package org.ricks.orm.cache;

import org.ricks.common.lang.Assert;
import org.ricks.common.lang.ExpirationListener;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public final class Caffeine<K, V> {
    long maximumSize = -1L;
    long maximumWeight = -1L;
    int initialCapacity = -1;
    long refreshNanos = -1L;
    long expireAfterAccessNanos = -1L;
    ExpirationListener expirationListener;

    private Caffeine() {}

    public Caffeine<K, V> expireAfterAccess(long duration, TimeUnit unit) {
        Assert.isTrue(duration >= 0, "duration cannot be negative: %s %s", duration, unit);
        this.expireAfterAccessNanos = unit.toMillis(duration);
        return this;
    }

    public Caffeine<K,V> expirationListener(ExpirationListener expirationListener) {
        this.expirationListener = expirationListener;
        return this;
    }
    public ExpirationListener getExpirationListener() {
        return expirationListener;
    }

    
    public static Caffeine<Object, Object> newBuilder() {
        return new Caffeine();
    }

    public Caffeine<K, V> initialCapacity( int initialCapacity) {
        Assert.isTrue(this.initialCapacity == -1, "initial capacity was already set to %s", this.initialCapacity);
        Assert.isTrue(initialCapacity >= 0);
        this.initialCapacity = initialCapacity;
        return this;
    }

    boolean hasInitialCapacity() {
        return this.initialCapacity != -1;
    }

    int getInitialCapacity() {
        return this.hasInitialCapacity() ? this.initialCapacity : 16;
    }

    
    public Caffeine<K, V> maximumSize( long maximumSize) {
        Assert.isTrue(this.maximumSize == -1L, "maximum size was already set to %s", this.maximumSize);
        Assert.isTrue(this.maximumWeight == -1L, "maximum weight was already set to %s", this.maximumWeight);
        Assert.isTrue(maximumSize >= 0L, "maximum size must not be negative");
        this.maximumSize = maximumSize;
        return this;
    }



    long getExpiresAfterAccessNanos() {
        return this.expiresAfterAccess() ? this.expireAfterAccessNanos : 0L;
    }

    boolean expiresAfterAccess() {
        return this.expireAfterAccessNanos != -1L;
    }

    
    public Caffeine<K, V> refreshAfterWrite( long duration,  TimeUnit unit) {
        Objects.requireNonNull(unit);
        Assert.isTrue(this.refreshNanos == -1L, "refresh was already set to %s ns", this.refreshNanos);
        Assert.isTrue(duration > 0L, "duration must be positive: %s %s", duration, unit);
        this.refreshNanos = unit.toNanos(duration);
        return this;
    }

    long getRefreshAfterAccessNanos() {
        return this.expiresAfterAccess() ? this.expireAfterAccessNanos : 0L;
    }

    boolean refreshes() {
        return this.refreshNanos != -1L;
    }

    
    public <K1 extends K, V1 extends V> LoadingCache<K1, V1> build( CacheLoader<? super K1, V1> loader) {
        return new UnboundedLocalCache.UnboundedLocalLoadingCache( this,loader);
    }

    static int ceilingPowerOfTwo(int x) {
        // From Hacker's Delight, Chapter 3, Harry S. Warren Jr.
        return 1 << -Integer.numberOfLeadingZeros(x - 1);
    }

    long getMaximum() {
        return  maximumSize;
    }

    private static long saturatedToNanos(Duration duration) {
        try {
            return duration.toNanos();
        } catch (ArithmeticException var2) {
            return duration.isNegative() ? -9223372036854775808L : 9223372036854775807L;
        }
    }

    public String toString() {
        StringBuilder s = new StringBuilder(64);
        s.append(this.getClass().getSimpleName()).append('{');
        int baseLength = s.length();
        if (this.initialCapacity != -1) {
            s.append("initialCapacity=").append(this.initialCapacity).append(", ");
        }

        if (this.maximumSize != -1L) {
            s.append("maximumSize=").append(this.maximumSize).append(", ");
        }

        if (this.maximumWeight != -1L) {
            s.append("maximumWeight=").append(this.maximumWeight).append(", ");
        }

        if (this.expireAfterAccessNanos != -1L) {
            s.append("expireAfterAccess=").append(this.expireAfterAccessNanos).append("ns, ");
        }
        if (this.refreshNanos != -1L) {
            s.append("refreshNanos=").append(this.refreshNanos).append("ns, ");
        }
        if (s.length() > baseLength) {
            s.deleteCharAt(s.length() - 2);
        }

        return s.append('}').toString();
    }

}

