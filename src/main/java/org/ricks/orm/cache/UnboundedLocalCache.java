package org.ricks.orm.cache;

import org.ricks.common.lang.ExpiringMap;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.function.*;

final class UnboundedLocalCache<K, V> implements LocalCache<K, V> {
    
    final ConcurrentMap<K, V> data;
    transient Set<K> keySet;
    final long expireAfterAccess;
    transient Collection<V> values;
    transient Set<Entry<K, V>> entrySet;
    UnboundedLocalCache(Caffeine<? super K, ? super V> builder) {
        this.expireAfterAccess = builder.getRefreshAfterAccessNanos();
        if(this.expireAfterAccess <= 0) {
            this.data = new ConcurrentHashMap<>();
        } else {
            this.data = ExpiringMap.builder().expiration(expireAfterAccess, TimeUnit.MILLISECONDS).build();

            if(Objects.nonNull(builder.expirationListener)) {
                ExpiringMap expiringMap = (ExpiringMap) data;
                expiringMap.addExpirationListener(builder.expirationListener);
            }
        }
    }

    public boolean hasWriteTime() {
        return false;
    }
    
    public V getIfPresent(Object key, boolean recordStats) {
        V value = this.data.get(key);
        return value;
    }

    
    public V getIfPresentQuietly(Object key, long[] writeTime) {
        return this.data.get(key);
    }

    public long estimatedSize() {
        return this.data.size();
    }

    public Map<K, V> getAllPresent(Iterable<?> keys) {
        Set<Object> uniqueKeys = new LinkedHashSet();
        Iterator var3 = keys.iterator();

        while(var3.hasNext()) {
            Object key = var3.next();
            uniqueKeys.add(key);
        }

        int misses = 0;
        Map<Object, Object> result = new LinkedHashMap(uniqueKeys.size());
        Iterator var5 = uniqueKeys.iterator();

        while(var5.hasNext()) {
            Object key = var5.next();
            Object value = this.data.get(key);
            if (value == null) {
                ++misses;
            } else {
                result.put(key, value);
            }
        }

        return (Map<K, V>) Collections.unmodifiableMap(result);
    }

    public void cleanUp() {
    }


    public void forEach(BiConsumer<? super K, ? super V> action) {
        this.data.forEach(action);
    }

    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
        Objects.requireNonNull(function);
        this.data.replaceAll((key, value) -> {
            V newValue = Objects.requireNonNull(function.apply(key, value));
            return newValue;
        });

    }

    public V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction,boolean recordStats, boolean recordLoad) {
        Objects.requireNonNull(mappingFunction);
        V value = this.data.get(key);
        if(value == null) {
            value = data.computeIfAbsent(key, k -> {
                return recordStats
                        ? statsAware(mappingFunction, recordLoad).apply(key)
                        : mappingFunction.apply(key);
            });
        }
        return value;
    }


    public V computeIfPresent(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        Objects.requireNonNull(remappingFunction);
        if (!this.data.containsKey(key)) {
            return null;
        } else {
            V nv = this.data.computeIfPresent(key, (k, value) -> {
                BiFunction<? super K, ? super V, ? extends V> function = this.statsAware(remappingFunction, false, true, true);
                V newValue = function.apply(k, value);
                return newValue;
            });

            return nv;
        }
    }

    public V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction, boolean recordMiss, boolean recordLoad, boolean recordLoadFailure) {
        Objects.requireNonNull(remappingFunction);
        return this.remap(key, this.statsAware(remappingFunction, recordMiss, recordLoad, recordLoadFailure));
    }

    public V merge(K key, V value, BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        Objects.requireNonNull(remappingFunction);
        Objects.requireNonNull(value);
        return this.remap(key, (k, oldValue) -> {
            return oldValue == null ? value : this.statsAware(remappingFunction).apply(oldValue, value);
        });
    }

    V remap(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        V nv = this.data.compute(key, (k, value) -> {
            V newValue = remappingFunction.apply(k, value);
            if (value == null && newValue == null) {
                return null;
            } else {
                return newValue;
            }
        });

        return nv;
    }

    public boolean isEmpty() {
        return this.data.isEmpty();
    }

    public int size() {
        return this.data.size();
    }

    public void clear() {
        this.data.clear();
    }

    public <K> void clear(K k) {
        this.data.remove(k);
    }

    public boolean containsKey(Object key) {
        return this.data.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return this.data.containsValue(value);
    }

    
    public V get(Object key) {
        return this.getIfPresent(key, false);
    }

    
    public V put(K key, V value) {
        return this.put(key, value, true);
    }

    
    public V put(K key, V value, boolean notifyWriter) {
        Objects.requireNonNull(value);
        V[] oldValue = (V[]) new Object[1];

        oldValue[0] = this.data.put(key, value);

        return oldValue[0];
    }

    
    public V putIfAbsent(K key, V value) {
        Objects.requireNonNull(value);
        boolean[] wasAbsent = new boolean[1];
        V val = this.data.computeIfAbsent(key, (k) -> {
            wasAbsent[0] = true;
            return value;
        });
        return wasAbsent[0] ? null : val;
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        this.data.putAll(map);
    }

    
    public V remove(Object key) {
        V[] oldValue = (V[]) new Object[1];
        oldValue[0] = this.data.remove(key);

        return oldValue[0];
    }

    public boolean remove(Object key, Object value) {
        if (value == null) {
            Objects.requireNonNull(key);
            return false;
        } else {
            V[] oldValue = (V[]) new Object[1];
            this.data.computeIfPresent((K) key, (k, v) -> {
                if (v.equals(value)) {
                    oldValue[0] = v;
                    return null;
                } else {
                    return v;
                }
            });
            boolean removed = oldValue[0] != null;

            return removed;
        }
    }

    
    public V replace(K key, V value) {
        Objects.requireNonNull(value);
        V[] oldValue = (V[]) new Object[1];
        this.data.computeIfPresent(key, (k, v) -> {

            oldValue[0] = v;
            return value;
        });

        return oldValue[0];
    }

    public boolean replace(K key, V oldValue, V newValue) {
        Objects.requireNonNull(oldValue);
        Objects.requireNonNull(newValue);
        V[] prev = (V[]) new Object[1];
        this.data.computeIfPresent(key, (k, v) -> {
            if (v.equals(oldValue)) {

                prev[0] = v;
                return newValue;
            } else {
                return v;
            }
        });
        boolean replaced = prev[0] != null;

        return replaced;
    }

    public boolean equals(Object o) {
        return this.data.equals(o);
    }

    public int hashCode() {
        return this.data.hashCode();
    }

    public String toString() {
        return this.data.toString();
    }

    public Set<K> keySet() {
        Set<K> ks = this.keySet;
        return ks == null ? (this.keySet = new UnboundedLocalCache.KeySetView(this)) : ks;
    }

    public Collection<V> values() {
        Collection<V> vs = this.values;
        return vs == null ? (this.values = new UnboundedLocalCache.ValuesView(this)) : vs;
    }

    public Set<Entry<K, V>> entrySet() {
        Set<Entry<K, V>> es = this.entrySet;
        return es == null ? (this.entrySet = new UnboundedLocalCache.EntrySetView(this)) : es;
    }

    static final class UnboundedLocalLoadingCache<K, V> extends UnboundedLocalCache.UnboundedLocalManualCache<K, V> implements LocalLoadingCache<K, V> {
        private static final long serialVersionUID = 1L;
        final Function<K, V> mappingFunction;
        final CacheLoader<? super K, V> loader;
        
        UnboundedLocalLoadingCache(Caffeine<K, V> builder, CacheLoader<? super K, V> loader) {
            super(builder);
            this.loader = loader;
            this.mappingFunction = LocalLoadingCache.newMappingFunction(loader);
        }

        public CacheLoader<? super K, V> cacheLoader() {
            return this.loader;
        }

        public Function<K, V> mappingFunction() {
            return this.mappingFunction;
        }


        private void readObject(ObjectInputStream stream) throws InvalidObjectException {
            throw new InvalidObjectException("Proxy required");
        }
    }

    static class UnboundedLocalManualCache<K, V> implements LocalManualCache<K, V>, Serializable {
        private static final long serialVersionUID = 1L;
        final UnboundedLocalCache<K, V> cache;
        
        UnboundedLocalManualCache(Caffeine<K, V> builder) {
            this.cache = new UnboundedLocalCache(builder);
        }

        public UnboundedLocalCache<K, V> cache() {
            return this.cache;
        }

        private void readObject(ObjectInputStream stream) throws InvalidObjectException {
            throw new InvalidObjectException("Proxy required");
        }

    }

    static final class EntrySpliterator<K, V> implements Spliterator<Entry<K, V>> {
        final Spliterator<Entry<K, V>> spliterator;
        final UnboundedLocalCache<K, V> cache;

        EntrySpliterator(UnboundedLocalCache<K, V> cache) {
            this(cache, cache.data.entrySet().spliterator());
        }

        EntrySpliterator(UnboundedLocalCache<K, V> cache, Spliterator<Entry<K, V>> spliterator) {
            this.spliterator = (Spliterator)Objects.requireNonNull(spliterator);
            this.cache = (UnboundedLocalCache)Objects.requireNonNull(cache);
        }

        public void forEachRemaining(Consumer<? super Entry<K, V>> action) {
            Objects.requireNonNull(action);
            this.spliterator.forEachRemaining((entry) -> {
                Entry<K, V> e = new WriteThroughEntry(this.cache, entry.getKey(), entry.getValue());
                action.accept(e);
            });
        }

        public boolean tryAdvance(Consumer<? super Entry<K, V>> action) {
            Objects.requireNonNull(action);
            return this.spliterator.tryAdvance((entry) -> {
                Entry<K, V> e = new WriteThroughEntry(this.cache, entry.getKey(), entry.getValue());
                action.accept(e);
            });
        }

        
        public UnboundedLocalCache.EntrySpliterator<K, V> trySplit() {
            Spliterator<Entry<K, V>> split = this.spliterator.trySplit();
            return split == null ? null : new UnboundedLocalCache.EntrySpliterator(this.cache, split);
        }

        public long estimateSize() {
            return this.spliterator.estimateSize();
        }

        public int characteristics() {
            return this.spliterator.characteristics();
        }
    }

    static final class EntryIterator<K, V> implements Iterator<Entry<K, V>> {
        final UnboundedLocalCache<K, V> cache;
        final Iterator<Entry<K, V>> iterator;
        
        Entry<K, V> entry;

        EntryIterator(UnboundedLocalCache<K, V> cache) {
            this.cache = (UnboundedLocalCache)Objects.requireNonNull(cache);
            this.iterator = cache.data.entrySet().iterator();
        }

        public boolean hasNext() {
            return this.iterator.hasNext();
        }

        public Entry<K, V> next() {
            this.entry = this.iterator.next();
            return new WriteThroughEntry(this.cache, this.entry.getKey(), this.entry.getValue());
        }

        public void remove() {
            if (this.entry == null) {
                throw new IllegalStateException();
            } else {
                this.cache.remove(this.entry.getKey());
                this.entry = null;
            }
        }
    }

    static final class EntrySetView<K, V> extends AbstractSet<Entry<K, V>> {
        final UnboundedLocalCache<K, V> cache;

        EntrySetView(UnboundedLocalCache<K, V> cache) {
            this.cache = (UnboundedLocalCache)Objects.requireNonNull(cache);
        }

        public boolean isEmpty() {
            return this.cache.isEmpty();
        }

        public int size() {
            return this.cache.size();
        }

        public void clear() {
            this.cache.clear();
        }

        public boolean contains(Object o) {
            if (!(o instanceof Entry)) {
                return false;
            } else {
                Entry<?, ?> entry = (Entry)o;
                V value = this.cache.get(entry.getKey());
                return value != null && value.equals(entry.getValue());
            }
        }

        public boolean remove(Object obj) {
            if (!(obj instanceof Entry)) {
                return false;
            } else {
                Entry<?, ?> entry = (Entry)obj;
                return this.cache.remove(entry.getKey(), entry.getValue());
            }
        }

        public boolean removeIf(Predicate<? super Entry<K, V>> filter) {
            Objects.requireNonNull(filter);
            boolean removed = false;
            Iterator var3 = this.cache.data.entrySet().iterator();

            while(var3.hasNext()) {
                Entry<K, V> entry = (Entry)var3.next();
                if (filter.test(entry)) {
                    removed |= this.cache.remove(entry.getKey(), entry.getValue());
                }
            }

            return removed;
        }

        public Iterator<Entry<K, V>> iterator() {
            return new UnboundedLocalCache.EntryIterator(this.cache);
        }

        public Spliterator<Entry<K, V>> spliterator() {
            return new UnboundedLocalCache.EntrySpliterator(this.cache);
        }
    }

    static final class ValuesIterator<K, V> implements Iterator<V> {
        final UnboundedLocalCache<K, V> cache;
        final Iterator<Entry<K, V>> iterator;
        
        Entry<K, V> entry;

        ValuesIterator(UnboundedLocalCache<K, V> cache) {
            this.cache = (UnboundedLocalCache)Objects.requireNonNull(cache);
            this.iterator = cache.data.entrySet().iterator();
        }

        public boolean hasNext() {
            return this.iterator.hasNext();
        }

        public V next() {
            this.entry = (Entry)this.iterator.next();
            return this.entry.getValue();
        }

        public void remove() {
            if (this.entry == null) {
                throw new IllegalStateException();
            } else {
                this.cache.remove(this.entry.getKey());
                this.entry = null;
            }
        }
    }

    static final class ValuesView<K, V> extends AbstractCollection<V> {
        final UnboundedLocalCache<K, V> cache;

        ValuesView(UnboundedLocalCache<K, V> cache) {
            this.cache = (UnboundedLocalCache)Objects.requireNonNull(cache);
        }

        public boolean isEmpty() {
            return this.cache.isEmpty();
        }

        public int size() {
            return this.cache.size();
        }

        public void clear() {
            this.cache.clear();
        }

        public boolean contains(Object o) {
            return this.cache.containsValue(o);
        }

        public boolean removeIf(Predicate<? super V> filter) {
            Objects.requireNonNull(filter);
            boolean removed = false;
            Iterator var3 = this.cache.data.entrySet().iterator();

            while(var3.hasNext()) {
                Entry<K, V> entry = (Entry)var3.next();
                if (filter.test(entry.getValue())) {
                    removed |= this.cache.remove(entry.getKey(), entry.getValue());
                }
            }

            return removed;
        }

        public Iterator<V> iterator() {
            return new UnboundedLocalCache.ValuesIterator(this.cache);
        }

        public Spliterator<V> spliterator() {
            return this.cache.data.values().spliterator();
        }
    }

    static final class KeyIterator<K> implements Iterator<K> {
        final UnboundedLocalCache<K, ?> cache;
        final Iterator<K> iterator;
        
        K current;

        KeyIterator(UnboundedLocalCache<K, ?> cache) {
            this.cache = (UnboundedLocalCache)Objects.requireNonNull(cache);
            this.iterator = cache.data.keySet().iterator();
        }

        public boolean hasNext() {
            return this.iterator.hasNext();
        }

        public K next() {
            this.current = this.iterator.next();
            return this.current;
        }

        public void remove() {
            if (this.current == null) {
                throw new IllegalStateException();
            } else {
                this.cache.remove(this.current);
                this.current = null;
            }
        }
    }

    static final class KeySetView<K> extends AbstractSet<K> {
        final UnboundedLocalCache<K, ?> cache;

        KeySetView(UnboundedLocalCache<K, ?> cache) {
            this.cache = (UnboundedLocalCache)Objects.requireNonNull(cache);
        }

        public boolean isEmpty() {
            return this.cache.isEmpty();
        }

        public int size() {
            return this.cache.size();
        }

        public void clear() {
            this.cache.clear();
        }

        public boolean contains(Object o) {
            return this.cache.containsKey(o);
        }

        public boolean remove(Object obj) {
            return this.cache.remove(obj) != null;
        }

        public Iterator<K> iterator() {
            return new UnboundedLocalCache.KeyIterator(this.cache);
        }

        public Spliterator<K> spliterator() {
            return this.cache.data.keySet().spliterator();
        }
    }
}
