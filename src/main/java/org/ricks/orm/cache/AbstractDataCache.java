package org.ricks.orm.cache;

import org.ricks.orm.repository.CacheRepository;
import org.ricks.orm.TableMeta;
import org.ricks.orm.anno.Table;
import org.ricks.common.exception.DataException;

import java.io.Serializable;
import java.util.List;
import java.util.function.Predicate;

/**
 * 数据缓存抽象的实现类.
 *
 */
abstract class AbstractDataCache<T, K extends Serializable> implements DataCache<T, K> {
    protected final TableMeta<T> entityMapping;
    protected final CacheRepository<T, K> repository;

    public AbstractDataCache(CacheRepository<T, K> repository) {
        this.repository = repository;
        this.entityMapping = repository.getEntityMapping();
    }

    @SuppressWarnings("unchecked")
    protected K getPrimaryIdValue(T entity) {
        return (K) entityMapping.getPrimaryIdValue(entity);
    }

    @Override
    public T load(Serializable playerId, K entityId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> loadAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> loadAll(Serializable roleId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void insert(T entity) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(T entity) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> deleteAll(Serializable playerId) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(T entity) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public void initCacheData() {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public List<T> loadAll(Predicate<T> filter) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public T load(Serializable playerId, Predicate<T> filter) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public long count() {
        throw new UnsupportedOperationException("非法操作 ...");
    }

    @Override
    public long count(Serializable playerId, Predicate<T> filter) {
        throw new UnsupportedOperationException("非法操作 ...");
    }

    @Override
    public List<T> loadAll(Serializable playerId, Predicate<T> filter) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public T delete(K entityId) {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    @Override
    public List<K> cacheKeys() {
        // 当不支持请求的操作时，抛出该异常。
        throw new UnsupportedOperationException();
    }

    /**
     * 断言实体的抓取策略为启服载入
     */
    protected void assertEntityFetchTypeIsStart() {
        if (entityMapping.getFetchType() != Table.FetchType.START) {
            throw new DataException("调用LoadAll接口时，当前实体类抓取策略为启服载入");
        }
    }
}