package org.ricks.orm.cache;

import org.ricks.orm.OrmException;
import java.util.function.Function;

interface LocalLoadingCache<K, V> extends LocalManualCache<K, V>, LoadingCache<K, V> {

    CacheLoader<? super K, V> cacheLoader();

    Function<K, V> mappingFunction();
    
    default V get(K key) {
        V v =  this.cache().computeIfAbsent(key, this.mappingFunction());
        return v;
    }


    static <K, V> Function<K, V> newMappingFunction(CacheLoader<? super K, V> cacheLoader) {
        return (key) -> {
            try {
                return cacheLoader.load(key);
            } catch (Exception var5) {
                throw new OrmException(var5);
            }
        };
    }

}
