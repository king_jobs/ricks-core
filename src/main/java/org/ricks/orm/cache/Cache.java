package org.ricks.orm.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

public interface Cache<K, V> {
    V getIfPresent( Object var1);

    V get( K var1,  Function<? super K, ? extends V> var2);

    void put( K var1,  V var2);

    void putAll( Map<? extends K, ? extends V> var1);

    V invalidate(Object var1);

    void invalidateAll( Iterable<?> var1);

    void invalidateAll();

    long estimatedSize();

    int size();

    ConcurrentMap<K, V> asMap();

    void cleanUp();

}
