package org.ricks.orm.cache;


import org.ricks.orm.repository.CacheRepository;
import org.ricks.orm.anno.Table;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 缓存管理中心
 */
public class CacheMgr<K extends Serializable> {

    //缓存集中管理，如果不定时销毁。就驱动clear
    private final List<CacheRepository> cacheRepositories = new ArrayList<>(128);

    private CacheMgr(){}
    private static CacheMgr INSTANCE = new CacheMgr();
    public static CacheMgr get(){
        return INSTANCE;
    }

    public void clear(K k) {
        cacheRepositories.stream().filter(cacheRepository -> cacheRepository.getEntityMapping().getFetchType() != Table.FetchType.START).forEach(r -> r.cacheInvalidate(k));
    }

    public void addCacheRepositories(CacheRepository cacheRepository) {
        cacheRepositories.add(cacheRepository);
    }

}
