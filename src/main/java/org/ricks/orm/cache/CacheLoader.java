package org.ricks.orm.cache;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;

public interface CacheLoader<K, V> extends AsyncCacheLoader<K, V> {
    
    V load( K var1) throws Exception;

    
    default Map<K, V> loadAll( Iterable<? extends K> keys) throws Exception {
        throw new UnsupportedOperationException();
    }

    
    default CompletableFuture<V> asyncLoad( K key,  Executor executor) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(executor);
        return CompletableFuture.supplyAsync(() -> {
            try {
                return this.load(key);
            } catch (RuntimeException var3) {
                throw var3;
            } catch (Exception var4) {
                throw new CompletionException(var4);
            }
        }, executor);
    }


    
    default V reload( K key,  V oldValue) throws Exception {
        return this.load(key);
    }

    
    default CompletableFuture<V> asyncReload( K key,  V oldValue,  Executor executor) {
        Objects.requireNonNull(key);
        Objects.requireNonNull(executor);
        return CompletableFuture.supplyAsync(() -> {
            try {
                return this.reload(key, oldValue);
            } catch (RuntimeException var4) {
                throw var4;
            } catch (Exception var5) {
                throw new CompletionException(var5);
            }
        }, executor);
    }
}
