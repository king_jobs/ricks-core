package org.ricks.orm.cache;

import org.ricks.common.lang.Logger;
import org.ricks.common.TimeUtils;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;
import java.util.function.Function;

interface LocalCache<K, V> extends ConcurrentMap<K, V> {

    boolean hasWriteTime();

    long estimatedSize();

    
    V getIfPresent( Object var1, boolean var2);

    
    V getIfPresentQuietly( Object var1, long[] var2);

    
    Map<K, V> getAllPresent( Iterable<?> var1);

    
    V put( K var1,  V var2, boolean var3);

    
    default V compute(K key, BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        return this.compute(key, remappingFunction, false, true, true);
    }

    
    V compute(K var1, BiFunction<? super K, ? super V, ? extends V> var2, boolean var3, boolean var4, boolean var5);


    default V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction) {
        long startTime = TimeUtils.currentTimeMillis();
        try {
            V v = computeIfAbsent(key, mappingFunction, /* recordStats */ true, /* recordLoad */ true);
            return v;
        } finally {
            long endTime = TimeUtils.currentTimeMillis();
            Logger.debug("Key[" + key + "] load data cost time:" + (endTime - startTime));
        }
    }

    V computeIfAbsent(K key, Function<? super K, ? extends V> mappingFunction,
                                boolean recordStats, boolean recordLoad);

    default void invalidateAll(Iterable<?> keys) {
        Iterator var2 = keys.iterator();

        while(var2.hasNext()) {
            Object key = var2.next();
            this.remove(key);
        }

    }

    void cleanUp();

    default Function<? super K, ? extends V> statsAware(Function<? super K, ? extends V> mappingFunction, boolean recordLoad) {
        return mappingFunction;
    }

    default <T, U, R> BiFunction<? super T, ? super U, ? extends R> statsAware(BiFunction<? super T, ? super U, ? extends R> remappingFunction) {
        return this.statsAware(remappingFunction, true, true, true);
    }

    default <T, U, R> BiFunction<? super T, ? super U, ? extends R> statsAware(BiFunction<? super T, ? super U, ? extends R> remappingFunction, boolean recordMiss, boolean recordLoad, boolean recordLoadFailure) {
        return remappingFunction;
    }
}
