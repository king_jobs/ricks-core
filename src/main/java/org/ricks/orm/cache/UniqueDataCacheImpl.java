
package org.ricks.orm.cache;


import org.ricks.orm.repository.CacheRepository;
import org.ricks.orm.anno.Table;
import org.ricks.common.exception.DataException;
import org.ricks.common.lang.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;

/**
 * 这类的，要么没有角色Id，要么就是角色Id就是主键。
 *
 * @param <T> 实体类
 * @param <K> 实体类Id
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public class UniqueDataCacheImpl<T, K extends Serializable> extends AbstractDataCache<T, K> {
    /**
     * 实体Id <==> 一个数据包装器
     */
    private final LoadingCache<K, DataWrapper<T>> caches;

    public UniqueDataCacheImpl(CacheRepository<T, K> repository) {
        super(repository);
        // 构建一个数据加载器
        CacheLoader<K, DataWrapper<T>> loader = entityId -> {
            // 如果是启服就载入的，就没有必要再去访问DB了...
            if (entityMapping.getFetchType() == Table.FetchType.START) {
                return new DataWrapper<>(null);
            }
            String tableName = repository.getEntityMapping().getName();
            // 没有缓存时，从数据访问策略中加载
            Logger.info("缓存没有命中，开始从mysql load tableName:"+tableName+"数据，entityId：" + entityId);
            return new DataWrapper<>(repository.load(entityId));
        };

        caches = Caffeine.newBuilder().build(loader);
    }

    @Override
    public void insert(T entity) {
        final K entityId = this.getPrimaryIdValue(entity);

        DataWrapper<T> wrapper = caches.get(entityId);
        if (wrapper.getEntity() == null) {
            wrapper.setEntity(entity);
        } else {
            throw new DataException("插入了重复Key:" + entityId);
        }
    }

    @Override
    public void delete(T entity) {
        this.delete(this.getPrimaryIdValue(entity));
    }

    @Override
    public List<T> deleteAll() {
        List<T> result = loadAll();
        caches.invalidateAll();
        return result;
    }

    @Override
    public void update(T entity) {
        final K entityId = this.getPrimaryIdValue(entity);

        DataWrapper<T> wrapper = caches.get(entityId);
        if (wrapper.getEntity() == null) {
            throw new DataException("修改了一个不存在的Key:" + entityId);
        } else {
            wrapper.setEntity(entity);
        }
    }

    @Override
    public T load(K entityId) {

        // 启服载入的实体加载，那不能主动创建包装，没人删除...
        if (entityMapping.getFetchType() == Table.FetchType.START) {
            DataWrapper<T> wrapper = caches.getIfPresent(entityId);
            return wrapper == null ? null : wrapper.getEntity();
        }

        return caches.get(entityId).getEntity();
    }

    @Override
    public List<T> loadAll() {
        return loadAllByQueryFilter(null);
    }

    @Override
    public List<T> loadAll(Predicate<T> filter) {
        return loadAllByQueryFilter(filter);
    }

    private List<T> loadAllByQueryFilter(Predicate<T> filter) {
        this.assertEntityFetchTypeIsStart();

        ConcurrentMap<K, DataWrapper<T>> map = caches.asMap();
        if (map.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<T> result = new ArrayList<>(map.size());
        for (Entry<K, DataWrapper<T>> e : map.entrySet()) {
            T entity = e.getValue().getEntity();
            if (entity == null) {
                continue;
            }
            if (filter != null) {
                if (!filter.test(entity)) {
                    continue;
                }
            }
            result.add(entity);
        }
        return result;
    }

    @Override
    public T delete(K entityId) {
        DataWrapper<T> wrapper = caches.get(entityId);
        if (wrapper.getEntity() == null) {
            throw new DataException("删除了一个不存在的Key:" + entityId);
        } else {
            T result = wrapper.getEntity();
            wrapper.setEntity(null);

            // 启服载入的实体删除，包装类也要删除，非启服载入还是走原来的超时
            if (entityMapping.getFetchType() == Table.FetchType.START) {
                caches.invalidate(entityId);
            }
            return result;
        }
    }

    @Override
    public long count() {
//        this.assertEntityFetchTypeIsStart();

        long result = 0;
        // 包装类中有数据才能计数
        ConcurrentMap<K, DataWrapper<T>> map = caches.asMap();
        for (Entry<K, DataWrapper<T>> e : map.entrySet()) {
            T entity = e.getValue().getEntity();
            if (entity == null) {
                continue;
            }
            result++;
        }
        return result;
    }

    @Override
    public List<K> cacheKeys() {
        List<K> keys = new ArrayList<>();
        // 包装类中有数据才能计数
        ConcurrentMap<K, DataWrapper<T>> map = caches.asMap();
        for (Entry<K, DataWrapper<T>> e : map.entrySet()) {
            T entity = e.getValue().getEntity();
            if (entity == null) {
                continue;
            }
            keys.add(e.getKey());
        }
        return keys;
    }

    @Override
    public void invalidate(K K) {
        caches.invalidate(K);
    }

    @Override
    public void invalidateAll(List<K> keys) {
        caches.invalidateAll(keys);
    }

    @Override
    public void initCacheData() {
        Logger.debug("实体类["+entityMapping.getTableClass()+"]抓取策略为启动服务器就加载缓存.");
        List<T> result = repository.loadAll();
        result.forEach(entity -> caches.put(this.getPrimaryIdValue(entity), new DataWrapper<>(entity)));
        Logger.debug("实体类["+entityMapping.getTableClass()+"]初始化缓存完成,一共 "+result.size()+" 条数据.");
    }



    /**
     * 数据包装器.
     * <p>
     * 这个类里有一个实体对象，可能为空，主要用来初始化缓存数据
     *
     * @param <E> 实体对象
     */
    private static class DataWrapper<E> {
        private E entity;

        private DataWrapper(E entity) {
            this.entity = entity;
        }

        E getEntity() {
            return entity;
        }

        void setEntity(E entity) {
            this.entity = entity;
        }
    }
}