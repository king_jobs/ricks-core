package org.ricks.orm.cache;

import org.ricks.orm.repository.CacheRepository;
import org.ricks.orm.anno.Table;
import org.ricks.common.exception.DataException;
import org.ricks.common.lang.Logger;
import org.ricks.common.MapUtils;

import java.io.Serializable;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * 这类的，就是一个玩家有多条数据.
 *
 * @param <T> 实体类
 * @param <K> 实体类Id
 *
 * LoadingCache 测试cache get 比 ConcurrentHashMap慢
 * 但是文档上面比 ConcurrentHashMap 将近一倍
 */
public class MultiDataCacheImpl<T, K extends Serializable> extends AbstractDataCache<T, K> {
    /**
     * 角色Id <==> 一个数据集合
     */
    private final LoadingCache<Serializable, ConcurrentMap<K, T>> caches;
    /**
     * 实体ID->对应的玩家ID
     */
    private ConcurrentHashMap<K, Serializable> CACHE_GROUP_ID = new ConcurrentHashMap<>(2048);

    public MultiDataCacheImpl(CacheRepository<T, K> repository) {
        super(repository);

        CacheLoader<Serializable, ConcurrentMap<K, T>> loader = playerId -> {
            // 如果是启服就载入的，就没有必要再去访问DB了...
            if (entityMapping.getFetchType() == Table.FetchType.START) {
                return MapUtils.newConcurrentHashMap(16);
            }
            String tableName = repository.getEntityMapping().getName();
            Logger.info("缓存没有命中，开始从mysql load tableName:"+tableName+"数据，playerId：" + playerId);
            List<T> result = repository.loadAll(playerId);
            int initSize = Math.max(result.size(), 32);
            ConcurrentHashMap<K, T> dataMap = MapUtils.newConcurrentHashMap(initSize);
            for (T entity : result) {
                dataMap.put(getPrimaryIdValue(entity), entity);
            }
            return dataMap;
        };

        caches = Caffeine.newBuilder().build(loader);
    }

    /**
     * 绑定实体ID与分组ID对应关系
     *
     * @param entityId 实体ID
     * @param groupId  分组ID
     */
    private void buildGroupId(K entityId, Serializable groupId) {
        if (entityMapping.getFetchType() == Table.FetchType.START) {
            CACHE_GROUP_ID.put(entityId, groupId);
        }
    }

    /**
     * 解绑实体ID与分组ID对应关系
     *
     * @param result 一些实体
     */
    private void unbindGroupId(List<T> result) {
        if (entityMapping.getFetchType() == Table.FetchType.START) {
            for (T entity : result) {
                K entityId = this.getPrimaryIdValue(entity);
                CACHE_GROUP_ID.remove(entityId);
            }
        }
    }

    @Override
    public void insert(T entity) {
        final Serializable playerId = entityMapping.getPlayerIdValue(entity);
        final ConcurrentMap<K, T> data = caches.get(playerId);
        final K entityId = this.getPrimaryIdValue(entity);
        if (data.containsKey(entityId)) {
            throw new DataException("插入了重复Key:" + entityId);
        }
        data.put(entityId, entity);

        // 绑定实体ID与分组ID对应关系
        this.buildGroupId(entityId, playerId);
    }

    @Override
    public void delete(T entity) {
        Serializable playerId = entityMapping.getPlayerIdValue(entity);
        final ConcurrentMap<K, T> data = caches.get(playerId);

        K entityId = this.getPrimaryIdValue(entity);
        T result = data.remove(entityId);
        if (result == null) {
            throw new DataException("删除了一个不存在的Key:" + entityId);
        }

        // 解绑实体ID与玩家ID对应关系
        if (entityMapping.getFetchType() == Table.FetchType.START) {
            CACHE_GROUP_ID.remove(entityId);
        }
    }

    @Override
    public List<T> deleteAll() {
        // 疯了，有角色区别删全部，希望你是故意的
        List<T> result = loadAll();
        caches.invalidateAll();

        // 解绑实体ID与玩家ID对应关系
        this.unbindGroupId(result);
        return result;
    }


    @Override
    public List<T> deleteAll(Serializable playerId) {
        final ConcurrentMap<K, T> data = caches.get(playerId);
        List<T> result = new ArrayList<>(data.values());
        data.clear();

        // 解绑实体ID与玩家ID对应关系
        this.unbindGroupId(result);
        return result;
    }

    @Override
    public void update(T entity) {
        Serializable playerId = entityMapping.getPlayerIdValue(entity);
        final ConcurrentMap<K, T> data = caches.get(playerId);
        K entityId = this.getPrimaryIdValue(entity);
        if (!data.containsKey(entityId)) {
            throw new DataException("修改了一个不存在的Key:" + entityId);
        }
        data.put(entityId, entity);

        // 绑定实体ID与分组ID对应关系
        this.buildGroupId(entityId, playerId);
    }

    @Override
    public T load(Serializable playerId, K entityId) {
        return caches.get(playerId).get(entityId);
    }

    @Override
    public T load(Serializable playerId, Predicate<T> filter) {
        return caches.get(playerId).values().stream().filter(filter).findFirst().orElse(null);
    }

    @Override
    public long count(Serializable playerId, Predicate<T> filter) {
        return caches.get(playerId).values().stream().filter(filter).count();
    }

    @Override
    public T load(K entityId) {
        // 目前这个功能只能留给启服载入的实体使用
        this.assertEntityFetchTypeIsStart();
        // 先取出对应的分组ID
        Serializable groupId = CACHE_GROUP_ID.get(entityId);
        if (groupId == null) {
            return null;
        }
        return load(groupId, entityId);
    }

    @Override
    public List<T> loadAll(Serializable playerId) {
        return new ArrayList<>(caches.get(playerId).values());
    }

    @Override
    public List<T> loadAll(Serializable playerId, Predicate<T> filter) {
        return caches.get(playerId).values().stream().filter(filter).collect(Collectors.toList());
    }

    @Override
    public void invalidate(K K) {
        caches.invalidate(K);
    }

    @Override
    public void invalidateAll(List<K> keys) {
        caches.invalidate(keys);
    }

    @Override
    public List<T> loadAll() {
        this.assertEntityFetchTypeIsStart();

        ConcurrentMap<Serializable, ConcurrentMap<K, T>> map = caches.asMap();
        if (map.isEmpty()) {
            return Collections.emptyList();
        }

        ArrayList<T> result = new ArrayList<>(map.size());
        for (Entry<Serializable, ConcurrentMap<K, T>> e : map.entrySet()) {
            result.addAll(e.getValue().values());
        }
        return result;
    }

    @Override
    public List<T> loadAll(Predicate<T> filter) {
        this.assertEntityFetchTypeIsStart();

        ConcurrentMap<Serializable, ConcurrentMap<K, T>> map = caches.asMap();
        if (map.isEmpty()) {
            return Collections.emptyList();
        }

        ArrayList<T> result = new ArrayList<>(map.size());
        for (Entry<Serializable, ConcurrentMap<K, T>> e : map.entrySet()) {
            result.addAll(e.getValue().values().stream().filter(filter).collect(Collectors.toList()));
        }
        return result;
    }

    @Override
    public void initCacheData() {
        Logger.debug("实体类["+entityMapping.getTableClass()+"]抓取策略为启动服务器就加载缓存.");
        List<T> result = repository.loadAll();
        if (!result.isEmpty()) {
            Map<Serializable, ConcurrentHashMap<K, T>> data = new HashMap<>(result.size());
            for (T entity : result) {
                Serializable playerId = entityMapping.getPlayerIdValue(entity);
                ConcurrentHashMap<K, T> ds = data.get(playerId);
                if (ds == null) {
                    ds = MapUtils.newConcurrentHashMap(Math.min(result.size(), 256));
                    data.put(playerId, ds);
                }
                K entityId = this.getPrimaryIdValue(entity);
                ds.put(entityId, entity);

                // 绑定实体ID与分组ID对应关系
                this.buildGroupId(entityId, playerId);
            }
            caches.putAll(data);
        }
        Logger.debug("实体类["+entityMapping.getTableClass()+"]初始化缓存完成,一共 "+result.size()+" 条数据.");
    }
}