package org.ricks.orm.cache;

import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

interface LocalManualCache<K, V> extends Cache<K, V> {
    LocalCache<K, V> cache();

    default long estimatedSize() {
        return this.cache().estimatedSize();
    }

    default void cleanUp() {
        this.cache().cleanUp();
    }

    default int size() {
        return this.cache().size();
    }
    
    default V getIfPresent(Object key) {
        return this.cache().getIfPresent(key, true);
    }

    
    default V get(K key, Function<? super K, ? extends V> mappingFunction) {
        return this.cache().computeIfAbsent(key, mappingFunction);
    }


    default void put(K key, V value) {
        this.cache().put(key, value);
    }

    default void putAll(Map<? extends K, ? extends V> map) {
        this.cache().putAll(map);
    }

    default V invalidate(Object key) {
        return this.cache().remove(key);
    }

    default void invalidateAll(Iterable<?> keys) {
        this.cache().invalidateAll(keys);
    }

    default void invalidateAll() {
        this.cache().clear();
    }

    default ConcurrentMap<K, V> asMap() {
        return this.cache();
    }
}