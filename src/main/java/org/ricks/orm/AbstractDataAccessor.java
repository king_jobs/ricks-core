package org.ricks.orm;

import org.ricks.common.exception.DataAccessException;
import org.ricks.common.exception.DataException;
import org.ricks.common.lang.Logger;
import org.ricks.common.MapUtils;
import org.ricks.common.MathUtils;
import org.ricks.common.StrUtil;

import java.sql.*;
import java.util.*;

/**
 * 抽象的数据访问接口.
 */
public abstract class AbstractDataAccessor implements DataAccessor {

    /**
     * 慢查询时间阀值(单位：毫秒),如果为0则不监控
     */
    protected int slowQuerySqlMillis = 0;

    /**
     * 是否输出执行SQL日志
     */
    protected boolean statementExecutableSqlLogEnable = false;

    protected final ConnectionSource dataSource;

    public AbstractDataAccessor(ConnectionSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void judgeAccessType() {
        judgeAccessType(dataSource);
    }

    public void setStatementExecutableSqlLogEnable(boolean statementExecutableSqlLogEnable) {
        this.statementExecutableSqlLogEnable = statementExecutableSqlLogEnable;
    }

    public void setSlowQuerySqlMillis(int slowQuerySqlMillis) {
        this.slowQuerySqlMillis = slowQuerySqlMillis;
    }

    /**
     * 针对一个数据源记录一下目标版本.
     *
     * @param ds 数据源
     */
    public static void judgeAccessType(ConnectionSource ds) {
        try (Connection conn = ds.getMasterConn()) {
            DatabaseMetaData meta = conn.getMetaData();
            String pnm = meta.getDatabaseProductName();
            String ver = meta.getDatabaseProductVersion();
            Logger.info("database product name:"+pnm+", version:"+ver);
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
    }

    protected <T> T executeStatement(StatementCallback<T> action,boolean isUpdate) {
        Connection con = null;
        Statement stmt = null;
        try  {
            con = dataSource.getConn(isUpdate);
            stmt = con.createStatement();
            return action.doInStatement(stmt);
        } catch (SQLException e) {
            throw new DataAccessException(e);
        } finally {
            dataSource.closeConnection(con,stmt,isUpdate);
        }
    }


    /**
     * 执行SQL的逻辑，只能是异常显示数据.
     *
     * @param <T>    数据类型
     * @param tm     实体对象描述
     * @param action PreparedStatement回调接口
     * @param sql    执行SQL
     * @param flag   如果遇到数据长度异常情况，是否自动扩容
     * @return 执行结果
     */
    protected <T> T execute(final TableMeta<?> tm, PreparedStatementCallback<T> action, String sql, boolean flag ,boolean isUpdate) {
        Connection con = null;
        PreparedStatement pstmt = null;
        long startTime = slowQuerySqlMillis > 0 ? System.nanoTime() : 0;
        final Map<String, Integer> columnMaxLenMap = MapUtils.newHashMap(tm.getColumns().size());
        try  {
            con = dataSource.getConn(isUpdate);
            pstmt = con.prepareStatement(sql);
            PreparedStatementProxy proxy = new PreparedStatementProxy(pstmt, statementExecutableSqlLogEnable, columnMaxLenMap);

            // 执行填充参数
            T result = action.doInPreparedStatement(proxy);
            // 记录日志
            this.logExecutableSql(proxy, sql, startTime, false);

            return result;
        } catch (Exception e) {
            // 不能扩容时，把异常向上抛出去...
            throw new DataAccessException(tm.getName(), e);
        } finally {
            dataSource.closeConnection(con,pstmt,isUpdate);
        }
    }

    protected <T> T executeBatch(TableMeta<?> tm, PreparedStatementCallback<T> action, String sql, boolean flag,boolean isUpdate) {
        long startTime = slowQuerySqlMillis > 0 ? System.nanoTime() : 0;
        final Map<String, Integer> columnMaxLenMap = MapUtils.newHashMap(tm.getColumns().size());
        // 批量执行，如果出现异常，数据将进行回滚
        try (Connection con = dataSource.getConn(isUpdate); PreparedStatement pstmt = con.prepareStatement(sql)) {
            // 关闭自动提交功能
            con.setAutoCommit(false);
            try {
                // 构建代理，拼接参数
                PreparedStatementProxy proxy = new PreparedStatementProxy(pstmt,statementExecutableSqlLogEnable, columnMaxLenMap);
                T result = action.doInPreparedStatement(proxy);
                // 手动提交
                con.commit();
                // 记录日志
                this.logExecutableSql(proxy, sql, startTime, true);
                return result;
            } catch (SQLException e) {
                con.rollback();
                // 不能扩容时，把异常向上抛出去...
                throw new DataAccessException(tm.getName(), e);
            } finally {
                // 还原为自动提交
                con.setAutoCommit(true);
                dataSource.closeConnection(con,pstmt,isUpdate);
            }
        } catch (Exception e) {
            throw new DataAccessException(tm.getTableClass().getName(), e);
        }
    }

    private void logExecutableSql(PreparedStatementProxy statement, String sql, long startTime, boolean isBatch) {
        // 不输出，直接忽略所有.
        if (!statementExecutableSqlLogEnable) {
            return;
        }

        StringBuilder formattedSql = new StringBuilder(256);
        if (slowQuerySqlMillis > 0) {
            float execTime = (System.nanoTime() - startTime) / 100_0000F;
            if (execTime >= slowQuerySqlMillis) {
                formattedSql.append("exec sql ").append(MathUtils.formatScale(execTime, 2)).append(" ms.");
            }
        }
        formattedSql.append("\n").append(sql.replaceAll("\\?", "{}"));
        // 当前操作为批量执行的
        if (isBatch) {
            Logger.debug("batch start...");
            // 没有参数
            if (statement.getBatchParameterList().isEmpty()) {
                Logger.info(formattedSql.toString());
            }
            // 有参数
            else {
                for (List<Object> parameters : statement.getBatchParameterList()) {
                    Logger.info(formattedSql.toString());
                }
            }
            Logger.debug("batch end...");
        }
        // 有参数就是单独执行的
        else {
            Logger.info(formattedSql.toString());
        }
    }

    /**
     * 判定一个表是否存在.
     *
     * @param tableName 表名
     * @return 如果存在返回true, 否则返回false
     */
    protected boolean exists(final String tableName) {
        return this.executeStatement((stmt) -> {
            String sql = "SELECT COUNT(1) FROM " + tableName + " where 1!=1";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                return rs.next();
            }
            // 有异常就是表不存在嘛~~~~
            catch (Exception e) {
                return Boolean.FALSE;
            }
        },true);
    }

    /**
     * 检查一下表结构是不是跟这个实体一样一样的.
     */
    @Override
    public synchronized <T> void checkupEntityFieldsWithDatabase(TableMeta<T> tm) {
        forceDataSource(new DBRunner.MasterDBRunner() {
            @Override
            public T run(ConnectionSource sm) {
                // 先判定一下，存不存在
                if (exists(tm.getName())) {
                    checkEntityTable(tm);
                } else {
                    // 不存在，直接创建
                    createEntityTable(tm);
                }
                return null;
            }
        });
        forceDataSource(new DBRunner.EachSlaveDbRunner() {
            @Override
            public T run(ConnectionSource sm) {
                // 先判定一下，存不存在
                if (exists(tm.getName())) {
                    checkEntityTable(tm);
                } else {
                    // 不存在，直接创建
                    createEntityTable(tm);
                }
                return null;
            }
        });

        // 先判定一下，存不存在
        if (this.exists(tm.getName())) {
            this.checkEntityTable(tm);
        } else {
            // 不存在，直接创建
            this.createEntityTable(tm);
        }
    }

    private synchronized <T> void checkEntityTable(final TableMeta<T> tm) {
        this.executeStatement((stmt) -> {
            try (ResultSet rs = stmt.executeQuery(StrUtil.join("SELECT * FROM ", tm.getName(), " LIMIT 0"))) {
                ResultSetMetaData rsmd = rs.getMetaData();
                // 缓存为Map的方式
                List<TableInfo> tableInfos = getTableInfo(stmt,tm.getName());
                final int len = rsmd.getColumnCount();
                Map<String, Integer> caches = new HashMap<>(len);
                for (int i = 1; i <= len; i++) {
                    caches.put(rsmd.getColumnName(i), i);
                }

                // 循环字段检查，如果属性比字段多，就自动补上...
                for (ColumnMeta cm : tm.getColumns()) {

                    modifyColumn(tm,cm,tableInfos);
                    // 字段如果有大写字母，则警告提示输出.
//                    if (!cm.getColumnName().equals(cm.getColumnName().toLowerCase())) {
//                        Logger.warn("字段名称中有大写字母,建议修正为下划线命名方式! entity="+tm.getTableClass().getName()+",field="+cm.getField().getName()+",columnName=" +cm.getColumnName());
//                    }

                    Integer index = caches.remove(cm.getColumnName());
                    // 字段不存在，修补字段
                    if (index == null) {
                        autoAlterTableAddColumn(tm, cm);
                        tryRepairTextDefaultValue(tm, cm);
                        continue;
                    }

                    // 字符串类型的字段，要修正长度的(只能变长，不能变短)
                    final int columnType = rsmd.getColumnType(index);
                    if (columnType == Types.VARCHAR) {
                        final int length = rsmd.getColumnDisplaySize(index);
                        if (cm.getLength() > length) {
                            autoAlterTableUpdateColumn(tm, cm);
                        } else if (cm.getLength() < length) {
                            Logger.warn("表中字段长度大于配置长度，建议手动修正! entity="+tm.getTableClass().getName()+",field="+cm.getField().getName()+",length="+ cm.getLength());
                        }
                    }
                    // Integer转Long（不能Long转Integer）
                    else if (columnType == Types.INTEGER && cm.isLong()) {
                        autoAlterTableUpdateColumn(tm, cm);
                    }

                }

                // 还有剩的，那表结构比字段属性多了...
                if (!caches.isEmpty()) {
                    // 不允许，那就异常阻止服务启动，把主动权交给研发人员...
                    throw new DataException("表结构字段比实体类属性多. 表[" + tm.getName() + "]中的属性：" + Arrays.toString(caches.keySet().toArray()));
                }
            }

            // 随便返回一个，没有实际意义
            return 0;
        },true);
    }

    private void modifyColumn(TableMeta tableMeta,ColumnMeta columnMeta, List<TableInfo> tableInfos) {
        tableInfos.forEach(tableInfo -> {
            if(tableInfo.getColumnName().equalsIgnoreCase(columnMeta.getColumnName())) {
                try {
                    String typeName = tableInfo.getColumnType();
                    boolean checkModifyColumn = !SqlBuilder.evalFieldType(columnMeta).equalsIgnoreCase(typeName);
                    if(checkModifyColumn) autoAlterTableUpdateColumn(tableMeta, columnMeta);
                } catch (Exception e) {
                    Logger.warn("modify column failure , column name:"+tableInfo.getColumnName()+" ,  column type:"+columnMeta.getType()+" and table type:" +tableInfo.getColumnType());
                }
                modifyColumnIndex(tableMeta,columnMeta,tableInfo);
            }
        });
    }

    private void modifyColumnIndex(TableMeta tableMeta,ColumnMeta columnMeta,TableInfo tableInfo) {
        if(!columnMeta.isPrimaryKey()) {
            if (StrUtil.isBlank(columnMeta.getIndex()) && tableInfo.getIndexName() != null) {
                columnMeta.setIndex(tableInfo.getIndexName());
                autoAlterTableDeleteColumnIndex(tableMeta,columnMeta);
            } else if (columnMeta.getIndex() != null && tableInfo.getIndexName() == null) {
                autoAlterTableAddColumnIndex(tableMeta,columnMeta);
            }
        }
    }

    /**
     * 自动添加字段索引
     *
     * @param <T> 实体类型
     * @param tm  实体描述对象
     * @param cm  实体指定属性描述对象
     */
    protected <T> void autoAlterTableAddColumnIndex(TableMeta<T> tm, ColumnMeta cm) {
        final String sql = SqlBuilder.getAddColumnIndex(tm, cm);
        Logger.warn("实体类["+tm.getTableClass()+"]对应的数据库表结构不一致，准备自动添加字段索引，SQL如下:\n"+ sql);
        this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
    }

    /**
     * 自动删除字段索引
     *
     * @param <T> 实体类型
     * @param tm  实体描述对象
     * @param cm  实体指定属性描述对象
     */
    protected <T> void autoAlterTableDeleteColumnIndex(TableMeta<T> tm, ColumnMeta cm) {
        final String sql = SqlBuilder.getAddColumnIndex(tm, cm);
        Logger.warn("实体类["+tm.getTableClass()+"]对应的数据库表结构不一致，准备自动删除字段索引，SQL如下:\n"+sql);
        this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
    }

    /**
     * 自动修正字段
     *
     * @param <T> 实体类型
     * @param tm  实体描述对象
     * @param cm  实体指定属性描述对象
     */
    protected <T> void autoAlterTableUpdateColumn(TableMeta<T> tm, ColumnMeta cm) {
        final String sql = SqlBuilder.genUpdateTableColumnSql(tm, cm);
        Logger.warn("实体类["+tm.getTableClass()+"]对应的数据库表结构不一致，准备自动修补表结构，SQL如下:\n"+ sql);
        this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
    }

    /**
     * 如果是Text智能修补一下默认值
     */
    private <T> void tryRepairTextDefaultValue(final TableMeta<T> tm, final ColumnMeta cm) {
        // 修正Text字段的默认值.
        if (cm.getLength() >= DataConstant.VARCHAT_MAX_WIDTH) {
            final String sql = SqlBuilder.genUpdateDefaultValueSql(tm, cm);
            Logger.warn("实体类["+tm.getTableClass()+"]中的字段["+cm.getColumnName()+"]不支持默认值，准备自动修补默认值，SQL如下:\n"+ sql);
            this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
        }
    }

    /**
     * 自动增加表中不存在的字段,并且赋予默认值
     */
    private <T> void autoAlterTableAddColumn(TableMeta<T> tm, ColumnMeta cm) {
        final String sql = SqlBuilder.genAddTableColumnSql(tm, cm);
        Logger.warn("实体类["+tm.getTableClass()+"]对应的数据库表结构不一致，准备自动修补新增的字段，SQL如下:\n"+ sql);
        this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
    }

    /**
     * 自动删除表中不再使用的字段
     */
    private <T> void autoAlterTableDropColumn(TableMeta<T> tm, String columnName) {
        String sql = SqlBuilder.genDropTableColumnSql(tm, columnName);
        Logger.warn("实体类["+tm.getTableClass()+"]对应的数据库表结构不一致，准备自动删除多余字段，SQL如下:\n"+ sql);
        this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
    }

    /**
     * 自动删除表中不再使用的字段
     */
    private <T> void autoAlterTableColumnIndex(TableMeta<T> tm, ColumnMeta cm) {
        String sql = SqlBuilder.getAddColumnIndex(tm, cm);
        Logger.warn("实体类["+tm.getTableClass()+"]对应的数据库表结构不一致，准备自动修正索引，SQL如下:\n"+ sql);
        this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
    }

    private List<TableInfo> getTableInfo(Statement stmt,String tableName) {
        String sql = SqlBuilder.getTableColumn(tableName);
        try (ResultSet rs = stmt.executeQuery(sql)) {
            List<TableInfo> result = new ArrayList<>();
            while (rs.next()) {
                TableInfo tableInfo = new TableInfo();
                tableInfo.setColumnName(rs.getString("columnName"));
                tableInfo.setColumnType(rs.getString("columnType"));
                tableInfo.setIndexName(rs.getString("indexName"));
                result.add(tableInfo);
            }
            return result;
        } catch (Exception e) {
            throw new DataException("Get Table Info error:{}"+ e.getMessage());
        }
    }


    /**
     * 创建实体对应的数据库表结构.
     */
    private synchronized <T> void createEntityTable(TableMeta<T> tm) {
        final String sql = SqlBuilder.genCreateTableSql(tm);
        Logger.warn("实体类["+tm.getTableClass()+"]对应的数据库表不存在，准备自动创建表结构，SQL如下:\n" +sql);
        this.executeStatement((stmt) -> stmt.executeUpdate(sql),true);
    }

    /**
     * 强制使用某个数据源，DBRunner.MasterRunner，DBRunner.SlaveRunner或者继承DBRunner自定义.
     *
     * 如下代码的，在主从库下，强制查询走主库
     * <pre>
     *    sqlManager.forceDataSource(new DBRunner.MasterRunner(){
     *    		public void run(SQLManager sqlManager){
     *          	sqlManager.select .....
     *          }
     *    )
     * </pre>
     * 如下代码，强制更新从库
     * <pre>
     *    sqlManager.forceDataSource(new DBRunner.SlaveRunner(){
     *    		public void run(SQLManager sqlManager){
     *          	sqlManager.update()
     *          }
     *    )
     * </pre>
     * @param runner
     */
    public <T> T  forceDataSource(DBRunner<T> runner){
        return runner.start(dataSource);
    }
}