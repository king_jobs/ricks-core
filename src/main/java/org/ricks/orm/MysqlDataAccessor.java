package org.ricks.orm;

import org.ricks.orm.adaptor.ValueAdaptor;
import org.ricks.orm.adaptor.ValueAdaptorManager;
import org.ricks.common.exception.DataException;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * MySQL数据访问类.
 *
 */
public class MysqlDataAccessor extends AbstractDataAccessor {

    public MysqlDataAccessor(ConnectionSource dataSource) {
        super(dataSource);
    }

    @Override
    public <T> int insert(final TableMeta<T> em, final T entity) {
        class InsertPreparedStatementCallback implements PreparedStatementCallback<Integer> {
            @Override
            public Integer doInPreparedStatement(PreparedStatementProxy pstmt) throws Exception {
                buildInsertParameter(em, entity, pstmt);
                return pstmt.executeUpdate();
            }
        }
        return execute(em, new InsertPreparedStatementCallback(), SqlBuilder.getInsert(em), true,true);
    }

    @Override
    public <T> int[] batchInsert(final TableMeta<T> tm, final List<T> entitys) {
        class InsertPreparedStatementCallback implements PreparedStatementCallback<int[]> {
            @Override
            public int[] doInPreparedStatement(PreparedStatementProxy pstmt) throws Exception {
                for (T entity : entitys) {
                    buildInsertParameter(tm, entity, pstmt);
                    pstmt.addBatch();
                }
                return pstmt.executeBatch();
            }
        }
        return executeBatch(tm, new InsertPreparedStatementCallback(), SqlBuilder.getInsert(tm), true,true);
    }

    private <T> void buildInsertParameter(TableMeta<T> tm, T entity, PreparedStatementProxy pstmt) throws Exception {
        int index = 1;
        for (ColumnMeta cm : tm.getColumns()) {
            setPstmtParameter(tm, cm, pstmt, entity, index++);
        }
    }

    @Override
    public <T> int delete(final TableMeta<T> tm, final T entity) {
        return delete(tm, tm.getPrimaryIdValue(entity));
    }

    private <K extends Serializable> int delete(final TableMeta<?> tm, final K id) {
        class DeletePreparedStatementCallback implements PreparedStatementCallback<Integer> {
            @Override
            public Integer doInPreparedStatement(PreparedStatementProxy pstmt) throws SQLException {
                pstmt.setObject(1, id);
                return pstmt.executeUpdate();
            }
        }
        return execute(tm, new DeletePreparedStatementCallback(), SqlBuilder.getDelete(tm), true,true);
    }

    @Override
    public <T> int[] batchDelete(TableMeta<T> tm, List<T> entitys) {
        class DeletePreparedStatementCallback implements PreparedStatementCallback<int[]> {
            @Override
            public int[] doInPreparedStatement(PreparedStatementProxy pstmt) throws SQLException {
                for (T entity : entitys) {
                    pstmt.setObject(1, tm.getPrimaryIdValue(entity));
                    pstmt.addBatch();
                }
                return pstmt.executeBatch();
            }
        }
        return executeBatch(tm, new DeletePreparedStatementCallback(), SqlBuilder.getDelete(tm), true,true);
    }

    @Override
    public <T> int update(final TableMeta<T> tm, final T entity) {
        class UpdatePreparedStatementCallback implements PreparedStatementCallback<Integer> {
            @Override
            public Integer doInPreparedStatement(PreparedStatementProxy pstmt) throws Exception {
                buildUpdateParameter(tm, entity, pstmt);
                return pstmt.executeUpdate();
            }
        }
        return execute(tm, new UpdatePreparedStatementCallback(), SqlBuilder.getUpdate(tm), true,true);
    }

    @Override
    public <T> int execute(final TableMeta<T> tm, String sql,Object[] parameters) {
        class ExecutePreparedStatementCallback implements PreparedStatementCallback<Integer> {
            @Override
            public Integer doInPreparedStatement(PreparedStatementProxy pstmt) throws Exception {
                buildFiled(pstmt,parameters);
                return pstmt.executeUpdate();
            }
        }
        return execute(tm, new ExecutePreparedStatementCallback(), sql, true,true);
    }


    @Override
    public <T> T executeLoad(TableMeta<T> tm, String sql, Object[] parameters) {
        class ExecutePreparedStatementCallback implements PreparedStatementCallback<T> {
            @Override
            public T doInPreparedStatement(PreparedStatementProxy pstmt) throws Exception {
                buildFiled(pstmt,parameters);
                try (ResultSet rs = pstmt.executeQuery()) {
                    return rs.next() ? newEntity(tm, rs) : null;
                } catch (Exception e) {
                    throw new DataException("加载数据时异常，请查看实体类[" + tm.getTableClass().getName() + "]配置", e);
                }
            }
        }
        return execute(tm, new ExecutePreparedStatementCallback(), sql, true,false);
    }

    @Override
    public <T> List<T> executeLoadAll(TableMeta<T> tm, String sql, Object[] parameters) {
        class ExecutePreparedStatementCallback implements PreparedStatementCallback<List<T>> {
            @Override
            public List<T> doInPreparedStatement(PreparedStatementProxy pstmt) throws Exception {
                buildFiled(pstmt,parameters);
                try (ResultSet rs = pstmt.executeQuery()) {
                    return newEntityList(tm, rs);
                } catch (Exception e) {
                    throw new DataException("加载数据时异常，请查看实体类[" + tm.getTableClass().getName() + "]配置", e);
                }
            }
        }
        return execute(tm, new ExecutePreparedStatementCallback(), sql, true,false);
    }


    @Override
    public <T> int[] batchUpdate(TableMeta<T> tm, List<T> entitys) {
        class UpdatePreparedStatementCallback implements PreparedStatementCallback<int[]> {
            @Override
            public int[] doInPreparedStatement(PreparedStatementProxy pstmt) throws Exception {
                for (T entity : entitys) {
                    buildUpdateParameter(tm, entity, pstmt);
                    pstmt.addBatch();
                }
                return pstmt.executeBatch();
            }
        }
        return executeBatch(tm, new UpdatePreparedStatementCallback(), SqlBuilder.getUpdate(tm), true,true);
    }

    private <T> void buildUpdateParameter(TableMeta<T> tm, T entity, PreparedStatementProxy pstmt) throws Exception {
        int index = 1;
        // 非主键
        for (ColumnMeta cm : tm.getColumns()) {
            if (cm.isPrimaryKey()) {
                continue;
            }
            setPstmtParameter(tm, cm, pstmt, entity, index++);
        }
        // 主键
        setPstmtParameter(tm, tm.getPrimaryColumn(), pstmt, entity, index);
    }

    @Override
    public <T, K extends Serializable> T load(final TableMeta<T> tm, final K id) {
        class LoadPreparedStatementCallback implements PreparedStatementCallback<T> {
            @Override
            public T doInPreparedStatement(PreparedStatementProxy pstmt) throws SQLException {
                pstmt.setObject(1, id);
                try (ResultSet rs = pstmt.executeQuery()) {
                    return rs.next() ? newEntity(tm, rs) : null;
                } catch (Exception e) {
                    throw new DataException("加载数据时异常，请查看实体类[" + tm.getTableClass().getName() + "]配置", e);
                }
            }
        }
        return execute(tm, new LoadPreparedStatementCallback(), SqlBuilder.getSelect(tm), true,false);
    }

    @Override
    public <T> T load(TableMeta<T> tm, Map<String,Object> parameterMap) {
        List<String> columnNames =  new ArrayList<>();
        parameterMap.forEach((k,v) -> columnNames.add(k));
        class LoadPreparedStatementCallback implements PreparedStatementCallback<T> {
            @Override
            public T doInPreparedStatement(PreparedStatementProxy pstmt) throws SQLException {
                buildFiled(pstmt,columnNames,parameterMap);
                try (ResultSet rs = pstmt.executeQuery()) {
                    return rs.next() ? newEntity(tm, rs) : null;
                } catch (Exception e) {
                    throw new DataException("加载数据时异常，请查看实体类[" + tm.getTableClass().getName() + "]配置", e);
                }
            }
        }
        return execute(tm, new LoadPreparedStatementCallback(), SqlBuilder.getSelect(tm,columnNames), true,false);
    }

    private <T> void buildFiled(PreparedStatementProxy pstmt,List<String> columnNames, Map<String,Object> parameterMap) {
        AtomicInteger index = new AtomicInteger(1);
        parameterMap.forEach((k,v) -> {
            try {
                pstmt.setObject(index.getAndIncrement(), v);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private <T> void buildFiled(PreparedStatementProxy pstmt, Object[] parameters) throws SQLException {
        for (int i = 1; i <= parameters.length; i++) {
            Object parameter = parameters[i-1];
            if(parameter instanceof byte[] bytes) {
                pstmt.setByteArray(i, bytes);
            } else if(parameter instanceof BitSet bitSet) {
                pstmt.setByteArray(i, bitSet.toByteArray());
            } else if(parameter instanceof String str) {
                pstmt.setString(i, str);
            } else if(parameter instanceof Long longNum) {
                pstmt.setLong(i, longNum);
            } else if(parameter instanceof Integer IntNum) {
                pstmt.setInt(i, IntNum);
            }
            else {
                pstmt.setObject(i, parameters[i-1]);
            }
        }
    }

    @Override
    public <T> List<T> loadAll(final TableMeta<T> tm) {
        class LoadAllPreparedStatementCallback implements PreparedStatementCallback<List<T>> {
            @Override
            public List<T> doInPreparedStatement(PreparedStatementProxy pstmt) {
                try (ResultSet rs = pstmt.executeQuery()) {
                    return newEntityList(tm, rs);
                } catch (Exception e) {
                    throw new DataException("加载数据时异常，请查看实体类[" + tm.getTableClass().getName() + "]配置", e);
                }
            }
        }
        return execute(tm, new LoadAllPreparedStatementCallback(), SqlBuilder.getSelectAll(tm), true,false);
    }

    public <T> List<T> newEntityList(final TableMeta<T> tm, ResultSet rs) throws Exception {
        List<T> result = new ArrayList<>();
        while (rs.next()) {
            result.add(newEntity(tm, rs));
        }
        return result;
    }

    public <T> T newEntity(final TableMeta<T> tm, ResultSet rs) throws Exception {
        T result = tm.newEntity();
        for (ColumnMeta cm : tm.getColumns()) {
            ValueAdaptorManager.getValueAdaptor(cm.getType()).resultSetToParameter(tm, cm, rs, result);
        }
        return result;
    }

    @Override
    public <T> List<T> loadAll(TableMeta<T> tm, Serializable playerId) {
        class LoadByPlayerIdIdPreparedStatementCallback implements PreparedStatementCallback<List<T>> {
            @Override
            public List<T> doInPreparedStatement(PreparedStatementProxy pstmt) throws SQLException {
                pstmt.setObject(1, playerId);

                try (ResultSet rs = pstmt.executeQuery()) {
                    return newEntityList(tm, rs);
                } catch (Exception e) {
                    throw new DataException("加载数据时异常，请查看实体类[" + tm.getTableClass().getName() + "]配置", e);
                }
            }
        }
        return execute(tm, new LoadByPlayerIdIdPreparedStatementCallback(), SqlBuilder.genSelectByPlayerId(tm), true,false);
    }

    private <T> void setPstmtParameter(TableMeta<T> tm, ColumnMeta cm, PreparedStatementProxy pstmt, final T entity, final int index) throws Exception {
        ValueAdaptor adaptor = ValueAdaptorManager.getValueAdaptor(cm.getType());
        adaptor.parameterToPreparedStatement(tm, cm, pstmt, entity, index);
    }
}