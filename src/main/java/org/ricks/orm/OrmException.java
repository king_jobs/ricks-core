package org.ricks.orm;

import org.ricks.common.ExceptionUtil;
import org.ricks.common.StrUtil;

/**
 * orm运行时异常，常用于对IOException的包装
 * 
 * @author xiaoleilu
 */
public class OrmException extends RuntimeException {
	private static final long serialVersionUID = 8247610319171014183L;

	public OrmException(Throwable e) {
		super(ExceptionUtil.getMessage(e), e);
	}

	public OrmException(String message) {
		super(message);
	}

	public OrmException(String messageTemplate, Object... params) {
		super(StrUtil.format(messageTemplate, params));
	}

	public OrmException(String message, Throwable throwable) {
		super(message, throwable);
	}

	public OrmException(Throwable throwable, String messageTemplate, Object... params) {
		super(StrUtil.format(messageTemplate, params), throwable);
	}

	/**
	 * 导致这个异常的异常是否是指定类型的异常
	 * 
	 * @param clazz 异常类
	 * @return 是否为指定类型异常
	 */
	public boolean causeInstanceOf(Class<? extends Throwable> clazz) {
		final Throwable cause = this.getCause();
		return null != clazz && clazz.isInstance(cause);
	}
}
