package org.ricks.orm;

import org.ricks.orm.anno.Column;
import org.ricks.orm.anno.PrimaryKey;
import org.ricks.common.lang.Sequence;

/**
 */
public class BaseLongIDEntity<K> extends AbstractEntity<K,Long> {

    private final static Sequence snowflakeIdWorker = new Sequence();

    @PrimaryKey()
    @Column(comment = "ID")
    private Long id = snowflakeIdWorker.nextId();

//    @Column()
//    @PlayerId
//    @IndexKey
//    private long roleId;

    public BaseLongIDEntity() {
    }

//    public BaseLongIDEntity(long roleId) {
//        this.roleId = roleId;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public long getRoleId() {
//        return roleId;
//    }
//
//    public void setRoleId(long roleId) {
//        this.roleId = roleId;
//    }
}
