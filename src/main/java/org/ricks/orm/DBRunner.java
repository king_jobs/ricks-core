package org.ricks.orm;

import javax.sql.DataSource;

/**
 * ConnectionSource提供了路由规则，一般而已，更新走主库，查询走从库。
 * 此类可以设定使用某个数据源来作为后续的操作，用户可以继承DBRunner来扩展数据源的选择，或者使用MasterDBRunner来强制走主数据源，
 * SlaveDBRunner走从数据源
 */
public abstract   class DBRunner<T> {


    /**
     * 使用数据源
     * @param connectionSource
     * @return
     */
    protected  abstract DataSource getTargetDataSource(ConnectionSource connectionSource);

    public <T> T  start(ConnectionSource connectionSource){
        try{
            DataSource ds = getTargetDataSource(connectionSource);
            connectionSource.forceBegin(ds);
            T t = run(connectionSource);
            return t;
        }finally {
            connectionSource.forceEnd();
        }

    }

    /**
     * 子类需要实现此方法，可以使用此SQLManager或者任何SQLManager生成的Mapper来访问数据库
     * @param sm
     * @param <T>
     * @return
     */
    abstract public <T> T  run(ConnectionSource sm);

    /**
     * 强制使用主数据源
     * @param <T>
     */
    public  abstract  static  class MasterDBRunner<T>  extends DBRunner<T>{

        protected   DataSource getTargetDataSource(ConnectionSource sqlManager){
            DataSource ds =  sqlManager.getMasterSource();
            return ds;
        }

    }

    /**
     * 强制使用第一个从库
     * @param <T>
     */
    public  abstract  static  class SlaveDBRunner<T>  extends DBRunner<T>{

        protected   DataSource getTargetDataSource(ConnectionSource sqlManager){
            DataSource[] ds =  sqlManager.getSlaves();
            if(ds==null){
                throw new IllegalArgumentException(" ConnectionSource 没有从数据源");
            }
            return ds[0];
        }

    }

    /**
     * 遍历所有slave，并执行
     */
    public abstract  static class EachSlaveDbRunner extends DBRunner<Object>{
        protected    DataSource getTargetDataSource(ConnectionSource sqlManager){
            throw new UnsupportedOperationException();
        }

        public Object start(ConnectionSource sm){
            try{
                DataSource[] slaves =  sm.getSlaves();
                if(slaves == null) return null;
                for(DataSource ds:slaves){
                    sm.forceBegin(ds);
                    run(sm);
                    sm.forceEnd();
                }
                return null;

            }finally {
                sm.forceEnd();
            }

        }
    }
}
