package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;

/**
 * Long类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class LongAdaptor extends AbstractValueAdaptor<Long> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Long value, int parameterIndex) throws Exception {
        pstmt.setLong(parameterIndex, value);
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return rs.getLong(cm.getColumnName());
    }
}