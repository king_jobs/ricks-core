
package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;

/**
 * Double类型属性
 *
 */
class DoubleAdaptor extends AbstractValueAdaptor<Double> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Double value, int parameterIndex) throws Exception {
        pstmt.setDouble(parameterIndex, value);
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return rs.getDouble(cm.getColumnName());
    }
}