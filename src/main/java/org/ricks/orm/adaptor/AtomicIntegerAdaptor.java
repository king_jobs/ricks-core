
package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * AtomicInteger类型属性
 *
 */
class AtomicIntegerAdaptor extends AbstractValueAdaptor<AtomicInteger> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, AtomicInteger value, int parameterIndex) throws Exception {
        pstmt.setInt(parameterIndex, value.intValue());
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return new AtomicInteger(rs.getInt(cm.getColumnName()));
    }
}