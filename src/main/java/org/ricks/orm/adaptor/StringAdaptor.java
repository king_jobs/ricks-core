package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;

/**
 * String类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class StringAdaptor extends AbstractValueAdaptor<String> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, String value, int parameterIndex) throws Exception {
        pstmt.setString( parameterIndex, value);
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return rs.getString(cm.getColumnName());
    }
}