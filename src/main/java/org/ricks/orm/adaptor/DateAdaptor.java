package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;

/**
 * Date类型属性
 *
 */
class DateAdaptor extends AbstractValueAdaptor<Date> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Date value, int parameterIndex) throws Exception {
        if (value == null) {
            pstmt.setNull(parameterIndex, Types.TIMESTAMP);
        } else if (value instanceof Date) {
            pstmt.setTimestamp(parameterIndex, new Timestamp(((Date) value).getTime()));
        } else {
            pstmt.setObject(parameterIndex, value);
        }
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        Timestamp ts = rs.getTimestamp(cm.getColumnName());
        return null == ts ? null : new Date(ts.getTime());
    }
}