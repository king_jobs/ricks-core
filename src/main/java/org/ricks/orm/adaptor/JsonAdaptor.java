
package org.ricks.orm.adaptor;

import org.ricks.common.json.JSONArray;
import org.ricks.common.json.JSONObject;
import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * Json类型属性
 *
 * Map List 以json格式存储
 */
class JsonAdaptor extends AbstractValueAdaptor<Object> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Object value, int parameterIndex) throws Exception {
        if (value == null) {
            pstmt.setString( parameterIndex, null);
        } else {
            Type klass = cm.getColumnClass();
            String json = null;
            if(klass instanceof Map) {
                JSONObject object = new JSONObject(value);
                json = object.toString();
            } else if(klass instanceof List) {
                JSONArray jsonArray = new JSONArray(value);
                json = jsonArray.toString();
            }
            pstmt.setString( parameterIndex, json);
        }
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        String jsonStr = rs.getString(cm.getColumnName());
        if(jsonStr == null) return null;
        Type klass = cm.getColumnClass();
        if(klass instanceof Map) {
            JSONObject object = new JSONObject(jsonStr);
            return object.toMap();
        } else if(klass instanceof List) {
            JSONArray jsonArray = new JSONArray(jsonStr);
            return jsonArray.toList();
        }
        return null;
    }
}