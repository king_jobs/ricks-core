
package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;

/**
 * Float类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class FloatAdaptor extends AbstractValueAdaptor<Float> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Float value, int parameterIndex) throws Exception {
        pstmt.setFloat(parameterIndex, value);
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return rs.getFloat(cm.getColumnName());
    }
}