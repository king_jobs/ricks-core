
package org.ricks.orm.adaptor;

import org.ricks.orm.FieldType;
import java.util.EnumMap;

/**
 * 属性值适配转换管理类.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public class ValueAdaptorManager {
    private static final EnumMap<FieldType, ValueAdaptor> ADAPTOR = new EnumMap<>(FieldType.class);

    static {
        ADAPTOR.put(FieldType.AsInteger, new IntegerAdaptor());
        ADAPTOR.put(FieldType.AsLong, new LongAdaptor());
        ADAPTOR.put(FieldType.AsString, new StringAdaptor());
        ADAPTOR.put(FieldType.AsAtomicInteger, new AtomicIntegerAdaptor());
        ADAPTOR.put(FieldType.AsAtomicLong, new AtomicLongAdaptor());
        ADAPTOR.put(FieldType.AsLongAdder, new LongAdderAdaptor());
        ADAPTOR.put(FieldType.AsBoolean, new BooleanAdaptor());
        ADAPTOR.put(FieldType.AsFloat, new FloatAdaptor());
        ADAPTOR.put(FieldType.AsDouble, new DoubleAdaptor());
        ADAPTOR.put(FieldType.AsDate, new DateAdaptor());
        ADAPTOR.put(FieldType.AsJson, new JsonAdaptor());
        ADAPTOR.put(FieldType.AsLocalDateTime, new LocalDateTimeAdaptor());
        ADAPTOR.put(FieldType.AsInstant, new InstantAdaptor());
        ADAPTOR.put(FieldType.AsBlob, new BlobAdaptor());
    }

    public static ValueAdaptor getValueAdaptor(FieldType type) {
        return ADAPTOR.getOrDefault(type, UnrealizedAdaptor.getInstance());
    }
}