
package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;

/**
 * Boolean类型属性
 */
class BooleanAdaptor extends AbstractValueAdaptor<Boolean> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Boolean value, int parameterIndex) throws Exception {
        pstmt.setBoolean(parameterIndex, value);
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return rs.getBoolean(cm.getColumnName());
    }
}