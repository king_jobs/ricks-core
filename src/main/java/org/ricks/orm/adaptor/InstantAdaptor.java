package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;

/**
 * Instant类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class InstantAdaptor extends AbstractValueAdaptor<Instant> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Instant value, int parameterIndex) throws Exception {
        if (value == null) {
            pstmt.setNull(parameterIndex, Types.TIMESTAMP);
        } else {
            pstmt.setTimestamp(parameterIndex, new Timestamp(value.toEpochMilli()));
        }
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        Timestamp ts = rs.getTimestamp(cm.getColumnName());
        return null == ts ? null : Instant.ofEpochMilli(ts.getTime());
    }
}