package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;
import org.ricks.orm.TableMeta;
import java.sql.ResultSet;

/**
 * 属性值适配转换接口.
 *
 */
public abstract class AbstractValueAdaptor<T> implements ValueAdaptor {

    @Override
    @SuppressWarnings("unchecked")
    public void parameterToPreparedStatement(TableMeta<?> tm, ColumnMeta cm, PreparedStatementProxy pstmt, Object entity, int index) throws Exception {
        T value = (T) tm.getMethodAccess().invoke(entity, cm.getGetMethodIndex());
        this.toPreparedStatement(cm, pstmt, value, index);
    }

    /**
     * 属性转化到PreparedStatement中
     *
     * @param cm             属性映射描述
     * @param pstmt          PreparedStatement代理对象
     * @param value          值
     * @param parameterIndex 参数位置
     * @throws Exception 可能出现SQL异常
     */
    protected abstract void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, T value, final int parameterIndex) throws Exception;

    @Override
    public void resultSetToParameter(TableMeta<?> tm, ColumnMeta cm, ResultSet rs, Object result) throws Exception {
        Object value = this.toParameter(cm, rs);
        tm.getMethodAccess().invoke(result, cm.getSetMethodIndex(), value);
    }

    /**
     * ResultSet中取出值
     *
     * @param cm 属性映射对象
     * @param rs 结果集
     * @return 返回属性值
     * @throws Exception 可能出现SQL异常
     */
    protected abstract Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception;
}