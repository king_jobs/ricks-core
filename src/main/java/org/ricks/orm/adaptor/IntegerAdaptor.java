package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;

/**
 * Integer类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class IntegerAdaptor extends AbstractValueAdaptor<Integer> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Integer value, int parameterIndex) throws Exception {
        pstmt.setInt(parameterIndex, value);
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return rs.getInt(cm.getColumnName());
    }
}