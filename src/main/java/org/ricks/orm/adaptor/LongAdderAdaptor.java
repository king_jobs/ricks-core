package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;
import java.util.concurrent.atomic.LongAdder;

/**
 * LongAdder类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.2.6
 */
class LongAdderAdaptor extends AbstractValueAdaptor<LongAdder> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, LongAdder value, int parameterIndex) throws Exception {
        pstmt.setLong(parameterIndex, value.longValue());
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        final LongAdder adder = new LongAdder();
        adder.add(rs.getLong(cm.getColumnName()));
        return adder;
    }
}