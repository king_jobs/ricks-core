package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * LocalDateTime类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class LocalDateTimeAdaptor extends AbstractValueAdaptor<LocalDateTime> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, LocalDateTime value, int parameterIndex) throws Exception {
        if (value == null) {
            pstmt.setNull(parameterIndex, Types.TIMESTAMP);
        } else {
            Date timestamp = Date.from(value.atZone(ZoneId.systemDefault()).toInstant());
            pstmt.setTimestamp(parameterIndex, new Timestamp(timestamp.getTime()));
        }
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        Timestamp ts = rs.getTimestamp(cm.getColumnName());
        return null == ts ? null : LocalDateTime.ofInstant(Instant.ofEpochMilli(ts.getTime()), ZoneId.systemDefault());
    }
}