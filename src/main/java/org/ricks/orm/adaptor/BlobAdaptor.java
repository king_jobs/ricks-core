package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;
import org.ricks.common.CharsetUtils;
import org.ricks.common.StrUtil;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.BitSet;


/**
 * Blob类型属性
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class BlobAdaptor extends AbstractValueAdaptor<Object> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Object value, int parameterIndex) throws Exception {
        // 空类型
        if (value == null) {
            pstmt.setNull(parameterIndex, Types.BLOB);
            return;
        }

        byte[] array;
        //  byte[]
        if (cm.getColumnClass() == byte[].class) {
            array = (byte[]) value;
        }
        // 字符串
        else if (cm.getColumnClass() instanceof CharSequence) {
            array = StrUtil.utf8Bytes((CharSequence) value);
        }
        // bitset
        else if (cm.getColumnClass() == BitSet.class){
            BitSet bitSet = (BitSet) value;
            array = bitSet.toByteArray();
        }
        // 其他都要进行序列化
        else {
            ByteBuf buf = new ByteBuf(1024);
            ProtocolManager.write(buf, (IPacket) value);
            array = buf.toArray();
        }

        pstmt.setByteArray( parameterIndex, array);
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        byte[] array = (byte[]) rs.getObject(cm.getColumnName());
        if (array == null) {
            return null;
        }

        //  byte[]
        if (cm.getColumnClass() == byte[].class) {
            return array;
        }
        // 字符串
        else if (cm.getColumnClass() == String.class) {
            return new String(array, CharsetUtils.CHARSET_UTF_8);
        }
        // StringBuilder
        else if (cm.getColumnClass() == StringBuilder.class) {
            return new StringBuilder(new String(array, CharsetUtils.CHARSET_UTF_8));
        }
        // StringBuffer
        else if (cm.getColumnClass() == StringBuffer.class) {
            return new StringBuffer(new String(array, CharsetUtils.CHARSET_UTF_8));
        }
        // bitset
        else if (cm.getColumnClass() == BitSet.class){
            return BitSet.valueOf(array);
        }
        // 其他都要进行序列化
        else {
            ByteBuf buf = new ByteBuf(array);
            return ProtocolManager.read(buf);
        }
    }
}