package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;
import org.ricks.orm.TableMeta;

import java.sql.ResultSet;

/**
 * 属性值适配转换接口.
 */
public interface ValueAdaptor {
    /**
     * 把结果集中的数据转化为属性值
     *
     * @param tm     实体映射描述对象
     * @param cm     属性映射描述对象
     * @param rs     结果集
     * @param result 属性的归属对象
     * @throws Exception 可能会出现转化失败的异常
     */
    void resultSetToParameter(TableMeta<?> tm, ColumnMeta cm, ResultSet rs, Object result) throws Exception;

    /**
     * 把属性值转化为查询参数
     *
     * @param tm     实体映射描述对象
     * @param cm     属性映射描述对象
     * @param pstmt  PreparedStatement代理对象
     * @param entity 实体类
     * @param index  参数占位编号
     * @throws Exception 可能会出现转化失败的异常
     */
    void parameterToPreparedStatement(TableMeta<?> tm, ColumnMeta cm, PreparedStatementProxy pstmt, Object entity, int index) throws Exception;
}