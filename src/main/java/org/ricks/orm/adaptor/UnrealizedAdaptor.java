package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;
import org.ricks.orm.TableMeta;
import org.ricks.common.exception.UnrealizedException;

import java.sql.ResultSet;

/**
 * 一个未实现的类型
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
class UnrealizedAdaptor extends AbstractValueAdaptor<Void> {
    private static final UnrealizedAdaptor INSTANCE = new UnrealizedAdaptor();

    private UnrealizedAdaptor() {
    }

    public static UnrealizedAdaptor getInstance() {
        return INSTANCE;
    }

    @Override
    public void parameterToPreparedStatement(TableMeta<?> tm, ColumnMeta cm, PreparedStatementProxy pstmt, Object entity, int index) {
        throw new UnrealizedException("未实现的数据存储类型:" + cm.getType());
    }

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, Void value, int parameterIndex) {
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        throw new UnrealizedException("未实现的数据存储类型:" + cm.getType());
    }
}