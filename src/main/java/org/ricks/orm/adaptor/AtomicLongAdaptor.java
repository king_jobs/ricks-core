package org.ricks.orm.adaptor;

import org.ricks.orm.ColumnMeta;
import org.ricks.orm.PreparedStatementProxy;

import java.sql.ResultSet;
import java.util.concurrent.atomic.AtomicLong;

/**
 * AtomicLong类型属性
 *
 */
class AtomicLongAdaptor extends AbstractValueAdaptor<AtomicLong> {

    @Override
    protected void toPreparedStatement(ColumnMeta cm, PreparedStatementProxy pstmt, AtomicLong value, int parameterIndex) throws Exception {
        pstmt.setLong(parameterIndex, value.longValue());
    }

    @Override
    protected Object toParameter(ColumnMeta cm, ResultSet rs) throws Exception {
        return new AtomicLong(rs.getLong(cm.getColumnName()));
    }
}