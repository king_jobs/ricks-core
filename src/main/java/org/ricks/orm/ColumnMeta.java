package org.ricks.orm;

import org.ricks.common.asm.MethodAccess;
import org.ricks.orm.anno.Column;
import org.ricks.ioc.Json;
import org.ricks.ioc.PlayerId;
import org.ricks.common.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.BitSet;
import java.util.Objects;

/**
 * @author ricks
 * @Description:
 * @date 2023/2/211:36
 */
public class ColumnMeta {

    private String columnName;
    private final Type klass;
    private FieldType type;
    private boolean readOnly;
    private Field field;
    private Class fieldGenericityClazz;
    private boolean nonNull;
    private boolean unsigned;
    private int length;
    private int precision;
    private boolean auto;
    private String index;
    private String comment;
    private boolean intNumber;
    private boolean primaryKey;
    private String defaultValue;

    private final Json json;
    private final Column column;
    private final PlayerId playerId;
    private final int getMethodIndex;
    private final int setMethodIndex;

    public ColumnMeta(Field field, MethodAccess methodAccess) {
        this.field = field;
        this.klass = field.getGenericType();
        this.column = field.getAnnotation(Column.class);
        this.json = field.getAnnotation(Json.class);
        this.playerId = field.getAnnotation(PlayerId.class);
        this.getMethodIndex = methodAccess.getIndex(FieldUtils.genGetMethodName(field));
        this.setMethodIndex = methodAccess.getIndex(FieldUtils.genSetMethodName(field));
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public boolean isNonNull() {
        return nonNull;
    }

    public void setNonNull(boolean nonNull) {
        this.nonNull = nonNull;
    }

    public boolean isUnsigned() {
        return unsigned;
    }

    public void setUnsigned(boolean unsigned) {
        this.unsigned = unsigned;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isIntNumber() {
        return intNumber;
    }

    public void setIntNumber(boolean intNumber) {
        this.intNumber = intNumber;
    }

    public boolean hasDefaultValue() {
        return column != null && !"".equals(column.defaultValue());
    }

    public String getDefaultValue() {
        return column.defaultValue();
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public boolean isPlayerId() {
        return playerId != null;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Class getFieldGenericityClazz() {
        return fieldGenericityClazz;
    }

    public void setFieldGenericityClazz(Class fieldGenericityClazz) {
        this.fieldGenericityClazz = fieldGenericityClazz;
    }

    public int getGetMethodIndex() {
        return getMethodIndex;
    }

    public int getSetMethodIndex() {
        return setMethodIndex;
    }

    public Type getColumnClass() {
        return klass;
    }

    public int getPrecision() {
        return precision;
    }

    public Column getColumn() {
        return column;
    }

    // ----------- 类型判定 ---------------------------

    public boolean isString() {
        return String.class == klass;
    }

    public boolean isBoolean() {
        return klass == boolean.class || klass == Boolean.class;
    }

    public boolean isInt() {
        return klass == int.class || klass == Integer.class;
    }

    public boolean isLong() {
        return klass == long.class || klass == Long.class;
    }

    /**
     * @return 当前对象是否为浮点
     */
    public boolean isFloat() {
        return klass == float.class || klass == Float.class;
    }

    /**
     * @return 当前对象是否为双精度浮点
     */
    public boolean isDouble() {
        return klass == double.class || klass == Double.class;
    }

    public boolean isBlob() {
        return klass == byte[].class || klass == BitSet.class || klass.toString().startsWith("java.util.Map") || klass.toString().startsWith("java.util.List");
    }

    public boolean isJson() {
        return json != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ColumnMeta that = (ColumnMeta) o;
        return Objects.equals(columnName, that.columnName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(columnName);
    }

}
