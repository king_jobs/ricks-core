package org.ricks.orm;

import app.vo.User;
import org.ricks.ModularContext;
import org.ricks.StopHook;
import org.ricks.common.conf.RicksConf;
import org.ricks.ioc.IocHolder;
import org.ricks.common.lang.Assert;
import org.ricks.common.lang.ExpirationListener;
import org.ricks.common.lang.Logger;
import org.ricks.orm.anno.Table;
import org.ricks.orm.repository.*;
import org.ricks.orm.write.AsyncWriteManager;
import org.ricks.orm.write.OrmWriteContainerLifeListener;
import org.ricks.common.AnnotationUtil;
import org.ricks.common.RandomUtil;

import java.io.FileOutputStream;
import java.io.Serializable;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Constructor;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * 管理 Repository，统一管理得目的 不用在Service层依赖IOC模块注入dao
 * @Repository 扫描自动注入
 * db对象使用asm 自动生成 Repository，比如：User -> UserRepository.然后再注入
 *
 *         var repository = load(clazz);
 *
 *         扫描@Table
 *
 *  Asm字节码增强生成Repository ，还是得依赖Bean进行配置注入。不然Repository无法使用
 *
 *  before Repository 维护在Bean中
 *
 *  after Repository 维护在OrmContext。注入还是Bean config 注入
 *
 *  统一对数据进行操作 load update save delete
 *
 *  统一管理异步回写机制
 */
public class OrmContext {

    private static final MethodHandles.Lookup lookup = MethodHandles.lookup();

    private static final Map<Class<? extends IEntity>, BaseRepository> repositoryMap = new ConcurrentHashMap<>();

    public void init() {
        Set<Class<?>> classSet = ModularContext.me().projectClasses()
                .stream()
                .filter(clazz -> AnnotationUtil.hasAnnotation(clazz,Table.class))
                .collect(Collectors.toSet());
        if(classSet.isEmpty()) {
            Logger.warn("ORM模块无法正常使用，没有发现带有@Table注解的entity ! ");
        }
        DataAccessor dataAccessor = IocHolder.getIoc().get(DataAccessor.class);
        AsyncWriteManager.me().setDataAccessor(dataAccessor); //注入真正数据操作
        ExpirationListener exceptionListener = IocHolder.getIoc().get(ExpirationListener.class);
        Assert.isNotNull(dataAccessor,"ORM模块异常，DataAccessor没办法从IOC中load 检测@Configuration中是否配置了Bean DataAccessor ");
        OrmWriteContainerLifeListener containerLifeListener = IocHolder.getIoc().get(OrmWriteContainerLifeListener.class);
        if(Objects.nonNull(containerLifeListener)) {
            Logger.info("开始注入OrmWriteContainerLifeListener，db异步回写容器监听机制 。");
            AsyncWriteManager.me().setWriteContainerLifeListener(containerLifeListener);
        }
        AsyncWriteManager.me().init();
        for (Class clazz : classSet) {
            BaseRepository repository = load(clazz);
            repository.dataAccessor = dataAccessor;
            if( repository instanceof AbstractCacheRepository abstractCacheRepository && Objects.nonNull(exceptionListener)) {
                abstractCacheRepository.setExpirationListener(exceptionListener);
            }
            repository.checkEntityAndInitCache();
            repositoryMap.put(clazz,repository);
        }
    }

    private OrmContext() {}

    public static OrmContext me() {
        return OrmContext.Holder.ME;
    }

    /** 通过 JVM 的类加载机制, 保证只加载一次 (singleton) */
    private static class Holder {
        static final OrmContext ME = new OrmContext();
    }



    private static <T extends BaseRepository> T load(Class<?> entityClazz) {
        try {
            byte[] data = RepositoryEnhance.enhance(entityClazz);
            FileOutputStream fos = new FileOutputStream("d://" +entityClazz.getSimpleName()+ "Repository.class");
            fos.write(data);
            fos.close();
            Class<?> clazz = lookup.defineClass(data);
            Constructor<?> constructor = clazz.getConstructor();
            return (T) constructor.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(entityClazz.getSimpleName() + "Repository字节增强失败  [" + entityClazz.getCanonicalName() + "]" + " ,无法字节增强并初始化");
        }
    }



    /**                              通用机制                                          */

    /**
     * 增删改
     * @param sql
     * @param <T>
     */
    public <T> void execute(Class<T> klass,String sql ,Object... parameters) {
        BaseRepository repository = getRepository(klass, BaseRepository.class);
        repository.execute(sql,parameters);
    }

    public <T> T executeLoad(Class<T> klass,String sql ,Object... parameters) {
        BaseRepository<T,?> repository = getRepository(klass, BaseRepository.class);
        return repository.executeLoad(sql,parameters);
    }

    public <T> List<T> executeLoadAll(Class<T> klass,String sql ,Object... parameters) {
        BaseRepository<T,?> repository = getRepository(klass, BaseRepository.class);
        return repository.executeLoadAll(sql,parameters);
    }

    /**
     * 插入一个对象
     * @param entity
     * @param <T>
     */
    public <T> void insert(T entity) {
        Table table = entity.getClass().getAnnotation(Table.class);
        BaseRepository repository = getRepository(entity.getClass(), table.repository());
        repository.insert(entity);
    }

    /**
     * 删除一个对象.
     *
     * @param entity 实体对象.
     */
    public <T> void delete(T entity) {
        Table table = entity.getClass().getAnnotation(Table.class);
        BaseRepository repository = getRepository(entity.getClass(), table.repository());
        repository.delete(entity);
    }


    /**
     * 修改数据.
     * @param entity 实体对象.
     */
    public <T> void update(T entity) {
        Table table = entity.getClass().getAnnotation(Table.class);
        BaseRepository repository = getRepository(entity.getClass(), table.repository());
        repository.update(entity);
    }

    /**
     * load数据.
     * @param entityClazz 实体对象.
     */
    public <T> T load(Class<T> entityClazz,Serializable entityId) {
        BaseRepository repository = getRepository(entityClazz, BaseRepository.class);
        return (T) repository.load(entityId);
    }





    /**                             多条数据缓存处理机制                                       */
    public <T> List<T> multiCacheLoad(Class<T> entityClazz, Serializable playerId) {
        MultiCacheRepository multiCacheRepository = getRepository(entityClazz, MultiCacheRepository.class);
        return multiCacheRepository.cacheLoadAll(playerId);
    }

    /**
     * 条件获取多条数据
     * @param entityClazz
     * @param playerId
     * @param filter
     * @return
     * @param <T>
     */
    public <T> List<T> multiCacheLoadAll(Class<T> entityClazz,Serializable playerId, Predicate<T> filter) {
        MultiCacheRepository multiCacheRepository = getRepository(entityClazz, MultiCacheRepository.class);
        return multiCacheRepository.cacheLoadAll(playerId, filter);
    }

    /**
     * 删除玩家单条数据
     * @param entityClazz
     * @param playerId
     * @param entityId
     */
    public void multiCacheDelete(Class<?> entityClazz, Serializable playerId,Serializable entityId) {
        MultiCacheRepository multiCacheRepository = getRepository(entityClazz, MultiCacheRepository.class);
        multiCacheRepository.cacheDelete(playerId,entityId);
    }

    /**
     * 删除玩家所有数据
     * @param entityClazz
     * @param playerId
     */
    public void multiCacheDeleteAll(Class<?> entityClazz, Serializable playerId) {
        MultiCacheRepository multiCacheRepository = getRepository(entityClazz, MultiCacheRepository.class);
        multiCacheRepository.cacheDeleteAll(playerId);
    }

    /**
     * ID 取玩家一条数据,允许null .由业务校验并返回Error
     * @param entityClazz
     * @param playerId
     * @param entityId
     * @return
     * @param <T>
     */
    public <T> T multiCacheGet(Class<T> entityClazz, Serializable playerId,Serializable entityId) {
        MultiCacheRepository multiCacheRepository = getRepository(entityClazz, MultiCacheRepository.class);
        return (T) multiCacheRepository.cacheGet(playerId,entityId);
    }

    /**
     * 条件获得单条数据
     * @param entityClazz
     * @param playerId
     * @param filter
     * @return
     * @param <T>
     */
    public <T> T multiCacheGet(Class<T> entityClazz, Serializable playerId, Predicate<T> filter) {
        MultiCacheRepository multiCacheRepository = getRepository(entityClazz, MultiCacheRepository.class);
        return (T) multiCacheRepository.cacheGet(playerId,filter);
    }

    /**
     * 玩家数据统计
     * @param entityClazz
     * @param playerId
     * @param filter
     * @return
     * @param <T>
     */
    public <T> long multiCacheCount(Class<T> entityClazz, Serializable playerId, Predicate<T> filter) {
        MultiCacheRepository multiCacheRepository = getRepository(entityClazz, MultiCacheRepository.class);
        return multiCacheRepository.cacheCount(playerId,filter);
    }











    /**                              单条数据处理机制                                       */

    public <T> Optional<T> uniqueCacheLoad(Class<T> entityClazz, Serializable playerId) {
        UniqueCacheRepository uniqueCacheRepository = getRepository(entityClazz, UniqueCacheRepository.class);
        return uniqueCacheRepository.cacheLoad(playerId);
    }

    /**
     * 从缓存中删除指定Id的对象.
     */
    public void uniqueCacheDelete(Class<?> entityClazz, Serializable playerId) {
        UniqueCacheRepository uniqueCacheRepository = getRepository(entityClazz, UniqueCacheRepository.class);
        uniqueCacheRepository.cacheDelete(playerId);
    }

    /**
     * 条件过滤 拉取数据
     * @param entityClazz
     * @param filter
     * @return
     * @param <T>
     */
    public <T> List<T> uniqueCacheLoadAll(Class<?> entityClazz,Predicate<T> filter) {
        UniqueCacheRepository uniqueCacheRepository = getRepository(entityClazz, UniqueCacheRepository.class);
        return uniqueCacheRepository.cacheLoadAll(filter);
    }

    /**
     * 统计数量
     * @param entityClazz
     * @return
     */
    public long uniqueCacheCount(Class<?> entityClazz) {
        UniqueCacheRepository uniqueCacheRepository = getRepository(entityClazz, UniqueCacheRepository.class);
        return  uniqueCacheRepository.cacheCount();
    }



    private <T> T getRepository(Class<?> entityClazz,Class<T> repositoryClazz) {
        BaseRepository entityRepository = repositoryMap.get(entityClazz);
        Assert.isTrue(Objects.nonNull(entityRepository),"实体[" + entityClazz.getCanonicalName() + "] ,没有映射相应得Repository无法操作DB数据 ");
        Assert.isTrue(repositoryClazz.isAssignableFrom(entityRepository.getClass()),"实体[" + entityClazz.getCanonicalName() + "] , 没有映射相应得"+repositoryClazz+"无法操作DB cacheLoad数据");
        return (T) entityRepository;
    }


    /**
     * ORM 销毁玩家
     * @param playerId
     */
    public void destroy(Serializable playerId) {
        /**
         * 销毁ORM 玩家缓存
         */
        repositoryMap.values().stream().filter(repository -> repository instanceof CacheRepository cacheRepository &&
                cacheRepository.getEntityMapping().fetchType != Table.FetchType.START)
                .map(baseRepository -> (CacheRepository)baseRepository)
                .forEach(cacheRepository -> cacheRepository.cacheInvalidate(playerId));
        /**
         * 销毁orm db回写容器
         */
        AsyncWriteManager.me().destroy(playerId);
    }

    public static void main(String[] args) throws InterruptedException {
        ModularContext.me().scan(Set.of("app"));
        RicksConf.loadingProperties(); //初始化配置
        IocHolder.initIoc();
        OrmContext.me().init();
        StopHook stopHook = new StopHook(IocHolder.getIoc());
        Runtime.getRuntime().addShutdownHook(stopHook);
        for (int i = 0; i < 10; i++) {
            test();
        }

        long userId = 1;

        Thread.ofVirtual().name("user-exit",userId).start(() -> {
            //网关RPC调用游戏，通知玩家退出。销毁缓存 销毁orm回写机制 flush玩家db数据
            OrmContext.me().destroy(userId);
            //通知RPC 网关，游戏服销毁玩家成功

        });
    }

    public  static void test() {

        System.err.println(OrmContext.me().repositoryMap.size());
        for (Map.Entry<Class<? extends IEntity>,BaseRepository> entry: OrmContext.me().repositoryMap.entrySet()) {
            System.err.println("k=" + entry.getKey().getSimpleName() + " & v=" + entry.getValue());
        }

        Optional<User> user = OrmContext.me().uniqueCacheLoad(User.class,1);

        User saveUser = new User();
        saveUser.setRoleName("ricks is god");
//        saveUser.setRoleId(RandomUtil.randomLong());
        OrmContext.me().insert(saveUser);

        OrmContext.me().execute(User.class, "update `user` set roleName = ?  where id = ?", "22222",733755323698081793L);

        OrmContext.me().execute(User.class, "DELETE FROM `user` where id = ?" , 733755407621910530l);

        OrmContext.me().execute(User.class, "INSERT INTO `user`(id) VALUES(?)", RandomUtil.randomLong());

        User uu = OrmContext.me().executeLoad(User.class, "SELECT * FROM `user` where id = ?" , 2);
        System.err.println(uu.toString());

        List<User> listAll = OrmContext.me().executeLoadAll(User.class, "SELECT * FROM `user` where roleName = ?" , "22222");
        for (User user1: listAll) {
            System.err.println(user1.toString());
        }


        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.err.println("正常结束");
    }
}
