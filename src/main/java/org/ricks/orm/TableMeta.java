package org.ricks.orm;

import org.ricks.common.asm.ConstructorAccess;
import org.ricks.common.asm.MethodAccess;
import org.ricks.orm.anno.Table;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TableMeta<T> {

    private final Class<T> klass;
    /**
     * 抓取策略
     */
    protected Table.FetchType fetchType;
    private final MethodAccess methodAccess;
    private final ConstructorAccess<T> constructorAccess;

    private String name;
    private String charset;
    private String collate;
    private String comment;
    private ColumnMeta primaryColumn;
    protected ColumnMeta playerIdColumn;//玩家ID字段
    private List<ColumnMeta> columns;
    private Set<ColumnMeta> indexKeys;
    private Map<String, ColumnMeta> columnMetaMap;

    public TableMeta(Class<T> klass) {
        this.klass = klass;
        Table entity = klass.getAnnotation(Table.class);
        if(entity != null)this.fetchType = entity.fetch();
        this.methodAccess = MethodAccess.get(klass);
        this.constructorAccess = ConstructorAccess.get(klass);
    }

    public String getName() {
        return name.toLowerCase();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getCollate() {
        return collate;
    }

    public void setCollate(String collate) {
        this.collate = collate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ColumnMeta getPrimaryColumn() {
        return primaryColumn;
    }

    public void setPrimaryColumn(ColumnMeta primaryColumn) {
        this.primaryColumn = primaryColumn;
    }

    public List<ColumnMeta> getColumns() {
        return columns;
    }

    public void setColumns(List<ColumnMeta> columns) {
        this.columns = columns;
    }

    public Set<ColumnMeta> getIndexKeys() {
        return indexKeys;
    }

    public void setIndexKeys(Set<ColumnMeta> indexKeys) {
        this.indexKeys = indexKeys;
    }

    public Map<String, ColumnMeta> getColumnMetaMap() {
        return columnMetaMap;
    }

    public void setColumnMetaMap(Map<String, ColumnMeta> columnMetaMap) {
        this.columnMetaMap = columnMetaMap;
    }

    public void setPlayerIdColumn(ColumnMeta playerIdColumn) {
        this.playerIdColumn = playerIdColumn;
    }

    public ColumnMeta getPlayerIdColumn() {
        return playerIdColumn;
    }

    /**
     * 获取主键的值.
     *
     * @param entity 实体对象
     * @return 对象的主键值
     */
    public Serializable getPrimaryIdValue(Object entity) {
        return (Serializable) methodAccess.invoke(entity, primaryColumn.getGetMethodIndex());
    }

    public Class<T> getTableClass() {
        return klass;
    }

    public T newEntity() {
        return constructorAccess.newInstance();
    }

    public MethodAccess getMethodAccess() {
        return methodAccess;
    }

    /**
     * 获取玩家ID的值.
     *
     * @param entity 实体对象
     * @return 对象的玩家ID
     */
    public Serializable getPlayerIdValue(Object entity) {
        return (Serializable) methodAccess.invoke(entity, playerIdColumn.getGetMethodIndex());
    }

    public Object getColumnValue(Object entity,ColumnMeta columnMeta) {
        return methodAccess.invoke(entity, columnMeta.getGetMethodIndex());
    }


    public ColumnMeta getPlayerId() {
        return playerIdColumn;
    }

    public Table.FetchType getFetchType() {
        return fetchType;
    }

    /**
     * 构造一个回写数据的唯一Key.
     * <p>
     * 类的全名+主键值
     *
     * @param entity 实体对象
     * @return 拼接后的唯一Key
     */
    public String getPrimaryKey(Object entity) {
        return new StringBuilder(64).append(klass.getName()).append(':').append(this.getPrimaryIdValue(entity)).toString();
    }

}
