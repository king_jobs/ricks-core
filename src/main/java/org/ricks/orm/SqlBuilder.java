package org.ricks.orm;

import org.ricks.common.exception.UnrealizedException;
import org.ricks.common.StrUtil;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SqlBuilder {

    public static String getTableExists(String tableName) {
        return String.format("SHOW TABLES LIKE '%s';", tableName);
    }

    /**
     * SELECT column_name,column_type FROM information_schema.columns WHERE table_name='%s' AND table_schema = (SELECT database());
     * @param tableName
     * @return
     */
    public static String getTableColumn(String tableName) {
        return String.format("SELECT O.column_name columnName,O.column_type columnType,T.INDEX_NAME indexName FROM information_schema.columns O LEFT JOIN INFORMATION_SCHEMA.STATISTICS T ON  (O.table_schema = T.TABLE_SCHEMA AND T.TABLE_NAME = O.TABLE_NAME AND T.COLUMN_NAME = O.COLUMN_NAME) WHERE O.table_schema = (SELECT database()) AND O.TABLE_NAME ='%s'", tableName);
    }

    public static String genCreateTableSql(TableMeta tableMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE IF NOT EXISTS `").append(tableMeta.getName()).append("` (\n");
        builder.append(" ").append(getCreateColumn(tableMeta.getPrimaryColumn()));
        List<ColumnMeta> columnMetas = tableMeta.getColumns();
        for (ColumnMeta columnMeta: columnMetas) {
            if(!columnMeta.isPrimaryKey()) {
                builder.append(",\n ").append(getCreateColumn(columnMeta));
            }
        }
//        builder.append(" PRIMARY KEY (`").append(tableMeta.getPrimaryColumn().getName()).append("`)");
        Set<ColumnMeta> indexKeys = tableMeta.getIndexKeys();
        indexKeys.forEach(columnMeta -> createIndex(builder,columnMeta));
        builder.append("\n)");
        if (tableMeta.getCharset() != null) {
            builder.append(" DEFAULT CHARACTER SET=").append(tableMeta.getCharset());
            if (tableMeta.getCollate() != null) {
                builder.append(" COLLATE=").append(tableMeta.getCollate());
            }
        }

        if (tableMeta.getComment() != null) {
            builder.append(" COMMENT='").append(tableMeta.getComment()).append("'");
        }
        builder.append(";");
        return builder.toString();
    }

    private static void createIndex(StringBuilder builder, ColumnMeta columnMeta) {
        if(StrUtil.isNotBlank(columnMeta.getIndex())) {
            builder.append(",\n").append(" INDEX `").append(columnMeta.getIndex())
                    .append("` (`").append(columnMeta.getColumnName()).append("`) USING BTREE ");
        } else {
            builder.append(",\n").append(" INDEX `idx_").append(columnMeta.getColumnName())
                    .append("` (`").append(columnMeta.getColumnName()).append("`) USING BTREE ");
        }
    }

    private static String getCreateColumn(ColumnMeta columnMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append(" `").append(columnMeta.getColumnName()).append("` ").append(evalFieldType(columnMeta));
        if (columnMeta.isUnsigned()) {
            builder.append(" unsigned");
        }
        if (columnMeta.isNonNull()) {
            builder.append(" NOT NULL");
        } else {
            builder.append(" NULL");
        }
        if (columnMeta.hasDefaultValue()) {
            switch (columnMeta.getType()) {
                case AsBoolean:
                case AsInteger:
                case AsAtomicInteger:
                case AsLong:
                case AsLongAdder:
                case AsAtomicLong:
                case AsFloat:
                case AsDouble:
                    builder.append(" DEFAULT ").append(columnMeta.getDefaultValue()).append("");
                    break;
                default:
                    // 超过这个值当Text啦，Text是不可以有默认值的.
                    if (columnMeta.getLength() < DataConstant.VARCHAT_MAX_WIDTH) {
                        builder.append(" DEFAULT '").append(columnMeta.getDefaultValue()).append("'");
                    }
                    break;
            }
        }
        if(columnMeta.isAuto()) {
            builder.append(" auto_increment ");
        }
        if(columnMeta.isPrimaryKey()) {
            builder.append(" PRIMARY KEY ");
        }
        if (columnMeta.getComment() != null) {
            builder.append(" COMMENT '").append(columnMeta.getComment()).append("'");
        }
        return builder.toString();
    }

    public static String getRemoveColumn(TableMeta tableMeta, ColumnMeta columnMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("ALTER TABLE ").append(tableMeta.getName()).append(" ");
        builder.append(" DROP COLUMN ").append(columnMeta.getColumnName());
//        builder.append(";");
        return builder.toString();
    }

    public static String getRemoveColumn(TableMeta tableMeta, String columnName) {
        StringBuilder builder = new StringBuilder();
        builder.append("ALTER TABLE ").append(tableMeta.getName()).append(" ");
        builder.append(" DROP COLUMN ").append(columnName);
//        builder.append(";");
        return builder.toString();
    }

    public static String getAddColumn(TableMeta tableMeta, ColumnMeta columnMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("ALTER TABLE ").append(tableMeta.getName()).append(" ");
        builder.append(" ADD COLUMN").append(getCreateColumn(columnMeta));
//        builder.append(";");
        return builder.toString();
    }

    public static String modifyColumn(TableMeta tableMeta, ColumnMeta columnMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("ALTER TABLE ").append(tableMeta.getName()).append(" ");
        builder.append(" modify COLUMN").append(getCreateColumn(columnMeta));
//        builder.append(";");
        return builder.toString();
    }

    public static String count(TableMeta tableMeta,String... fileds) {
        if(fileds == null || fileds.length == 0) {
            return count(tableMeta);
        }
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT COUNT( ").append(tableMeta.getPrimaryColumn().getColumnName()).append( ") FROM `").append(tableMeta.getName()).append("` WHERE `");
        for (int i = 0; i < fileds.length; i++) {
            builder.append(fileds[i]).append("` = ? ");
        }
        return builder.toString();
    }

    public static String count(TableMeta tableMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT COUNT( ").append(tableMeta.getPrimaryColumn().getColumnName()).append( ") FROM ").append(tableMeta.getName());
        return builder.toString();
    }

    public static String getAddColumnIndex(TableMeta tableMeta, ColumnMeta columnMeta) {
        StringBuilder builder = new StringBuilder();
        if(columnMeta.isPrimaryKey()) {
            builder.append("ALTER TABLE `").append(tableMeta.getName()).append("` ");
            builder.append(" ADD ").append(columnMeta.getIndex()).append(" PRIMARY KEY").append("(").append(columnMeta.getColumnName()).append(");");
        } else {
            builder.append("ALTER TABLE `").append(tableMeta.getName()).append("` ");
            builder.append(" ADD ").append(columnMeta.getIndex()).append(" INDEX index_")
                    .append(columnMeta.getColumnName()).append("(").append(columnMeta.getColumnName()).append(")");
        }
        return builder.toString();
    }

    public static String getDelColumnIndex(TableMeta tableMeta, ColumnMeta columnMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("ALTER TABLE `").append(tableMeta.getName()).append("` ");
        builder.append(" drop ").append(" INDEX index_")
                .append(columnMeta.getColumnName());
        return builder.toString();
    }

    public static String getInsert(TableMeta tableMeta) {
        // INSERT [LOW_PRIORITY | DELAYED] [IGNORE]
        // [INTO] tbl_name [(col_name,...)]
        // VALUES (expression,...),(...),...
        StringBuilder sb = new StringBuilder(128);
        sb.append("INSERT INTO ");
        append(sb, tableMeta.getName()).append(" (");
        List<ColumnMeta> columnMetas = tableMeta.getColumns();
        int count = 0;
        for (ColumnMeta cm : columnMetas) {
            append(sb, cm.getColumnName()).append(',');
            count++;
        }
        sb.setCharAt(sb.length() - 1, ')');

        sb.append(" VALUES (");
        for (int i = 0; i < count; i++) {
            sb.append("?,");
        }
        sb.setCharAt(sb.length() - 1, ')');
        return sb.toString();
    }

    public static String getDelete(TableMeta tableMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("DELETE FROM `").append(tableMeta.getName()).append("` WHERE `")
                .append(tableMeta.getPrimaryColumn().getColumnName()).append("` = ? ");
        return builder.toString();
    }

    public static String getUpdate(TableMeta tableMeta) {
        List<ColumnMeta> columnMetas = tableMeta.getColumns();
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE ").append(tableMeta.getName()).append(" ");
        if (!tableMeta.getColumns().isEmpty()) {
            builder.append(" SET");
            Iterator<ColumnMeta> iterator = columnMetas.stream().filter(columnMeta -> !columnMeta.isPrimaryKey()).iterator();
            if (iterator.hasNext()) {
                while (true) {
                    ColumnMeta columnMeta = iterator.next();
                    if (columnMeta.isReadOnly()) {
                        if (!iterator.hasNext()) {
                            break;
                        }
                    } else{
                        if(columnMeta.isPrimaryKey()) break;
                        builder.append(" ").append(columnMeta.getColumnName()).append(" = ?");
                        if (iterator.hasNext()) {
                            builder.append(",");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        builder.append(" WHERE ").append(tableMeta.getPrimaryColumn().getColumnName()).append(" = ?");
        return builder.toString();
    }

    public static String getSelect(TableMeta tableMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM `").append(tableMeta.getName()).append("` WHERE `")
                .append(tableMeta.getPrimaryColumn().getColumnName()).append("` = ? ");
        return builder.toString();
    }

    public static String getSelect(TableMeta tableMeta,List<String> columnNames) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM `").append(tableMeta.getName()).append("`");
        Iterator<String> iterator = columnNames.iterator();
        if (iterator.hasNext()) {
            builder.append(" WHERE");
            while (true) {
                String key = iterator.next();
                builder.append(" `").append(key).append("` = ?");
                if (iterator.hasNext()) {
                    builder.append(" and");
                } else {
                    break;
                }
            }
        }
        return builder.toString();
    }

    public static String getLimitSelect(TableMeta tableMeta,String... fileds) {
        if(fileds.length == 0) {
            return getSelect(tableMeta);
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append("SELECT * FROM `").append(tableMeta.getName()).append("` WHERE `");
            for (int i = 0; i < fileds.length; i++) {
                builder.append(fileds[i]).append("` = ? ");
            }
            builder.append(" limit ?,? ");
            return builder.toString();
        }
    }


    /**
     * 不能放开查询table 所有数据，线上毁灭性打击
     * @param tableMeta
     * @return
     */
    public static String getSelectAll(TableMeta tableMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM `").append(tableMeta.getName()).append("` ");
        return builder.toString();
    }

    public static String getSelectLimit(TableMeta tableMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM `").append(tableMeta.getName()).append("` limit ?,? ");
        return builder.toString();
    }

    public static String getMaxPrimaryKey(TableMeta tableMeta) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT MAX(`").append(tableMeta.getPrimaryColumn().getColumnName()).append("`) FROM `")
                .append(tableMeta.getName()).append("` ");
        return builder.toString();
    }

    public static String getPrimaryColumnSelectField(TableMeta tableMeta, List<String> fields) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT `").append(tableMeta.getPrimaryColumn().getColumnName()).append("` FROM `")
                .append(tableMeta.getName()).append("`");
        Iterator<String> iterator = fields.iterator();
        if (iterator.hasNext()) {
            builder.append(" WHERE");
            while (true) {
                String key = iterator.next();
                builder.append(" `").append(key).append("` = ?");
                if (iterator.hasNext()) {
                    builder.append(" and");
                } else {
                    break;
                }
            }
        }
//        builder.append(";");
        return builder.toString();
    }


    public static <T> String genSelectByPlayerId(TableMeta<T> tm) {
        // Select id from item where role_id = ?
        StringBuilder sb = new StringBuilder(128);
        sb.append("SELECT ");
        for (ColumnMeta cm : tm.getColumns()) {
            append(sb, cm.getColumnName()).append(',');
        }
        sb.setCharAt(sb.length() - 1, ' ');
        sb.append("FROM ");
        append(sb, tm.getName());
        if (tm.getPlayerId() != null) {
            sb.append(" WHERE ");
            append(sb, tm.getPlayerId().getColumnName()).append("=?");
        }
        return sb.toString();
    }

    public static  <T> String genAddTableColumnSql(TableMeta<T> tm, ColumnMeta cm) {
        return genAddOrUpdateTableColumnSql(tm, cm, false);
    }

    public static <T> String genUpdateTableColumnSql(TableMeta<T> tm, ColumnMeta cm) {
        return genAddOrUpdateTableColumnSql(tm, cm, true);
    }

    private static  <T> String genAddOrUpdateTableColumnSql(TableMeta<T> tm, ColumnMeta cm, boolean update) {
        StringBuilder sb = new StringBuilder(128);
        sb.append("ALTER TABLE `").append(tm.getName()).append("` ").append(update ? "MODIFY" : "ADD").append(" COLUMN `").append(cm.getColumnName());
        sb.append("` ").append(evalFieldType(cm));

        if (cm.isNonNull()) {
            sb.append(" NOT NULL");
        } else if (cm.getType() == FieldType.AsDate) {
            sb.append(" NULL");
        }

        sb.append(" COMMENT '").append(cm.getComment()).append("'");
        return sb.toString();
    }

    public static String evalFieldType(ColumnMeta cm) {
        switch (cm.getType()) {

            // 游戏嘛，数字就是int(11)不要想多啦，简单直接明了
            case AsInteger:
            case AsAtomicInteger:
                return "INT";

            // 20就20吧~~~
            case AsLong:
            case AsLongAdder:
            case AsAtomicLong:
                return "BIGINT";

            // 有小数的就直接写上他写的参数
            case AsDouble:
                return "DOUBLE(" + cm.getPrecision() + "," + 15 + ")";

            case AsFloat:
                return "FLOAT(" + cm.getPrecision() + "," + 10 + ")";

            // 其它的参照默认字段规则 ...
            default:
                return defaultEvalFieldType(cm);
        }
    }

    /**
     * 将来有其他SQL时，如果不合适，就把此层改进为通用型，具体实现放入底层重写.
     * <p>
     * MySql参考： http://dev.mysql.com/doc/refman/5.0/es/connector-j-reference-type- conversions.html
     *
     * @param cm 属性描述对象.
     * @return 返回当前属性对应SQL中的类型.
     */
    private static String defaultEvalFieldType(ColumnMeta cm) {
        switch (cm.getType()) {
            // Boolean直接写死啦，不可能为其他值的.
            case AsBoolean:
                return "BIT(1)";

            // 字符串类型的，过长需要换类型
            case AsString:
            case AsJson:
                // 如果大于65535，那就要转化为MEDIUMTEXT，大概能存~16M
                if (cm.getLength() > DataConstant.TEXT_MAX_WIDTH) {
                    return "MEDIUMTEXT";
                }
                // 如果长度等于10K，那就转化为TEXT，大概能存~64K
                else if (cm.getLength() >= DataConstant.VARCHAT_MAX_WIDTH) {
                    return "TEXT";
                }
                // 其他情况还是使用VarChar方式
                return "VARCHAR(" + cm.getLength() + ")";

            // 日期类型的，三种，其他用不着就不实现啦.
            case AsInstant:
            case AsLocalDateTime:
            case AsDate:
                return "DATETIME";

            // 数字类型的就写成通用的，Mysql的由子类重写
            case AsInteger:
            case AsAtomicInteger:
            case AsLong:
            case AsLongAdder:
            case AsAtomicLong:
                // 用户自定义了宽度
                if (cm.getLength() > 0) {
                    return "INT(" + cm.getLength() + ")";
                }
                // 用数据库的默认宽度
                return "INT";

            case AsDouble: return "NUMERIC(15,10)";
            case AsFloat:
                // 用户自定义了精度
                if (cm.getLength() > 0 && cm.getLength() > 0) {
                    return "NUMERIC(" + cm.getLength() + "," + cm.getPrecision() + ")";
                }
                return "FLOAT";
            case AsBlob:
                // 如果大于65535，那就要转化为MEDIUMTEXT，大概能存~16M
                if (cm.getLength() > DataConstant.BLOB_MAX_WIDTH) {
                    return "MEDIUMBLOB";
                }
                // 小于65535，就使用Blob
                return "BLOB";
            default:
                throw new UnrealizedException("未实现的Java属性转Mysql类型：" + cm.getType());
        }
    }

    public static  <T> String genDropTableColumnSql(TableMeta<T> tm, String columnName) {
        // alter table tableName drop column xxx;
        StringBuilder sb = new StringBuilder(128);
        sb.append("ALTER TABLE `").append(tm.getName()).append("` DROP COLUMN `").append(columnName).append("` ");
        return sb.toString();
    }

    public static <T> String genUpdateDefaultValueSql(TableMeta<T> tm, ColumnMeta cm) {
        StringBuilder sb = new StringBuilder(64);
        sb.append("UPDATE ");
        append(sb, tm.getName()).append(" SET ");
        append(sb, cm.getColumnName()).append("='").append(cm.getDefaultValue()).append("'");
        return sb.toString();
    }

    /**
     * 添加字段名字，如果是关键字则要添加反点号...
     *
     * @param sb   StringBuilder对象
     * @param name 字段名称
     * @return 修正关键字的名称
     */
    private static StringBuilder append(StringBuilder sb, String name) {
        if (MysqlKeyword.isKeyword(name)) {
            return sb.append("`").append(name).append("`");
        } else {
            return sb.append(name);
        }
    }
}
