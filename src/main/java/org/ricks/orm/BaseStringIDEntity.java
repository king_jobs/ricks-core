package org.ricks.orm;

import org.ricks.orm.anno.Column;
import org.ricks.orm.anno.IndexKey;
import org.ricks.orm.anno.PrimaryKey;
import org.ricks.ioc.PlayerId;

public class BaseStringIDEntity<K> extends AbstractEntity<K,String> {

    private static final long serialVersionUID = 4165629356522195369L;

    @PrimaryKey()
    @Column(comment = "ID")
    private String id;

    @IndexKey
    @PlayerId
    @Column()
    private long roleId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
