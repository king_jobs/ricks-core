package org.ricks.orm.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    boolean readOnly() default false;
    boolean nonNull() default false;
    boolean unsigned() default false;
    int length() default 255;
    String comment() default "";
    String columnName() default "";
    int precision() default 15;

    /**
     * 建表时的默认值。
     *
     * @return 如果有此属性，则在生成建表语句时添加此默认值
     */
    String defaultValue() default "";

}
