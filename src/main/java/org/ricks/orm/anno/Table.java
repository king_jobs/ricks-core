package org.ricks.orm.anno;

import org.ricks.orm.BaseRepository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Table {

    String name() default "";
    String charset() default "utf8";
    String collate() default "";
    String comment() default "";

    /**
     * 返回当前实体类的抓取策略.
     *
     * @return 返回配置的抓取策略，默认值为什么用，什么时候初始化.
     */
    FetchType fetch() default FetchType.USE;

    /**
     * db对象 必然会对应一个 Orm Repository
     * 扫描@Table自动生成 Repository
     * 如果存在自定义得 @Repository 则不会根据ASM自动生成
     */
    Class<? extends BaseRepository> repository() default BaseRepository.class;

    /**
     * 抓取策略.
     * <p>
     * 1.启动服务器的时候，初始化当前实体数据.<br>
     * 2.登录游戏的时候，初始化当前实体数据.<br>
     * 3.什么时候用，什么时候初始化当前实体数据.<br>
     *
     * @author 小流氓[176543888@qq.com]
     */
    public enum FetchType {
        /**
         * 启动服务器的时候，初始化当前实体数据.
         */
        START,
        /**
         * 什么时候用，什么时候初始化当前实体数据.
         */
        USE;
    }
}
