package org.ricks.orm.anno;

import java.lang.annotation.*;

/**
 * 开发者给db对象自定义了 Repository。默认 Repository class(db对象 + Repository)
 * Repository 标识
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Repository {

}
