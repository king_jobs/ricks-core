package org.ricks.protocol;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ByteBuf {

    public static final byte[] EMPTY_BYTES = new byte[0];
    private byte[]  array;
    private int     writeIndex = 0;
    private int     readIndex = 0;

    public ByteBuf(int size)
    {
        array = new byte[size];
    }

    public ByteBuf(byte[] array)
    {
        this.array = array;
    }

    public static ByteBuf allocate(int size)
    {
        return new ByteBuf(size);
    }

    public static ByteBuf allocate()
    {
        return new ByteBuf(1024);
    }

    public static ByteBuf wrap(byte[] array)
    {
        return new ByteBuf(array);
    }

    protected void ensureCapacity(int len) {
        if (len > array.length - writeIndex) {
            int    newLen = len + writeIndex > (array.length << 1) ? len + writeIndex + array.length : (array.length << 1);
            byte[] tmp    = new byte[newLen];
            System.arraycopy(array, 0, tmp, 0, array.length);
            array = tmp;
        }
    }

    public void sureRead(int n) {
//        if (readIndex + n > writeIndex) {
//            throw new RuntimeException("read not enough");
//        }
    }


    public void clear()
    {
        writeIndex = readIndex = 0;
    }

    public void put(byte value)
    {
        ensureCapacity(1);
        array[writeIndex] = value;
        writeIndex += 1;
    }

    public void setByte(int off, byte b)
    {
        array[off] = b;
    }

    public void put(byte[] data)
    {
        ensureCapacity(data.length);
        System.arraycopy(data, 0, array, writeIndex, data.length);
        writeIndex += data.length;
    }

    public byte get()
    {
        byte result = array[readIndex];
        readIndex += 1;
        return result;
    }

    public byte[] toArray()
    {
        byte[] result = new byte[writeIndex];
        System.arraycopy(array, 0, result, 0, writeIndex);
        return result;
    }

    public int getwriteIndex() {
        return writeIndex;
    }


    public int getReadIndex() {
        return readIndex;
    }

    public void setwriteIndex(int writeIndex) {
        this.writeIndex = writeIndex;
    }

    public void setReadPosi(int readPosi)
    {
        this.readIndex = readPosi;
    }

    public boolean readBool() {
        sureRead(1);
        return array[(readIndex++)] != 0;
    }

    public void writeBool(boolean x) {
        ensureCapacity(1);
        array[(writeIndex++)] = x ? (byte) 1 : 0;
    }

    public byte readByte() {
        sureRead(1);
        return array[(readIndex++)];
    }

    public void writeByte(byte x) {
        ensureCapacity(1);
        array[writeIndex++] = x;
    }

    public void writeShort(short x) {
        writeCompactShort(x);
    }

    public short readShort() {
        return readCompactShort();
    }

    public short readCompactShort() {
        int h = (array[readIndex] & 0xff);
        if (h < 0x80) {
            sureRead(1);
            readIndex++;
            return (short)h;
        } else if (h < 0xc0) {
            sureRead(2);
            int x = ((h & 0x3f) << 8) | (array[readIndex + 1] & 0xff);
            readIndex += 2;
            return (short)x;
        } else if( (h == 0xff)){
            sureRead(3);
            int x = ((array[readIndex + 1] & 0xff) << 8) | (array[readIndex + 2] & 0xff);
            readIndex += 3;
            return (short)x;
        } else {
            throw new RuntimeException("exceed max short");
        }
    }

    public void writeCompactShort(short x) {
        if (x >= 0) {
            if (x < 0x80) {
                ensureCapacity(1);
                array[writeIndex++] = (byte) x;
                return;
            } else if (x < 0x4000) {
                ensureCapacity(2);
                array[writeIndex + 1] = (byte) x;
                array[writeIndex] = (byte) ((x >> 8) | 0x80);
                writeIndex += 2;
                return;
            }
        }
        ensureCapacity(3);
        array[writeIndex] = (byte) 0xff;
        array[writeIndex + 2] = (byte) x;
        array[writeIndex + 1] = (byte) (x >> 8);
        writeIndex += 3;
    }

    public byte[] readBytes() {
        int n = readSize();
        if(n > 0) {
            sureRead(n);
            int start = readIndex;
            readIndex += n;
            return Arrays.copyOfRange(array, start, readIndex);
        } else {
            return EMPTY_BYTES;
        }
    }

    public void writeBytes(byte[] x) {
        int n = x.length;
        if(n > 0) {
            ensureCapacity(n);
            System.arraycopy(x, 0, array, writeIndex, n);
            writeIndex += n;
        }
    }

    public int readSize() {
        return readCompactUint();
    }

    public int readCompactUint() {
        int n = readCompactInt();
        if (n >= 0) {
            return n;
        } else {
            throw new RuntimeException("unmarshal CompactUnit");
        }
    }

    public int readCompactInt() {
        int h = array[readIndex] & 0xff;
        if (h < 0x80) {
            sureRead(1);
            readIndex++;
            return h;
        } else if (h < 0xc0) {
            sureRead(2);
            int x = ((h & 0x3f) << 8) | (array[readIndex + 1] & 0xff);
            readIndex += 2;
            return x;
        } else if (h < 0xe0) {
            sureRead(3);
            int x = ((h & 0x1f) << 16) | ((array[readIndex + 1] & 0xff) << 8) | (array[readIndex + 2] & 0xff);
            readIndex += 3;
            return x;
        } else if (h < 0xf0) {
            sureRead(4);
            int x = ((h & 0x0f) << 24) | ((array[readIndex + 1] & 0xff) << 16) | ((array[readIndex + 2] & 0xff) << 8) | (array[readIndex + 3] & 0xff);
            readIndex += 4;
            return x;
        } else {
            sureRead(5);
            int x = ((array[readIndex + 1] & 0xff) << 24) | ((array[readIndex + 2] & 0xff) << 16) | ((array[readIndex + 3] & 0xff) << 8) | (array[readIndex + 4] & 0xff);
            readIndex += 5;
            return x;
        }
    }

    public void writeCompactInt(int x) {
        if (x >= 0) {
            if (x < 0x80) {
                ensureCapacity(1);
                array[writeIndex++] = (byte) x;
                return;
            } else if (x < 0x4000) {
                ensureCapacity(2);
                array[writeIndex + 1] = (byte) x;
                array[writeIndex] = (byte) ((x >> 8) | 0x80);
                writeIndex += 2;
                return;
            } else if (x < 0x200000) {
                ensureCapacity(3);
                array[writeIndex + 2] = (byte) x;
                array[writeIndex + 1] = (byte) (x >> 8);
                array[writeIndex] = (byte) ((x >> 16) | 0xc0);
                writeIndex += 3;
                return;
            } else if (x < 0x10000000) {
                ensureCapacity(4);
                array[writeIndex + 3] = (byte) x;
                array[writeIndex + 2] = (byte) (x >> 8);
                array[writeIndex + 1] = (byte) (x >> 16);
                array[writeIndex] = (byte) ((x >> 24) | 0xe0);
                writeIndex += 4;
                return;
            }
        }
        ensureCapacity(5);
        array[writeIndex] = (byte) 0xf0;
        array[writeIndex + 4] = (byte) x;
        array[writeIndex + 3] = (byte) (x >> 8);
        array[writeIndex + 2] = (byte) (x >> 16);
        array[writeIndex + 1] = (byte) (x >> 24);
        writeIndex += 5;
    }

    public void writeCompactUint(int x) {
        writeCompactInt(x);
    }

    public int readInt() {
        return readCompactInt();
    }

    public void writeInt(int x) {
        writeCompactInt(x);
    }

    public long readLong() {
        return readCompactLong();
    }

    public void writeLong(long x) {
        writeCompactLong(x);
    }

    public long readCompactLong() {
        sureRead(1);
        int h = array[readIndex] & 0xff;
        if (h < 0x80) {
            readIndex++;
            return h;
        } else if (h < 0xc0) {
            sureRead(2);
            int x = ((h & 0x3f) << 8) | (array[readIndex + 1] & 0xff);
            readIndex += 2;
            return x;
        } else if (h < 0xe0) {
            sureRead(3);
            int x = ((h & 0x1f) << 16) | ((array[(readIndex+ 1)] & 0xff) << 8) | (array[(readIndex+ 2)] & 0xff);
            readIndex += 3;
            return x;
        } else if (h < 0xf0) {
            sureRead(4);
            int x = ((h & 0x0f) << 24) | ((array[(readIndex+ 1)] & 0xff) << 16) | ((array[(readIndex+ 2)] & 0xff) << 8) | (array[(readIndex+ 3)] & 0xff);
            readIndex += 4;
            return x;
        } else if (h < 0xf8) {
            sureRead(5);
            int xl = (array[(readIndex+ 1)] << 24) | ((array[(readIndex+ 2)] & 0xff) << 16) | ((array[(readIndex+ 3)] & 0xff) << 8) | (array[(readIndex+ 4)] & 0xff);
            int xh = h & 0x07;
            readIndex += 5;
            return ((long) xh << 32) | (xl & 0xffffffffL);
        } else if (h < 0xfc) {
            sureRead(6);
            int xl = (array[(readIndex+ 2)] << 24) | ((array[(readIndex+ 3)] & 0xff) << 16) | ((array[(readIndex + 4)] & 0xff) << 8) | (array[(readIndex + 5)] & 0xff);
            int xh = ((h & 0x03) << 8) | (array[(readIndex + 1)] & 0xff);
            readIndex += 6;
            return ((long) xh << 32) | (xl & 0xffffffffL);
        } else if (h < 0xfe) {
            sureRead(7);
            int xl = (array[(readIndex + 3)] << 24) | ((array[(readIndex + 4)] & 0xff) << 16) | ((array[(readIndex + 5)] & 0xff) << 8) | (array[(readIndex + 6)] & 0xff);
            int xh = ((h & 0x01) << 16) | ((array[(readIndex + 1)] & 0xff) << 8) | (array[(readIndex + 2)] & 0xff);
            readIndex += 7;
            return ((long) xh << 32) | (xl & 0xffffffffL);
        } else if (h < 0xff) {
            sureRead(8);
            int xl = (array[(readIndex + 4)] << 24) | ((array[(readIndex + 5)] & 0xff) << 16) | ((array[(readIndex + 6)] & 0xff) << 8) | (array[(readIndex + 7)] & 0xff);
            int xh = /*((h & 0x0) << 16) | */
                    ((array[(readIndex + 1)] & 0xff) << 16) | ((array[(readIndex + 2)] & 0xff) << 8) | (array[(readIndex + 3)] & 0xff);
            readIndex += 8;
            return ((long) xh << 32) | (xl & 0xffffffffL);
        } else {
            sureRead(9);
            int xl = (array[(readIndex + 5)] << 24) | ((array[(readIndex + 6)] & 0xff) << 16) | ((array[(readIndex + 7)] & 0xff) << 8) | (array[(readIndex + 8)] & 0xff);
            int xh = (array[(readIndex + 1)] << 24) | ((array[(readIndex + 2)] & 0xff) << 16) | ((array[(readIndex + 3)] & 0xff) << 8) | (array[(readIndex + 4)] & 0xff);
            readIndex += 9;
            return ((long) xh << 32) | (xl & 0xffffffffL);
        }
    }

    public void writeCompactLong(long x) {
        if (x >= 0) {
            if (x < 0x80) {
                ensureCapacity(1);
                array[(writeIndex++)] = (byte) x;
                return;
            } else if (x < 0x4000) {
                ensureCapacity(2);
                array[(writeIndex + 1)] = (byte) x;
                array[(writeIndex)] = (byte) ((x >> 8) | 0x80);
                writeIndex += 2;
                return;
            } else if (x < 0x200000) {
                ensureCapacity(3);
                array[(writeIndex + 2)] = (byte) x;
                array[(writeIndex + 1)] = (byte) (x >> 8);
                array[(writeIndex)] = (byte) ((x >> 16) | 0xc0);
                writeIndex += 3;
                return;
            } else if (x < 0x10000000) {
                ensureCapacity(4);
                array[(writeIndex + 3)] = (byte) x;
                array[(writeIndex + 2)] = (byte) (x >> 8);
                array[(writeIndex + 1)] = (byte) (x >> 16);
                array[(writeIndex)] = (byte) ((x >> 24) | 0xe0);
                writeIndex += 4;
                return;
            } else if (x < 0x800000000L) {
                ensureCapacity(5);
                array[(writeIndex + 4)] = (byte) x;
                array[(writeIndex + 3)] = (byte) (x >> 8);
                array[(writeIndex + 2)] = (byte) (x >> 16);
                array[(writeIndex + 1)] = (byte) (x >> 24);
                array[(writeIndex)] = (byte) ((x >> 32) | 0xf0);
                writeIndex += 5;
                return;
            } else if (x < 0x40000000000L) {
                ensureCapacity(6);
                array[(writeIndex + 5)] = (byte) x;
                array[(writeIndex + 4)] = (byte) (x >> 8);
                array[(writeIndex + 3)] = (byte) (x >> 16);
                array[(writeIndex + 2)] = (byte) (x >> 24);
                array[(writeIndex + 1)] = (byte) (x >> 32);
                array[(writeIndex)] = (byte) ((x >> 40) | 0xf8);
                writeIndex += 6;
                return;
            } else if (x < 0x200000000000L) {
                ensureCapacity(7);
                array[(writeIndex + 6)] = (byte) x;
                array[(writeIndex + 5)] = (byte) (x >> 8);
                array[(writeIndex + 4)] = (byte) (x >> 16);
                array[(writeIndex + 3)] = (byte) (x >> 24);
                array[(writeIndex + 2)] = (byte) (x >> 32);
                array[(writeIndex + 1)] = (byte) (x >> 40);
                array[(writeIndex)] = (byte) ((x >> 48) | 0xfc);
                writeIndex += 7;
                return;
            } else if (x < 0x100000000000000L) {
                ensureCapacity(8);
                array[(writeIndex + 7)] = (byte) x;
                array[(writeIndex + 6)] = (byte) (x >> 8);
                array[(writeIndex + 5)] = (byte) (x >> 16);
                array[(writeIndex + 4)] = (byte) (x >> 24);
                array[(writeIndex + 3)] = (byte) (x >> 32);
                array[(writeIndex + 2)] = (byte) (x >> 40);
                array[(writeIndex + 1)] = (byte) (x >> 48);
                array[(writeIndex)] = /*(x >> 56) | */ (byte) 0xfe;
                writeIndex += 8;
                return;
            }
        }
        ensureCapacity(9);
        array[(writeIndex + 8)] = (byte) x;
        array[(writeIndex + 7)] = (byte) (x >> 8);
        array[(writeIndex + 6)] = (byte) (x >> 16);
        array[(writeIndex + 5)] = (byte) (x >> 24);
        array[(writeIndex + 4)] = (byte) (x >> 32);
        array[(writeIndex + 3)] = (byte) (x >> 40);
        array[(writeIndex + 2)] = (byte) (x >> 48);
        array[(writeIndex + 1)] = (byte) (x >> 56);
        array[(writeIndex)] = (byte) 0xff;
        writeIndex += 9;
    }

    public float readFloat() {
        return Float.intBitsToFloat(readFint());
    }

    public void writeFloat(float z) {
        writeFint(Float.floatToIntBits(z));
    }

    public int readFint() {
        sureRead(4);
        int x = (array[(readIndex)] & 0xff) | ((array[(readIndex + 1)] & 0xff) << 8) | ((array[(readIndex + 2)] & 0xff) << 16) | ((array[(readIndex + 3)] & 0xff) << 24);
        readIndex += 4;
        return x;
    }

    public void writeFint(int x) {
        ensureCapacity(4);
        array[(writeIndex)] = (byte) (x & 0xff);
        array[(writeIndex + 1)] = (byte) ((x >> 8) & 0xff);
        array[(writeIndex + 2)] = (byte) ((x >> 16) & 0xff);
        array[(writeIndex + 3)] = (byte) ((x >> 24) & 0xff);
        writeIndex += 4;
    }

    public double readDouble() {
        return Double.longBitsToDouble(readFlong());
    }

    public void writeDouble(double z) {
        writeFlong(Double.doubleToLongBits(z));
    }

    public long readFlong() {
        sureRead(8);
        long x = ((array[(readIndex + 7)] & 0xffL) << 56) | ((array[(readIndex + 6)] & 0xffL) << 48) | ((array[(readIndex + 5)] & 0xffL) << 40) | ((array[(readIndex + 4)] & 0xffL) << 32) | ((array[(readIndex + 3)] & 0xffL) << 24) | ((array[(readIndex + 2)] & 0xffL) << 16) | ((array[(readIndex + 1)] & 0xffL) << 8) | (array[(readIndex)] & 0xffL);
        readIndex += 8;
        return x;
    }

    public void writeFlong(long x) {
        ensureCapacity(8);
        array[(writeIndex + 7)] = (byte) (x >> 56);
        array[(writeIndex + 6)] = (byte) (x >> 48);
        array[(writeIndex + 5)] = (byte) (x >> 40);
        array[(writeIndex + 4)] = (byte) (x >> 32);
        array[(writeIndex + 3)] = (byte) (x >> 24);
        array[(writeIndex + 2)] = (byte) (x >> 16);
        array[(writeIndex + 1)] = (byte) (x >> 8);
        array[(writeIndex)] = (byte) x;
        writeIndex += 8;
    }

    public String readString() {
        int n = ByteBufUtils.readInt(this);
        if(n > 0) {
            sureRead(n);
            int start = readIndex;
            readIndex += n;
            return new String(array, start, n, StandardCharsets.UTF_8);
        } else {
            return "";
        }
    }

    public void writeString(String x) {
        if(x.length() > 0) {
            byte[] bytes = x.getBytes( StandardCharsets.UTF_8);
            int n = bytes.length;
            ensureCapacity(n);
            ByteBufUtils.writeInt(this,n);
            System.arraycopy(bytes, 0, array, writeIndex, n);
            writeIndex += n;
        } else {
            writeByte((byte) 0);
        }
    }

    public boolean isReadable() {
        return writeIndex > readIndex;
    }

    public int readableBytes() {
        return writeIndex - readIndex;
    }

    public ByteBuf writerIndex(int writeIndex) {
        this.writeIndex = writeIndex;
        return this;
    }

    public byte getByte(int index) {
        return HeapByteBufUtil.getByte(array, index);
    }

    public ByteBuf readIndex(int readIndex) {
        this.readIndex = readIndex;
        return this;
    }

    public ByteBuf writeBytes(byte[] src, int srcIndex, int length) {
        ensureCapacity(length);
        setBytes(writeIndex, src, srcIndex, length);
        writeIndex += length;
        return this;
    }

    private ByteBuf setBytes(int writeIndex, byte[] src, int srcIndex, int length) {
        System.arraycopy(src, srcIndex, array, writeIndex, length);
        return this;
    }

    public ByteBuf readBytes(byte[] dst) {
        readBytes(dst, 0, dst.length);
        return this;
    }

    public ByteBuf readBytes(byte[] dst, int dstIndex, int length) {
//        checkReadableBytes(length);
        System.arraycopy(array, readIndex , dst, dstIndex, length);
        readIndex += length;
        return this;
    }

    private static final int INT_ZERO = 0;
    public static int checkPositiveOrZero(int i, String name) {
        if (i < INT_ZERO) {
            throw new IllegalArgumentException(name + " : " + i + " (expected: >= 0)");
        }
        return i;
    }

    private void checkReadableBytes0(int minimumReadableBytes) {
        if (readIndex > writeIndex - minimumReadableBytes) {
            throw new IndexOutOfBoundsException(String.format(
                    "readerIndex(%d) + length(%d) exceeds writerIndex(%d): %s",
                    readIndex, minimumReadableBytes, writeIndex, this));
        }
    }

    protected final void checkReadableBytes(int minimumReadableBytes) {
        checkReadableBytes0(checkPositiveOrZero(minimumReadableBytes, "minimumReadableBytes"));
    }
}
