package org.ricks.protocol;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Objects;

public class HashSetInt extends AbstractSet<Integer> {

    private final IntObjectHashMap<Boolean> map;

    public HashSetInt() {
        map = new IntObjectHashMap<>();
    }

    public HashSetInt(int initialCapacity) {
        map = new IntObjectHashMap<>(initialCapacity);
    }

    @Override
    public Iterator<Integer> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    @Override
    public boolean add(Integer e) {
        return map.put(e, Boolean.TRUE) == null;
    }

    public boolean add(int e) {
        return map.put(e, Boolean.TRUE) == null;
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) != null;
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var obj = (HashSetInt) o;
        return Objects.equals(map, obj.map);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(map);
    }
}
