package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceBooleanSerializer implements IEnhanceSerializer {

    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();

        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        switch (fieldSimpleName) {
            case "boolean":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Z", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeBoolean", "(Lorg/ricks/protocol/ByteBuf;Z)V", false);
                break;
            case "Boolean":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/lang/Boolean;", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeBooleanBox", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Boolean;)V", false);
                break;
            default:

        }
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();
        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);
        String amsReturnStr = "";
        switch (fieldSimpleName){
            case "boolean":
                amsReturnStr = "Z";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readBoolean", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);
                break;
            case "Boolean":
                amsReturnStr = "Ljava/lang/Boolean;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readBooleanBox", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);
        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz,field), "("+amsReturnStr+")V", false);
        }
        return amsReturnStr;
    }
}
