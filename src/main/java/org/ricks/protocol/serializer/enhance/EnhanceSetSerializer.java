package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.BaseField;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.registration.field.ObjectProtocolField;
import org.ricks.protocol.registration.field.SetField;
import org.ricks.protocol.utils.EnhanceUtils;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceSetSerializer implements IEnhanceSerializer {

    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var setField = (SetField) fieldRegistration;
        var arrayName = getSetClassName(setField);


        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/util/Set;", false);

        switch (arrayName) {
            case "boolean":
            case "Boolean":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeBooleanSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            case "byte":
            case "Byte":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeByteSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            case "short":
            case "Short":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeShortSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            case "int":
            case "Integer":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            case "long":
            case "Long":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            case "float":
            case "Float":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeFloatSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            case "double":
            case "Double":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeDoubleSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            case "String":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;)V", false);
                break;
            default:
                if (setField.getSetElementRegistration() instanceof ObjectProtocolField) {
                    methodVisitor.visitVarInsn(ALOAD, 0);
                    var fieldProtocolId = ((ObjectProtocolField) setField.getSetElementRegistration()).getProtocolId();
                    methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                    methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writePacketSet", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Set;Lorg/ricks/protocol/registration/IProtocolRegistration;)V", false);
                }
        }

    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var setField = (SetField) fieldRegistration;
        var arrayName = getSetClassName(setField);

        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);

        switch (arrayName) {
            case "boolean":
            case "Boolean":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readBooleanSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            case "byte":
            case "Byte":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readByteSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            case "short":
            case "Short":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readShortSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            case "int":
            case "Integer":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            case "long":
            case "Long":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            case "float":
            case "Float":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readFloatSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            case "double":
            case "Double":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readDoubleSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            case "String":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readStringSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;", false);

                break;
            default:
                if (setField.getSetElementRegistration() instanceof ObjectProtocolField) {
                    methodVisitor.visitVarInsn(ALOAD, 0);
                    var fieldProtocolId = ((ObjectProtocolField) setField.getSetElementRegistration()).getProtocolId();
                    methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                    methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readPacketSet", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Set;Lorg/ricks/protocol/registration/IProtocolRegistration;", false);
                }
        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);

            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz,field), "(Ljava/util/Set;)V", false);

        }
        return "Ljava/util/Set;";
    }

    public String getSetClassName(SetField setField) {
        if (setField.getSetElementRegistration() instanceof BaseField) {
            return ((Class<?>) ((ParameterizedType) setField.getType()).getActualTypeArguments()[0]).getSimpleName();
        } else {
            return setField.getType().getTypeName();
        }
    }
}
