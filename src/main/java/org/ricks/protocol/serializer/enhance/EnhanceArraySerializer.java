package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.ArrayField;
import org.ricks.protocol.registration.field.BaseField;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.registration.field.ObjectProtocolField;
import org.ricks.protocol.utils.EnhanceUtils;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceArraySerializer implements IEnhanceSerializer {


    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var arrayField = (ArrayField) fieldRegistration;
        var arrayName = getArrayClassName(arrayField);

//        Label label = new Label();
//        methodVisitor.visitLabel(label);
        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        switch (arrayName) {
            case "boolean":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Z", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeBooleanArray", "(Lorg/ricks/protocol/ByteBuf;[Z)V", false);
                break;
            case "Boolean":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/Boolean;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeBooleanBoxArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Boolean;)V", false);
                break;
            case "byte":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[B", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeByteArray", "(Lorg/ricks/protocol/ByteBuf;[B)V", false);
                break;
            case "Byte":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/Byte;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeByteBoxArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Byte;)V", false);
                break;
            case "short":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[S", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeShortArray", "(Lorg/ricks/protocol/ByteBuf;[S)V", false);
                break;
            case "Short":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/Short;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeShortBoxArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Short;)V", false);
                break;
            case "int":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[I", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntArray", "(Lorg/ricks/protocol/ByteBuf;[I)V", false);
                break;
            case "Integer":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/Integer;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntBoxArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Integer;)V", false);
                break;
            case "long":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[J", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongArray", "(Lorg/ricks/protocol/ByteBuf;[J)V", false);
                break;
            case "Long":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/Long;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongBoxArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Long;)V", false);
                break;
            case "float":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[F", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeFloatArray", "(Lorg/ricks/protocol/ByteBuf;[F)V", false);
                break;
            case "Float":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/Float;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeFloatBoxArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Float;)V", false);
                break;
            case "double":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[D", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeDoubleArray", "(Lorg/ricks/protocol/ByteBuf;[D)V", false);
                break;
            case "Double":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/Double;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeDoubleBoxArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Double;)V", false);
                break;
            case "String":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Ljava/lang/String;", false);

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/String;)V", false);
                break;
            default:
                if (arrayField.getArrayElementRegistration() instanceof ObjectProtocolField) {
                    methodVisitor.visitVarInsn(ALOAD, 1);
                    methodVisitor.visitVarInsn(ALOAD, 3);
                    var fieldProtocolId = ((ObjectProtocolField) arrayField.getArrayElementRegistration()).getProtocolId();
                    var fieldClazzName = arrayName.replace(".","/");
                    methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[L"+fieldClazzName+";", false);
                    methodVisitor.visitVarInsn(ALOAD, 0);
                    methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");


                    methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writePacketArray", "(Lorg/ricks/protocol/ByteBuf;[Ljava/lang/Object;Lorg/ricks/protocol/registration/IProtocolRegistration;)V", false);
                }
        }
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var arrayField = (ArrayField) fieldRegistration;
        var arrayName = getArrayClassName(arrayField);
        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);
        String amsReturnStr = "";
        switch (arrayName) {
            case "boolean":
                amsReturnStr = "[Z";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readBooleanArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);
                break;
            case "Boolean":
                amsReturnStr = "[Ljava/lang/Boolean;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readBooleanBoxArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "byte":
                amsReturnStr = "[B";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readByteArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Byte":
                amsReturnStr = "[Ljava/lang/Byte;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readByteBoxArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "short":
                amsReturnStr = "[S";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readShortArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Short":
                amsReturnStr = "[Ljava/lang/Short;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readShortBoxArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "int":
                amsReturnStr = "[I";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Integer":
                amsReturnStr = "[Ljava/lang/Integer;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntBoxArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "long":
                amsReturnStr = "[J";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Long":
                amsReturnStr = "[Ljava/lang/Long;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongBoxArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "float":
                amsReturnStr = "[F";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readFloatArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Float":
                amsReturnStr = "[Ljava/lang/Float;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readFloatBoxArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "double":
                amsReturnStr = "[D";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readDoubleArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Double":
                amsReturnStr = "[Ljava/lang/Double;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readDoubleBoxArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "String":
                amsReturnStr = "[Ljava/lang/String;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readStringArray", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            default:
                if (arrayField.getArrayElementRegistration() instanceof ObjectProtocolField) {

                    var fieldClazzName = arrayField.getType().getCanonicalName().replace(".", "/");
                    var fieldProtocolId = ((ObjectProtocolField) arrayField.getArrayElementRegistration()).getProtocolId();
                    amsReturnStr = "[L" + fieldClazzName + ";";


                    methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readPacketArray", "(Lorg/ricks/protocol/ByteBuf;)[L" + fieldClazzName + ";", false);

//                    methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz, field), "([L" + fieldClazzName + ";)V", false);


                    break;
                }
        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz, field), "("+amsReturnStr+")V", false);
        }
        return amsReturnStr;
    }

    public String getArrayClassName(ArrayField arrayField) {
        if (arrayField.getArrayElementRegistration() instanceof BaseField) {
            return arrayField.getType().getSimpleName();
        } else {
            return arrayField.getType().getCanonicalName();
        }
    }
}
