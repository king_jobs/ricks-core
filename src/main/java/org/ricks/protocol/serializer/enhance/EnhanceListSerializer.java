package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.*;
import org.ricks.protocol.utils.EnhanceUtils;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.ricks.common.asm.Opcodes.*;
import static org.ricks.common.asm.Opcodes.INVOKESTATIC;

public class EnhanceListSerializer implements IEnhanceSerializer {

    private static AtomicBoolean Init = new AtomicBoolean(false);
    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var listField = (ListField) fieldRegistration;
        var arrayName = getListClassName(listField);
        var flag = false;
        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/util/List;", false);

        switch (arrayName) {
            case "boolean":
            case "Boolean":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeBooleanList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            case "byte":
            case "Byte":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()[Lbyte;", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeByteList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            case "short":
            case "Short":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeShortList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            case "int":
            case "Integer":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            case "long":
            case "Long":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            case "float":
            case "Float":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeFloatList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            case "double":
            case "Double":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeDoubleList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            case "String":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;)V", false);
                break;
            default:
                if (listField.getListElementRegistration() instanceof ObjectProtocolField) {
                    methodVisitor.visitVarInsn(ALOAD, 0);
                    var fieldProtocolId = ((ObjectProtocolField) listField.getListElementRegistration()).getProtocolId();
                    methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                    methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writePacketList", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/List;Lorg/ricks/protocol/registration/IProtocolRegistration;)V", false);
                } else{
                    flag = true;
                }
        }
      /*  if(flag) {
            methodVisitor.visitTypeInsn(CHECKCAST, "java/util/List");
            methodVisitor.visitVarInsn(ASTORE, 4);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/collection/CollectionUtils", "size", "(Ljava/util/Collection;)I", false);
            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeInt", "(Lorg/ricks/protocol/ByteBuf;I)I", false);
            methodVisitor.visitInsn(POP);
            methodVisitor.visitVarInsn(ALOAD, 4);
            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/collection/CollectionUtils", "iterator", "(Ljava/util/Collection;)Ljava/util/Iterator;", false);
            methodVisitor.visitVarInsn(ASTORE, 5);
            Label label1 = new Label();
            methodVisitor.visitJumpInsn(GOTO, label1);
            Label label2 = new Label();
            methodVisitor.visitLabel(label2);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 2, new Object[]{"java/util/List", "java/util/Iterator"}, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 5);
            methodVisitor.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "next", "()Ljava/lang/Object;", true);
            methodVisitor.visitVarInsn(ASTORE, 6);
            methodVisitor.visitVarInsn(ALOAD, 6);
            methodVisitor.visitTypeInsn(CHECKCAST, "java/util/List");
            AsmUtils.enhanceSerializer(listField.getListElementRegistration().serializer()).writeObject(methodVisitor,registration,field,listField.getListElementRegistration());
            methodVisitor.visitVarInsn(ASTORE, 7);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitVarInsn(ALOAD, 7);
            methodVisitor.visitLabel(label1);
            methodVisitor.visitFrame(Opcodes.F_CHOP, 3, null, 0, null);
            methodVisitor.visitVarInsn(ALOAD, 5);
            methodVisitor.visitMethodInsn(INVOKEINTERFACE, "java/util/Iterator", "hasNext", "()Z", true);
            methodVisitor.visitJumpInsn(IFNE, label2);
            methodVisitor.visitVarInsn(ALOAD, 3);

        }*/
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var listField = (ListField) fieldRegistration;
        var arrayName = getListClassName(listField);

        var flag = false;
        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);

        switch (arrayName) {
            case "boolean":
            case "Boolean":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readBooleanList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            case "byte":
            case "Byte":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readByteList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            case "short":
            case "Short":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readShortList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            case "int":
            case "Integer":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            case "long":
            case "Long":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            case "float":
            case "Float":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readFloatList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            case "double":
            case "Double":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readDoubleList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            case "String":
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readStringList", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/List;", false);

                break;
            default:
                if (listField.getListElementRegistration() instanceof ObjectProtocolField) {
                    methodVisitor.visitVarInsn(ALOAD, 0);
                    var fieldProtocolId = ((ObjectProtocolField) listField.getListElementRegistration()).getProtocolId();
                    methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");


                    methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readPacketList", "(Lorg/ricks/protocol/ByteBuf;Lorg/ricks/protocol/registration/IProtocolRegistration;)Ljava/util/List;", false);

                } else{
                    flag = true;
                }
        }

        if(flag) {
//            methodVisitor.visitVarInsn(ALOAD, 1);
//            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readInt", "(Lorg/ricks/protocol/ByteBuf;)I", false);
//            methodVisitor.visitVarInsn(ISTORE, 52);
//            methodVisitor.visitVarInsn(ILOAD, 52);
//            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/collection/CollectionUtils", "newList", "(I)Ljava/util/List;", false);
//            methodVisitor.visitVarInsn(ASTORE, 53);
//            methodVisitor.visitInsn(ICONST_0);
//            methodVisitor.visitVarInsn(ISTORE, 54);
//            Label label7 = new Label();
//            methodVisitor.visitLabel(label7);
//            methodVisitor.visitFrame(Opcodes.F_FULL, 53, new Object[]{"com/zfoo/protocol/registration/ProtocolRegistration100", "io/netty/buffer/ByteBuf", "com/zfoo/protocol/packet/ComplexObject", Opcodes.INTEGER, "java/lang/Byte", "[B", "[Ljava/lang/Byte;", Opcodes.INTEGER, "java/lang/Short", "[S", "[Ljava/lang/Short;", Opcodes.INTEGER, "java/lang/Integer", "[I", "[Ljava/lang/Integer;", Opcodes.LONG, "java/lang/Long", "[J", "[Ljava/lang/Long;", Opcodes.FLOAT, "java/lang/Float", "[F", "[Ljava/lang/Float;", Opcodes.DOUBLE, "java/lang/Double", "[D", "[Ljava/lang/Double;", Opcodes.INTEGER, "java/lang/Boolean", "[Z", "[Ljava/lang/Boolean;", Opcodes.INTEGER, "java/lang/Character", "[C", "[Ljava/lang/Character;", "java/lang/String", "[Ljava/lang/String;", "com/zfoo/protocol/packet/ObjectA", Opcodes.INTEGER, "[Lcom/zfoo/protocol/packet/ObjectA;", Opcodes.INTEGER, Opcodes.TOP, "java/util/List", Opcodes.INTEGER, "java/util/List", Opcodes.INTEGER, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.TOP, Opcodes.INTEGER, "java/util/List", Opcodes.INTEGER}, 0, new Object[]{});
//            methodVisitor.visitVarInsn(ILOAD, 54);
//            methodVisitor.visitVarInsn(ILOAD, 52);
//            Label label8 = new Label();
//            methodVisitor.visitJumpInsn(IF_ICMPGE, label8);
//            AsmUtils.enhanceSerializer(listField.getListElementRegistration().serializer()).readObject(methodVisitor,registration,field,listField.getListElementRegistration());
//
//
//            methodVisitor.visitVarInsn(ASTORE, 55);
//            methodVisitor.visitVarInsn(ALOAD, 53);
//            methodVisitor.visitVarInsn(ALOAD, 55);
//            methodVisitor.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "add", "(Ljava/lang/Object;)Z", true);
//            methodVisitor.visitInsn(POP);
//            methodVisitor.visitIincInsn(54, 1);
//            methodVisitor.visitJumpInsn(GOTO, label7);
//            methodVisitor.visitLabel(label8);
//            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
//            methodVisitor.visitVarInsn(ALOAD, 2);
//            methodVisitor.visitVarInsn(ALOAD, 53);
//            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz, field), "(Ljava/util/List;)V", false);
        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);

            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz, field), "(Ljava/util/List;)V", false);
        }
        return "Ljava/util/List;";
    }

    public String getListClassName(ListField listField) {
        if (listField.getListElementRegistration() instanceof BaseField) {
            return ((Class<?>) ((ParameterizedType) listField.getType()).getActualTypeArguments()[0]).getSimpleName();
        } else {
            return listField.getType().getTypeName();
        }
    }
}
