package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceLongSerializer implements IEnhanceSerializer {
    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();

        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);

        switch (fieldSimpleName) {
            case "long":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()J", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLong", "(Lorg/ricks/protocol/ByteBuf;J)V", false);
                break;
            case "Long":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/lang/Long;", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongBox", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Long;)V", false);
        }
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();
        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);
        String amsReturnStr = "";
        switch (fieldSimpleName) {
            case "long":
                amsReturnStr = "J";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLong", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Long":
                amsReturnStr = "Ljava/lang/Long;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongBox", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);

            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz, field), "("+amsReturnStr+")V", false);
        }
        return amsReturnStr;
    }
}
