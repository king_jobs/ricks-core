package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceFloatSerializer implements IEnhanceSerializer {
    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();

        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        switch (fieldSimpleName) {
            case "float":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()F", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeFloat", "(Lorg/ricks/protocol/ByteBuf;F)V", false);
                break;
            case "Float":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/lang/Float;", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeFloatBox", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Float;)V", false);
        }
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();
        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);
        String amsReturnStr = "";
        switch (fieldSimpleName){
            case "float":
                amsReturnStr = "F";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readFloat", "(Lorg/ricks/protocol/ByteBuf;)"+amsReturnStr, false);
                break;
            case "Float":
                amsReturnStr = "Ljava/lang/Float;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readFloatBox", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);
        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz,field), "("+amsReturnStr+")V", false);
        }
        return amsReturnStr;
    }
}
