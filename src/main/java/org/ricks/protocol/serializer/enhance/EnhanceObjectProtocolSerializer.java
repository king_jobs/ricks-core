package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.registration.field.ObjectProtocolField;
import org.ricks.protocol.utils.EnhanceUtils;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceObjectProtocolSerializer implements IEnhanceSerializer {

    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var objectProtocolField = (ObjectProtocolField) fieldRegistration;
        String fieldPacketClazzName =  field.getType().getCanonicalName().replace(".","/");

        methodVisitor.visitVarInsn(ALOAD, 0);
        methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(objectProtocolField.getProtocolId()), "Lorg/ricks/protocol/registration/IProtocolRegistration;");
        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()L" + fieldPacketClazzName + ";", false);
        methodVisitor.visitTypeInsn(CHECKCAST, "java/lang/Object");
        methodVisitor.visitMethodInsn(INVOKEINTERFACE, "org/ricks/protocol/registration/IProtocolRegistration", "write", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Object;)V", true);
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".", "/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".", "/");
        var objectProtocolField = (ObjectProtocolField) fieldRegistration;

        String fieldPacketClazzName = field.getType().getCanonicalName().replace(".", "/");
        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 0);

        methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(objectProtocolField.getProtocolId()), "Lorg/ricks/protocol/registration/IProtocolRegistration;");
        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitMethodInsn(INVOKEINTERFACE, "org/ricks/protocol/registration/IProtocolRegistration", "read", "(Lorg/ricks/protocol/ByteBuf;)Ljava/lang/Object;", true);
        methodVisitor.visitTypeInsn(CHECKCAST, fieldPacketClazzName);
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz, field), "(L" + fieldPacketClazzName + ";)V", false);
        }
        return "L" + fieldPacketClazzName + ";";
    }
}
