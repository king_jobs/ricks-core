package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.*;
import org.ricks.protocol.serializer.IntSerializer;
import org.ricks.protocol.serializer.LongSerializer;
import org.ricks.protocol.serializer.ObjectProtocolSerializer;
import org.ricks.protocol.serializer.StringSerializer;
import org.ricks.protocol.utils.EnhanceUtils;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;
import static org.ricks.common.asm.Opcodes.INVOKESTATIC;

public class EnhanceMapSerializer implements IEnhanceSerializer {

    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var mapField = (MapField) fieldRegistration;
        var keyRegistration = mapField.getMapKeyRegistration();
        var valueRegistration = mapField.getMapValueRegistration();
        var keySerializer = keyRegistration.serializer();
        var valueSerializer = valueRegistration.serializer();
//        Assert.isTrue(keyRegistration instanceof BaseField,"map字段 key必须是基础数据类型 class - " + packetClazz + " , field - " + field.getName());


//        Label label = new Label();
//        methodVisitor.visitLabel(label);
        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/util/Map;", false);
        if (keySerializer == IntSerializer.INSTANCE) {
            if (valueSerializer == IntSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntIntMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == LongSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntLongMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == StringSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntStringMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == ObjectProtocolSerializer.INSTANCE) {
                methodVisitor.visitVarInsn(ALOAD, 0);
                var fieldProtocolId = ((ObjectProtocolField) mapField.getMapValueRegistration()).getProtocolId();
                methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeIntPacketMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;Lorg/ricks/protocol/registration/IProtocolRegistration;)V", false);
            }
        } else if (keySerializer == LongSerializer.INSTANCE) {
            if (valueSerializer == IntSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongIntMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == LongSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongLongMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == StringSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongStringMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == ObjectProtocolSerializer.INSTANCE) {
                methodVisitor.visitVarInsn(ALOAD, 0);
                var fieldProtocolId = ((ObjectProtocolField) mapField.getMapValueRegistration()).getProtocolId();
                methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");


                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeLongPacketMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;Lorg/ricks/protocol/registration/IProtocolRegistration;)V", false);
            }
        } else if (keySerializer == StringSerializer.INSTANCE) {
            if (valueSerializer == IntSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringIntMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == LongSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringLongMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == StringSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringStringMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;)V", false);
            } else if (valueSerializer == ObjectProtocolSerializer.INSTANCE) {
                methodVisitor.visitVarInsn(ALOAD, 0);
                var fieldProtocolId = ((ObjectProtocolField) mapField.getMapValueRegistration()).getProtocolId();
                methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringPacketMap", "(Lorg/ricks/protocol/ByteBuf;Ljava/util/Map;Lorg/ricks/protocol/registration/IProtocolRegistration;)V", false);
            }
        }
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");

        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var mapField = (MapField) fieldRegistration;
        var keyRegistration = mapField.getMapKeyRegistration();
        var valueRegistration = mapField.getMapValueRegistration();
        var keySerializer = keyRegistration.serializer();
        var valueSerializer = valueRegistration.serializer();

        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);

        if (keySerializer == IntSerializer.INSTANCE) {
            if (valueSerializer == IntSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntIntMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == LongSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntLongMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == StringSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntStringMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == ObjectProtocolSerializer.INSTANCE) {
                methodVisitor.visitVarInsn(ALOAD, 1);
                methodVisitor.visitVarInsn(ALOAD, 0);
                var fieldProtocolId = ((ObjectProtocolField) mapField.getMapValueRegistration()).getProtocolId();

                methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readIntPacketMap", "(Lorg/ricks/protocol/ByteBuf;Lorg/ricks/protocol/registration/IProtocolRegistration;)Ljava/util/Map;", false);
                methodVisitor.visitVarInsn(ASTORE, 5);
                methodVisitor.visitVarInsn(ALOAD, 2);
                methodVisitor.visitVarInsn(ALOAD, 5);


//                if (className.equals("org/ricks/protocol/registration/ProtocolRegistration100") && FieldUtils.fieldToSetMethod(packetClazz, field).equals("setMm")) {
//                    methodVisitor.visitFieldInsn(GETSTATIC, "java/lang/System", "err", "Ljava/io/PrintStream;");
//                    methodVisitor.visitLdcInsn("333");
//                    methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false);
//                }
            }
        } else if (keySerializer == LongSerializer.INSTANCE) {
            if (valueSerializer == IntSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongIntMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == LongSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongLongMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == StringSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongStringMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == ObjectProtocolSerializer.INSTANCE) {
                methodVisitor.visitVarInsn(ALOAD, 0);
                var fieldProtocolId = ((ObjectProtocolField) mapField.getMapValueRegistration()).getProtocolId();
                methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readLongPacketMap", "(Lorg/ricks/protocol/ByteBuf;Lorg/ricks/protocol/registration/IProtocolRegistration;)Ljava/util/Map;", false);
            }
        } else if (keySerializer == StringSerializer.INSTANCE) {
            if (valueSerializer == IntSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readStringIntMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == LongSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readStringLongMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == StringSerializer.INSTANCE) {
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readStringStringMap", "(Lorg/ricks/protocol/ByteBuf;)Ljava/util/Map;", false);

            } else if (valueSerializer == ObjectProtocolSerializer.INSTANCE) {
                methodVisitor.visitVarInsn(ALOAD, 0);
                var fieldProtocolId = ((ObjectProtocolField) mapField.getMapValueRegistration()).getProtocolId();
                methodVisitor.visitFieldInsn(GETFIELD, className, EnhanceUtils.getProtocolRegistrationFieldNameByProtocolId(fieldProtocolId), "Lorg/ricks/protocol/registration/IProtocolRegistration;");

                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeStringPacketMap", "(Lorg/ricks/protocol/ByteBuf;Lorg/ricks/protocol/registration/IProtocolRegistration;)Ljava/util/Map;", false);
            }
        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz, field), "(Ljava/util/Map;)V", false);
        }
        return "Ljava/util/Map;";
    }
}
