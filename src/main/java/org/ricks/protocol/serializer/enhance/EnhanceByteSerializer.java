package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceByteSerializer implements IEnhanceSerializer {

    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();

        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        switch (fieldSimpleName) {
            case "byte":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()B", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeByte", "(Lorg/ricks/protocol/ByteBuf;B)V", false);
                break;
            case "Byte":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/lang/Byte;", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeByteBox", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Byte;)V", false);

        }
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();

        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);
        String amsReturnStr = "";

        switch (fieldSimpleName) {
            case "byte" :
                amsReturnStr = "B";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readByte", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);
                break;
            case "Byte":
                amsReturnStr = "Ljava/lang/Byte;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readByteBox", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz,field), "("+amsReturnStr+")V", false);
        }
        return amsReturnStr;
    }
}
