package org.ricks.protocol.serializer.enhance;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.utils.FieldUtils;

import java.lang.reflect.Field;

import static org.ricks.common.asm.Opcodes.*;

public class EnhanceShortSerializer implements IEnhanceSerializer {
    @Override
    public void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        var fieldSimpleName = field.getType().getSimpleName();

        methodVisitor.visitVarInsn(ALOAD, 1);
        methodVisitor.visitVarInsn(ALOAD, 3);
        switch (fieldSimpleName) {
            case "short":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()S", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeShort", "(Lorg/ricks/protocol/ByteBuf;S)V", false);
                break;
            case "Short":
                methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToGetMethod(packetClazz, field), "()Ljava/lang/Short;", false);
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writeShortBox", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Short;)V", false);
        }
//        Label label = new Label();
//        methodVisitor.visitLabel(label);
    }

    @Override
    public String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration) {
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");
        if(!packetClazz.isRecord()) {
            methodVisitor.visitVarInsn(ALOAD, 2);
        }
        methodVisitor.visitVarInsn(ALOAD, 1);

        var fieldSimpleName = field.getType().getSimpleName();
        String amsReturnStr = "";
        switch (fieldSimpleName) {
            case "short":
                amsReturnStr = "S";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readShort", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

                break;
            case "Short":
                amsReturnStr = "Ljava/lang/Short;";
                methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readShortBox", "(Lorg/ricks/protocol/ByteBuf;)" + amsReturnStr, false);

        }
        if(!packetClazz.isRecord()) {
//            methodVisitor.visitVarInsn(ALOAD, 2);

            methodVisitor.visitMethodInsn(INVOKEVIRTUAL, paketClazzName, FieldUtils.fieldToSetMethod(packetClazz,field), "("+amsReturnStr+")V", false);
        }
        return amsReturnStr;
    }
}
