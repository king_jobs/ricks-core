/*
 * Copyright (C) 2020 The zfoo Authors
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package org.ricks.protocol.serializer;

import org.ricks.protocol.collection.CollectionUtils;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.registration.field.SetField;
import org.ricks.protocol.ByteBuf;

import java.util.Set;


public class SetSerializer implements ISerializer {

    public static final SetSerializer INSTANCE = new SetSerializer();

    @Override
    public void writeObject(ByteBuf buffer, Object object, IFieldRegistration fieldRegistration) {
        if (object == null) {
            buffer.writeInt( 0);
            return;
        }

        Set<?> set = (Set<?>) object;
        SetField setField = (SetField) fieldRegistration;

        int size = set.size();
        if (size == 0) {
            buffer.writeInt( 0);
            return;
        }
        buffer.writeInt( size);

        for (Object element : set) {
            setField.getSetElementRegistration().serializer().writeObject(buffer, element, setField.getSetElementRegistration());
        }
    }

    @Override
    public Object readObject(ByteBuf buffer, IFieldRegistration fieldRegistration) {
        var size = buffer.readInt();
        var setField = (SetField) fieldRegistration;
        Set<Object> set = CollectionUtils.newSet(size);

        for (int i = 0; i < size; i++) {
            Object value = setField.getSetElementRegistration().serializer().readObject(buffer, setField.getSetElementRegistration());
            set.add(value);
        }

        return set;
    }

}
