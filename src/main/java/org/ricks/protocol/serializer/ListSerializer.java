/*
 * Copyright (C) 2020 The zfoo Authors
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package org.ricks.protocol.serializer;

import org.ricks.protocol.collection.CollectionUtils;
import org.ricks.protocol.registration.field.IFieldRegistration;
import org.ricks.protocol.registration.field.ListField;
import org.ricks.protocol.ByteBuf;

import java.util.List;


public class ListSerializer implements ISerializer {

    public static final ListSerializer INSTANCE = new ListSerializer();

    @Override
    public void writeObject(ByteBuf buffer, Object object, IFieldRegistration fieldRegistration) {
        if (object == null) {
            buffer.writeInt( 0);
            return;
        }

        List<?> list = (List<?>) object;
        ListField listField = (ListField) fieldRegistration;

        int size = list.size();
        if (size == 0) {
            buffer.writeInt( 0);
            return;
        }
        buffer.writeInt( size);

        for (Object element : list) {
            listField.getListElementRegistration().serializer().writeObject(buffer, element, listField.getListElementRegistration());
        }
    }

    @Override
    public Object readObject(ByteBuf buffer, IFieldRegistration fieldRegistration) {
        var size = buffer.readInt();
        var listField = (ListField) fieldRegistration;
        List<Object> list = CollectionUtils.newList(size);
        for (int i = 0; i < size; i++) {
            Object value = listField.getListElementRegistration().serializer().readObject(buffer, listField.getListElementRegistration());
            list.add(value);
        }

        return list;
    }
}
