package org.ricks.protocol;

import org.ricks.common.asm.MethodVisitor;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.registration.field.IFieldRegistration;
import java.lang.reflect.Field;

public interface IEnhanceSerializer {


    /**
     * IProtocolRegistration.write(ByteBuf buffer, IPacket packet);
     * $1=buffer
     * $2=packet
     */
    void writeObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration);

    /**
     * IProtocolRegistration.Object read(ByteBuf buffer);
     * $1=buffer
     *
     *  return String Asm返回结果类型
     */
    String readObject(MethodVisitor methodVisitor, ProtocolRegistration registration, Field field, IFieldRegistration fieldRegistration);
}
