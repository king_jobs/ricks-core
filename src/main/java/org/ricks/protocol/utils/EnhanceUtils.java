package org.ricks.protocol.utils;

import org.ricks.protocol.IEnhanceSerializer;
import org.ricks.protocol.registration.ProtocolAnalysis;
import org.ricks.protocol.registration.ProtocolRegistration;
import org.ricks.protocol.serializer.*;
import org.ricks.protocol.serializer.enhance.*;
import org.ricks.common.StrUtil;
import org.ricks.common.asm.*;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;


/**
 * asm 协议字节增强
 */
public class EnhanceUtils implements Opcodes {

    private static Map<ISerializer, IEnhanceSerializer> tempEnhanceSerializerMap = new HashMap<>();


    static {
        tempEnhanceSerializerMap.put(BooleanSerializer.INSTANCE, new EnhanceBooleanSerializer());
        tempEnhanceSerializerMap.put(ByteSerializer.INSTANCE, new EnhanceByteSerializer());
        tempEnhanceSerializerMap.put(ShortSerializer.INSTANCE, new EnhanceShortSerializer());
        tempEnhanceSerializerMap.put(IntSerializer.INSTANCE, new EnhanceIntSerializer());
        tempEnhanceSerializerMap.put(LongSerializer.INSTANCE, new EnhanceLongSerializer());
        tempEnhanceSerializerMap.put(FloatSerializer.INSTANCE, new EnhanceFloatSerializer());
        tempEnhanceSerializerMap.put(DoubleSerializer.INSTANCE, new EnhanceDoubleSerializer());
//        tempEnhanceSerializerMap.put(CharSerializer.INSTANCE, new EnhanceCharSerializer());
        tempEnhanceSerializerMap.put(StringSerializer.INSTANCE, new EnhanceStringSerializer());
        tempEnhanceSerializerMap.put(ObjectProtocolSerializer.INSTANCE, new EnhanceObjectProtocolSerializer());
        tempEnhanceSerializerMap.put(ListSerializer.INSTANCE, new EnhanceListSerializer());
        tempEnhanceSerializerMap.put(SetSerializer.INSTANCE, new EnhanceSetSerializer());
        tempEnhanceSerializerMap.put(MapSerializer.INSTANCE, new EnhanceMapSerializer());
        tempEnhanceSerializerMap.put(ArraySerializer.INSTANCE, new EnhanceArraySerializer());
    }

    public static IEnhanceSerializer enhanceSerializer(ISerializer serializer) {
        return tempEnhanceSerializerMap.get(serializer);
    }

    public static byte[] dump(ProtocolRegistration registration) throws Exception {

        ClassWriter classWriter = new ClassWriter( ClassWriter.COMPUTE_FRAMES);
        FieldVisitor fieldVisitor;
        MethodVisitor methodVisitor;

        var protocolId = registration.getId();
        var className = (ProtocolRegistration.class.getCanonicalName() + protocolId).replace(".","/");
        var simpleClassName = ProtocolRegistration.class.getSimpleName() + protocolId;
        var constructor = registration.getConstructor();
        var packetClazz = constructor.getDeclaringClass();
        var paketClazzName = packetClazz.getCanonicalName().replace(".","/");

        // 定义类所包含的所有子协议成员
        var allSubProtocolIds = ProtocolAnalysis.getAllSubProtocolIds(protocolId)
                .stream()
                .sorted((a, b) -> Short.compare(a, b))
                .collect(Collectors.toList());

        classWriter.visit(V21, ACC_PUBLIC | ACC_SUPER, className, null, "java/lang/Object", new String[]{"org/ricks/protocol/registration/IProtocolRegistration"});

        classWriter.visitSource(simpleClassName + ".java", null);

        {
            fieldVisitor = classWriter.visitField(ACC_PRIVATE, "constructor", "Ljava/lang/reflect/Constructor;", null, null);
            fieldVisitor.visitEnd();
        }

        {
            for (var subProtocolId : allSubProtocolIds) {
                String name = getProtocolRegistrationFieldNameByProtocolId(subProtocolId);
                fieldVisitor = classWriter.visitField(ACC_PRIVATE, name, "Lorg/ricks/protocol/registration/IProtocolRegistration;", null, null);
                fieldVisitor.visitEnd();
            }
        }

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "(Ljava/lang/reflect/Constructor;)V", null, null);
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitFieldInsn(PUTFIELD, className, "constructor", "Ljava/lang/reflect/Constructor;");
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitMaxs(2, 2);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_FINAL, "protocolId", "()S", null, null);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitIntInsn(SIPUSH, protocolId);
            methodVisitor.visitInsn(IRETURN);
            Label label1 = new Label();
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLocalVariable("this", "L"+className+";", null, label0, label1, 0);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_FINAL, "protocolConstructor", "()Ljava/lang/reflect/Constructor;", null, null);
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn(ALOAD, 0);
            methodVisitor.visitFieldInsn(GETFIELD, className, "constructor", "Ljava/lang/reflect/Constructor;");
            methodVisitor.visitInsn(ARETURN);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }

        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_FINAL, "module", "()B", null, null);
            methodVisitor.visitCode();
            methodVisitor.visitInsn(ICONST_0);
            methodVisitor.visitInsn(IRETURN);
            methodVisitor.visitMaxs(1, 1);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_FINAL, "write", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Object;)V", null, null);
            methodVisitor.visitCode();
            methodVisitor.visitVarInsn(ALOAD, 2);
            methodVisitor.visitTypeInsn(CHECKCAST, paketClazzName);
            methodVisitor.visitVarInsn(ASTORE, 3);
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitVarInsn(ALOAD, 3);
            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "writePacketFlag", "(Lorg/ricks/protocol/ByteBuf;Ljava/lang/Object;)Z", false);
            Label label1 = new Label();
            methodVisitor.visitJumpInsn(IFEQ, label1);
            methodVisitor.visitInsn(RETURN);
            methodVisitor.visitLabel(label1);
            methodVisitor.visitFrame(Opcodes.F_APPEND, 1, new Object[]{paketClazzName}, 0, null);

            var fields = registration.getFields();
            var fieldRegistrations = registration.getFieldRegistrations();
            for (int i = 0; i < fields.length; i++) {
                var field = fields[i];
                var fieldRegistration = fieldRegistrations[i];
                enhanceSerializer(fieldRegistration.serializer()).writeObject(methodVisitor,registration,field,fieldRegistration);

            }
            methodVisitor.visitInsn(RETURN);

            methodVisitor.visitMaxs(-1,-1);
            methodVisitor.visitEnd();
        }
        {
            methodVisitor = classWriter.visitMethod(ACC_PUBLIC | ACC_FINAL, "read", "(Lorg/ricks/protocol/ByteBuf;)Ljava/lang/Object;", null, null);
            methodVisitor.visitParameter("var1", 0);
            methodVisitor.visitCode();
            Label label0 = new Label();
            methodVisitor.visitLabel(label0);
            methodVisitor.visitLineNumber(44, label0);
            methodVisitor.visitVarInsn(ALOAD, 1);
            methodVisitor.visitMethodInsn(INVOKESTATIC, "org/ricks/protocol/ByteBufUtils", "readBoolean", "(Lorg/ricks/protocol/ByteBuf;)Z", false);
            Label label1 = new Label();
            methodVisitor.visitJumpInsn(IFNE, label1);
            Label label2 = new Label();
            methodVisitor.visitLabel(label2);
            methodVisitor.visitLineNumber(45, label2);
            methodVisitor.visitInsn(ACONST_NULL);
            methodVisitor.visitInsn(ARETURN);
            methodVisitor.visitLabel(label1);
            methodVisitor.visitLineNumber(47, label1);
            methodVisitor.visitFrame(Opcodes.F_SAME, 0, null, 0, null);

            if(!packetClazz.isRecord()) {
                methodVisitor.visitTypeInsn(NEW, paketClazzName);
                methodVisitor.visitInsn(DUP);
                methodVisitor.visitMethodInsn(INVOKESPECIAL, paketClazzName, "<init>", "()V", false);
                methodVisitor.visitVarInsn(ASTORE, 2);
            }

            var fields = registration.getFields();
            var fieldRegistrations = registration.getFieldRegistrations();
            var fieldNames = ProtocolAnalysis.getFields(packetClazz).stream().map(Field::getName).toList();
            List<String> constructorParam = fieldNames.stream().collect(Collectors.toList());
            var temFields = Arrays.stream(fields).collect(Collectors.toList());
            Map<Integer,Integer> indexList = new HashMap<>();
            int varInsnIndex = 2;
            for (int i = 0; i < fields.length; i++) {
                var field = fields[i];
                var fieldRegistration = fieldRegistrations[i];
                var fieldRegistrationSerializerClazzName = fieldRegistration.serializer().getClass().getCanonicalName().replace(".","/");
                var fieldRegistrationClazzName  = fieldRegistration.getClass().getCanonicalName().replace(".","/");

                var readObject = enhanceSerializer(fieldRegistration.serializer()).readObject(methodVisitor,registration,field,fieldRegistration);
                if(packetClazz.isRecord()) {
                    int index = fieldNames.indexOf(field.getName());
                    constructorParam.set(index, readObject);
                    temFields.set(index,field);
                    indexList.put(index, varInsnIndex);
                    var code = store(field);
//                    System.err.println(field.getType() + " index;" + varInsnIndex);
                    methodVisitor.visitVarInsn(code, varInsnIndex);
                    varInsnIndex = varIndex(field,varInsnIndex);
                }
            }
            if(packetClazz.isRecord()) {
                methodVisitor.visitTypeInsn(NEW, paketClazzName);
                methodVisitor.visitInsn(DUP);
                //如果没有上面两行代码 会报弹出空栈
//                System.err.println(constructorParam.stream().collect(Collectors.joining()));
                for (int i = 0; i < fieldNames.size(); i++) {
                    int index = fieldNames.indexOf(fieldNames.get(i));
                    Field field = temFields.get(index);
                    int varIndex = indexList.get(index);
//                    System.err.println(field.getType() + " index;" + varIndex);
                    methodVisitor.visitVarInsn(load(field), varIndex);
                }
                methodVisitor.visitMethodInsn(INVOKESPECIAL, paketClazzName, "<init>", "("+ constructorParam.stream().collect(Collectors.joining()) +")V", false);
                methodVisitor.visitInsn(ARETURN);
            } else {
                methodVisitor.visitVarInsn(ALOAD, 2);
                methodVisitor.visitInsn(ARETURN);
            }
            methodVisitor.visitMaxs(-1, -1);
            methodVisitor.visitEnd();
        }
        classWriter.visitEnd();

        return classWriter.toByteArray();
    }

    public static String getProtocolRegistrationFieldNameByProtocolId(short id) {
        return StrUtil.format("{}{}", StrUtil.uncapitalize(ProtocolRegistration.class.getSimpleName()), id);
    }

    public static String getProtocolRegistrationClassNameByProtocolId(short id) {
        return StrUtil.format("{}{}", StrUtil.uncapitalize(ProtocolRegistration.class.getCanonicalName()), id);
    }

    /**
     * javassist字节增加本质就是操作字符串，而Asm不同 asm有约定
     *
     * @param field
     * @return
     */
    public static int store(Field field) {
        var fieldSimpleName = field.getType().getSimpleName();
        switch (fieldSimpleName) {
            case "byte" :
            case "short" :
            case "int":
            case "char":
            case "boolean":
                return ISTORE;
            case "long" : return LSTORE;
            case "float" : return FSTORE;
            case "double" : return DSTORE;
            default: return ASTORE;
        }
    }


    public static int varIndex(Field field,int varIndex) {
        var fieldSimpleName = field.getType().getSimpleName();
        switch (fieldSimpleName) {
            case "long" :
            case "double" : return varIndex + 2;
            default: return varIndex + 1;
        }
    }

    public static int load(Field field){
        var fieldSimpleName = field.getType().getSimpleName();
        switch (fieldSimpleName) {
            case "byte" :
            case "short" :
            case "int":
            case "char":
            case "boolean":
                return ILOAD;
            case "long" : return LLOAD;
            case "float" : return FLOAD;
            case "double" : return DLOAD;
            default: return ALOAD;
        }
    }
}
