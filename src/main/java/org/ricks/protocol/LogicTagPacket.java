package org.ricks.protocol;

/**
 * 逻辑节点 -> 网关节点 同步tag标识
 */
public class LogicTagPacket implements IPacket{

    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
