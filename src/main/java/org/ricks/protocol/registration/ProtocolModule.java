

package org.ricks.protocol.registration;

import org.ricks.common.StrUtil;

/**
 * 模块id和模块名的封装
 *
 * @author godotg
 * @version 3.0
 */
public class ProtocolModule {

    public static final ProtocolModule DEFAULT_PROTOCOL_MODULE = new ProtocolModule((byte) 0, "default");

    private byte id;

    private String name;

    public ProtocolModule(byte id, String name) {
        if (id < 0) {
            throw new IllegalArgumentException(StrUtil.format("模块[{}]的id[{}]必须大于0", name, id));
        }

        this.id = id;
        this.name = name;
    }


    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProtocolModule module = (ProtocolModule) o;
        return id == module.id;
    }

    @Override
    public int hashCode() {
        return this.id;
    }

    @Override
    public String toString() {
        return StrUtil.format("[id:{}][name:{}]", id, name);
    }
}
