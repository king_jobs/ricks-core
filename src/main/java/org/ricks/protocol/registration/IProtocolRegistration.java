package org.ricks.protocol.registration;

import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.IPacket;
import java.lang.reflect.Constructor;


public interface IProtocolRegistration {

    short protocolId();

    byte module();

    Constructor<?> protocolConstructor();


    /**
     * 序列化
     */
    void write(ByteBuf buffer, Object packet);

    /**
     * 反序列化
     */
    Object read(ByteBuf buffer);

}
