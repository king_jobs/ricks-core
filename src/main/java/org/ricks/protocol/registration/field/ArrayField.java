

package org.ricks.protocol.registration.field;


import org.ricks.protocol.serializer.ArraySerializer;
import org.ricks.protocol.serializer.ISerializer;


public class ArrayField implements IFieldRegistration {

    private IFieldRegistration arrayElementRegistration;
    private Class<?> type;

    public static ArrayField valueOf(IFieldRegistration arrayElementRegistration, Class<?> type) {
        ArrayField arrayField = new ArrayField();
        arrayField.arrayElementRegistration = arrayElementRegistration;
        arrayField.type = type;
        return arrayField;
    }

    public Class<?> getType() {
        return type;
    }

    @Override
    public ISerializer serializer() {
        return ArraySerializer.INSTANCE;
    }

    public IFieldRegistration getArrayElementRegistration() {
        return arrayElementRegistration;
    }
}

