

package org.ricks.protocol.registration.field;


import org.ricks.protocol.serializer.ISerializer;

/**
 * 标记性接口，所有协议里描述变量都要实现这个接口
 *
 * @author godotg
 * @version 3.0
 */
public interface IFieldRegistration {

    ISerializer serializer();

}
