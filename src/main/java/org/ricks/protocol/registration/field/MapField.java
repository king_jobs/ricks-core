

package org.ricks.protocol.registration.field;

import org.ricks.protocol.serializer.ISerializer;
import org.ricks.protocol.serializer.MapSerializer;

import java.lang.reflect.Type;


public class MapField implements IFieldRegistration {

    private IFieldRegistration mapKeyRegistration;
    private IFieldRegistration mapValueRegistration;

    private Type type;

    public static MapField valueOf(IFieldRegistration mapKeyRegistration, IFieldRegistration mapValueRegistration, Type type) {
        MapField mapField = new MapField();
        mapField.mapKeyRegistration = mapKeyRegistration;
        mapField.mapValueRegistration = mapValueRegistration;
        mapField.type = type;
        return mapField;
    }


    @Override
    public ISerializer serializer() {
        return MapSerializer.INSTANCE;
    }

    public IFieldRegistration getMapKeyRegistration() {
        return mapKeyRegistration;
    }

    public IFieldRegistration getMapValueRegistration() {
        return mapValueRegistration;
    }

    public Type getType() {
        return type;
    }
}
