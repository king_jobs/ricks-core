

package org.ricks.protocol.registration.field;

import org.ricks.protocol.serializer.ISerializer;
import org.ricks.protocol.serializer.ObjectProtocolSerializer;


public class ObjectProtocolField implements IFieldRegistration {

    /**
     * 协议序列号是ProtocolRegistration的id
     */
    private short protocolId;

    public static ObjectProtocolField valueOf(short protocolId) {
        ObjectProtocolField objectProtocolField = new ObjectProtocolField();
        objectProtocolField.protocolId = protocolId;
        return objectProtocolField;
    }

    public short getProtocolId() {
        return protocolId;
    }

    @Override
    public ISerializer serializer() {
        return ObjectProtocolSerializer.INSTANCE;
    }
}
