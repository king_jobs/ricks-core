

package org.ricks.protocol.registration.field;


import org.ricks.protocol.serializer.ISerializer;

/**
 * 一个包里所包含的变量还有这个变量的序列化器
 * 描述boolean，byte，short，int，long，float，double，char，String等基本序列化器
 *
 * @author godotg
 * @version 3.0
 */
public class BaseField implements IFieldRegistration {

    private ISerializer serializer;

    public static BaseField valueOf(ISerializer serializer) {
        BaseField packetField = new BaseField();
        packetField.serializer = serializer;
        return packetField;
    }

    @Override
    public ISerializer serializer() {
        return serializer;
    }

}
