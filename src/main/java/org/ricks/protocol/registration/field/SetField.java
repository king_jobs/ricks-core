

package org.ricks.protocol.registration.field;

import org.ricks.protocol.serializer.ISerializer;
import org.ricks.protocol.serializer.SetSerializer;

import java.lang.reflect.Type;


public class SetField implements IFieldRegistration {

    private IFieldRegistration setElementRegistration;
    private Type type;

    public static SetField valueOf(IFieldRegistration listElementRegistration, Type type) {
        SetField setField = new SetField();
        setField.setElementRegistration = listElementRegistration;
        setField.type = type;
        return setField;
    }

    @Override
    public ISerializer serializer() {
        return SetSerializer.INSTANCE;
    }

    public IFieldRegistration getSetElementRegistration() {
        return setElementRegistration;
    }

    public Type getType() {
        return type;
    }

}
