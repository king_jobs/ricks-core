

package org.ricks.protocol.registration.field;

import org.ricks.protocol.serializer.ISerializer;
import org.ricks.protocol.serializer.ListSerializer;

import java.lang.reflect.Type;


public class ListField implements IFieldRegistration {

    private IFieldRegistration listElementRegistration;
    private Type type;

    public static ListField valueOf(IFieldRegistration listElementRegistration, Type type) {
        ListField listField = new ListField();
        listField.listElementRegistration = listElementRegistration;
        listField.type = type;
        return listField;
    }

    @Override
    public ISerializer serializer() {
        return ListSerializer.INSTANCE;
    }

    public IFieldRegistration getListElementRegistration() {
        return listElementRegistration;
    }

    public Type getType() {
        return this.type;
    }

}
