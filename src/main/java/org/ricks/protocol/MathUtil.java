package org.ricks.protocol;

public class MathUtil {

    public static final byte FREE = 0;
    public static final byte REMOVED = 1;
    public static final byte FILLED = 2;

    public static int findNextPositivePowerOfTwo(final int value) {
        assert value > Integer.MIN_VALUE && value < 0x40000000;
        return 1 << (32 - Integer.numberOfLeadingZeros(value - 1));
    }

    public static int safeFindNextPositivePowerOfTwo(final int value) {
        return value <= 0 ? 1 : value >= 0x40000000 ? 0x40000000 : findNextPositivePowerOfTwo(value);
    }

    public static int calcMaxSize(int capacity) {
        // Clip the upper bound so that there will always be at least one available slot.
        int upperBound = capacity - 1;
        return Math.min(upperBound, (int) (capacity * 0.5f));
    }

    /**
     * Get the next sequential index after index and wraps if necessary.
     */
    public static int probeNext(int index, int mask) {
        // The array lengths are always a power of two, so we can use a bitmask to stay inside the array bound
        return (index + 1) & mask;
    }
}
