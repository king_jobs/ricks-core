package org.ricks.protocol;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Objects;

public class HashSetShort extends AbstractSet<Short> {

    private final ShortObjectHashMap<Boolean> map;

    public HashSetShort() {
        map = new ShortObjectHashMap<>();
    }

    public HashSetShort(int initialCapacity) {
        map = new ShortObjectHashMap<>(initialCapacity);
    }

    @Override
    public Iterator<Short> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    @Override
    public boolean add(Short e) {
        return map.put(e, Boolean.TRUE) == null;
    }

    public boolean add(short e) {
        return map.put(e, Boolean.TRUE) == null;
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) != null;
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        var obj = (HashSetShort) o;
        return Objects.equals(map, obj.map);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(map);
    }
}

