package org.ricks.protocol;

import org.ricks.common.lang.Assert;
import org.ricks.protocol.collection.HashMapIntShort;
import org.ricks.protocol.registration.IProtocolRegistration;
import org.ricks.protocol.registration.ProtocolAnalysis;
import org.ricks.protocol.registration.ProtocolModule;

import java.util.*;

/**
 *  Asm 实例化对象 & Asm调用get set 函数，来替代反射
 *  Asm 字节增加 替代 javassist字节增强
 *  序列化字节偏大，猜测 int long string short 压缩算法
 *
 *  一步一步的来实现，每一步梳理清楚思绪
 *
 *  集群节点探测 目前架构是依赖redis ,理想状态 架构层面剥离redis ,那就意味要做 无中心化集群  gossip协议还是首选。
 *  如果架构中集入gossip协议 ，启动业务节点 只需要连接一个 网关节点，然后依赖 gossip协议 通知业务节点 其它存在的网关节点。再进行连接
 *
 */
public class ProtocolManager {

    public static final String PROTOCOL_ID = "PROTOCOL_ID";
    public static final String PROTOCOL_METHOD = "protocolId";
    public static final short MAX_PROTOCOL_NUM = Short.MAX_VALUE;
    public static final byte MAX_MODULE_NUM = Byte.MAX_VALUE;

    /**
     * The protocol corresponding to the protocolId.(协议号protocolId对应的协议，数组下标是协议号protocolId)
     */
    public static final IProtocolRegistration[] protocols = new IProtocolRegistration[MAX_PROTOCOL_NUM];
    /**
     * The modules of the protocol.(协议的模块)
     */
    public static final ProtocolModule[] modules = new ProtocolModule[MAX_MODULE_NUM];

    /**
     * key:packet class，value:protocolId.(如果所有协议Class返回的hashcode都不相同（大概率事件），则使用高性能的HashMapIntShort)
     */
    public static Map<Class<?>, Short> protocolIdMap = new HashMap<>();
    public static HashMapIntShort protocolIdPrimitiveMap = new HashMapIntShort();

    static {
        // default protocol module
        modules[0] = ProtocolModule.DEFAULT_PROTOCOL_MODULE;
    }

    /**
     * serialize the packet into the buffer
     */
    public static void write(ByteBuf buffer, Object packet) {
        var protocolId = protocolId(packet.getClass());
        // write the protocolId
        buffer.writeShort( protocolId);
        // write the package
        protocols[protocolId].write(buffer, packet);
    }

    /**
     * deserialization a packet from the buffer
     */
    public static Object read(ByteBuf buffer) {
        return  protocols[buffer.readShort()].read(buffer);
    }

    public static IProtocolRegistration getProtocol(short protocolId) {
        return protocols[protocolId];
    }

    public static ProtocolModule moduleByProtocolId(short id) {
        return modules[protocols[id].module()];
    }

    /**
     * Find the module based on the module ID
     */
    public static ProtocolModule moduleByModuleId(byte moduleId) {
        var module = modules[moduleId];
        Assert.notNull(module, "[moduleId:{}]不存在", moduleId);
        return module;
    }

    /**
     * Find modules by module name
     */
    public static ProtocolModule moduleByModuleName(String name) {
        var moduleOptional = Arrays.stream(modules)
                .filter(Objects::nonNull)
                .filter(it -> it.getName().equals(name))
                .findFirst();
        if (moduleOptional.isEmpty()) {
            return null;
        }
        return moduleOptional.get();
    }

    public static short protocolId(Class<?> clazz) {
        return protocolIdMap == null ? protocolIdPrimitiveMap.getPrimitive(clazz.hashCode()) : protocolIdMap.get(clazz);
    }

    public static void initProtocol(Set<Class<?>> protocolClassSet) {
        ProtocolAnalysis.analyze(protocolClassSet);
    }

    /**
     * EN:Register protocol and automatically generates a protocol ID if the subprotocol does not specify a protocol ID
     * CN:子协议会自动注册协议号protocolId，如果子协议没有指定protocolId则自动生成protocolId
     */
    public static void initProtocolAuto(Set<Class<?>> protocolClassSet) {
        ProtocolAnalysis.analyzeAuto(protocolClassSet);
    }

}
