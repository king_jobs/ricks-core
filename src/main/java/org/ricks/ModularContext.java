package org.ricks;


import org.ricks.common.lang.Logger;
import org.ricks.common.PackageUtils;
import java.util.Arrays;
import java.util.Set;

/**
 * 模块上下文，扫描包统一维护
 */
public class ModularContext {

    private final Set<Class<?>> classes;


    /**
    private Set<Destroy> destroyClasses;

    /**
     * 默认扫描 org.ricks 包下所有 class
     * project启动必修扫描目录
     */
    private ModularContext() {
        classes = PackageUtils.getClasses("org.ricks");
    }

    public static ModularContext me() {
        return ModularContext.Holder.ME;
    }

    /** 通过 JVM 的类加载机制, 保证只加载一次 (singleton) */
    private static class Holder {
        static final ModularContext ME = new ModularContext();
    }

    /**
     * 扫描包，不同模块使用 IOC ORM PROTOCO
     * @param scanBasePackages
     */
    public void scan(Set<String> scanBasePackages) {
        scanBasePackages.forEach(packageName -> classes.addAll(PackageUtils.getClasses(packageName)));
        Logger.info("包名"+Arrays.toString(scanBasePackages.toArray()) + " 扫描成功，共扫描class[" + classes.size() + "]");
    }

    public Set<Class<?>> projectClasses() {
        return classes;
    }


    /**
     * 模块进行扫描
     * @param args
     */
    public static void main(String[] args) {
        ModularContext.me().scan(Set.of("org.ricks","app"));
    }
}
