package org.ricks.common.event;

/**
 * 定时定点的事件包裹类.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.3.9
 */
final class FixedTimeEventWrapper extends AbstractDelayEvent {
    /**
     * 事件源
     */
    private final FixedTimeEvent source;

    public FixedTimeEventWrapper(FixedTimeEvent source) {
        this.source = source;
    }

    public FixedTimeEvent getSource() {
        return source;
    }
}