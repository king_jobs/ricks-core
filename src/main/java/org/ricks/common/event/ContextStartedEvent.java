package org.ricks.common.event;

public class ContextStartedEvent extends ApplicationContextEvent{
    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public ContextStartedEvent(Object source) {
        super(source);
    }
}
