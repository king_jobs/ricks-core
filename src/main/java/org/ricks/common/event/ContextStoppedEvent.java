package org.ricks.common.event;

public class ContextStoppedEvent extends ApplicationContextEvent{
    /**
     * Constructs a prototypical Event.
     *
     * @param source the object on which the Event initially occurred
     * @throws IllegalArgumentException if source is null
     */
    public ContextStoppedEvent(Object source) {
        super(source);
    }
}
