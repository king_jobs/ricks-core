package org.ricks.common.event;

import org.ricks.common.RandomUtil;

/**
 * @author ricks
 * 如果是业务层面，一般都是role actor  roleId % threadNum ，继续网络事件 actor执行
 * 如果想使用其它actor执行，只能自定义
 * @date 2022/9/2617:34
 */
public interface Event {

    /**
     * 这个返回的是一个用于确定事件在EventBus中的哪个线程池的执行的一个参数，只有异步事件才会有作用
     * <p>
     * 比如cpu是四核，那么EventBus中的executors线程池的数量为8个，通过取余可以确定异步事件在哪个线程池中执行。
     * 如果参数是0，0 % 8 = 0，那么异步事件最终会在executors[0]表示的线程池中执行
     * 如果参数是1，1 % 8 = 1，那么异步事件最终会在executors[1]表示的线程池中执行
     * 如果参数是8，8 % 8 = 0，那么异步事件最终会在executors[0]表示的线程池中执行
     * 如果参数是9，9 % 8 = 1，那么异步事件最终会在executors[1]表示的线程池中执行
     * <p>
     * 通过返回的参数，可以轻松控制异步事件在哪个线程池中去执行。
     * 因为EventBus中的线程池都是单线程线程池，如果将一些异步事件放在同一个线程池中执行，可以不用加锁，提高程序运行的效率。
     * <p>
     * 默认返回一个随机数，这就导致如果不重写这个方法，那么异步事件有可能会在EventBus中的任何一条线程池中去执行。
     *
     * @return 线程池的执行的一个参数
     */
    default long threadId() {
        return RandomUtil.randomInt();
    }

    /**
     * 默认actor ACTORS[threadId]
     * 业务actor ActorMgr.get().get(threadId)
     *
     * threadId 一般是业务的roleId ,也可以自定义 threadId
     * @param threadId
     * @return
     */
}
