package org.ricks.common.event;

/**
 * 事件管理器.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public interface EventManager {
    /**
     * 发布一个容器事件.
     *
     * @param event 事件源
     */
    void publish(ApplicationEvent event);
    /**
     * 发布一个事件.
     *
     * @param event 事件源
     */
    void publish(Event event);

    /**
     * 发布一个延迟事件.
     * <p>
     * 一定要重新HashCode和equals
     *
     * @param event 事件源
     */
    void publish(DelayEvent event);

    /**
     * 发布一个定时任务事件.
     *
     * @param event 事件源
     */
    void publish(FixedTimeEvent event);

    /**
     * 从事件管理器中移除最先执行的那个延迟事件（无论它是否过期）.
     * <p>
     * 注：<br>
     * 1.一定要重新HashCode和equals<br>
     * 2.只会删除一个事件，正常事件管理器里也不会有两个一样的事件，不是吗？<br>
     *
     * @param event 事件源
     * @return 如果移除成功则返回true, 否则返回false.
     */
    boolean remove(DelayEvent event);

    /**
     * 从事件管理器中移除指定的延迟事件（无论它是否过期）.
     * <p>
     * 注：<br>
     * 1.一定要重新HashCode和equals<br>
     * 2.移除所有指定的延迟事件，就是一个事件可能多次添加<br>
     * 3.用于重用事件，但没移除就又加进去了，建议有增有减，有序执行，理不清就是给将来维护留坑啊
     *
     * @param event 事件源
     * @return 如果移除成功则返回true, 否则返回false.
     */
    boolean removeAll(DelayEvent event);
}