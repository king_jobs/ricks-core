package org.ricks.common.event;

import org.ricks.common.lang.Logger;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.TimeUnit;

/**
 * 延迟事件处理线程.
 */
class DelayEventThread extends Thread {
    private static final int SHUTDOWN_TIMEOUT = 1;
    static final DelayQueue<DelayEvent> QUEUE = new DelayQueue<>();
    private final EventBus eventManager;
    private volatile boolean starting = true;

    public DelayEventThread(EventBus eventManager) {
        super("delay-event");
        this.eventManager = eventManager;
    }

    @Override
    public void run() {
        Logger.info("延迟任务调度线程开始啦...");
        while (starting) {
            try {
                DelayEvent event = QUEUE.take();
                // 停止事件...
                if (event instanceof ShutdownEvent) {
                    Logger.info("延迟任务调度线程停止啦...");
                    this.starting = false;
                    ((ShutdownEvent) event).countDown();
                }

                // 延迟任务...
                else if (event instanceof ScheduledEvent) {
                    eventManager.notifyScheduledHandler((ScheduledEvent) event);
                }

                // 定时任务
                else if (event instanceof FixedTimeEventWrapper fixedTimeEvent) {
                    eventManager.notifyFixedTimeEventHandler(fixedTimeEvent);
                }

                // 延迟事件
                else {
                    eventManager.notifyListeners(event);
                }
            } catch (Throwable e) {
                Logger.error("调度线程异常", e);
            }
        }
    }

    public boolean addDelayEvent(DelayEvent event) {
        return QUEUE.add(event);
    }

    public boolean remove(DelayEvent event) {
        return QUEUE.remove(event);
    }

    public boolean removeAll(DelayEvent event) {
        return QUEUE.removeIf(v -> v.equals(event));
    }

    /**
     * 停止执行延迟事件.
     * 如果没有启动，调用事件停止会阻塞一分钟
     */
    public void shutdown() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        this.addDelayEvent(new ShutdownEvent(countDownLatch));
        try {
            countDownLatch.await(SHUTDOWN_TIMEOUT, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            Logger.warn("shutdown event exec exception. {}", e);
        }
    }
}