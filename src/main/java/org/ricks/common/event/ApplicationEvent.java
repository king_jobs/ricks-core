package org.ricks.common.event;

public abstract class ApplicationEvent implements Event{

    protected transient Object source;
    public ApplicationEvent(Object source) {
        this.source = source;
    }
}
