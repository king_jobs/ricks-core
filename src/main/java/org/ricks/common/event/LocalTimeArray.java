
package org.ricks.common.event;

import org.ricks.common.DateUtils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * LocalTime数组.
 * <p>
 * 主要用于策划想定时触发指定功能时好配置，如:
 *
 * <pre>
 * 08:00:00,12:00:00,18:00:00<br>
 * 08:00,12:00,18:00<br>
 * </pre>
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.3.9
 */
public class LocalTimeArray {
    /**
     * 每天最大的秒数
     */
    private static final int MAX_SECOND_BY_DAY = 24 * 60 * 60 - 1;

    private final LocalTime[] array;

    public LocalTimeArray(LocalTime[] array) {
        this.array = array;
    }

    public LocalTime[] getArray() {
        return array;
    }

    /**
     * 以当前时间算，计算出下次触发时间
     *
     * @return 下次触发时间
     */
    public Date doNext() {
        return doNext(LocalTime.now());
    }

    /**
     * 以指定时间算，计算出下次触发时间
     *
     * @param now 指定时间
     * @return 下次触发时间
     */
    public Date doNext(LocalTime now) {
        return doNext(null, now);
    }

    /**
     * 以当前时间算，计算出指定开始日期后第一次触发时间
     *
     * @param start 指定开始日期
     * @return 下次触发时间
     */
    public Date doNext(LocalDate start) {
        return doNext(start, LocalTime.now());
    }

    /**
     * 以指定时间算，计算出下次触发时间
     *
     * @param start 指定开始日期
     * @param now   指定时间
     * @return 下次触发时间
     */
    public Date doNext(LocalDate start, LocalTime now) {
        // 有指定开始日期且在今天之后
        boolean flag = start != null && start.isAfter(LocalDate.now());

        // 计算出来最小的那个时间
        int minSecond = MAX_SECOND_BY_DAY;
        int nextSecond = MAX_SECOND_BY_DAY;
        final int todaySecond = now.toSecondOfDay();
        for (LocalTime time : array) {
            int targetSecond = time.toSecondOfDay();
            minSecond = Math.min(minSecond, targetSecond);

            if (!flag && targetSecond > todaySecond) {
                nextSecond = Math.min(nextSecond, targetSecond - todaySecond);
            }
        }

        // 还是初始值，那就计算过天的下个时间
        if (nextSecond == MAX_SECOND_BY_DAY) {
            nextSecond = MAX_SECOND_BY_DAY + 1 - todaySecond + minSecond;
        }

        Calendar calendar = Calendar.getInstance();

        // 对日期有要求的
        if (flag) {
            calendar.set(Calendar.YEAR, start.getYear());
            calendar.set(Calendar.MONTH, start.getMonthValue() - 1);
            calendar.set(Calendar.DAY_OF_MONTH, start.getDayOfMonth() - 1);
        }

        calendar.set(Calendar.HOUR_OF_DAY, now.getHour());
        calendar.set(Calendar.MINUTE, now.getMinute());
        calendar.set(Calendar.SECOND, now.getSecond());
        calendar.add(Calendar.SECOND, nextSecond);
        return calendar.getTime();
    }

    /**
     * 计算从上次触发时间到当前时间，已触发了多少次了.
     *
     * @param lastTriggerTime 上次触发时间
     * @param now             当前时间
     * @return 返回已触犯的次数，最小值为0
     */
    public int triggerTimes(Date lastTriggerTime, Date now) {
        // 如果刷新时间比当前时间还大，那就直接返回0
        if (lastTriggerTime.getTime() >= now.getTime()) {
            return 0;
        }

        final int lastTriggerSecond = DateUtils.toLocalTime(lastTriggerTime).toSecondOfDay();
        final int nowSecond = DateUtils.toLocalTime(now).toSecondOfDay();
        final long days = DateUtils.diffDays(now, lastTriggerTime);

        int result = 0;

        for (LocalTime time : array) {
            int targetSecond = time.toSecondOfDay();
            // 上次触发时间不是今天
            if (days > 0) {
                // 今天触发的次数
                if (targetSecond <= nowSecond) {
                    result++;
                }
                // 最前面那天的次数
                if (lastTriggerSecond < targetSecond) {
                    result++;
                }
            }
            // 上次触发时间是今天
            else if (lastTriggerSecond < targetSecond && targetSecond <= nowSecond) {
                result++;
            }
        }

        // 中间的天数要算全部
        if (days > 1) {
            result += (days - 1) * array.length;
        }
        return result;
    }

    /**
     * 判定指定时间是不是在这个区间之内.
     *
     * @param localTime 指定时间
     * @return 如果在这个区间内返回true，否则返回false
     */
    public boolean inSection(LocalTime localTime) {
        if (array.length != 2) {
            throw new RuntimeException("只有配置两个时间，还可以使用此方法" + this);
        }

        long startTime = array[0].toSecondOfDay();
        long endTime = array[1].toSecondOfDay();
        long now = localTime.toSecondOfDay();
        return startTime <= now && now <= endTime;
    }

    @Override
    public String toString() {
        return "LocalTimeArray [array=" + Arrays.toString(array) + "]";
    }
}