
package org.ricks.common.event;


/**
 * 定时任务事件接口.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.3.9
 */
public interface FixedTimeEvent extends Event {
    /**
     * 获取定时触发的时间配置
     *
     * @return 定时触发的时间配置
     */
    public LocalTimeArray getTrigger();
}