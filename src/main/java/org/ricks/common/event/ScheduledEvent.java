
package org.ricks.common.event;

/**
 * 延迟任务事件.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.2.6
 */
public final class ScheduledEvent extends AbstractDelayEvent {
}