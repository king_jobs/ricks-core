package org.ricks.common.event;

import java.util.Date;
import java.util.concurrent.Delayed;

/**
 * 延迟事件.
 * <p>
 * 在事件的基础上添加一个延迟执行的功能.<br>
 * <b>一定要重新HashCode和equals</b>
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public interface DelayEvent extends Event, Delayed {

    /**
     * 获取当前延迟事件的结束时间.
     *
     * @return 结束时间
     */
    Date getEndTime();
}