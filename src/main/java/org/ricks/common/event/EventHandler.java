package org.ricks.common.event;

import org.ricks.ioc.RicksMethod;

import java.lang.annotation.*;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:事件处理者
 * @date 2021/8/514:06
 */
@Documented
@RicksMethod
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventHandler {

    /**
     * 是否异步执行
     * <p>
     * 默认就是异步执行的，当需要同步时可以设计false<br>
     * @return 如果为true异步执行, 否则同步执行
     */
    boolean async() default true;
}