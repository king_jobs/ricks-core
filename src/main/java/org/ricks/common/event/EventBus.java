package org.ricks.common.event;

import org.ricks.ModularContext;
import org.ricks.common.actor.Actor;
import org.ricks.common.actor.Actors;
import org.ricks.common.lang.Assert;
import org.ricks.ioc.manager.EventMethodManager;
import org.ricks.ioc.manager.ScheduledMethodManager;
import org.ricks.ioc.wrap.EventMethodWrapper;
import org.ricks.ioc.wrap.ScheduledMethodWrapper;
import org.ricks.common.lang.Logger;
import org.ricks.common.ClassUtil;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * 一个提供延迟执行事件功能的实现类.
 *
 */
public class EventBus implements EventManager {
    private final DelayEventThread handler = new DelayEventThread(this);

    /**
     * Application 监听器
     * 1。注册监听，扫描实现ApplicationListener class，加入容器 完成注册
     * 2.事件发布，执行 onApplicationEvent。
     */
    private final Set<ApplicationListener<?>> applicationListeners = new LinkedHashSet<>();

    private static final EventBus INSTANCE = new EventBus();

    private EventBus(){}

    public static EventBus me() {
        return INSTANCE;
    }

    public void init() {
        handler.start();
        this.initScheduled();
//        this.initApplicationEvent();
    }

    /**
     * 初始化 Application 事件机制
     */
    public void initApplicationEvent() {
        Set<Class<?>> classes = ModularContext.me().projectClasses();
        for (Class<?> klass: classes) {
            if(!ClassUtil.isAssignable(ApplicationListener.class,klass)) {
                continue;
            }
            ApplicationListener applicationListener = (ApplicationListener) ClassUtil.newInstance(klass);
            applicationListeners.add(applicationListener);
        }
    }

    /**
     * 延迟任务初始化
     */
    private void initScheduled() {
        for (ScheduledMethodWrapper sch : ScheduledMethodManager.get().getHandlers()) {
            ScheduledEvent event = new ScheduledEvent();
            event.setId(sch.getId());
            event.setEndTime(sch.nextExecutionTime());
            this.publish(event);
        }
    }

    public void destroy() {
        handler.shutdown();
    }

    @Override
    public void publish(Event event) {
        this.notifyListeners(event);
    }

    @Override
    public void publish(ApplicationEvent event) {
        this.notifyListeners(event);
    }

    @Override
    public void publish(DelayEvent event) {
        // 未配置这个结束时间，会死人的....
        Assert.isNotNull("未配置延迟事件的结束时间. class=" + event.getClass().getName());
        handler.addDelayEvent(event);
    }

    @Override
    public boolean remove(DelayEvent event) {
        return handler.remove(event);
    }

    @Override
    public boolean removeAll(DelayEvent event) {
        return handler.removeAll(event);
    }

    @Override
    public void publish(FixedTimeEvent event) {
        // 未配置触发时间，会死人的....
        Assert.isNotNull("未配置定时任务的触发时间. class=" + event.getClass().getName());
        this.resetEndTimeAndPublish(new FixedTimeEventWrapper(event));
    }

    /**
     * 修正时间，进行下一次事件发布...
     *
     * @param event 定时任务包裹对象
     */
    private void resetEndTimeAndPublish(FixedTimeEventWrapper event) {
        event.setEndTime(event.getSource().getTrigger().doNext());
        this.publish(event);
    }

    /**
     * 通知监听器.
     *
     * @param event 事件源
     */
    void notifyListeners(Event event) {
        List<EventMethodWrapper> handlers = EventMethodManager.me().getEventMethodWrappers(event.getClass());
        if (handlers.isEmpty()) {
            Logger.warn("No subscription event. class= "+ event.getClass());
            return;
        }

        for (EventMethodWrapper handler : handlers) {
            try {
                // 异步执行，投递进线程池中派发
                if (handler.isAsync()) {
                    Actor actor = Actors.me().getEventActor(event.threadId());
                    actor.execute("event",() -> handler.invoke(event));
                }
                // 有一些特别的情况需要同步执行.
                else {
                    handler.invoke(event);
                }
            } catch (Exception e) {
                Logger.warn("handle event exception. {}", e);
            }
        }
    }

    void notifyListeners(ApplicationEvent event) {
        for (ApplicationListener applicationListener : applicationListeners) {
            try {
                // 异步执行，投递进线程池中派发
                if (false) {
                    Actor actor = Actors.me().getEventActor(event.threadId());
                    actor.execute("event",() -> applicationListener.onApplicationEvent(event));
                }
                // 有一些特别的情况需要同步执行.
                else {
                    applicationListener.onApplicationEvent(event);
                }
            } catch (Exception e) {
                Logger.warn("handle event exception. {}", e);
            }
        }
    }

    void notifyScheduledHandler(ScheduledEvent event) {
        final ScheduledMethodWrapper method = ScheduledMethodManager.get().getHandler(event.getId());
        // 派发延迟任务...
        method.invoke(event);

        // 修正时间，进行下一次事件发布...
        event.setEndTime(method.nextExecutionTime());
        this.publish(event);
    }

    /**
     * 定时任务时间到了，通知监听器执行处理逻辑
     *
     * @param event 事件源
     */
    void notifyFixedTimeEventHandler(FixedTimeEventWrapper event) {
        this.notifyListeners(event.getSource());
        this.resetEndTimeAndPublish(event);
    }

    void notifyListeners(FixedTimeEvent event) {
        List<EventMethodWrapper> handlers = EventMethodManager.me().getEventMethodWrappers(event.getClass());
        if (handlers.isEmpty()) {
            Logger.warn("No subscription event. class= "+ event.getClass());
            return;
        }

        for (EventMethodWrapper handler : handlers) {
            try {
                handler.invoke(event);
            } catch (Exception e) {
                Logger.warn("handle event exception. {}", e);
            }
        }
    }
}