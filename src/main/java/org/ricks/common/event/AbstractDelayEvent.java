package org.ricks.common.event;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 抽象的延迟事件.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public class AbstractDelayEvent implements DelayEvent {

    private long id;

    private Date endTime;

    @Override
    public int compareTo(Delayed o) {
        return endTime.compareTo(((DelayEvent) o).getEndTime());
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(endTime.getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {

        this.endTime = endTime;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        AbstractDelayEvent other = (AbstractDelayEvent) obj;
        if (id != other.id) {
            return false;
        }
        return true;
    }
}