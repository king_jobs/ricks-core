package org.ricks.common.event;

import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * 停服事件.
 */
public class ShutdownEvent implements DelayEvent {
    private final CountDownLatch countDownLatch;
    private final Date endTime = new Date();

    public ShutdownEvent(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public int compareTo(Delayed o) {
        return endTime.compareTo(((DelayEvent) o).getEndTime());
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(endTime.getTime() - System.currentTimeMillis(), TimeUnit.NANOSECONDS);
    }

    @Override
    public Date getEndTime() {
        return endTime;
    }

    public void countDown() {
        countDownLatch.countDown();
    }
}