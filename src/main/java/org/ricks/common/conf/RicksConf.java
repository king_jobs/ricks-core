package org.ricks.common.conf;

import org.ricks.net.http.utils.StringUtils;
import org.ricks.common.BooleanUtil;
import org.ricks.common.MapUtils;
import org.ricks.common.StrUtil;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * 属性文件加载器.
 */
public class RicksConf {
    private static final String RICKS_PREFIX = "ricks";
    private static final String APPLICATION_PREFIX = "application";
    private static final String PROPERTIES_SUFFIX = ".properties";

    private static final ClassLoader loader = RicksConf.class.getClassLoader();
    /**
     * 所有配置
     */
    private static final HashMap<String, String> properties = MapUtils.newHashMap(128);


    /**
     * 加载系统配置文件中的内容.
     * <p>
     * application-test.properties中的内容会覆盖application.properties中的配置
     */
    public static HashMap<String, String> loadingProperties() {

        String filePath = System.getProperty("config.file");
        if(!StringUtils.isBlank(filePath)){
            properties.putAll(loadingFileByPath(filePath));
        }else {
            // --noark.profiles.active=dev/prod/test     # 指定运行环境
            String profile = properties.getOrDefault("ricks.active", "test");

            // 优先载入bootstrap.properties
            properties.putAll(loadingFile(RICKS_PREFIX, profile));

            // 然后再载入application.properties
            properties.putAll(loadingFile(APPLICATION_PREFIX, profile));
        }
        return properties;
    }


    private static HashMap<String, String> loadingFile(String filename, String profile) {
        HashMap<String, String> config = MapUtils.newHashMap(128);
        loadingFile(filename + PROPERTIES_SUFFIX, config);

        // 加载指定的Profile
        if (StrUtil.isNotEmpty(profile)) {
            loadingFile(filename + "-" + profile + PROPERTIES_SUFFIX, config);
        }
        return config;
    }

    private static void loadingFile(String filename, Map<String, String> config) {
        try (InputStream in = loader.getResourceAsStream(filename)) {
            if (in == null) {
                return;
            }
            Properties props = new Properties();
            InputStreamReader isr = new InputStreamReader(in);
            props.load(isr);

            for (Entry<Object, Object> e : props.entrySet()) {
                String key = e.getKey().toString().trim();
                // 有更高优化级的配置，忽略这个配置
                if (properties.containsKey(key)) {
                    continue;
                }

                // 收录这个新的配置
                String value = e.getValue().toString().trim();
                if (config.put(key, value) != null) {
                    System.err.println("覆盖配置 >>" + key + "=" + value);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("配置文件格式异常... filename=" + filename);
        }
    }

    private static HashMap<String, String> loadingFileByPath(String filePath) {
        HashMap<String, String> config = MapUtils.newHashMap(128);
        try {
            File file =new File(filePath);
            Properties props = new Properties();
            FileInputStream isr = new FileInputStream(file);
            props.load(isr);

            for (Entry<Object, Object> e : props.entrySet()) {
                String key = e.getKey().toString().trim();
                // 有更高优化级的配置，忽略这个配置
                if (properties.containsKey(key)) {
                    continue;
                }

                // 收录这个新的配置
                String value = e.getValue().toString().trim();
                if (config.put(key, value) != null) {
                    System.err.println("覆盖配置 >>" + key + "=" + value);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("配置文件格式异常... filePath=" + filePath);
        }
        return config;
    }

    public static Map<String, String> getProperties() {
        return properties;
    }

    public static boolean toBool(String key) {
        return BooleanUtil.toBoolean(properties.getOrDefault(key,"false"));
    }

    public static int toInt(String key) {
        String value = properties.getOrDefault(key,"0");
        return Integer.parseInt(value);
    }

    public static short toShort(String key) {
        String value = properties.getOrDefault(key,"0");
        return Short.parseShort(value);
    }

    public static int toIntOrDefault(String key,int defaultValue) {
        String value = properties.get(key);
        return StrUtil.isBlank(value) ? defaultValue : Integer.parseInt(value);
    }

    public static String get(String key) {
        return properties.getOrDefault(key,"");
    }

    public static String getOrDefault(String key,String defaultValue) {
        return properties.getOrDefault(key,defaultValue);
    }

    public static void main(String[] args) {
        RicksConf loadr = new RicksConf();
        loadr.loadingProperties();
        loadr.getProperties().forEach((k,v) -> System.err.println(k +" and "+ v));

    }
}