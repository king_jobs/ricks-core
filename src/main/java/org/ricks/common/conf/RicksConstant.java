package org.ricks.common.conf;

public class RicksConstant {

    /**              log start                       */
    public static final String LOG_LEVEL = "log.level";
    public static final String LOG_FORMAT = "log.format";
    public static final String LOG_CONSOLE = "log.console";
    public static final String LOG_PATH = "log.path";
    public static final String LOG_POLICIES = "log.policies";



}
