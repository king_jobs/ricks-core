package org.ricks.common.exception;

/**
 * 在找不到指定Public属性时抛出.
 *
 */
public class NoPublicFieldException extends RuntimeException {

    private static final long serialVersionUID = -4025153777427983801L;

    public NoPublicFieldException(String msg) {
        super(msg);
    }
}
