package org.ricks.common.exception;

import org.ricks.common.StrUtil;

/**
 * @author ricks
 * @Description:
 * @date 2020/9/314:30
 */
public class ValidationException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public ValidationException(String s) {
        super(s);
    }

    public ValidationException(String template, Object... args) {
        super(StrUtil.format(template, args));
    }
}
