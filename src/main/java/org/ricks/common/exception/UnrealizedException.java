package org.ricks.common.exception;

/**
 * 未实现的异常，一种可能会出来，但却没有对应实现的异常.
 *
 */
public class UnrealizedException extends RuntimeException {
    private static final long serialVersionUID = 8947098802476902429L;

    public UnrealizedException(String msg) {
        super(msg);
    }
}