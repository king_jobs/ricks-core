
package org.ricks.common.exception;

/**
 * 服务器启动异常类.
 *
 * @author 小流氓[176543888@qq.com]
 * @since 3.0
 */
public class ServerBootstrapException extends RuntimeException {
    private static final long serialVersionUID = -1979831464877053819L;

    public ServerBootstrapException(String msg) {
        super(msg);
    }

    public ServerBootstrapException(String msg, Exception e) {
        super(msg, e);
    }
}