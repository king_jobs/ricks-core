
package org.ricks.common.exception;

/**
 * Gossip协议异常
 *
 */
public class GossipException extends RuntimeException {
    private static final long serialVersionUID = -1979831464877053819L;

    public GossipException(String msg) {
        super(msg);
    }

    public GossipException(String msg, Exception e) {
        super(msg, e);
    }
}