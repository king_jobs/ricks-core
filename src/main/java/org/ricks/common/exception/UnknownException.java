

package org.ricks.common.exception;

import org.ricks.common.StrUtil;


public class UnknownException extends RuntimeException {

    public UnknownException() {
        super();
    }

    public UnknownException(Throwable cause) {
        super(cause);
    }

    public UnknownException(String message) {
        super(message);
    }

    public UnknownException(String template, Object... args) {
        super(StrUtil.format(template, args));
    }

    public UnknownException(Throwable cause, String message) {
        super(message, cause);
    }

    public UnknownException(Throwable cause, String template, Object... args) {
        super(StrUtil.format(template, args), cause);
    }

}
