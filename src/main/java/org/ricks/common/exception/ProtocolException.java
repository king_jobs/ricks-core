package org.ricks.common.exception;

/**
 * 内部序列化异常
 *
 */
public class ProtocolException extends RuntimeException {
    private static final long serialVersionUID = 8947098802476902429L;

    public ProtocolException(String msg) {
        super(msg);
    }
}