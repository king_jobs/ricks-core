

package org.ricks.common.exception;


import org.ricks.common.StrUtil;


public class AssertException extends RuntimeException {

    public AssertException(String message) {
        super(message);
    }

    public AssertException(String template, Object... args) {
        super(StrUtil.format(template, args));
    }

}
