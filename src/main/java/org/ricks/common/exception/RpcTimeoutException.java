package org.ricks.common.exception;

/**
 */
public class RpcTimeoutException extends RuntimeException {

    private static final long serialVersionUID = -6315329503841905147L;


    public RpcTimeoutException(String message) {
        super(message);
    }

    public RpcTimeoutException(String format, Object... args) {
        super(String.format(format, args));
    }
}

