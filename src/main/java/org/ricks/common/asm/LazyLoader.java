package org.ricks.common.asm;


public interface LazyLoader extends Callback {
    
    Object loadObject() throws Exception;
}
