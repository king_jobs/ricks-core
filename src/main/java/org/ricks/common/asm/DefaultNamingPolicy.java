package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2510:13
 */
public class DefaultNamingPolicy implements NamingPolicy {
    public static final DefaultNamingPolicy INSTANCE = new DefaultNamingPolicy();

    public String getClassName(String prefix, String source, Object key, Predicate names) {


        StringBuffer sb = new StringBuffer();
        sb.append(
                (prefix != null) ?
                        (
                                prefix.startsWith("java") ?
                                        "$" + prefix : prefix
                        )
                        : "net.sf.cglib.empty.Object"
        );
        sb.append("$$");
        sb.append(source.substring(source.lastIndexOf('.') + 1));
        sb.append("ByCGLIB$$");
        sb.append(Integer.toHexString(key.hashCode()));
        String base = sb.toString();
        String attempt = base;
        int index = 2;
        while (names.evaluate(attempt)) {
            attempt = base + "_" + index++;
        }

        return attempt;
    }
}
