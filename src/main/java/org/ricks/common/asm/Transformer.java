package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2513:46
 */
public interface Transformer {
    Object transform(Object value);
}
