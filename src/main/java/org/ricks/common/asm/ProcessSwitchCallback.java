package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2511:46
 */
public interface ProcessSwitchCallback {
    void processCase(int key, Label end) throws Exception;
    void processDefault() throws Exception;
}
