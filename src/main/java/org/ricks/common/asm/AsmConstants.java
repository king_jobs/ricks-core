package org.ricks.common.asm;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2511:06
 */
interface AsmConstants {
    static final String CONSTANT_VALUE = "ConstantValue";
    static final String CODE = "Code";
    static final String STACK_MAP_TABLE = "StackMapTable";
    static final String EXCEPTIONS = "Exceptions";
    static final String INNER_CLASSES = "InnerClasses";
    static final String ENCLOSING_METHOD = "EnclosingMethod";
    static final String SYNTHETIC = "Synthetic";
    static final String SIGNATURE = "Signature";
    static final String SOURCE_FILE = "SourceFile";
    static final String SOURCE_DEBUG_EXTENSION = "SourceDebugExtension";
    static final String LINE_NUMBER_TABLE = "LineNumberTable";
    static final String LOCAL_VARIABLE_TABLE = "LocalVariableTable";
    static final String LOCAL_VARIABLE_TYPE_TABLE = "LocalVariableTypeTable";
    static final String DEPRECATED = "Deprecated";
    static final String RUNTIME_VISIBLE_ANNOTATIONS = "RuntimeVisibleAnnotations";
    static final String RUNTIME_INVISIBLE_ANNOTATIONS = "RuntimeInvisibleAnnotations";
    static final String RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS = "RuntimeVisibleParameterAnnotations";
    static final String RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS = "RuntimeInvisibleParameterAnnotations";
    static final String RUNTIME_VISIBLE_TYPE_ANNOTATIONS = "RuntimeVisibleTypeAnnotations";
    static final String RUNTIME_INVISIBLE_TYPE_ANNOTATIONS = "RuntimeInvisibleTypeAnnotations";
    static final String ANNOTATION_DEFAULT = "AnnotationDefault";
    static final String BOOTSTRAP_METHODS = "BootstrapMethods";
    static final String METHOD_PARAMETERS = "MethodParameters";
    static final String MODULE = "Module";
    static final String MODULE_PACKAGES = "ModulePackages";
    static final String MODULE_MAIN_CLASS = "ModuleMainClass";
    static final String NEST_HOST = "NestHost";
    static final String NEST_MEMBERS = "NestMembers";
    static final String PERMITTED_SUBCLASSES = "PermittedSubclasses";
    static final String RECORD = "Record";
    static final int ACC_CONSTRUCTOR = 262144;

    int V1_1 = 196653;
    int V1_2 = 46;
    int V1_3 = 47;
    int V1_4 = 48;
    int V1_5 = 49;
    int ACC_PUBLIC = 1;
    int ACC_PRIVATE = 2;
    int ACC_PROTECTED = 4;
    int ACC_STATIC = 8;
    int ACC_FINAL = 16;
    int ACC_SUPER = 32;
    int ACC_SYNCHRONIZED = 32;
    int ACC_VOLATILE = 64;
    int ACC_BRIDGE = 64;
    int ACC_VARARGS = 128;
    int ACC_TRANSIENT = 128;
    int ACC_NATIVE = 256;
    int ACC_INTERFACE = 512;
    int ACC_ABSTRACT = 1024;
    int ACC_STRICT = 2048;
    int ACC_SYNTHETIC = 4096;
    int ACC_ANNOTATION = 8192;
    int ACC_ENUM = 16384;
    int ACC_DEPRECATED = 131072;
    int T_BOOLEAN = 4;
    int T_CHAR = 5;
    int T_FLOAT = 6;
    int T_DOUBLE = 7;
    int T_BYTE = 8;
    int T_SHORT = 9;
    int T_INT = 10;
    int T_LONG = 11;
    int NOP = 0;
    int ACONST_NULL = 1;
    int ICONST_M1 = 2;
    int ICONST_0 = 3;
    int ICONST_1 = 4;
    int ICONST_2 = 5;
    int ICONST_3 = 6;
    int ICONST_4 = 7;
    int ICONST_5 = 8;
    int LCONST_0 = 9;
    int LCONST_1 = 10;
    int FCONST_0 = 11;
    int FCONST_1 = 12;
    int FCONST_2 = 13;
    int DCONST_0 = 14;
    int DCONST_1 = 15;
    int BIPUSH = 16;
    int SIPUSH = 17;
    int LDC = 18;
    int ILOAD = 21;
    int LLOAD = 22;
    int FLOAD = 23;
    int DLOAD = 24;
    int ALOAD = 25;
    int IALOAD = 46;
    int LALOAD = 47;
    int FALOAD = 48;
    int DALOAD = 49;
    int AALOAD = 50;
    int BALOAD = 51;
    int CALOAD = 52;
    int SALOAD = 53;
    int ISTORE = 54;
    int LSTORE = 55;
    int FSTORE = 56;
    int DSTORE = 57;
    int ASTORE = 58;
    int IASTORE = 79;
    int LASTORE = 80;
    int FASTORE = 81;
    int DASTORE = 82;
    int AASTORE = 83;
    int BASTORE = 84;
    int CASTORE = 85;
    int SASTORE = 86;
    int POP = 87;
    int POP2 = 88;
    int DUP = 89;
    int DUP_X1 = 90;
    int DUP_X2 = 91;
    int DUP2 = 92;
    int DUP2_X1 = 93;
    int DUP2_X2 = 94;
    int SWAP = 95;
    int IADD = 96;
    int LADD = 97;
    int FADD = 98;
    int DADD = 99;
    int ISUB = 100;
    int LSUB = 101;
    int FSUB = 102;
    int DSUB = 103;
    int IMUL = 104;
    int LMUL = 105;
    int FMUL = 106;
    int DMUL = 107;
    int IDIV = 108;
    int LDIV = 109;
    int FDIV = 110;
    int DDIV = 111;
    int IREM = 112;
    int LREM = 113;
    int FREM = 114;
    int DREM = 115;
    int INEG = 116;
    int LNEG = 117;
    int FNEG = 118;
    int DNEG = 119;
    int ISHL = 120;
    int LSHL = 121;
    int ISHR = 122;
    int LSHR = 123;
    int IUSHR = 124;
    int LUSHR = 125;
    int IAND = 126;
    int LAND = 127;
    int IOR = 128;
    int LOR = 129;
    int IXOR = 130;
    int LXOR = 131;
    int IINC = 132;
    int I2L = 133;
    int I2F = 134;
    int I2D = 135;
    int L2I = 136;
    int L2F = 137;
    int L2D = 138;
    int F2I = 139;
    int F2L = 140;
    int F2D = 141;
    int D2I = 142;
    int D2L = 143;
    int D2F = 144;
    int I2B = 145;
    int I2C = 146;
    int I2S = 147;
    int LCMP = 148;
    int FCMPL = 149;
    int FCMPG = 150;
    int DCMPL = 151;
    int DCMPG = 152;
    int IFEQ = 153;
    int IFNE = 154;
    int IFLT = 155;
    int IFGE = 156;
    int IFGT = 157;
    int IFLE = 158;
    int IF_ICMPEQ = 159;
    int IF_ICMPNE = 160;
    int IF_ICMPLT = 161;
    int IF_ICMPGE = 162;
    int IF_ICMPGT = 163;
    int IF_ICMPLE = 164;
    int IF_ACMPEQ = 165;
    int IF_ACMPNE = 166;
    int GOTO = 167;
    int JSR = 168;
    int RET = 169;
    int TABLESWITCH = 170;
    int LOOKUPSWITCH = 171;
    int IRETURN = 172;
    int LRETURN = 173;
    int FRETURN = 174;
    int DRETURN = 175;
    int ARETURN = 176;
    int RETURN = 177;
    int GETSTATIC = 178;
    int PUTSTATIC = 179;
    int GETFIELD = 180;
    int PUTFIELD = 181;
    int INVOKEVIRTUAL = 182;
    int INVOKESPECIAL = 183;
    int INVOKESTATIC = 184;
    int INVOKEINTERFACE = 185;
    int NEW = 187;
    int NEWARRAY = 188;
    int ANEWARRAY = 189;
    int ARRAYLENGTH = 190;
    int ATHROW = 191;
    int CHECKCAST = 192;
    int INSTANCEOF = 193;
    int MONITORENTER = 194;
    int MONITOREXIT = 195;
    int MULTIANEWARRAY = 197;
    int IFNULL = 198;
    int IFNONNULL = 199;



    static final int F_INSERT = 256;
    static final int LDC_W = 19;
    static final int LDC2_W = 20;
    static final int ILOAD_0 = 26;
    static final int ILOAD_1 = 27;
    static final int ILOAD_2 = 28;
    static final int ILOAD_3 = 29;
    static final int LLOAD_0 = 30;
    static final int LLOAD_1 = 31;
    static final int LLOAD_2 = 32;
    static final int LLOAD_3 = 33;
    static final int FLOAD_0 = 34;
    static final int FLOAD_1 = 35;
    static final int FLOAD_2 = 36;
    static final int FLOAD_3 = 37;
    static final int DLOAD_0 = 38;
    static final int DLOAD_1 = 39;
    static final int DLOAD_2 = 40;
    static final int DLOAD_3 = 41;
    static final int ALOAD_0 = 42;
    static final int ALOAD_1 = 43;
    static final int ALOAD_2 = 44;
    static final int ALOAD_3 = 45;
    static final int ISTORE_0 = 59;
    static final int ISTORE_1 = 60;
    static final int ISTORE_2 = 61;
    static final int ISTORE_3 = 62;
    static final int LSTORE_0 = 63;
    static final int LSTORE_1 = 64;
    static final int LSTORE_2 = 65;
    static final int LSTORE_3 = 66;
    static final int FSTORE_0 = 67;
    static final int FSTORE_1 = 68;
    static final int FSTORE_2 = 69;
    static final int FSTORE_3 = 70;
    static final int DSTORE_0 = 71;
    static final int DSTORE_1 = 72;
    static final int DSTORE_2 = 73;
    static final int DSTORE_3 = 74;
    static final int ASTORE_0 = 75;
    static final int ASTORE_1 = 76;
    static final int ASTORE_2 = 77;
    static final int ASTORE_3 = 78;
    static final int WIDE = 196;
    static final int GOTO_W = 200;
    static final int JSR_W = 201;
    static final int WIDE_JUMP_OPCODE_DELTA = 33;
    static final int ASM_OPCODE_DELTA = 49;
    static final int ASM_IFNULL_OPCODE_DELTA = 20;
    static final int ASM_IFEQ = 202;
    static final int ASM_IFNE = 203;
    static final int ASM_IFLT = 204;
    static final int ASM_IFGE = 205;
    static final int ASM_IFGT = 206;
    static final int ASM_IFLE = 207;
    static final int ASM_IF_ICMPEQ = 208;
    static final int ASM_IF_ICMPNE = 209;
    static final int ASM_IF_ICMPLT = 210;
    static final int ASM_IF_ICMPGE = 211;
    static final int ASM_IF_ICMPGT = 212;
    static final int ASM_IF_ICMPLE = 213;
    static final int ASM_IF_ACMPEQ = 214;
    static final int ASM_IF_ACMPNE = 215;
    static final int ASM_GOTO = 216;
    static final int ASM_JSR = 217;
    static final int ASM_IFNULL = 218;
    static final int ASM_IFNONNULL = 219;
    static final int ASM_GOTO_W = 220;


    static void checkAsmExperimental(Object caller) {
        Class<?> callerClass = caller.getClass();
        String internalName = callerClass.getName().replace('.', '/');
        if (!isWhitelisted(internalName)) {
            checkIsPreview(callerClass.getClassLoader().getResourceAsStream(internalName + ".class"));
        }

    }

    static boolean isWhitelisted(String internalName) {
        if (!internalName.startsWith("org/objectweb/asm/")) {
            return false;
        } else {
            String member = "(Annotation|Class|Field|Method|Module|RecordComponent|Signature)";
            return internalName.contains("Test$") || Pattern.matches("org/objectweb/asm/util/Trace" + member + "Visitor(\\$.*)?", internalName) || Pattern.matches("org/objectweb/asm/util/Check" + member + "Adapter(\\$.*)?", internalName);
        }
    }

    static void checkIsPreview(InputStream classInputStream) {
        if (classInputStream == null) {
            throw new IllegalStateException("Bytecode not available, can't check class version");
        } else {
            int minorVersion;
            try {
                DataInputStream callerClassStream = new DataInputStream(classInputStream);
                Throwable var3 = null;

                try {
                    callerClassStream.readInt();
                    minorVersion = callerClassStream.readUnsignedShort();
                } catch (Throwable var13) {
                    var3 = var13;
                    throw var13;
                } finally {
                    if (callerClassStream != null) {
                        if (var3 != null) {
                            try {
                                callerClassStream.close();
                            } catch (Throwable var12) {
                            }
                        } else {
                            callerClassStream.close();
                        }
                    }

                }
            } catch (IOException var15) {
                throw new IllegalStateException("I/O error, can't check class version", var15);
            }

            if (minorVersion != 65535) {
                throw new IllegalStateException("ASM9_EXPERIMENTAL can only be used by classes compiled with --enable-preview");
            }
        }
    }
}
