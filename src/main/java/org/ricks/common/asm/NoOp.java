package org.ricks.common.asm;

import javax.security.auth.callback.Callback;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2514:19
 */
public interface NoOp extends Callback
{
    /**
     * A thread-safe singleton instance of the <code>NoOp</code> callback.
     */
    public static final NoOp INSTANCE = new NoOp() { };
}
