package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2318:51
 */
public interface ClassGenerator {
    void generateClass(ClassVisitor var1) throws Exception;
}
