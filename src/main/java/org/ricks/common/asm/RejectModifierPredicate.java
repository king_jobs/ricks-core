package org.ricks.common.asm;

import java.lang.reflect.Member;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2514:27
 */
public class RejectModifierPredicate implements Predicate {
    private int rejectMask;

    public RejectModifierPredicate(int rejectMask) {
        this.rejectMask = rejectMask;
    }

    public boolean evaluate(Object arg) {
        return (((Member)arg).getModifiers() & rejectMask) == 0;
    }
}
