package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2514:50
 */
public class CodeAdapter implements CodeVisitor {
    protected CodeVisitor cv;

    public CodeAdapter(CodeVisitor var1) {
        this.cv = var1;
    }

    public void visitInsn(int var1) {
        this.cv.visitInsn(var1);
    }

    public void visitIntInsn(int var1, int var2) {
        this.cv.visitIntInsn(var1, var2);
    }

    public void visitVarInsn(int var1, int var2) {
        this.cv.visitVarInsn(var1, var2);
    }

    public void visitTypeInsn(int var1, String var2) {
        this.cv.visitTypeInsn(var1, var2);
    }

    public void visitFieldInsn(int var1, String var2, String var3, String var4) {
        this.cv.visitFieldInsn(var1, var2, var3, var4);
    }

    public void visitMethodInsn(int var1, String var2, String var3, String var4) {
        this.cv.visitMethodInsn(var1, var2, var3, var4);
    }

    public void visitJumpInsn(int var1, Label var2) {
        this.cv.visitJumpInsn(var1, var2);
    }

    public void visitLabel(Label var1) {
        this.cv.visitLabel(var1);
    }

    public void visitLdcInsn(Object var1) {
        this.cv.visitLdcInsn(var1);
    }

    public void visitIincInsn(int var1, int var2) {
        this.cv.visitIincInsn(var1, var2);
    }

    public void visitTableSwitchInsn(int var1, int var2, Label var3, Label[] var4) {
        this.cv.visitTableSwitchInsn(var1, var2, var3, var4);
    }

    public void visitLookupSwitchInsn(Label var1, int[] var2, Label[] var3) {
        this.cv.visitLookupSwitchInsn(var1, var2, var3);
    }

    public void visitMultiANewArrayInsn(String var1, int var2) {
        this.cv.visitMultiANewArrayInsn(var1, var2);
    }

    public void visitTryCatchBlock(Label var1, Label var2, Label var3, String var4) {
        this.cv.visitTryCatchBlock(var1, var2, var3, var4);
    }

    public void visitMaxs(int var1, int var2) {
        this.cv.visitMaxs(var1, var2);
    }

    public void visitLocalVariable(String var1, String var2, Label var3, Label var4, int var5) {
        this.cv.visitLocalVariable(var1, var2, var3, var4, var5);
    }

    public void visitLineNumber(int var1, Label var2) {
        this.cv.visitLineNumber(var1, var2);
    }

    public void visitAttribute(Attribute var1) {
        this.cv.visitAttribute(var1);
    }
}
