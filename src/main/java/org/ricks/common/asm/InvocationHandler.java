package org.ricks.common.asm;

import java.lang.reflect.Method;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2514:21
 */
public interface InvocationHandler extends Callback {
    /**
     */
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable;

}
