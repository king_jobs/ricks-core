package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2318:53
 */
public interface NamingPolicy {
    String getClassName(String var1, String var2, Object var3, Predicate var4);

    boolean equals(Object var1);
}
