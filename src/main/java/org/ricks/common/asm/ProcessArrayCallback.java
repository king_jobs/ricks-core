package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2514:05
 */
public interface ProcessArrayCallback {
    void processElement(Type type);
}
