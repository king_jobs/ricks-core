package org.ricks.common.asm;


public interface FixedValue extends Callback {
    
    Object loadObject() throws Exception;
}
