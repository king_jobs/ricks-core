package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2514:08
 */
public interface ObjectSwitchCallback {
    void processCase(Object key, Label end) throws Exception;
    void processDefault() throws Exception;
}