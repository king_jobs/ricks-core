package org.ricks.common.asm;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2021/1/2510:14
 */
public interface Predicate {
    boolean evaluate(Object arg);
}
