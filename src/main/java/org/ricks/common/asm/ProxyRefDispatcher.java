package org.ricks.common.asm;


public interface ProxyRefDispatcher extends Callback {
    
    Object loadObject(Object proxy) throws Exception;
}
