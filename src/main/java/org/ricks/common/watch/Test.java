package org.ricks.common.watch;

import org.ricks.common.lang.Logger;

import java.nio.file.Path;
import java.nio.file.WatchEvent;

public class Test {

    public static void main(String[] args) {
        Watcher watcher = new SimpleWatcher(){
            @Override
            public void onCreate(WatchEvent<?> event, Path currentPath) {
                Object obj = event.context();
                Logger.info("创建："+currentPath+"-> "+ obj);
            }

            @Override
            public void onModify(WatchEvent<?> event, Path currentPath) {
                Object obj = event.context();
                Logger.info("修改："+ currentPath+"-> "+ obj);
            }

            @Override
            public void onDelete(WatchEvent<?> event, Path currentPath) {
                Object obj = event.context();
                Logger.info("删除："+ currentPath+"-> "+ obj);
            }

            @Override
            public void onOverflow(WatchEvent<?> event, Path currentPath) {
                Object obj = event.context();
                Logger.info("Overflow："+currentPath+"-> " + obj);
            }
        };

        WatchMonitor monitor = WatchMonitor.createAll("d:/test", new DelayWatcher(watcher, 500));
        WatchMonitor monitor1 = WatchMonitor.createAll("d:/test1", new DelayWatcher(watcher, 500));

        monitor.start();

        monitor1.start();
    }
}
