package org.ricks.common.watch;

import org.ricks.common.exception.IORuntimeException;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;

/**
 * 监听工具类<br>
 * 主要负责文件监听器的快捷创建
 *
 * @author Looly
 * @since 3.1.0
 */
public class WatchUtil {



	/**
	 * 创建并初始化监听
	 *
	 * @param uri URI
	 * @param events 监听的事件列表
	 * @return 监听对象
	 */
	public static WatchMonitor create(URI uri,  WatchEvent.Kind<?>... events) {
		return create(Paths.get(uri),  events);
	}


	/**
	 * 创建并初始化监听
	 *
	 * @param file 文件
	 * @param events 监听的事件列表
	 * @return 监听对象
	 */
	public static WatchMonitor create(File file,  WatchEvent.Kind<?>... events) {
		return create(file.toPath(), events);
	}


	/**
	 * 创建并初始化监听
	 *
	 * @param path 路径
	 * @param events 监听的事件列表
	 * @return 监听对象
	 */
	public static WatchMonitor create(String path, WatchEvent.Kind<?>... events) {
		return create(Paths.get(path),  events);
	}


	/**
	 * 创建并初始化监听
	 *
	 * @param path 路径
	 * @param events 监听事件列表
	 * @return 监听对象
	 */
	public static WatchMonitor create(Path path, WatchEvent.Kind<?>... events) {
		return new WatchMonitor(path, events);
	}

	// ---------------------------------------------------------------------------------------------------------- createAll


	/**
	 * 创建并初始化监听，监听所有事件
	 *
	 * @param uri URI
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 */
	public static WatchMonitor createAll(URI uri, Watcher watcher) {
		return createAll(Paths.get(uri),  watcher);
	}


	/**
	 * 创建并初始化监听，监听所有事件
	 *
	 * @param file 被监听文件
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 */
	public static WatchMonitor createAll(File file,  Watcher watcher) {
		return createAll(file.toPath(),  watcher);
	}


	/**
	 * 创建并初始化监听，监听所有事件
	 *
	 * @param path 路径
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 */
	public static WatchMonitor createAll(String path, Watcher watcher) {
		return createAll(Paths.get(path),  watcher);
	}

	/**
	 * 创建并初始化监听，监听所有事件
	 *
	 * @param path 路径
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 */
	public static WatchMonitor createAll(Path path,  Watcher watcher) {
		final WatchMonitor watchMonitor = create(path, WatchMonitor.EVENTS_ALL);
		watchMonitor.setWatcher(watcher);
		return watchMonitor;
	}

	// ---------------------------------------------------------------------------------------------------------- createModify


	/**
	 * 创建并初始化监听，监听修改事件
	 *
	 * @param uri URI
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 * @since 4.5.2
	 */
	public static WatchMonitor createModify(URI uri,  Watcher watcher) {
		return createModify(Paths.get(uri),  watcher);
	}

	/**
	 * 创建并初始化监听，监听修改事件
	 *
	 * @param file 被监听文件
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 * @since 4.5.2
	 */
	public static WatchMonitor createModify(File file,  Watcher watcher) {
		return createModify(file.toPath(),  watcher);
	}

	/**
	 * 创建并初始化监听，监听修改事件
	 *
	 * @param path 路径
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 * @since 4.5.2
	 */
	public static WatchMonitor createModify(String path, Watcher watcher) {
		return createModify(Paths.get(path),  watcher);
	}

	/**
	 * 创建并初始化监听，监听修改事件
	 *
	 * @param path 路径
	 * @param watcher {@link Watcher}
	 * @return {@link WatchMonitor}
	 * @since 4.5.2
	 */
	public static WatchMonitor createModify(Path path,  Watcher watcher) {
		final WatchMonitor watchMonitor = create(path,  WatchMonitor.ENTRY_MODIFY);
		watchMonitor.setWatcher(watcher);
		return watchMonitor;
	}

	/**
	 * 注册Watchable对象到WatchService服务
	 *
	 * @param watchable 可注册对象
	 * @param watcher WatchService对象
	 * @param events 监听事件
	 * @return {@link WatchKey}
	 * @since 4.6.9
	 */
	public static WatchKey register(Watchable watchable, WatchService watcher, WatchEvent.Kind<?>... events){
		try {
			return watchable.register(watcher, events);
		} catch (IOException e) {
			throw new IORuntimeException(e);
		}
	}
}
