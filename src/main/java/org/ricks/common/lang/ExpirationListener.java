package org.ricks.common.lang;

public interface ExpirationListener<K, V> {
    /**
     * Called when a map entry expires.
     *
     * @param key Expired key
     * @param value Expired value
     */
    void expired(K key, V value);
}