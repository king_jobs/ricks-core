/*
 * Copyright 2016 Martin Winandy
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package org.ricks.common.lang;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Static logger for logging internal warnings and errors to console.
 */
public final class Logger {

	private static final String DEFAULT_DATE_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss SSS";

	/**
	 * 日志中输出的时间格式
	 */
	private static final SimpleDateFormat formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT_PATTERN);


	public static Level LEVEL = Level.INFO;

	private static final int BUFFER_SIZE = 256;

	/** */
	private Logger() {
	}
	public static void error(final String message) {
		log(Level.ERROR,message);
	}

	public static void error(final String message,final Throwable exception) {
		log(Level.ERROR,exception,message);
	}
	public static void warn(final String message) {
		log(Level.WARN,message);
	}

	public static void warn(final String message,final Throwable exception) {
		log(Level.WARN,exception,message);
	}

	public static void info(final String message,Object... objects ) {
		log(Level.INFO,message);
	}

	public static void debug(final String message,final Throwable exception) {
		log(Level.DEBUG,exception,message);
	}

	public static void debug(final String message) {
		log(Level.DEBUG,message);
	}

	public static void log(final Level level, final String message) {
		if(isEnabled(level)) {
			String time = formatter.format(new Date());
			System.out.println(time + " [" + level + "]: " + message);
		}
	}

	/**
	 * Logs a caught exception.
	 *
	 * @param level
	 *            Severity level
	 * @param exception
	 *            Caught exception
	 */
	public static void log(final Level level, final Throwable exception) {
		String nameOfException = exception.getClass().getName();
		String messageOfException = exception.getMessage();
		String time = formatter.format(new Date());
		if (messageOfException == null || messageOfException.isEmpty()) {
			System.err.println(time+" [" + level + "]: " + nameOfException);
		} else {
			System.err.println(time+ " [" + level + "]: " + messageOfException + " (" + nameOfException + ")");
		}
	}

	/**
	 * Logs a caught exception with a custom text message.
	 *
	 * @param level
	 *            Severity level
	 * @param message
	 *            Plain text message
	 * @param exception
	 *            Caught exception
	 */
	public static void log(final Level level, final Throwable exception, final String message) {
		String nameOfException = exception.getClass().getName();
		String messageOfException = exception.getMessage();

		StringBuilder builder = new StringBuilder(BUFFER_SIZE);
		builder.append("LOGGER ");
		builder.append(level);
		builder.append(": ");
		builder.append(message);
		builder.append(" (");
		builder.append(nameOfException);
		if (messageOfException != null && !messageOfException.isEmpty()) {
			builder.append(": ");
			builder.append(messageOfException);
		}
		builder.append(")");

		System.err.println(builder);
	}

	/**
	 * 判定日志级别是否达标
	 *
	 * @param level 日志级别
	 * @return 如果达标则返回true
	 */
	private static boolean isEnabled(Level level) {
		return LEVEL.getValue() <= level.getValue();
	}

}
