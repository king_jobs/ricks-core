package org.ricks.common.lang;

/**
 * 日志等级
 * @author Looly
 *
 */
public enum Level {

	/**
	 * 'DEBUG' log level.
	 */
	DEBUG(1),
	/**
	 * 'INFO' log level.
	 */
	INFO(2),
	/**
	 * 'WARN' log level.
	 */
	WARN(3),
	/**
	 * 'ERROR' log level.
	 */
	ERROR(4);

	private final int value;

	Level(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
