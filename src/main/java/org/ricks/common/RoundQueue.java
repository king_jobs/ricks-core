package org.ricks.common;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ricks
 * @Description:循环队列 【场景应用 固定历史记录】 比如：排行榜
 * @date 2021/7/213:43
 */
public class RoundQueue<T> {

    private T[] queue = null;

    private int front = 0;

    private int rear = 0;

    private boolean empty = true;//true表示循环队列为空

    public RoundQueue(int max) {//构造指定大小的循环队列
        this.queue = (T[])new Object[max];
    }

    public void clearQueue(){
        this.front = 0;
        this.rear = 0;
        this.empty = true;
    }

    public boolean queueEmpty() {
        if(this.empty == true) return true; else return false;
    }

    public int queueLength() {
        if (this.front == this.rear && this.empty == false) {
            return this.queue.length;//如果循环队列已满，返回数组长度即元素个数
        }
        return (this.rear - this.front + this.queue.length) % this.queue.length;//否则，取模运算得到长度
    }

    public T[] getHead() {

        if (this.empty == true) {
            return null;
        }

        T [] i = (T[]) new Object[1];
        i[0] = queue[this.front];
        return i;
    }

    public boolean enQueue(T value) {

//        if (this.empty == false && this.front == this.rear) {
//            return false; //循环队列，重新从index 0 set 数据
//        }

        this.queue[this.rear] = value;
        this.rear = (this.rear + 1) % this.queue.length;
        this.empty = false;
        return true;
    }

    public T [] deQueue() {
        if (this.empty == true) return null;
        T [] i = (T[]) new Object[1];
        i[0] = this.queue[this.front];//获取队头元素
        this.front = (this.front + 1) % this.queue.length;//删除队头元素（front指针加一）
        if(this.front == this.rear) {//如果循环队列变空，改变标志位
            this.empty = true;
        }
        return i;
    }

    public List<T> getData() {
        return Arrays.stream(queue).collect(Collectors.toList());
    }

    //此处用输出循环队列表示遍历
    public String queueTraverse() {

        String s = "";
        int i = this.front;//i指向第一个元素

        if(this.front == this.rear && this.empty == false) {//如果循环队列已满，取出第一个元素，i指向下一个元素
            s += this.queue[i] + "、";
            i = (i + 1) % this.queue.length;
        }

        for (int j = 0; j < queue.length; j++) {
            if(this.queue[j] != null) s += this.queue[j] + "、";
        }

        if(s.length() == 0) {//如果没有读取到元素，直接返回空字符串
            return s;
        }
        return s.substring(0,s.length() - 1); //除去最后的顿号返回
    }

    public static void main(String[] args) {
        RoundQueue roundQueue = new RoundQueue<Integer>(5);
        for (int i = 1; i < 4; i++) {
            roundQueue.enQueue(i);
        }

        System.err.println(roundQueue.queueTraverse());

        List list = roundQueue.getData();
        System.err.println();
    }

}
