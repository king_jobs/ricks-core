package org.ricks.common.json;

import app.vo.Card;

import java.util.List;

public class Test {

    public static void main(String[] args) {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(1111);
        System.err.println(jsonArray.toString());

        List list = List.of("111","3333");
        jsonArray.put(list);
        System.err.println(jsonArray.toString());

        JSONObject jsonObject = new JSONObject("{\"cardName\":\"法师\",\"count\":1,\"cardId\":2}");
        Card card = jsonObject.toBean(Card.class);
        System.err.println(card.toString());
        System.err.println(jsonObject.toMap());


        List cards = List.of(new Card(1,"ricks",11), new Card(2,"is",22), new Card(2,"god",33));
        JSONArray jsonArray1 = new JSONArray(cards);
        System.err.println(jsonArray1.toString());
        List l = jsonArray1.toList();
        System.err.println(l);
    }
}
