package org.ricks.common;

/**
 * @author ricks
 * @date 2022/9/618:11
 */
public class SystemUtils {

    public static boolean isWindows() {
        String osName = getOsName();
        return osName != null && osName.startsWith("Windows");
    }

    public static boolean isMacOs() {
        String osName = getOsName();
        return osName != null && osName.startsWith("Mac");
    }

    public static boolean isLinux() {
        String osName = getOsName();
        return (osName != null && osName.startsWith("Linux") || (!isWindows() && !isMacOs()));
    }


    public static String getOsName() {
        return System.getProperty("os.name");
    }
}
