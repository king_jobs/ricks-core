package org.ricks.common.actor;

import org.ricks.common.lang.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ricks
 * @Description:actor概念
 * counter 处理方式，如果机器人不睡眠循环发送消息，这时候就会出现消息堆积，而消息任务会递归一直到队列消息消费殆尽
 * 如果队列消息堆积到一定程度，递归循环达到栈内存上限 就会栈内存溢出
 *
 * 还需要优化，队列目前存在意义不大
 * @date 2022/9/1913:52
 */
public class Actor implements IActor{

    private ExecutorService execute;
    private LinkedTransferQueue<ActorRunnable> queues = new LinkedTransferQueue();
    private AtomicInteger counter = new AtomicInteger();
    private AtomicBoolean starter = new AtomicBoolean(true);
    private final Worker worker;

    public Actor(ExecutorService execute) {
        this.worker = new Worker();
        this.execute = execute;
    }

    @Override
    public void execute(String key, Runnable task) {
        execute(new ActorRunnable(key,task));
    }

    @Override
    public void execute(ActorRunnable task) {
        if(!starter.get()) {
            Logger.info("进程开始停止，actor 拒绝接收新的消息。");
            return;
        }
        queues.offer(task);
        int a = counter.getAndIncrement();
        if(a == 0) {
            execute.execute(worker);
        }
//        System.err.println(this + ":开始投递actor队列， queues size:" + queues.size() + " , counter:" + counter.get());
//        if(waitSize() > 500) Logger.info("offer 超出队列 size:" + waitSize()  + " counter num: " + a);
//        if (counter.incrementAndGet() == 1) {
//            execute.execute(worker);
//        }
//        execute.execute(worker);
    }

    @Override
    public int waitSize() {
        return queues.size();
    }

    @Override
    public void shutdown() {
        try {
            starter.set(false);
            execute.shutdown();
            while (!execute.isTerminated()) sleep(100);
        } catch (RejectedExecutionException e) {
            Logger.warn("拒绝接收消息。");
        }
    }

    private void sleep(int num) {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class Worker implements Runnable {

        @Override
        public void run() {
            int curr = counter.get();
            while (curr > 0){
                try{
                    ActorRunnable worker = queues.poll();
                    worker.run();
//                    System.err.println(this+":执行的actor work key:" + worker.getKey());
//                    curr = counter.decrementAndGet();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    curr = counter.decrementAndGet();
//                    if(waitSize() > 500) System.err.println(this+":actor队列消费完成， queues size:" + queues.size() + " , counter:" + curr);
                }
            }
//            if(counter.decrementAndGet() > 0) run();  //这种递归写法如果消费能力远远不如生产，就会出现栈内存溢出
        }
    }
}
