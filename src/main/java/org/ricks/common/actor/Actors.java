package org.ricks.common.actor;

import org.ricks.common.lang.ThreadNameFactory;
import org.ricks.common.lang.Logger;
import org.ricks.common.RandomUtil;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ricks
 * @Description:网络消息actor & db actor & log actor
 * @date 2022/9/2113:18
 */
public class Actors {

    private static final String EXECUTOR = "work";
    private static final String EVENT_EXECUTOR = "event";

    private static final String KCP_EXECUTOR = "kcp";

    private static int threadNum = Runtime.getRuntime().availableProcessors();

    //业务线程
    private static final Actor[] actors = Actors.me().createActors(threadNum,EXECUTOR);
    //事件actor
    private static final Actor[] eventActor = Actors.me().createActors(threadNum,EVENT_EXECUTOR);
    //KCP flush
    private static final Actor[] kcpActors = Actors.me().createActors(threadNum,KCP_EXECUTOR);

    private static final Actor tcp_write = Actors.me().createActor("tcp-write");

    public int getThreadNum() {
        return threadNum;
    }


    /**
     * @Description: 创建actor入口 ，供应给工程使用
     * @param threadNum 线程数量
     * @return Actor[]
     * @author ricks
     * @date 2022/11/23 14:21
     */
    public Actor[] createActors(int threadNum,String name) {
        Actor[] actors = new Actor[threadNum];
        for (int i = 0; i < threadNum; i++) {
            ExecutorService executor = Executors.newSingleThreadExecutor(new ThreadNameFactory(i,name));
            actors[i] = new Actor(executor);
        }
        return actors;
    }

    public Actor[] createActors(String name) {
        Actor[] actors = new Actor[threadNum];
        for (int i = 0; i < threadNum; i++) {
            ExecutorService executor = Executors.newSingleThreadExecutor(new ThreadNameFactory(i,name));
            actors[i] = new Actor(executor);
        }
        return actors;
    }

    public Actor createActor(String name) {
        ExecutorService executor = Executors.newSingleThreadExecutor(new ThreadNameFactory(0,name));
        Actor actor = new Actor(executor);
        return actor;
    }

    private static class SingleCase {
        public static final Actors INSTANCE = new Actors();
    }

    private Actors() {

    }

    public static Actors me() {
        return Actors.SingleCase.INSTANCE;
    }

    public void destroy() {
        Logger.info("进程停止中 -- 开始销毁内部维护actor .......");
        Arrays.stream(actors).forEach(Actor::shutdown);
        Logger.info("进程停止中 -- work actor销毁成功 .......");
        Arrays.stream(eventActor).forEach(Actor::shutdown);
        Logger.info("进程停止中 -- event actor销毁成功 .......");
        Arrays.stream(kcpActors).forEach(Actor::shutdown);
        Logger.info("进程停止中 -- kcp actor销毁成功 .......");
        Logger.info("进程停止中 -- 内部维护actor已经全部销毁 .......");
    }

    public Actor get(long index) {
        return actors[(int) (Math.abs(index) % actors.length)];
    }

    public Actor getEventActor(long index) {
        return eventActor[(int) (Math.abs(index) % eventActor.length)];
    }

    public Actor getRandom() {
        return actors[RandomUtil.randomInt(threadNum)];
    }

    public Actor getKcpActor(long index) {
        return kcpActors[(int) (Math.abs(index) % kcpActors.length)];
    }

    public Actor getTcpWrite() {
        return tcp_write;
    }

}
