package org.ricks.common.actor;

/**
 * @author ricks
 * @Description:
 * @date 2022/9/1913:55
 */
public interface IActor {

    void execute(String key,Runnable task);

    void execute(ActorRunnable task);

    int waitSize();

    void shutdown();
}
