package org.ricks.common.actor;

/**
 * @author ricks
 * @date 2022/9/1913:58
 */
public class ActorRunnable {

    private String key;
    private Runnable worker;

    public ActorRunnable(String key, Runnable worker) {
        this.key = key;
        this.worker = worker;
    }

    public void run() {
        worker.run();
    }

    public String getKey() {
        return key;
    }
}

