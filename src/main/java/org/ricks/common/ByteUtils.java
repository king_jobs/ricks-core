package org.ricks.common;

import java.util.Arrays;

/**
 * @author ricks
 * @date 2022/11/111:39
 */
public class ByteUtils {

    // encode 8 bits unsigned int
    public static void ikcp_encode8u(byte[] p, int offset, byte c) {
        p[0 + offset] = c;
    }

    /* encode 16 bits unsigned int (msb) */
    public static void ikcp_encode16u(byte[] p, int offset, int w) {
        p[offset + 0] = (byte) (w >> 8);
        p[offset + 1] = (byte) (w >> 0);
    }

    /* encode 32 bits unsigned int (msb) */
    public static void ikcp_encode32u(byte[] p, int offset, long l) {
        p[offset + 0] = (byte) (l >> 24);
        p[offset + 1] = (byte) (l >> 16);
        p[offset + 2] = (byte) (l >> 8);
        p[offset + 3] = (byte) (l >> 0);
    }

    public static byte[] slice(byte[] bytes,int from,int to) {
        int low = Math.max(from,0);
        int high = Math.min(to,bytes.length);
        return Arrays.copyOfRange(bytes,low,high);
    }
}
