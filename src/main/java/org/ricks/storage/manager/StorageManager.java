

package org.ricks.storage.manager;

import org.ricks.common.exception.RunException;
import org.ricks.common.lang.Logger;
import org.ricks.storage.model.IStorage;
import org.ricks.storage.util.function.Func1;
import org.ricks.common.IoUtil;
import org.ricks.common.PathUtil;

import java.io.*;
import java.util.*;

/**
 *  需要作为 config bean 处理，zfoo protocol ，proto buff  , flat buff ,不同处理机制
 *
 *  luban导出bin ,封装成data.dat机制 进行数据处理
 */
public class StorageManager implements IStorageManager {
    

    /**
     * 在当前项目被依赖注入，被使用的Storage
     */
    private final Map<Class<?>, IStorage<?, ?>> storageMap = new HashMap<>();


    @Override
    public void initBefore() {
        Map<String, byte[]> tableDataMap = new HashMap<>();
        ObjectInputStream input = null;
        Object obj = null;
        try{
            input = new ObjectInputStream(new FileInputStream(new File(PathUtil.currPath() + "/data.dat")));
            obj = input.readObject();
            if (obj == null || !(obj instanceof Map)) {
                System.err.println("Object can't been reload by ObjectInputStreamMethod");
                return;
            }
            tableDataMap = (Map<String, byte[]>) obj;
        } catch(Exception e){
            e.printStackTrace();
        } finally {
            if(Objects.nonNull(input)) {
                IoUtil.close(input);
            }
        }

    }

    @Override
    public void inject() {

    }

    @Override
    public void initAfter() {

    }

    public <T> List<T> getList(Class<T> clazz) {
        IStorage<?, ?> storage = getStorage(clazz);
        return (List<T>) storage.getAll();
    }

    public <T, K> List<T> getIndexes(Class<T> clazz, Func1<T, ?> func, K indexId) {
        var storage = getStorage(clazz);
        return storage.getIndexes(func, indexId);
    }

    public <T, UQ> T get(Class<T> clazz, UQ keyId) {
        IStorage<UQ, T> storage = getStorage(clazz);
        return storage.get(keyId);
    }

    @Override
    public <K, V, T extends IStorage<K, V>> T getStorage(Class<V> clazz) {
        var storage = storageMap.get(clazz);
        if (storage == null) {
            throw new RunException("There is no [{}] defined Storage and unable to get it", clazz.getCanonicalName());
        }
        if (storage.isRecycle()) {
            // Storage没有使用，为了节省内存提前释放了它；只有使用ResInjection注解的Storage才能被动态获取或者关闭配置recycle属性
            Logger.warn("Storage ["+clazz.getCanonicalName()+"] is not used, it was freed to save memory; use @ResInjection or turn off recycle configuration");
        }
        return (T) storage;
    }

    @Override
    public Map<Class<?>, IStorage<?, ?>> storageMap() {
        return storageMap;
    }

    @Override
    public void updateStorage(Class<?> clazz, IStorage<?, ?> storage) {
        storageMap.put(clazz, storage);
    }
}
