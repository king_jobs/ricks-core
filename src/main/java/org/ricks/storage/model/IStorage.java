/*
 * Copyright (C) 2020 The zfoo Authors
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 */

package org.ricks.storage.model;

import org.ricks.storage.util.function.Func1;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author godotg
 */
public interface IStorage<K, V> {

    boolean contain(K key);

    boolean contain(int key);

    boolean contain(long key);

    V get(K id);

    V get(int id);

    V get(long id);

    void recycleStorage();

    boolean isRecycle();

    void setRecycle(boolean recycle);

    Collection<V> getAll();

    List<V> getList();

    Map<K, V> getData();

    IdDef getIdDef();

    <K> List<V> getIndexes(Func1<V, ?> function, K key);

    <K, V> V getUniqueIndex(Func1<V, ?> function, K key);

    int size();
}
