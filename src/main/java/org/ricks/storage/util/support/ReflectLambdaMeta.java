package org.ricks.storage.util.support;


import org.ricks.common.ClassUtil;
import org.ricks.common.StrUtil;

/**
 * Created by hcl at 2021/5/14
 */
public class ReflectLambdaMeta implements LambdaMeta {
    private final java.lang.invoke.SerializedLambda lambda;

    private final ClassLoader classLoader;

    public ReflectLambdaMeta(java.lang.invoke.SerializedLambda lambda, ClassLoader classLoader) {
        this.lambda = lambda;
        this.classLoader = classLoader;
    }

    @Override
    public String getImplMethodName() {
        return lambda.getImplMethodName();
    }

    @Override
    public Class<?> getInstantiatedClass() {
        String instantiatedMethodType = lambda.getInstantiatedMethodType();
        String instantiatedType = instantiatedMethodType.substring(2, instantiatedMethodType.indexOf(StrUtil.SEMICOLON)).replace(StrUtil.SLASH, StrUtil.PERIOD);
        return ClassUtil.toClassConfident(instantiatedType, this.classLoader);
    }
}