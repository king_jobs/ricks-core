package org.ricks.storage.util.support;

import org.ricks.common.ClassUtil;
import org.ricks.common.StrUtil;

/**
 * 基于 {@link SerializedLambda} 创建的元信息
 * <p>
 * Create by hcl at 2021/7/7
 */
public class ShadowLambdaMeta implements LambdaMeta {
    private final SerializedLambda lambda;

    public ShadowLambdaMeta(SerializedLambda lambda) {
        this.lambda = lambda;
    }

    @Override
    public String getImplMethodName() {
        return lambda.getImplMethodName();
    }

    @Override
    public Class<?> getInstantiatedClass() {
        String instantiatedMethodType = lambda.getInstantiatedMethodType();
        String instantiatedType = instantiatedMethodType.substring(2, instantiatedMethodType.indexOf(StrUtil.SEMICOLON)).replace(StrUtil.SLASH, StrUtil.PERIOD);
        return ClassUtil.toClassConfident(instantiatedType, lambda.getCapturingClass().getClassLoader());
    }

}
