//package org.ricks.storage;
//
//import org.ricks.utils.event.ApplicationContextEvent;
//import org.ricks.utils.event.ApplicationListener;
//import org.ricks.utils.event.ContextRefreshedEvent;
//import org.ricks.ioc.IocHolder;
//import org.ricks.lang.Logger;
//import org.ricks.lang.StopWatch;
//import org.ricks.storage.manager.IStorageManager;
//import org.ricks.storage.util.function.Func1;
//
//import java.util.List;
//
//public class StorageContext implements ApplicationListener<ApplicationContextEvent> {
//
//
//    private static StorageContext instance;
////
////
//    private IStorageManager storageManager;
//
//    public static StorageContext getStorageContext() {
//        return instance;
//    }
//
//    public static IStorageManager getStorageManager() {
//        return instance.storageManager;
//    }
//    public static <V, K> V get(Class<V> clazz, K keyId) {
//        return instance.storageManager.getStorage(clazz).get(keyId);
//    }
//
//    public static <V> List<V> getList(Class<V> clazz) {
//        return instance.storageManager.getStorage(clazz).getList();
//    }
//
//    public static <K, V> List<V> getIndexes(Class<V> clazz, Func1<V, ?> function, K keyId) {
//        return instance.storageManager.getStorage(clazz).getIndexes(function, keyId);
//    }
//
//    public static <K, V> V getUniqueIndex(Class<V> clazz, Func1<V, ?> func, K keyId) {
//        return instance.storageManager.getStorage(clazz).getUniqueIndex(func, keyId);
//    }
//
//
//    @Override
//    public void onApplicationEvent(ApplicationContextEvent event) {
//
////        System.err.println("触发事件，，，，，，，，" + event);
////
////        if (event instanceof ContextRefreshedEvent) {
////            var stopWatch = new StopWatch();
////            // 初始化上下文
////            StorageContext.instance = this;
////            //storageManager IOC 进行装载
////            instance.storageManager = IocHolder.getBean(IStorageManager.class);
////
////            // 初始化，并读取配置表
////            instance.storageManager.initBefore();
////
////            // 注入配置表资源
////            instance.storageManager.inject();
////
////            // 移除没有被引用的不必要资源，为了节省服务器内存
////            instance.storageManager.initAfter();
////            Logger.info("Storage started successfully and cost ["+stopWatch.getLastTaskTimeMillis()+"] millis");
////        }
//    }
//
////    @Override
////    public int getOrder() {
////        return Ordered.HIGHEST_PRECEDENCE;
////    }
//}
