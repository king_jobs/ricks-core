package org.ricks.dispatch;

import org.ricks.common.asm.ClassVisitor;
import org.ricks.common.asm.MethodVisitor;
import org.ricks.net.Context;

import java.lang.invoke.MethodHandles;

/**
 * @author ricks
 * @date 2022/11/2915:37
 */
public class Dispatcher extends ClassVisitor {

    private static final MethodHandles.Lookup lookup = MethodHandles.lookup();
    public Dispatcher(int api, ClassVisitor classVisitor) {
        super(api, classVisitor);
    }


    //
//    private static class SingleCase {
//        public static final Dispatcher INSTANCE = new Dispatcher();
//    }
//
//    public static Dispatcher get() {
//        return Dispatcher.SingleCase.INSTANCE;
//    }
//

    @Override
    public final MethodVisitor visitMethod(
            final int access,
            final String name,
            final String descriptor,
            final String signature,
            final String[] exceptions) {
        System.err.println(name + "........");
        return super.visitMethod(access,name,descriptor,signature,exceptions);
    }
    public void dispatcher(Context context) {
        System.err.println("初始状态。。。。。。。。。。" + Dispatcher.class.getClassLoader().getName());
    }


    public static void load(byte[] data) {
        try {
            lookup.defineClass(data);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
//    public static void dispatcher(Context context) {
//        System.err.println("初始状态。。。。。。。。。。" + Dispatcher.class.getClassLoader().getName());
//    }
}
