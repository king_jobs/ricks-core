package org.ricks.net;

import org.ricks.common.exception.IORuntimeException;
import org.ricks.ioc.manager.RpcMethodManager;
import org.ricks.ioc.wrap.RpcMethodWrapper;
import org.ricks.common.lang.Logger;
import org.ricks.net.rpc.RpcPackage;
import org.ricks.net.rpc.stub.RpcStub;
import org.ricks.net.rpc.stub.RpcSyncStub;
import org.ricks.protocol.IPacket;
import org.ricks.common.MapUtils;

import java.util.Objects;
import java.util.concurrent.ConcurrentMap;

/**
 *  路由规则，接口模式 允许业务自定义路由规则
 *  Router 已经知道对端的Session . rpc server to client
 *  TcpClient client = new TcpClient()
 *  TcpSession session = client.start()
 *  NetContext.getRouter().send(session,packet)
 */
public class Router implements IRouter{

    private final ConcurrentMap<Integer, RpcSyncStub> stubMap = MapUtils.newConcurrentHashMap(512);


    /**
     * RPC 服务器消费者
     */
    @Override
    public void receive(AioSession session, IPacket rpc) {
        if(rpc instanceof RpcPackage rpcPackage) {
            if(rpcPackage.isRequest()) {
                receiveRequest(session,rpcPackage);
                return;
            }
            receiveResponse(session,rpcPackage);
        }
    }

    private void receiveRequest(AioSession session, RpcPackage rpcRequest) {
        short cmd = rpcRequest.getCmd();
        RpcMethodWrapper methodDefinition = RpcMethodManager.getRpcHandler(cmd);
        if (Objects.isNull(methodDefinition)) {
            Logger.error("RPC 路由错误 rpcCmd:" + cmd);
            return;
        }
        /**
         * 玩家都在固定的actor执行业务逻辑
         * 登录逻辑 单独一个线程处理？
         */
        Object result =  methodDefinition.invoke(rpcRequest);
        if(result instanceof RpcPackage rpcResponse) {
            NetContext.getRouter().send(session,rpcResponse);
        } else if(result instanceof byte[] data) {
            RpcPackage rpcResponse = rpcRequest.valueOf(data);
            NetContext.getRouter().send(session,rpcResponse);
        } else {
            Logger.error("RPC请求返回未知数据类型 ！");
        }
    }

    private void receiveResponse(AioSession session, RpcPackage rpcResponse) {
        //响应
        RpcStub stub = NetContext.getRouter().getRpcStub(rpcResponse.getReqId());;
        // 存根已消失，响应迟到，丢了就行
        if (stub == null) {
            return;
        }
        // 同步执行的RPC
        if (stub.isSync()) {
            stub.submit(rpcResponse);
        }
        // 异步回调
        else {
            stub.callback(rpcResponse);
        }
    }

    @Override
    public void send(AioSession session, IPacket packet) {
        /**
         * 注意IPacket会被DataCodecKit.encode 转换字节流
         * DataCodecKit 编码规则可以自己设置
         */
        session.send(packet);
    }

    /**
     * rpc调用返回 如果做到通用化 ？
     * rpc client to server 使用的是内部协议通信， 游戏服 处理完逻辑
     *
     * @param session     一个网络通信的会话
     * @param data      一个网络通信包，消息体
     *                    如果为null，则不会检查这个class类的协议号是否和返回消息体的协议号相等；
     *                    如果不为null，会检查返回包的协议号。为null的情况主要用在网关。
     * @param ?    参数，主要用来计算一致性hashId。
     *                    1.IConsumer会使用这个参数计算负载到哪个服务提供者；
     *                    2.服务提供者收到请求过后会使用这个参数来计算再哪个线程执行任务；
     *                    综上所述，这个参数会在上面两种情况使用。
     * @return byte[]     返回字节流，至于对端使用什么序列化方式不管。网关只需要转发 ，如果是问答方式就意味序列化方式只知道的。比如：要么使用内部序列化方式，要么使用外部Luban PB FB
     */
    @Override
    public byte[] syncAsk(AioSession session,short cmd, byte[] data) {
        long startTime = System.nanoTime();
        // 1.生成请求唯一ID
        final Integer reqId = session.nextReqId();
        RpcSyncStub stub = new RpcSyncStub(reqId);
        try {
            // 2.绑定存根
            stubMap.put(reqId, stub);

            // 3.发消息
            this.send(session,new RpcPackage(reqId, cmd, data));

            // 4.等待直到拿到结果
            return stub.waitTillResult();
        } catch (Exception e) {
            throw new IORuntimeException(e);
        }finally {
            // 5.解绑回调
            stubMap.remove(reqId);
            float exec = (System.nanoTime() - startTime) / 1000_000 ;
            Logger.info("rpc sync call. reqId="+reqId+", exec="+exec+" ms");
        }
    }

    @Override
    public byte[] asyncAsk(AioSession session, IPacket packet, Object argument) {
        return null;
    }

    public RpcStub getRpcStub(Integer reqId) {
        return stubMap.get(reqId);
    }
}
