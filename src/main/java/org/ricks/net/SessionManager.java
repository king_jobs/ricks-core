package org.ricks.net;

import org.ricks.common.lang.Logger;
import org.ricks.protocol.ConcurrentHashMapLongObject;

import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * session管理，还得进行分组
 */
public class SessionManager {

    /**
     * 作为服务器，被别的客户端连接的Session
     * 如：自己作为网关，那肯定有一大堆客户端连接，他们连接上来后，就会保存下来这些信息。
     * 因此：要全局消息广播，其实要用这个Map
     */
    private final ConcurrentHashMapLongObject<AioSession> serverSessionMap = new ConcurrentHashMapLongObject<>(64);


    /**
     * 作为客户端，连接别的服务器上后，保存下来的Session
     * 如：自己配置了Consumer，说明自己作为消费者将要消费远程接口，就会创建一个TcpClient去连接Provider，那么连接上后，就会保存下来到这个Map中
     */
    private final ConcurrentHashMapLongObject<AioSession> clientSessionMap = new ConcurrentHashMapLongObject<>(8);

    private static final SessionManager INSTANCE = new SessionManager();

    public static SessionManager get() {
        return INSTANCE;
    }


    public void addServerSession(AioSession session) {
        try {
            if (serverSessionMap.containsKey(session.sid())) {
                Logger.error("Server received duplicate [session:{[ip:"+session.getRemoteAddress().toString()+"][sid:"+session.sid()+"]}]");
                return;
            }
            serverSessionMap.put(session.sid(), session);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void removeServerSession(AioSession session) {
        try {
            if (!serverSessionMap.containsKey(session.sid())) {
                Logger.error("[session:{[ip:"+session.getRemoteAddress().toString()+"][sid:"+session.sid()+"]}] does not exist");
                return;
            }
            if (Objects.nonNull(session)) {
                serverSessionMap.remove(session.sid());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public AioSession getServerSession(long sid) {
        return serverSessionMap.get(sid);
    }

    public int serverSessionSize() {
        return serverSessionMap.size();
    }

    public void forEachServerSession(Consumer<AioSession> consumer) {
        serverSessionMap.forEachPrimitive(it -> consumer.accept(it.value()));
    }


    public void addClientSession(AioSession session) {
        try {
            if (clientSessionMap.containsKey(session.sid())) {
                Logger.error("client received duplicate [session:{[ip:"+session.getRemoteAddress().toString()+"][sid:"+session.sid()+"]}]");
                return;
            }
            clientSessionMap.put(session.sid(), session);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        clientSessionChangeId = CLIENT_ATOMIC.incrementAndGet();
    }

    public void removeClientSession(AioSession session) {
        try {
            if (!clientSessionMap.containsKey(session.sid())) {
                Logger.error("[session:{[ip:"+session.getRemoteAddress().toString()+"][sid:"+session.sid()+"]}] does not exist");
                return;
            }
            if (Objects.nonNull(session)) {
                clientSessionMap.remove(session.sid());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
//        clientSessionChangeId = CLIENT_ATOMIC.incrementAndGet();
    }

    public AioSession getClientSession(long sid) {
        return clientSessionMap.get(sid);
    }

    public void forEachClientSession(Consumer<AioSession> consumer) {
        clientSessionMap.forEachPrimitive(it -> consumer.accept(it.value()));
    }

    public int clientSessionSize() {
        return clientSessionMap.size();
    }
}
