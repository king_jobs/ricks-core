package org.ricks.net;


import org.ricks.ioc.IocHolder;
import org.ricks.common.lang.Logger;
import org.ricks.net.rpc.ConsumerConfig;

import java.util.Objects;

/**
 * 网络模块上下文
 * 路由行为已经提炼出来了
 *
 * 下一步，IRouter 作为 Bean 注入 NetContext中，不然就得启动服务调用
 * NetContext.me().setRouter(router); 优先级不高，可以先把节点分配规则弄好，就是获取Session行为
 */
public class NetContext {

    private static NetContext INSTANCE = new NetContext();
    private NetContext(){}
    public static NetContext me() {
        return INSTANCE;
    }

    public void init() {
        IRouter iRouter = IocHolder.getIoc().get(IRouter.class);
        IConsumer iConsumer = IocHolder.getIoc().get(IConsumer.class);
        ConsumerConfig config = IocHolder.getIoc().get(ConsumerConfig.class);
//        Assert.isTrue(Objects.nonNull(iRouter),"RICKS IOC未配置注入IRouter .....");
//        Assert.isTrue(Objects.nonNull(iConsumer),"RICKS IOC未配置注入IConsumer .....");
//        Assert.isTrue(Objects.nonNull(config),"RICKS IOC未配置注入ConsumerConfig .....");
        if(Objects.nonNull(iRouter)) {
            this.router = iRouter;
            Logger.info("IOC load IRouter bean success ! ");
        }
        if(Objects.nonNull(iConsumer)) {
            this.consumer = iConsumer;
            Logger.info("IOC load IConsumer bean success ! ");
        }
        if(Objects.nonNull(consumerConfig)) {
            this.consumerConfig = config;
            Logger.info("IOC load ConsumerConfig bean success ! ");
        }
    }


    /**
     * 使用Router时，使用的send方法的第1个参数指定session发送消息
     * 如：创建了一个TcpClient，那明确得到了这个session，则可以通过这个session发送消息
     */
    private IRouter router;

    private IConsumer consumer;

    private ConsumerConfig consumerConfig;

    public static IRouter getRouter() {
        return INSTANCE.router;
    }

    public void setRouter(IRouter router) {
        this.router = router;
    }

    public static IConsumer getConsumer() {
        return INSTANCE.consumer;
    }

    public void setConsumer(IConsumer consumer) {
        this.consumer = consumer;
    }

    public static ConsumerConfig consumerConfig() {
        return INSTANCE.consumerConfig;
    }

    public void setConsumerConfig(ConsumerConfig consumerConfig) {
        this.consumerConfig = consumerConfig;
    }
}
