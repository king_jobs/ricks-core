package org.ricks.net;

import org.ricks.net.buffer.WriteBuffer;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.ProtocolManager;
import org.ricks.common.TimeUtils;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class AioSession {

    /**
     * Session状态:已关闭
     */
    protected static final byte SESSION_STATUS_CLOSED = 1;
    /**
     * Session状态:关闭中
     */
    protected static final byte SESSION_STATUS_CLOSING = 2;
    /**
     * Session状态:正常
     */
    protected static final byte SESSION_STATUS_ENABLED = 3;


    /**
     * 会话当前状态
     *
     * @see AioSession#SESSION_STATUS_CLOSED
     * @see AioSession#SESSION_STATUS_CLOSING
     * @see AioSession#SESSION_STATUS_ENABLED
     */
    protected byte status = SESSION_STATUS_ENABLED;
    /**
     * 是否读通道以至末尾
     */
    protected boolean eof;
    protected int modCount = 0;

    protected long lastRecieveTime = TimeUtils.currentTimeMillis();

    protected final long sessionId = SessionIdCreator.createId();

    /**
     * 请求ID ,请求响应模式
     */
    private static final AtomicInteger reqId = new AtomicInteger(0);


    /**
     * 附件对象
     */
    private Object attachment;

    /**
     * 获取WriteBuffer用以数据输出
     *
     * @return WriteBuffer
     */
    public abstract WriteBuffer writeBuffer();

    /**
     * 获取读缓冲区对象
     */
    public abstract ByteBuffer readBuffer();

    /**
     * 强制关闭当前AIOSession。
     * <p>若此时还存留待输出的数据，则会导致该部分数据丢失</p>
     */
    public final void close() {
        close(true);
    }

    public abstract void awaitRead();

    /**
     * 继续触发读行为，该方法仅可在异步处理模式下可使用，否则会触发不可预知的异常
     */
    public abstract void signalRead();

    /**
     * 是否立即关闭会话
     *
     * @param immediate true:立即关闭,false:响应消息发送完后关闭
     */
    public abstract void close(boolean immediate);

    /**
     * 获取当前Session的唯一标识
     *
     * @return sessionId
     */
    public long sid() {
        return sessionId;
    }

    public int nextReqId() {
        return reqId.incrementAndGet();
    }

    /**
     * 当前会话是否已失效
     *
     * @return 是否失效
     */
    public boolean isInvalid() {
        return status != SESSION_STATUS_ENABLED;
    }


    /**
     * 获取附件对象
     *
     * @param <A> 附件对象类型
     * @return 附件
     */
    public final <A> A getAttachment() {
        return (A) attachment;
    }

    /**
     * 存放附件，支持任意类型
     *
     * @param <A>        附件对象类型
     * @param attachment 附件对象
     */
    public final <A> void setAttachment(A attachment) {
        this.attachment = attachment;
    }

    /**
     * 获取当前会话的本地连接地址
     *
     * @return 本地地址
     * @throws IOException IO异常
     * @see AsynchronousSocketChannel#getLocalAddress()
     */
    public abstract InetSocketAddress getLocalAddress() throws IOException;

    /**
     * 获取当前会话的远程连接地址
     *
     * @return 远程地址
     * @throws IOException IO异常
     * @see AsynchronousSocketChannel#getRemoteAddress()
     */
    public abstract InetSocketAddress getRemoteAddress() throws IOException;

    /**
     * 获得数据输入流对象。
     * <p>
     * faster模式下调用该方法会触发UnsupportedOperationException异常。
     * </p>
     * <p>
     * MessageProcessor采用异步处理消息的方式时，调用该方法可能会出现异常。
     * </p>
     *
     * @return 输入流
     * @throws IOException IO异常
     */
    public InputStream getInputStream() throws IOException {
        throw new UnsupportedOperationException();
    }

    /**
     * 获取已知长度的InputStream
     *
     * @param length InputStream长度
     * @return 输入流
     * @throws IOException IO异常
     */
    public InputStream getInputStream(int length) throws IOException {
        throw new UnsupportedOperationException();
    }

    public void setLastRecieveTime(long lastRecieveTime) {
        this.lastRecieveTime = lastRecieveTime;
    }

    /**
     * 考虑适用不同的消息处理器，写回客户端对象进行序列化 并且写出对端
     * server to client  proto buffer【跨平台 java <-> c++ or c#】
     * server to server  jdk序列化 ？【内部通信没必要适用pb ,而是选用性能最佳的序列化工具】
     *
     * cmd + code + data
     * code = 消息状态 0 成功
     * data = 消息包
     *
     * 内部通信进行封装，外部通信 可以自定义GameCodec序列化方式
     * Session writeBuffer();获取写Buffer ,然后按前后协议自定义写出规则
     */
    public void send(Object obj)  {
        ByteBuf buf = new ByteBuf(1024);
        ProtocolManager.write(buf, obj); //对象转换成字节流
        byte[] data = buf.toArray();
        send(data);
    }

    public void send(byte[] data) {
        try {
            WriteBuffer writeBuffer = writeBuffer();
            writeBuffer.writeInt(data.length);
            writeBuffer.write(data);
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            writeBuffer().flush();
        }
    }
}
