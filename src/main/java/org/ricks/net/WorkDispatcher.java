package org.ricks.net;

import org.ricks.common.lang.Logger;
import org.ricks.net.handler.MessageProcessor;
import org.ricks.net.transport.UdpAioSession;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 消息分发器
 */
public class WorkDispatcher implements Runnable {
    public final static RequestTask EXECUTE_TASK_OR_SHUTDOWN = new RequestTask(null, null);
    private final BlockingQueue<RequestTask> taskQueue = new LinkedBlockingQueue<>();
    private final MessageProcessor processor;

    public WorkDispatcher(MessageProcessor processor) {
        this.processor = processor;
    }

    @Override
    public void run() {
        while (true) {
            try {
                RequestTask unit = taskQueue.take();
                if (unit == EXECUTE_TASK_OR_SHUTDOWN) {
                    Logger.info("shutdown thread:"+Thread.currentThread());
                    break;
                }
                processor.process(unit.session, unit.message);
                unit.session.writeBuffer().flush();
            } catch (InterruptedException e) {
                Logger.warn("InterruptedException", e);
            } catch (Exception e) {
                Logger.error(e.getClass().getName(), e);
            }
        }
    }

    /**
     * 任务分发
     */
    public void dispatch(UdpAioSession session, Object request) {
        dispatch(new RequestTask(session, request));
    }

    /**
     * 任务分发
     */
    public void dispatch(RequestTask requestTask) {
        taskQueue.offer(requestTask);
    }

    static class RequestTask {
        private Object message;
        private AioSession session;

        public RequestTask(UdpAioSession session, Object request) {
            this.session = session;
            this.message = request;
        }
    }
}
