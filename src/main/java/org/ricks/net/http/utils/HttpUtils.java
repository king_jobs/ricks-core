/*******************************************************************************
 * Copyright (c) 2017-2020, org.smartboot. All rights reserved.
 * project name: smart-http
 * file name: HttpUtils.java
 * Date: 2020-11-22
 * Author: sandao (zhengjunweimail@163.com)
 ******************************************************************************/

package org.ricks.net.http.utils;

import org.ricks.common.json.JSONObject;
import org.ricks.net.http.HttpRequest;
import org.ricks.net.http.HttpResponse;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.net.URLDecoder;
import java.nio.ByteBuffer;
import java.util.Map;

/**
 * @author 三刀
 * @version V1.0 , 2020/11/22
 */
public class HttpUtils {
    /**
     * 解码URI中的参数
     *
     * @param paramStr http参数字符串： aa=111&bb=222
     * @param paramMap 参数存放Map
     */
    public static void decodeParamString(String paramStr, Map<String, String[]> paramMap) {
        if (StringUtils.isBlank(paramStr)) {
            return;
        }
        String[] uriParamStrArray = StringUtils.split(paramStr, "&");
        for (String param : uriParamStrArray) {
            int index = param.indexOf("=");
            if (index == -1) {
                continue;
            }
            try {
                String key = StringUtils.substring(param, 0, index);
                String value = URLDecoder.decode(StringUtils.substring(param, index + 1), "utf8");
                String[] values = paramMap.get(key);
                if (values == null) {
                    paramMap.put(key, new String[]{value});
                } else {
                    String[] newValue = new String[values.length + 1];
                    System.arraycopy(values, 0, newValue, 0, values.length);
                    newValue[values.length] = value;
                    paramMap.put(key, newValue);
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public static Object[] getMethodArgs(Parameter[] parameterTypes,ByteBuffer bodyBuffer, HttpRequest request, HttpResponse response)  {
        Object[] params = new Object[parameterTypes.length];
        Map<String,? extends Object> requestParams = bodyBuffer != null ? (Map<String, Object>) new JSONObject(new String(bodyBuffer.array())).toMap() : request.getParameters();


        for (int i = 0; i < parameterTypes.length; i++) {
            //构建方法参数
            if (parameterTypes[i].getParameterizedType() == HttpRequest.class) {
                params[i] = request;
            } else if (parameterTypes[i].getParameterizedType() == HttpResponse.class) {
                params[i] = response;
            } else {
                Parameter parameterType = parameterTypes[i];
                ParameterizedType type;
                Class c = null;
                //更具基础类型转换
                String typeName = parameterType.getName();
                Object o = requestParams.get(typeName);
                try {
                    switch (parameterType.getType().getName()) {
                        case "int":
                        case "java.lang.Integer":
                            params[i] = o == null ? 0 : Integer.parseInt(o.toString());
                            break;

                        case "double":
                        case "java.lang.Double":
                            params[i] = o == null ? 0 : Double.parseDouble(o.toString());
                            break;

                        case "long":
                        case "java.lang.Long":
                            params[i] = o == null ? 0 : Long.parseLong(o.toString());
                            break;

                        case "short":
                        case "java.lang.java.lang.Short":
                            params[i] = o == null ? 0 : Short.parseShort(o.toString());
                            break;

                        case "float":
                        case "java.lang.Float":
                            params[i] = o == null ? 0 : Float.parseFloat(o.toString());
                            break;

                        case "boolean":
                        case "java.lang.Boolean":
                            params[i] = o == null ? false : Boolean.valueOf(o.toString());
                            break;
                        case "java.lang.String":
                            params[i] = o.toString();
                            break;
                        case "List":
                        case "java.util.List":
                            Object v = requestParams.get(typeName);
                            type = (ParameterizedType) parameterType.getParameterizedType();
                            c = (Class) type.getActualTypeArguments()[0];
//                            JSON.toBean(o.toString(),c);
                            break;


//                        case "Map":
//                        case "java.util.Map":
//                            JsonObject jsonObject = new JsonObject(requestParams.getOrDefault(typeName,""));
//                            Object o = jsonObject.toBean(parameterType.getType());
//                            Object obj =JSONUtil.toBean(jsonObject.toJSONString(0),parameterType.getType());
//                            System.err.println();
                        default:
//                            params[i] = JSON.toBean(o.toString(),parameterType.getType()); //Map<Object,Object[]> 有问题需要优化
                            System.err.println("");
                            break;
                    }
                } catch (Exception e) {
                    throw e;
                }
            }
        }
        return params;
    }


    public static Object convert(Field field, String res) {
        if (res == null) {
            return null;
        }
        Object object = null;
        try {
            switch (field.getType().getName()) {
                case "int":
                case "java.lang.Integer":
                    object = Integer.parseInt(res);
                    break;

                case "double":
                case "java.lang.Double":
                    object = Double.parseDouble(res);
                    break;

                case "long":
                case "java.lang.Long":
                    object = Long.parseLong(res);
                    break;

                case "short":
                case "java.lang.java.lang.Short":
                    object = Short.parseShort(res);
                    break;

                case "float":
                case "java.lang.Float":
                    object = Float.parseFloat(res);
                    break;

                case "boolean":
                case "java.lang.Boolean":
                    object = Boolean.valueOf(res);
                    break;
                case "java.lang.String":
                    object = res;
                    break;
                default:
                    return null;
            }
        } catch (Exception ignored) {
        }
        return object;
    }
}
