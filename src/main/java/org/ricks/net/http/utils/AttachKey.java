package org.ricks.net.http.utils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

public final class AttachKey<T> {
    public static final int MAX_ATTACHE_COUNT = 128;
    private static final ConcurrentMap<String, AttachKey> NAMES = new ConcurrentHashMap();
    private static final AtomicInteger INDEX_BUILDER = new AtomicInteger(0);
    private final String key;
    private final int index;

    private AttachKey(String key) {
        this.key = key;
        this.index = INDEX_BUILDER.getAndIncrement();
        if (this.index < 0 || this.index >= 128) {
            throw new RuntimeException("too many attach key");
        }
    }

    public static <T> AttachKey<T> valueOf(String name) {
        AttachKey<T> option = (AttachKey)NAMES.get(name);
        if (option == null) {
            option = new AttachKey(name);
            AttachKey<T> old = (AttachKey)NAMES.putIfAbsent(name, option);
            if (old != null) {
                option = old;
            }
        }

        return option;
    }

    public String getKey() {
        return this.key;
    }

    int getIndex() {
        return this.index;
    }
}