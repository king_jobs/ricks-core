package org.ricks.net.http.utils;

public class Attachment {
    private Object[] values = new Object[8];

    public Attachment() {
    }

    public <T> void put(AttachKey<T> key, T value) {
        int index = key.getIndex();
        if (index > this.values.length) {
            Object[] old = this.values;
            int i = 1;

            do {
                i <<= 1;
            } while(i < index);

            this.values = new Object[i];
            System.arraycopy(old, 0, this.values, 0, old.length);
        }

        this.values[index] = value;
    }

    public <T> T get(AttachKey<T> key) {
        return (T) this.values[key.getIndex()];
    }

    public <T> void remove(AttachKey<T> key) {
        this.values[key.getIndex()] = null;
    }
}