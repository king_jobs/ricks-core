package org.ricks.net.http;

class HttpResponseImpl extends AbstractResponse {

    public HttpResponseImpl(HttpRequestImpl httpRequest, Request request) {
        init(httpRequest, new HttpOutputStream(httpRequest, this, request));
    }
}
