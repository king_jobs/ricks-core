package org.ricks.net.http;

import java.util.concurrent.CompletableFuture;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2022/7/2
 */
@Controller
public class DefaultController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String helloWorld() {

        CompletableFuture completableFuture = new CompletableFuture();

        Thread.startVirtualThread(() -> System.err.println(11)).start();

        Thread.ofVirtual().start(() -> System.err.println(111));

        return "hello smart-http-rest";
    }
}
