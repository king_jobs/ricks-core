package org.ricks.net.http;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2022/7/2
 */
public enum RequestMethod {
    GET,HEAD,POST,PUT,PATCH,DELETE,OPTIONS,TRACE
}
