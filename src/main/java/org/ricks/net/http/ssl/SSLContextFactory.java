package org.ricks.net.http.ssl;

import javax.net.ssl.SSLContext;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2022/6/20
 */
public interface SSLContextFactory {
    SSLContext create() throws Exception;
}
