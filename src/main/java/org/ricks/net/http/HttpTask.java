package org.ricks.net.http;

import java.io.IOException;

@FunctionalInterface
public interface HttpTask {

    void task(HttpRequest request, HttpResponse response) throws Throwable;
}
