package org.ricks.net.http;

import org.ricks.ioc.RicksMethod;

import java.lang.annotation.*;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2022/7/2
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@RicksMethod
@Documented
public @interface RequestMapping {
    String value() default "";

    RequestMethod[] method() default {};
}
