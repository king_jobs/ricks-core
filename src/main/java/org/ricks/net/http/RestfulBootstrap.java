//package org.ricks.net.http;
//
//import org.ricks.net.buffer.BufferPagePool;
//import org.ricks.net.handler.Plugin;
//
//import java.net.StandardSocketOptions;
//
//public class RestfulBootstrap {
//    private final HttpBootstrap httpBootstrap = new HttpBootstrap();
//    private final RestfulHandler restfulHandler = new RestfulHandler();
//
//    private RestfulBootstrap() {
//        httpBootstrap.httpHandler(restfulHandler);
//    }
//
//    public static RestfulBootstrap get() throws Exception {
//        return new RestfulBootstrap();
//    }
//
//    public HttpBootstrap addPlugin(Plugin plugin) {
//        httpBootstrap.addPlugin(plugin);
//        return httpBootstrap;
//    }
//
//    public HttpBootstrap bootstrap() {
//        return httpBootstrap;
//    }
//
//    public void start(int port) {
//        BufferPagePool bufferPagePool = new BufferPagePool(1024 * 1024 * 16, Runtime.getRuntime().availableProcessors() + 1, true);
//        httpBootstrap.setReadBufferSize(1024 * 1024);
//        httpBootstrap.setWriteBuffer(4096, 512);
//        httpBootstrap.setBufferFactory(() -> bufferPagePool);
//        httpBootstrap.setOption(StandardSocketOptions.SO_RCVBUF,1024 * 1024);
//        httpBootstrap.setOption(StandardSocketOptions.SO_REUSEADDR,true);
//        httpBootstrap.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
//        httpBootstrap.start(port);
//    }
//}
