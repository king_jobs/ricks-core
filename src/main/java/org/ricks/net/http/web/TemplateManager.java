package org.ricks.net.http.web;

import org.ricks.common.StrUtil;

import java.util.Map;

/**
 * @author ricks
 * @Description:模板管理者
 * 渲染模板
 * 接口可以多种实现，抽象方法只能继承一个
 * @date 2023/1/2913:24
 */
public abstract class TemplateManager<T> {

    public static String templateBeanName = "template";
    protected T t;

    public TemplateManager() {
        t = init();
        templateBeanName = StrUtil.lowerFirst(getClass().getSimpleName());
    }

    public abstract <T> T init();
    public abstract String renderToString(String html, Map map);
}
