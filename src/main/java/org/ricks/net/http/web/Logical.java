package org.ricks.net.http.web;

/**
 * 处理逻辑, 多个权限使用AND还是OR
 * @author hxm
 */
public enum Logical {
    AND, OR
}
