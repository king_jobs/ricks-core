package org.ricks.net.http.web.router;

import org.ricks.common.lang.Logger;
import org.ricks.net.http.RequestMethod;
import org.ricks.net.http.web.MethodNotSupportException;
import org.ricks.net.http.web.PatternUri;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author hxm
 */
public class RouterManager {

    /**
     * 池子 <uri,<method,method_info>>
     */
    private final static Map<String, Map<String, RouterInfo>> router2 = new ConcurrentHashMap<>();

    /**
     * 记录url是否是需要url正则匹配的
     * 池子 <uri,<method,method_info>>
     */
    private final static Map<String, Map<String, PatternUri>> ISPAURI = new ConcurrentHashMap<>();

    /**
     * 存储路由权限
     */
    private final static Map<String, Map<String, RouterPermission>> routerPermission = new ConcurrentHashMap<>();


    private static Map<String, RouterPermission> routerPermission(String method) {
        Map<String, RouterPermission> stringRouterPermissionMap = routerPermission.get(method);
        if (stringRouterPermissionMap == null) {
            stringRouterPermissionMap = new ConcurrentHashMap<>();
            routerPermission.put(method, stringRouterPermissionMap);
        }
        return stringRouterPermissionMap;
    }


    public static void addRouter(RouterInfo routerInfo) {
        if (routerInfo != null) {
            String url = routerInfo.getUrl();
            //对URL检查是否是正则的 如果是正则的就进行替换为正则的方便后期校验
            List<String> pattern = isPattern(url);
            if (pattern.size() > 0) {
                String s = url;
                for (int i = 0; i < pattern.size(); i++) {
                    if (i == pattern.size() - 1) {
                        s = s.replaceAll("\\{" + pattern.get(i) + "\\}", "(.+)");
                    } else {
                        s = s.replaceAll("\\{" + pattern.get(i) + "\\}", "(.+?)");
                    }
                }
                s = "^" + s;
                Map<String, PatternUri> ispauri = ISPAURI.get(s);
                if (ispauri == null) {
                    ispauri = new ConcurrentHashMap<>();
                    ISPAURI.put(s, ispauri);
                }
                ispauri.put(routerInfo.getReqMethodName(), new PatternUri(pattern, url, s, routerInfo.getReqMethodName()));
            }

            Map<String, RouterInfo> httpMethodRouterInfoMap = router2.get(url);
            if (httpMethodRouterInfoMap == null) {
                httpMethodRouterInfoMap = new ConcurrentHashMap<>();
                router2.put(url, httpMethodRouterInfoMap);
            }
            if (httpMethodRouterInfoMap.containsKey(routerInfo.getReqMethodName())) {
                Logger.warn("url< "+url+" >映射路径已经存在，可能会影响程序使用，class:"+routerInfo.getaClass().getName()+",method:"+ routerInfo.getMethod().getName());
            }
            httpMethodRouterInfoMap.put(routerInfo.getReqMethodName(), routerInfo);
        }
    }


    private static List<String> isPattern(String url) {
        String regex = "(\\{.*?\\})";
        Matcher matcher = Pattern.compile(regex).matcher(url);
        List<String> patterns = new ArrayList<>();
        while (matcher.find()) {
            String group = matcher.group(1);
            if (group != null) {
                patterns.add(group.substring(1, group.length() - 1));
            }
        }
        return patterns;
    }

    private static PatternUri isPattern(String url, String method) throws MethodNotSupportException {
        for (String next : ISPAURI.keySet()) {
            if (Pattern.compile(next).matcher(url).find()) {
                Map<String, PatternUri> stringPatternUriMap = ISPAURI.get(next);
                if (stringPatternUriMap!=null){
                    PatternUri patternUri = stringPatternUriMap.get(method);
                    if (patternUri!=null){
                        return patternUri;
                    }
                    throw new MethodNotSupportException();
                }
            }
        }
        return null;
    }

    public static void addPermission(RouterPermission routerPermission) {
        if (routerPermission != null) {
            String url = routerPermission.getUrl();
            Map<String, RouterPermission> stringRouterPermissionMap = routerPermission(routerPermission.getReqMethodName());
            if (stringRouterPermissionMap.containsKey(url)) {
                Logger.warn("url< "+url+" >权限映射已经存在，可能会影响程序使用");
            }
            stringRouterPermissionMap.put(url, routerPermission);
        }
    }

    public static RouterPermission getRouterPermission(String url, String requestType) {
        Map<String, RouterPermission> stringRouterPermissionMap = routerPermission(requestType);
        RouterPermission routerPermission = stringRouterPermissionMap.get(url);
        if (routerPermission != null) {
            return routerPermission;
        } else {
            try {
                PatternUri pattern = isPattern(url, requestType);
                if (pattern != null) {
                    return stringRouterPermissionMap.get(pattern.getOrgUrl());
                }
            }catch (MethodNotSupportException e){
                return null;
            }
        }
        return null;
    }

    public static List<RouterPermission> getRouterPermissions() {
        List<RouterPermission> permissions = new ArrayList<>();
        RequestMethod[] requestMethodAll = RequestMethod.values();
        for (RequestMethod s : requestMethodAll) {
            Map<String, RouterPermission> stringRouterPermissionMap = routerPermission(s.name());
            stringRouterPermissionMap.forEach((a, b) -> permissions.add(b));
        }
        return permissions;
    }
}
