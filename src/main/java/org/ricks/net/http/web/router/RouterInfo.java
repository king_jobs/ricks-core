package org.ricks.net.http.web.router;

import org.ricks.net.http.RequestMethod;

import java.lang.reflect.Method;

/**
 * @author hxm
 */
public class RouterInfo {

    private String url;
    private Method method;
    private RequestMethod requestMethod;
    private Class<?> aClass;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }


    public Class<?> getaClass() {
        return aClass;
    }

    public void setaClass(Class<?> aClass) {
        this.aClass = aClass;
    }


    public String getReqMethodName() {
        return requestMethod.name();
    }
}
