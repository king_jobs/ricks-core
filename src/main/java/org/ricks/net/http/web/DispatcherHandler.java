//package org.ricks.net.http.web;
//
//import org.ricks.ioc.IocHolder;
//import org.ricks.ioc.manager.HttpMethodManager;
//import org.ricks.ioc.wrap.HttpMethodWrapper;
//import org.ricks.json.JSON;
//import org.ricks.lang.Logger;
//import org.ricks.net.http.*;
//import org.ricks.net.http.utils.HttpUtils;
//import org.ricks.net.http.web.router.RouterManager;
//import org.ricks.net.http.web.router.RouterPermission;
//import org.ricks.utils.ExceptionUtil;
//
//import java.util.List;
//import java.util.Objects;
//import java.util.concurrent.CompletableFuture;
//
///**
// * @author ricks
// * @Description:
// * @date 2023/1/2811:42
// *
// * todo 使用自己得一套模式来使用
// */
//public class DispatcherHandler {
//
//    private final static StaticHandler STATIC_HANDLER = new StaticHandler();
//
//    /**
//     * 静态文件的处理
//     *
//     * @param hServerContext
//     * @return
//     */
//    static WebContext staticFile(WebContext hServerContext) {
//        StaticFile handler = STATIC_HANDLER.handle(hServerContext.getRequest().getRequestURI());
//        if (handler != null) {
//            hServerContext.setStaticFile(true);
//            hServerContext.setStaticFile(handler);
//        }
//        return hServerContext;
//    }
//
//    /**
//     * 拦截器
//     *
//     * @param hServerContext
//     * @return
//     */
//    public static WebContext filter(WebContext hServerContext) {
//
//        /**
//         * 如果静态文件就跳过当前的处理，否则就去执行控制器的方法
//         */
//        if (hServerContext.isStaticFile()) {
//            return hServerContext;
//        }
//
//        /**
//         * 检测下Filter的过滤哈哈
//         */
//        if (IocHolder.getIoc().get(FilterAdapter.class) != null) {
//            try {
//                List<FilterAdapter> listBean = IocHolder.getBeans(FilterAdapter.class);
//                for (FilterAdapter filterAdapter : listBean) {
//                    filterAdapter.doFilter(hServerContext);
//                    if (hServerContext.getResponse().hasData()) {
//                        break;
//                    }
//                }
//            } catch (Exception e) {
//                throw new RuntimeException( "拦截器异常", e);
//            }
//        }
//        return hServerContext;
//    }
//
//
//    /**
//     * 构建返回对象
//     *
//     * @param e
//     * @return
//     */
//    static byte[] handleException(Throwable e) {
//        try {
//            String errorMsg = ExceptionUtil.getMessage(e);
//            //一般是走自己的异常
//            if (e.getCause() instanceof WebException webException) {
//                if (webException.getHttpCode() == HttpStatus.NOT_FOUND.value()) {
//                    Logger.error(webException.getErrorDescription());
//                } else {
//                    Logger.error(ExceptionUtil.getMessage(webException.getThrowable()));
//                }
//                return ExceptionUtil.getMessage(e).getBytes();
//            } else {
//                Logger.error(ExceptionUtil.getMessage(e));
//                return errorMsg.getBytes();
//            }
//        }  catch( Exception e2){
//            return ExceptionUtil.getMessage(e2).getBytes();
//        }
//    }
//
//    static void write(HttpResponse response, CompletableFuture<WebContext> future, byte[] data) {
//        try {
//            byte[] bytes = data;
//            if(Objects.isNull(data)) bytes = "".getBytes();
//            //如果在controller中已经触发过write，此处的contentLength将不准，且不会生效
//            response.setContentLength(bytes.length);
//            response.write(bytes);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            future.complete(null);
//        }
//    }
//
//    /**
//     * 权限验证
//     *
//     * @param hServerContext
//     * @return
//     */
//    static WebContext permission(WebContext hServerContext) {
//
//        //如果是静态文件就不进行权限验证了
//        if (hServerContext.isStaticFile()) {
//            return hServerContext;
//        }
//        /**
//         * 检查下Webkit是否设置了值
//         *  文本输出
//         *  下载
//         *  重定向
//         *
//         */
//        if (hServerContext.getResponse().hasData()) {
//            return hServerContext;
//        }
//
//        List<PermissionAdapter> listBean = IocHolder.getBeans(PermissionAdapter.class);
//        if (listBean != null) {
//            RouterPermission routerPermission = RouterManager.getRouterPermission(hServerContext.getRequest().getRequestURI(), hServerContext.getRequest().getContentType());
//            if (routerPermission != null) {
//                for (PermissionAdapter permissionAdapter : listBean) {
//                    if (routerPermission.getRequiresPermissions() != null) {
//                        try {
//                            permissionAdapter.requiresPermissions(routerPermission.getRequiresPermissions(), hServerContext.getRequest(),hServerContext.getResponse());
//                            if (hServerContext.getResponse().hasData()) {
//                                break;
//                            }
//                        } catch (Exception e) {
//                            throw new WebException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "权限验证", e, hServerContext.getRequest(),hServerContext.getResponse());
//                        }
//                    }
//                    if (routerPermission.getRequiresRoles() != null) {
//                        try {
//                            permissionAdapter.requiresRoles(routerPermission.getRequiresRoles(), hServerContext.getRequest(),hServerContext.getResponse());
//                            if (hServerContext.getResponse().hasData()) {
//                                break;
//                            }
//                        } catch (Exception e) {
//                            throw new WebException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "角色验证", e, hServerContext.getRequest(),hServerContext.getResponse());
//                        }
//                    }
//                    if (routerPermission.getSign() != null) {
//                        try {
//                            permissionAdapter.sign(routerPermission.getSign(), hServerContext.getRequest(),hServerContext.getResponse());
//                            if (hServerContext.getResponse().hasData()) {
//                                break;
//                            }
//                        } catch (Exception e) {
//                            throw new WebException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Sign验证", e, hServerContext.getRequest(),hServerContext.getResponse());
//                        }
//                    }
//                }
//            }
//        }
//        return hServerContext;
//    }
//
//    public static WebContext doHandle(WebContext context) {
//        Object rsp = null;
//        HttpRequest request = context.getRequest();
//        HttpResponse response = context.getResponse();
//        HttpMethodWrapper handler = HttpMethodManager.getHttpHandler(request.getRequestURI());
//        if(handler != null) {
//            Object[] params = HttpUtils.getMethodArgs(handler.getParameters(),null,request,response);
//            rsp = handler.invoke(params);
//        }
//        if(rsp != null) {
//            if (rsp instanceof String str) {
//                context.getResponse().sendText(str);
//            } else {
//                response.setHeader(HeaderNameEnum.CONTENT_TYPE.getName(), HeaderValueEnum.APPLICATION_JSON.getName());
//                context.getResponse().sendJson(JSON.toJsonStr(rsp));
//            }
//        } else if(response.getResult() == null) response.sendText("");
//        return context;
//    }
//
//    static byte[] buildResponse(WebContext context) {
//        try {
//            byte[] bytes = null;
//            /**
//             * 如果是文件特殊处理下,是静态文件，同时需要下载的文件
//             */
//            if (context.isStaticFile()) {
//                //显示型的静态文件
//                bytes = BuildResponse.buildStaticShowType(context);
//            } else if (context.getResponse().isDownload()) {
//                //控制器下载文件的
//                bytes = BuildResponse.buildControllerDownloadType(context.getResponse());
//            } else if (context.getResponse().getResult() != null) {
//                String tempResult = context.getResponse().getResult();
//                return tempResult.getBytes();
//            }
//            return bytes;
//        } catch (Exception e) {
//            throw new WebException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "构建Response对象异常", e, context.getRequest(),context.getResponse());
//        }
//    }
//
//}
