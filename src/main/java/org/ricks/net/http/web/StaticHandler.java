/*******************************************************************************
 * Copyright (c) 2017-2020, org.smartboot. All rights reserved.
 * project name: smart-http
 * file name: StaticResourceHandle.java
 * Date: 2020-01-01
 * Author: sandao (zhengjunweimail@163.com)
 ******************************************************************************/

package org.ricks.net.http.web;

import org.ricks.net.http.utils.StringUtils;
import org.ricks.common.PathUtil;

import java.io.*;
import java.net.URL;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * 静态资源加载Handle
 *
 * @author 三刀
 * @version V1.0 , 2018/2/7
 */
public class StaticHandler {

    private final static String BASE_PATH = "/static";

    /**
     * 此处的静态文件缓存是非常有必要的，直接拉低了整体QPS.
     */
    private final static CopyOnWriteArraySet<String> STATIC_FILE_URI = new CopyOnWriteArraySet<>();

    static {
        /**
         * 把静态文件递归遍历出来.
         */
        String classPath = PathUtil.currPath();;
        developFile(classPath+BASE_PATH,(classPath + BASE_PATH).replaceAll("\\\\","/"));
    }

    private static final int READ_BUFFER = 1024 * 1024;
    private static final String URL_404 =
            "<html>" +
                    "<head>" +
                    "<title>smart-http 404</title>" +
                    "</head>" +
                    "<body><h1>smart-http 找不到你所请求的地址资源，404</h1></body>" +
                    "</html>";


    public StaticFile handle(String uri) {
        if (StringUtils.endsWith(uri, "/")) {
            uri += "index.html";
        }
        if (!STATIC_FILE_URI.contains(uri)){
            return null;
        }
        InputStream input = StaticHandler.class.getResourceAsStream(BASE_PATH + uri);
        if (input != null) {
            return buildStaticFile(input, uri);
        }
        return null;
    }

    /**
     * 构建一个静态文件对象
     *
     * @param input
     * @param url
     * @return
     */
    private StaticFile buildStaticFile(InputStream input, String url) {
        StaticFile staticFile;
        try {
            //获取文件大小
            int available = input.available();
            staticFile = new StaticFile();
            staticFile.setSize(available);
            //获取文件名
            int i = url.lastIndexOf("/");
            int i1 = url.lastIndexOf(".");
            if (i > -1 && i1 > 0) {
                String fileName = url.substring(i + 1, url.length());
                String[] split = fileName.split("\\.");
                staticFile.setFileName(fileName);
                //设置文件是下载还
                staticFile.setFileType(split[split.length - 1]);
            } else {
                return null;
            }
            staticFile.setInputStream(input);
        } catch (Exception e) {
            throw new RuntimeException("获取文件大小异常", e);
        }
        return staticFile;
    }

    private static void developFile(String path,String re) {
        File file = new File(path);
        if (file.exists()) {
            File[] files = file.listFiles();
            if (null != files) {
                for (File file2 : files) {
                    if (file2.isDirectory()) {
                        developFile(file2.getAbsolutePath(),re);
                    } else {
                        try {
                            URL url = new URL("file:" + file2.getAbsolutePath());
                            STATIC_FILE_URI.add(url.getPath().replaceAll(re,""));
                        }catch (Exception ignored){}
                    }
                }
            }
        }
    }
}
