package org.ricks.net.http.web;

import org.ricks.net.http.HttpRequest;
import org.ricks.net.http.HttpResponse;
import org.ricks.net.http.web.router.RouterManager;
import org.ricks.net.http.web.router.RouterPermission;

import java.util.List;

/**
 * 权限验证接口
 * @author hxm
 */
public interface PermissionAdapter {

    /**
     * 自定义实现权限检查
     * @param requiresPermissions
     * @throws Exception
     */
    void requiresPermissions(RequiresPermissions requiresPermissions, HttpRequest request, HttpResponse response) throws Exception;

    /**
     * 自定义实现角色检查
     *
     * @param requiresRoles
     * @throws Exception
     */
    void requiresRoles(RequiresRoles requiresRoles, HttpRequest request, HttpResponse response) throws Exception;

    /**
     * 自定义实现sign检查
     *
     * @param sign
     * @throws Exception
     */
    void sign(Sign sign, HttpRequest request, HttpResponse response) throws Exception;


    /**
     * 获取所有的权限，可以用于同步后台数据库，方便操作
     *
     * @return
     */
    static List<RouterPermission> getRouterPermissions() {
        return RouterManager.getRouterPermissions();
    }
}
