package org.ricks.net.http.web.router;

import org.ricks.net.http.RequestMethod;
import org.ricks.net.http.web.RequiresPermissions;
import org.ricks.net.http.web.RequiresRoles;
import org.ricks.net.http.web.Sign;

/**
 * @author hxm
 */
public class RouterPermission {
    private String url;
    private RequestMethod requestMethod;
    private RequiresPermissions requiresPermissions;
    private RequiresRoles requiresRoles;
    private Sign sign;
    private String controllerPackageName;
    private String controllerName;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public RequiresPermissions getRequiresPermissions() {
        return requiresPermissions;
    }

    public void setRequiresPermissions(RequiresPermissions requiresPermissions) {
        this.requiresPermissions = requiresPermissions;
    }

    public RequiresRoles getRequiresRoles() {
        return requiresRoles;
    }

    public void setRequiresRoles(RequiresRoles requiresRoles) {
        this.requiresRoles = requiresRoles;
    }

    public Sign getSign() {
        return sign;
    }

    public void setSign(Sign sign) {
        this.sign = sign;
    }


    public String getControllerPackageName() {
        return controllerPackageName;
    }

    public void setControllerPackageName(String controllerPackageName) {
        this.controllerPackageName = controllerPackageName;
    }

    public String getControllerName() {
        return controllerName;
    }

    public void setControllerName(String controllerName) {
        this.controllerName = controllerName;
    }

    public String getReqMethodName() {
        return requestMethod.name();
    }
}
