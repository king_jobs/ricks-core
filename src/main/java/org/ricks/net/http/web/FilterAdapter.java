package org.ricks.net.http.web;


/**
 * 拦截器
 * @author hxm
 */
public interface FilterAdapter {

    /**
     * 所有拦截先走这里过滤一次
     * @throws Exception
     */
    void doFilter(WebContext context) throws Exception;
}
