package org.ricks.net.http.web;

import org.ricks.common.lang.Logger;
import org.ricks.net.http.HeaderNameEnum;
import org.ricks.net.http.HttpResponse;
import org.ricks.common.ExceptionUtil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * 构建返回对象的数据工具集合
 *
 * @author hxm
 */
public class BuildResponse {

    private static byte[] streamToBytes(InputStream inputStream) {
        byte[] bytes = null;
        ByteArrayOutputStream babs = new ByteArrayOutputStream();
        int size = 0;
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buffer)) > -1) {
                size += len;
                babs.write(buffer, 0, len);
            }
            babs.flush();
            bytes = babs.toByteArray();
            inputStream.close();
        } catch (IOException ignored) {
            Logger.error(ExceptionUtil.getMessage(ignored));
        }
        return bytes;
    }

    /**
     * 静态文件
     *
     * @param context
     * @return
     */
    public static byte[] buildStaticShowType(WebContext context) {
        HttpResponse response = context.getResponse();
        InputStream inputStream = context.getStaticFile().getInputStream();
        response.setHeader(HeaderNameEnum.CONTENT_TYPE.getName(), context.getStaticFile().getFileHead() + ";charset=UTF-8");
       return streamToBytes(inputStream);
    }

//    /**
//     * 控制下载类型的文件
//     *
//     * @param response
//     * @return
//     */
//    public static byte[] buildControllerDownloadType(HttpResponse response) {
//        byte[] bytes = null;
//        if (response.getFile() == null) {
//            InputStream inputStream = response.getInputStream();
//            bytes = streamToBytes(inputStream);
//        } else {
//            //File类型
//            try {
//                bytes = streamToBytes(new FileInputStream(response.getFile()));
//            } catch (Exception e) {
//
//            }
//        }
//
//        response.setHeader(response.getContentType(), MimeType.getFileType(response.getFileName()) + ";charset=UTF-8");
//        response.setHeader(HeaderNameEnum.CONTENT_LENGTH.getName(), String.format("attachment; filename=\"%s\"", response.getFileName()));
//        return bytes;
//    }

}
