//package org.ricks.net.http.web;
//
//import org.ricks.net.http.HttpRequest;
//import org.ricks.net.http.HttpResponse;
//import org.ricks.net.http.RestfulHandler;
//
//import java.io.IOException;
//import java.util.Map;
//import java.util.Objects;
//import java.util.concurrent.*;
//
///**
// * @author ricks
// * @Description:解析web
// * @date 2023/1/2811:06
// */
//public class WebHandler extends RestfulHandler {
//
//    private static final Map<Thread, Executor> cache = new ConcurrentHashMap<>();
//
//    @Override
//    public void handle(HttpRequest request, HttpResponse response) throws IOException {
//        WebContext webContext = new WebContext();
//        webContext.setRequest(request);
//        webContext.setResponse(response);
////        Executor executor = getExecutor();
////        CompletableFuture<WebContext> future = CompletableFuture.completedFuture(webContext);
////
////        future.thenApplyAsync(req -> DispatcherHandler.staticFile(webContext),executor)
////                .thenApplyAsync(DispatcherHandler::filter,executor)
////                .thenApplyAsync(DispatcherHandler::permission,executor)
////                .thenApplyAsync(DispatcherHandler::doHandle,executor)
////                .thenApplyAsync(DispatcherHandler::buildResponse,executor)
////                .exceptionally(DispatcherHandler::handleException)
////                .thenAcceptAsync(msg -> DispatcherHandler.write(response, future, msg),executor);
//        DispatcherHandler.staticFile(webContext);
//        DispatcherHandler.filter(webContext);
//        DispatcherHandler.doHandle(webContext);
//        byte[] bytes = DispatcherHandler.buildResponse(webContext);
//        write(response,bytes);
//    }
//
////    private Executor getExecutor() {
////        Executors.newSingleThreadExecutor(new ThreadFactory() {
////            @Override
////            public Thread newThread(Runnable r) {
////                return null;
////            }
////        })
////        return cache.getOrDefault(Thread.currentThread(),Executors.newSingleThreadExecutor());
////    }
//
//    @Override
//    public void write(HttpResponse response, Object data) {
//        try {
//            if(data instanceof byte[] bytes) {
//                if (Objects.isNull(data)) bytes = "".getBytes();
//                //如果在controller中已经触发过write，此处的contentLength将不准，且不会生效
//                response.setContentLength(bytes.length);
//                response.write(bytes);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//}
