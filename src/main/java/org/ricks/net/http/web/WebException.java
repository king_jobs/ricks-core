package org.ricks.net.http.web;

import org.ricks.net.http.HttpRequest;
import org.ricks.net.http.HttpResponse;
import org.ricks.net.http.HttpStatus;

/**
 * 业务异常
 * @author hxm
 */
public class WebException extends RuntimeException {

    /**
     * 错误状态码
     */
    private Integer httpCode;
    /**
     * 错误类型
     */
    private String errorDescription;
    /**
     * req和resp
     */
    private HttpRequest request;
    private HttpResponse response;

    /**
     * 真实的报错
     */
    private Throwable throwable;

    public WebException() {
        super();
    }

    public WebException(Integer httpCode, String errorDescription, Throwable throwable, HttpRequest request,HttpResponse response) {
        super();
        this.httpCode = httpCode;
        this.errorDescription = errorDescription;
        this.request=request;
        this.response=response;
        this.throwable=throwable;
    }

    public WebException(String s) {
        super(s);
    }

    public Integer getHttpCode() {
        if (httpCode != null) {
            return httpCode;
        } else {
            return HttpStatus.INTERNAL_SERVER_ERROR.value();
        }
    }


    public String getErrorDescription() {
        return errorDescription;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public Throwable getThrowable() {
        return throwable;
    }
}
