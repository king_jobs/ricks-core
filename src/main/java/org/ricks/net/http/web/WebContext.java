package org.ricks.net.http.web;

import org.ricks.net.http.HttpRequest;
import org.ricks.net.http.HttpResponse;

/**
 * @author ricks
 * @Description:
 * @date 2023/1/2811:22
 */
public class WebContext {

    private HttpRequest request;

    private HttpResponse response;

    private boolean isStaticFile;

    private StaticFile staticFile;

    public HttpRequest getRequest() {
        return request;
    }

    public void setRequest(HttpRequest request) {
        this.request = request;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public void setResponse(HttpResponse response) {
        this.response = response;
    }

    public boolean isStaticFile() {
        return isStaticFile;
    }

    public void setStaticFile(boolean staticFile) {
        isStaticFile = staticFile;
    }

    public StaticFile getStaticFile() {
        return staticFile;
    }

    public void setStaticFile(StaticFile staticFile) {
        this.staticFile = staticFile;
    }
}
