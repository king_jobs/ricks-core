package org.ricks.net.http;

import org.ricks.common.lang.Logger;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.function.BiConsumer;

/**
 * @author ricks
 * @Description:http & https
 * https todo
 * @date 2023/1/1216:33
 */
public class RestfulHandler extends HttpServerHandler {

    private final HttpRouteHandler httpRouteHandler;
    private BiConsumer<HttpRequest, HttpResponse> inspect = (httpRequest, response) -> {
    };
    private final MethodInterceptor interceptor = MethodInvocation::proceed;


    public RestfulHandler(HttpServerHandler defaultHandler) {
        this.httpRouteHandler = defaultHandler != null ? new HttpRouteHandler(defaultHandler) : new HttpRouteHandler();
    }

    public void addController(Object object) {
        Class<?> clazz = object.getClass();
        String rootPath = clazz.getDeclaredAnnotation(Controller.class).value();
        for (Method method : clazz.getDeclaredMethods()) {
            RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
            if (requestMapping == null) {
                continue;
            }
            String mappingUrl = getMappingUrl(rootPath, requestMapping);
            Logger.info("restful mapping: "+mappingUrl+" -> " + clazz.getName() + "." + method.getName());
            httpRouteHandler.route(mappingUrl, new ControllerHandler(method, object, inspect, interceptor));
        }
    }


    private String getMappingUrl(String rootPath, RequestMapping requestMapping) {
        StringBuilder sb = new StringBuilder("/");
        if (rootPath.length() > 0) {
            if (rootPath.charAt(0) == '/') {
                sb.append(rootPath, 1, rootPath.length());
            } else {
                sb.append(rootPath);
            }
        }
        if (requestMapping.value().length() > 0) {
            char sbChar = sb.charAt(sb.length() - 1);
            if (requestMapping.value().charAt(0) == '/') {
                if (sbChar == '/') {
                    sb.append(requestMapping.value(), 1, requestMapping.value().length());
                } else {
                    sb.append(requestMapping.value());
                }
            } else {
                if (sbChar != '/') {
                    sb.append('/');
                }
                sb.append(requestMapping.value());
            }
        }
        return sb.toString();
    }

    @Override
    public void onHeaderComplete(Request request) throws IOException {
        httpRouteHandler.onHeaderComplete(request);
    }

    public void setInspect(BiConsumer<HttpRequest, HttpResponse> inspect) {
        this.inspect = inspect;
    }

}
