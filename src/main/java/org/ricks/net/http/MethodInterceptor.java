package org.ricks.net.http;

public interface MethodInterceptor {
    Object invoke(MethodInvocation invocation) throws Throwable;
}

