package org.ricks.net.http;

import java.lang.annotation.*;

/**
 * 正常反射无法获取方法参数名称，只能在jvm参数增加  -parameters 反射才能拿到方法参数名称
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Param {

    /**
     * 参数名称
     *
     * @return 参数名称
     */
    String value();

}
