package org.ricks.net;

/**
 * @author ricks
 * @Description:通信通道接口
 * @date 2023/2/914:14
 */
public interface ChannelContext {
    /**
     * 发送响应给请求端
     *
     * @param responseObject 响应对象
     */
    void sendResponse(Object responseObject);
}
