package org.ricks.net;

import org.ricks.net.rpc.stub.RpcStub;
import org.ricks.protocol.IPacket;

/**
 * 接口，业务可以定制 根据业务需求 写符合业务得规则
 * 此时已经根据 策略获取到数据传输得session
 *
 * 既然使用了RPC 内部通信直接RPC方式进行
 * rpc 本质就是封装远程调用过程
 */
public interface IRouter {


    void receive(AioSession session, IPacket rpc);


    /**
     * EN:send() and receive() are the entry points for sending and receiving messages, which can be called directly
     * CN:send()和receive()是消息的发送和接收的入口，可以直接调用
     */
    void send(AioSession session, IPacket packet);


    /**
     * attention：syncAsk和asyncAsk只能客户端调用
     * 同一个客户端可以同时发送多条同步或者异步消息。
     * 服务器对每个请求消息也只能回复一条消息，不能在处理一条不同或者异步消息的时候回复多条消息。
     *
     * @param session     一个网络通信的会话
     * @param data      一个网络通信包，至于序列化方式不管
     * @param 《需不需要考虑一下？    参数，主要用来计算一致性hashId。
     *                    1.IConsumer会使用这个参数计算负载到哪个服务提供者；
     *                    2.服务提供者收到请求过后会使用这个参数来计算再哪个线程执行任务；
     *                    综上所述，这个参数会在上面两种情况使用。
     * @return 服务器返回的消息Response
     * @throws Exception 如果超时或者其它异常
     */
    byte[] syncAsk(AioSession session,short cmd, byte[] data);

    byte[] asyncAsk(AioSession session, IPacket packet,   Object argument);

    /**
     * 请求ID
     * @param reqId
     * @return
     */
    public RpcStub getRpcStub(Integer reqId);
}
