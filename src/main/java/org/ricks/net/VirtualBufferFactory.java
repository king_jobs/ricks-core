/*******************************************************************************
 * Copyright (c) 2017-2021, org.smartboot. All rights reserved.
 * project name: smart-socket
 * file name: VirtualBufferFactory.java
 * Date: 2021-01-31
 * Author: sandao (zhengjunweimail@163.com)
 *
 ******************************************************************************/

package org.ricks.net;

import org.ricks.net.buffer.BufferPage;
import org.ricks.net.buffer.VirtualBuffer;

/**
 * @author 三刀（zhengjunweimail@163.com）
 * @version V1.0 , 2021/1/31
 */
public interface VirtualBufferFactory {
    VirtualBuffer newBuffer(BufferPage bufferPage);
}
