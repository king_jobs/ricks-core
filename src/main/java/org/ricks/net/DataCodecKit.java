package org.ricks.net;

import org.ricks.net.codec.DataCodec;
import org.ricks.net.codec.JsonDataCodec;

/**
 * @author ricks
 * @Description:业务消息的编解码器
 * @date 2023/1/410:37
 */
public class DataCodecKit {

    //本来以为是使用pb协议，梅想使用luban 所以业务消息解析器默认json 。后续使用什么协议动态set
    static DataCodec dataCodec = new JsonDataCodec();

    /**
     * 将业务参数编码成字节数组
     *
     * @param data 业务参数 (指的是请求端的请求参数)
     * @return bytes
     */
    public static byte[] encode(Object data) {
        return dataCodec.encode(data);
    }

    /**
     * 将字节数组解码成对象
     *
     * @param data       业务参数 (指的是请求端的请求参数)
     * @param paramClazz clazz
     * @param <T>        t
     * @return 业务参数
     */
    public static <T> T decode(byte[] data, Class<T> paramClazz) {
        return dataCodec.decode(data, paramClazz);
    }

    public static void setDataCodec(DataCodec dataCodec) {
        DataCodecKit.dataCodec = dataCodec;
    }

}
