package org.ricks.net.handler;

import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/11/416:01
 */
public interface Plugin<T> extends NetMonitor {

    /**
     * 对请求消息进行预处理，并决策是否进行后续的MessageProcessor处理。
     * 若返回false，则当前消息将被忽略。
     * 若返回true，该消息会正常秩序MessageProcessor.process.
     *
     * @param session
     * @param t
     * @return
     */
    boolean preProcess(AioSession session, T t);


    /**
     * 监听状态机事件
     *
     * @param stateMachineEnum
     * @param session
     * @param throwable
     */
    void stateEvent(StateMachineEnum stateMachineEnum, AioSession session, Throwable throwable);

}