package org.ricks.net.handler;

import org.ricks.common.lang.Logger;
import org.ricks.net.Context;
import org.ricks.ioc.Message;
import org.ricks.ioc.manager.ActionMethodManager;
import org.ricks.ioc.wrap.ActionMethodWrapper;
import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;
import org.ricks.net.buffer.WriteBuffer;
import java.io.IOException;
import java.util.Objects;

/**
 * @author ricks
 * @Description:默认消息处理器
 * 目前方面asm动态生成class switch(cmd) case ....  case ....
 * todo 调整为asm 反射 和 直接调用区别不大。
 * 默认handler asm反射机制
 *
 * 默认路由机制
 * @date 2022/12/2913:29
 */
public class DefaultHandler extends AbstractMessageProcessor<Message<Short,Byte[]>>{

    @Override
    public void process0(AioSession session, Message<Short,Byte[]> msg) {
        ActionMethodWrapper methodDefinition = ActionMethodManager.getActionMethod(msg.getCmd());
        Context context = new Context(session, msg);
        if (Objects.isNull(methodDefinition)) {
            Logger.error("路由错误 ， cmd: "+ msg.getCmd());
        }
        methodDefinition.invoke(context);
    }

    @Override
    public void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {
        switch (stateMachineEnum) {
            case NEW_SESSION: {
                //如果是tcp session 写回客户端，唯一标识给kcp做convId
//                if(session instanceof TcpAioSession tcpAioSession) {
//                    sessionIdToClient(tcpAioSession);
//                }
                Logger.debug(" create session.......");
                break;
            }
            case PROCESS_EXCEPTION:
                Logger.error("process exception", throwable);
//                session.close();
                break;
            case SESSION_CLOSED:
                Logger.debug(" close session.......");
                break;
            case DECODE_EXCEPTION: {
                Logger.warn("decode exception,", throwable);
                break;
            }
        }
    }

    private void sessionIdToClient(TcpAioSession session) {
        WriteBuffer writeBuffer = session.writeBuffer();
        try {
            writeBuffer.writeLong(session.sid());
            writeBuffer.flush();
        } catch (IOException e) {
            Logger.error(" SessionId 写回客户端异常, case:",e);
        }
    }
}
