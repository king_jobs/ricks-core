package org.ricks.net.handler;

import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;
import org.ricks.common.TimeUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/11/416:02
 */
public abstract class HeartPlugin<T> extends AbstractPlugin<T> {
    private static final TimeoutCallback DEFAULT_TIMEOUT_CALLBACK = new TimeoutCallback() {
        @Override
        public void callback(AioSession session, long lastTime) {
            session.close(true);
        }
    };
    private Map<AioSession, Long> sessionMap = new HashMap<>();
    /**
     * 心跳频率
     */
    private long heartRate = 5;
    /**
     * 在超时时间内未收到消息,关闭连接。
     */
    private long timeout;
    private TimeoutCallback timeoutCallback;

    /**
     * 心跳插件
     *
     * @param heartRate 心跳触发频率
     * @param timeUnit  heatRate单位
     */
    public HeartPlugin(int heartRate, TimeUnit timeUnit) {
        this(heartRate, 0, timeUnit);
    }

    /**
     * 心跳插件
     * <p>
     * 心跳插件在断网场景可能会触发TCP Retransmission,导致无法感知到网络实际状态,可通过设置timeout关闭连接
     * </p>
     *
     * @param heartRate 心跳触发频率
     * @param timeout   消息超时时间
     * @param unit      时间单位
     */
    public HeartPlugin(int heartRate, int timeout, TimeUnit unit) {
        this(heartRate, timeout, unit, DEFAULT_TIMEOUT_CALLBACK);
    }

    /**
     * 心跳插件
     * <p>
     * 心跳插件在断网场景可能会触发TCP Retransmission,导致无法感知到网络实际状态,可通过设置timeout关闭连接
     * </p>
     *
     * @param heartRate 心跳触发频率
     * @param timeout   消息超时时间
     */
    public HeartPlugin(int heartRate, int timeout, TimeUnit timeUnit, TimeoutCallback timeoutCallback) {
        if (timeout > 0 && heartRate >= timeout) {
            throw new IllegalArgumentException("heartRate must little then timeout");
        }
        this.heartRate = timeUnit.toMillis(heartRate);
        this.timeout = timeUnit.toMillis(timeout);
        this.timeoutCallback = timeoutCallback;
    }

    @Override
    public final boolean preProcess(AioSession session, T t) {
        sessionMap.put(session, TimeUtils.currentTimeMillis());
        //是否心跳响应消息
        if (isHeartMessage(session, t)) {
            //延长心跳监测时间
            return false;
        }
        return true;
    }

    @Override
    public final void stateEvent(StateMachineEnum stateMachineEnum, AioSession session, Throwable throwable) {
        switch (stateMachineEnum) {
            case NEW_SESSION:
                sessionMap.put(session, TimeUtils.currentTimeMillis());
                registerHeart(session, heartRate);
                //注册心跳监测
                break;
            case SESSION_CLOSED:
                //移除心跳监测
                sessionMap.remove(session);
                break;
            default:
                break;
        }
    }

    /**
     * 自定义心跳消息并发送
     *
     * @param session
     * @throws IOException
     */
    public abstract void sendHeartRequest(AioSession session) throws IOException;

    /**
     * 判断当前收到的消息是否为心跳消息。
     * 心跳请求消息与响应消息可能相同，也可能不同，因实际场景而异，故接口定义不做区分。
     *
     * @param session
     * @param msg
     * @return
     */
    public abstract boolean isHeartMessage(AioSession session, T msg);

    private void registerHeart(final AioSession session, final long heartRate) {
        if (heartRate <= 0) {
            Logger.debug("session:"+session+" 因心跳超时时间为:"+heartRate+",终止启动心跳监测任务");
            return;
        }
        Logger.debug("session:"+session+"注册心跳任务,超时时间:"+ heartRate);
        QuickTimerTask.SCHEDULED_EXECUTOR_SERVICE.schedule(new TimerTask() {
            @Override
            public void run() {
                if (session.isInvalid()) {
                    sessionMap.remove(session);
                    Logger.info("session:"+session+" 已失效，移除心跳任务");
                    return;
                }
                Long lastTime = sessionMap.get(session);
                if (lastTime == null) {
                    Logger.warn("session:"+session+" timeout is null");
                    lastTime = TimeUtils.currentTimeMillis();
                    sessionMap.put(session, lastTime);
                }
                long current = TimeUtils.currentTimeMillis();
                //超时未收到消息，关闭连接
                if (timeout > 0 && (current - lastTime) > timeout) {
                    timeoutCallback.callback(session, lastTime);
                    Logger.info("session:"+session+" timeout ,lastTime:  "+lastTime);
                } else if (current - lastTime > heartRate) { //超时未收到消息,尝试发送心跳消息
                    try {
                        sendHeartRequest(session);
                        session.writeBuffer().flush();
                    } catch (IOException e) {
                        Logger.error("heart exception,will close session:"+ session, e);
                        session.close(true);
                    }
                }
                registerHeart(session, heartRate);
            }
        }, heartRate, TimeUnit.MILLISECONDS);
    }

    public interface TimeoutCallback {
        public void callback(AioSession session, long lastTime);
    }
}
