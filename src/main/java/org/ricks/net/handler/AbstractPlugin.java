package org.ricks.net.handler;

import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;

import java.nio.channels.AsynchronousSocketChannel;

/**
 * @author ricks
 * @Title:
 * @Package
 * @Description:
 * @date 2022/11/416:03
 */
public abstract class AbstractPlugin<T> implements Plugin<T> {
    @Override
    public boolean preProcess(AioSession session, T t) {
        return true;
    }

    @Override
    public void stateEvent(StateMachineEnum stateMachineEnum, AioSession session, Throwable throwable) {

    }

    @Override
    public AsynchronousSocketChannel shouldAccept(AsynchronousSocketChannel channel) {
        return channel;
    }

    @Override
    public void afterRead(AioSession session, int readSize) {

    }

    @Override
    public void afterWrite(AioSession session, int writeSize) {

    }

    @Override
    public void beforeRead(AioSession session) {

    }

    @Override
    public void beforeWrite(AioSession session) {

    }
}
