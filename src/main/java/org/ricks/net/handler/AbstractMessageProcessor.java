package org.ricks.net.handler;

import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;

import java.nio.channels.AsynchronousSocketChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ricks
 * @Description:
 * @date 2022/11/416:01
 */
public abstract class AbstractMessageProcessor<T> implements MessageProcessor<T>, NetMonitor {

    private final List<Plugin<T>> plugins = new ArrayList<>();

    @Override
    public final void afterRead(AioSession session, int readSize) {
        for (Plugin<T> plugin : plugins) {
            plugin.afterRead(session, readSize);
        }
    }

    @Override
    public final void afterWrite(AioSession session, int writeSize) {
        for (Plugin<T> plugin : plugins) {
            plugin.afterWrite(session, writeSize);
        }
    }

    @Override
    public final void beforeRead(AioSession session) {
        for (Plugin<T> plugin : plugins) {
            plugin.beforeRead(session);
        }
    }

    @Override
    public final void beforeWrite(AioSession session) {
        for (Plugin<T> plugin : plugins) {
            plugin.beforeWrite(session);
        }
    }

    @Override
    public final AsynchronousSocketChannel shouldAccept(AsynchronousSocketChannel channel) {
        AsynchronousSocketChannel acceptChannel = channel;
        for (Plugin<T> plugin : plugins) {
            acceptChannel = plugin.shouldAccept(acceptChannel);
            if (acceptChannel == null) {
                return null;
            }
        }
        return acceptChannel;
    }

    //todo 写法可以优化
    @Override
    public final void process(AioSession session, T msg) {
        boolean flag = true;
        for (Plugin<T> plugin : plugins) {
            if (!plugin.preProcess(session, msg)) {
                flag = false;
            }
        }
        if (flag) {
            process0(session, msg);
        }
    }

    /**
     * 处理接收到的消息
     *
     * @param session
     * @param msg
     * @see MessageProcessor#process(AioSession, Object)
     */
    public abstract void process0(AioSession session, T msg);

    /**
     * @param session          本次触发状态机的AioSession对象
     * @param stateMachineEnum 状态枚举
     * @param throwable        异常对象，如果存在的话
     */
    @Override
    public final void stateEvent(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable) {
        for (Plugin<T> plugin : plugins) {
            plugin.stateEvent(stateMachineEnum, session, throwable);
        }
        stateEvent0(session, stateMachineEnum, throwable);
    }

    /**
     * @param session
     * @param stateMachineEnum
     * @param throwable
     * @see #stateEvent(AioSession, StateMachineEnum, Throwable)
     */
    public abstract void stateEvent0(AioSession session, StateMachineEnum stateMachineEnum, Throwable throwable);

    public final void addPlugin(Plugin<T> plugin) {
        this.plugins.add(plugin);
    }
}