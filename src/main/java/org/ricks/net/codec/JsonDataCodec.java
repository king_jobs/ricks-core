package org.ricks.net.codec;


/**
 * @author ricks
 * @Description:
 * @date 2023/1/410:41
 */
public class JsonDataCodec implements DataCodec{

    @Override
    public byte[] encode(Object data) {
//        return JSON.toJsonStr(data).getBytes();

        return new byte[0];
    }

    @Override
    public <T> T decode(byte[] data, Class<?> dataClass) {
//        return (T) JSON.toBean(new String(data),dataClass);

        return null;
    }
}
