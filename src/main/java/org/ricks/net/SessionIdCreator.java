package org.ricks.net;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ricks
 * @date 2022/10/2018:21
 */
public class SessionIdCreator {

    private static AtomicLong idCreator = new AtomicLong();

    public static long createId() {
        return idCreator.getAndIncrement();
    }
}
