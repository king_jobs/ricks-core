package org.ricks.net;


import org.ricks.ioc.Message;
import org.ricks.net.AioSession;

/**
 * @author ricks
 * @Description:这种结构体，cmd & byte[] 太过简单
 * IO-Game 返回客户端会带上状态码，错误信息（简化网络消息传输，不用带上错误信息，知道错误码就可以定位错误原因）。心跳包 业务包 区分，pb 直接解析所有信息。
 * 还有一种方式，自定义协议头。包体
 * @date 2021/5/2720:23
 */
public class Context<K,T> {

    private Message<K,T> message;

    private AioSession session;

    public Context(AioSession session, Message message) {
        this.message = message;
        this.session = session;
    }

    public void send () {
        session.send(message);
    }

    public void send (byte[] bytes) {
        session.send(bytes);
    }

    public void send (Message message) {
        session.send(message);
    }

    public K getCmd () {
        return message.getCmd();
    }

    public T getMessage() {
        return  message.getData();
    }

    public AioSession getSession() {
        return session;
    }
}
