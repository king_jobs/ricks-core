package org.ricks.net;

/**
 * 如果分配逻辑节点session 使用session hash进行绑定，找不到就意味 逻辑节点重启了。重新分配
 * group 则是tag 模块标识。维护tag分组得 逻辑节点session
 * 1.逻辑节点session 以模块分组 维护
 * 2.制定 获取tag分组之下得 sessions ,随机 hash一致性 或则 直接分配 【规则】
 * 3.拿到了tag 逻辑节点 与 玩家 映射得session ,制定 路由规则  问答方式 ，请求无响应方式。结合实际业务定制规则 ，提供接口 实现业务自定义实现
 *
 * 玩家登录开始给每个tag分配逻辑节点 进行绑定，维护在 UserSession 随着玩家退出 UserSession销毁 解除绑定
 *
 * 顾虑 之前是moduleId + serverId .这样就知道哪个tag 分配在哪个节点上面。
 * 其实可以用 address.这样逻辑节点连上网关 只需要同步tag,然后维护在分组中。根据address 规则从分组中找到session 即使断线重连 session对象变化 hash变化，也能依赖address找到 逻辑节点
 *
 */
public interface GroupIo {
    /**
     * 将AioSession加入群组group
     *
     * @param group
     * @param session
     */
    void join(String group, AioSession session);


    /**
     * 将AioSession从群众group中移除
     *
     * @param group
     * @param session
     */
    void remove(String group, AioSession session);

    /**
     * AioSession从所有群组中退出
     *
     * @param session
     */
    void remove(AioSession session);

    /**
     * 群发消息
     *
     * @param group
     * @param t
     */
    void writeToGroup(String group, byte[] t);
}
