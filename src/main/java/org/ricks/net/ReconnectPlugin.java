package org.ricks.net;

import org.ricks.common.lang.Logger;

import java.io.IOException;
import java.nio.channels.AsynchronousChannelGroup;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 断链重连插件
 *
 */
public class ReconnectPlugin implements Runnable {

    public static final long time = 5000;
    private static final ReconnectPlugin INSTANCE = new ReconnectPlugin();
    private static final Map<String,AsynchronousChannelGroup> asynchronousChannelGroupMap = new ConcurrentHashMap<>();
    private static final Map<String,AioClient> clientMap = new ConcurrentHashMap();
    private static ScheduledExecutorService executorService;


    private ReconnectPlugin() {
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(this,time,time,TimeUnit.MILLISECONDS);
    }

    public static ReconnectPlugin get() {
        return INSTANCE;
    }


    public void addReconnectPlugin(AioClient client) {
        try {
            String serverHostStr = client.getSession().getRemoteAddress().getHostString();
            clientMap.putIfAbsent(serverHostStr, client);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void addReconnectPlugin(AioClient client, AsynchronousChannelGroup asynchronousChannelGroup) {
        try {
            String serverHostStr = client.getSession().getRemoteAddress().getHostString();
            asynchronousChannelGroupMap.putIfAbsent(serverHostStr,asynchronousChannelGroup);
            clientMap.putIfAbsent(serverHostStr, client);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void doStart(AioClient client,AsynchronousChannelGroup asynchronousChannelGroup) {
        try {
            String serverHostStr = client.getSession().getRemoteAddress().getHostString();
            Logger.info( "触发重连机制,  "+serverHostStr);
            if (asynchronousChannelGroup == null) {
                client.start();
            } else {
                client.start(asynchronousChannelGroup);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        for (Map.Entry<String,AioClient> entry: clientMap.entrySet()) {
            String remoteAdd = entry.getKey();
            AioClient client = entry.getValue();
            if(client.getSession() == null || (client.getSession() != null && client.getSession().isInvalid())) {
                doStart(client,asynchronousChannelGroupMap.get(remoteAdd));
            }
        }
    }

    public boolean exist(String remoteAdd) {
        return clientMap.containsKey(remoteAdd);
    }

    /**
     * 清除 client重连机制
     * @param remoteAdd
     */
    public void cleanInvalid(String remoteAdd) {
        clientMap.remove(remoteAdd);
        asynchronousChannelGroupMap.remove(remoteAdd);
    }
}
