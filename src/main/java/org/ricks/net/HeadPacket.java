package org.ricks.net;

public class HeadPacket {

    private Byte module;

    private short cmd;

    public HeadPacket() {
    }

    public HeadPacket(byte module, short cmd) {
        this.module = module;
        this.cmd = cmd;
    }

    public Byte getModule() {
        return module;
    }

    public void setModule(byte module) {
        this.module = module;
    }

    public short getCmd() {
        return cmd;
    }

    public void setCmd(short cmd) {
        this.cmd = cmd;
    }
}
