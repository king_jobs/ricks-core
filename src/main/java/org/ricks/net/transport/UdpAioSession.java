package org.ricks.net.transport;

import org.ricks.net.buffer.BufferPage;
import org.ricks.net.buffer.VirtualBuffer;
import org.ricks.net.buffer.WriteBuffer;
import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.function.Consumer;

/**
 */
public class UdpAioSession extends AioSession {

    protected final UdpChannel udpChannel;

    protected final SocketAddress remote;

    protected final WriteBuffer byteBuf;

    public UdpAioSession(final UdpChannel udpChannel, final SocketAddress remote, BufferPage bufferPage) {
        this.udpChannel = udpChannel;
        this.remote = remote;
        Consumer<WriteBuffer> consumer = var -> {
            VirtualBuffer writeBuffer = var.poll();
            if (writeBuffer != null) {
                udpChannel.write(writeBuffer, UdpAioSession.this);
            }
        };
        this.byteBuf = new WriteBuffer(bufferPage, consumer, udpChannel.config.getWriteBufferSize(), 1);
        udpChannel.config.getProcessor().stateEvent(this, StateMachineEnum.NEW_SESSION, null);
    }

    @Override
    public WriteBuffer writeBuffer() {
        return byteBuf;
    }

    @Override
    public ByteBuffer readBuffer() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void awaitRead() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void signalRead() {
        throw new UnsupportedOperationException();
    }

    /**
     * 为确保消息尽可能发送，UDP不支持立即close
     *
     * @param immediate true:立即关闭,false:响应消息发送完后关闭
     */
    @Override
    public void close(boolean immediate) {
        byteBuf.flush();
    }

    @Override
    public InetSocketAddress getLocalAddress() throws IOException {
        return (InetSocketAddress) udpChannel.getChannel().getLocalAddress();
    }

    @Override
    public InetSocketAddress getRemoteAddress() {
        return (InetSocketAddress) remote;
    }
}
