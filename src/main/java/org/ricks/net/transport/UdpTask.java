package org.ricks.net.transport;

import org.ricks.common.exception.DecoderException;
import org.ricks.net.AioSession;
import org.ricks.net.StateMachineEnum;
import org.ricks.net.buffer.BufferPagePool;
import org.ricks.net.buffer.VirtualBuffer;
import org.ricks.net.handler.IoServerConfig;
import org.ricks.net.handler.NetMonitor;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

public class UdpTask implements Runnable{

    protected final UdpChannel channel;
    protected final SocketAddress remote;
    protected final BufferPagePool bufferPool;
    protected final ByteBuffer buffer;
    protected final VirtualBuffer readyBuffer;

    public UdpTask(UdpChannel channel, SocketAddress remote, BufferPagePool bufferPool, ByteBuffer buffer,VirtualBuffer readyBuffer) {
        this.channel = channel;
        this.remote = remote;
        this.bufferPool = bufferPool;
        this.buffer = buffer;
        this.readyBuffer = readyBuffer;
    }

    @Override
    public void run() {
        IoServerConfig config = channel.config;
        //解码
        AioSession session = channel.createAndCacheSession(remote);
        try {
            NetMonitor netMonitor = config.getMonitor();
            if (netMonitor != null) {
                netMonitor.beforeRead(session);
                netMonitor.afterRead(session, buffer.remaining());
            }
            do {
                Object request = config.getProtocol().decode(buffer, session);
                //理论上每个UDP包都是一个完整的消息
                if (request == null) {
                    config.getProcessor().stateEvent(session, StateMachineEnum.DECODE_EXCEPTION, new DecoderException("decode result is null, buffer size: " + buffer.remaining()));
                    break;
                } else {
                    config.getProcessor().process(session, request);
                }
            } while (buffer.hasRemaining());
        } catch (Throwable e) {
            e.printStackTrace();
            config.getProcessor().stateEvent(session, StateMachineEnum.DECODE_EXCEPTION, e);
        }
    }

    protected void run(AioSession session, ByteBuffer buffer){
        IoServerConfig config = channel.config;
        try {
            NetMonitor netMonitor = config.getMonitor();
            if (netMonitor != null) {
                netMonitor.beforeRead(session);
                netMonitor.afterRead(session, buffer.remaining());
            }
            Object request = config.getProtocol().decode(buffer, session);
            //理论上每个UDP包都是一个完整的消息
            if (request == null) {
                config.getProcessor().stateEvent(session, StateMachineEnum.DECODE_EXCEPTION, new DecoderException("decode result is null, buffer size: " + buffer.remaining()));
            } else {
                config.getProcessor().process(session, request);
            }
        } catch (Throwable e) {
            e.printStackTrace();
            config.getProcessor().stateEvent(session, StateMachineEnum.DECODE_EXCEPTION, e);
        }
    }

    public UdpAioSession createAioSession() {
        return new UdpAioSession(channel, remote, bufferPool.allocateBufferPage());
    }
}
