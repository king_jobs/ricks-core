package org.ricks.net.transport.kcp;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ricks
 * @date 2022/10/2018:21
 */
public class ConvCreator {

    private static AtomicInteger convCreator = new AtomicInteger();

    public static int createId() {
        return convCreator.getAndIncrement();
    }
}
