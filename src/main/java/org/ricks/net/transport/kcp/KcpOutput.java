package org.ricks.net.transport.kcp;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:szhnet@gmail.com">szh</a>
 */
public interface KcpOutput {

    void out(KcpSession session, ByteBuffer data);

}
