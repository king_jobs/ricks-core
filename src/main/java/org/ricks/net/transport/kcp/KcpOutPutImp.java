package org.ricks.net.transport.kcp;

import org.ricks.net.buffer.WriteBuffer;
import java.nio.ByteBuffer;


/**
 * Created by JinMiao
 * 2018/9/21.
 */
public class KcpOutPutImp implements KcpOutput {



    @Override
    public void out(KcpSession session, ByteBuffer data) {
        if(!session.isInvalid()) {
            WriteBuffer buffer = session.writeBuffer();
            buffer.write(data);
        }
    }
}
