package org.ricks.net.transport.kcp;

import org.ricks.common.actor.Actor;
import org.ricks.common.actor.Actors;
import org.ricks.net.buffer.BufferPage;
import org.ricks.net.transport.UdpAioSession;

import java.net.SocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * @author ricks
 * KCP 默认开启了快速，数据写回就 flush
 * @date 2022/10/1218:45
 */
public class KcpSession extends UdpAioSession  {

    //经过kcp处理过的数据包（原始数据来自send（bb）中的bb
    private final Ukcp ukcp;

    private static ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

//    private final ScheduledFuture<?> scheduledFuture;


    public KcpSession(KcpChannel kcpChannel, SocketAddress remote, BufferPage bufferPage) {
        super(kcpChannel, remote, bufferPage);
        Actor actor = Actors.me().getKcpActor(sid());
        ukcp = new Ukcp(kcpChannel,this,actor);
//        scheduledFuture = scheduledExecutorService.scheduleAtFixedRate(() -> ukcp.update(),10,10, TimeUnit.MILLISECONDS);
    }


    public Ukcp getUkcp() {
        return ukcp;
    }


    @Override
    public void close(boolean immediate) {
        super.close(immediate);
        int conv = ukcp.getConv();
        KcpChannel kcpChannel = (KcpChannel) udpChannel;
        kcpChannel.removeSession(conv);
//        scheduledFuture.cancel(true);
    }

    /**
     *
     * @param data
     */
    @Override
    public void send(byte[] data) {
        ukcp.getActor().execute("send",() -> {
            int code;
            try {
                code = ukcp.send(data);
            } catch (Exception e) {
                code = -9;
            }
            if(code != 0) {
                close(true);
            }
        });
    }
}



