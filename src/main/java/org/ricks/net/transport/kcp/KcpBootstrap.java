package org.ricks.net.transport.kcp;

import org.ricks.common.lang.ExpiringMap;
import org.ricks.net.handler.MessageProcessor;
import org.ricks.net.handler.Protocol;
import org.ricks.net.transport.UdpBootstrap;
import org.ricks.net.transport.UdpChannel;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.DatagramChannel;
import java.util.concurrent.TimeUnit;

/**
 * KCP & UDP 没有所谓server  client
 * @author ricks
 * @date 2022/8/3016:24
 */
public class KcpBootstrap extends UdpBootstrap {

    private final ExpiringMap<SocketAddress,Long> lastKcpResetTimeMap = ExpiringMap.builder().expiration(5000, TimeUnit.MILLISECONDS).build();

    private static final long KCP_RESET_TIME = 10_000;

    public <T> KcpBootstrap( Protocol<T> protocol, MessageProcessor<T> messageProcessor) {
        super(protocol,messageProcessor);
    }

    @Override
    public UdpChannel open(String host, int port) {
        try {
            DatagramChannel channel = openDatagramChannel(host,port);
            return new KcpChannel(channel,worker,config,bufferPool.allocateBufferPage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
