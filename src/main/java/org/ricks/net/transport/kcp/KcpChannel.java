package org.ricks.net.transport.kcp;

import org.ricks.common.lang.Logger;
import org.ricks.net.buffer.BufferPage;
import org.ricks.net.handler.IoServerConfig;
import org.ricks.net.transport.UdpChannel;
import org.ricks.net.transport.Worker;

import java.net.SocketAddress;
import java.nio.channels.DatagramChannel;
import java.util.concurrent.*;

/**
 * @author ricks
 * @Description:
 * @date 2023/1/517:16
 */
public class KcpChannel extends UdpChannel {

    /**
     * 与当前Kcp通道对接的会话
     *  k:conv v:kcp session
     *  kcp断线重连 IP 端口可能变化，无法依赖 IP 端口 作为会话标识 。只能使用 conv,如果conv说明是新的kcp会话
     *
     *  外部KCP 使用 tcp sessionId 作为KCP会话标识
     *  内部KCP IP 端口 hash code 负数，作为内部KCP 会话标识，这样内部就不需要从服务端获取 conv ,又保障了服务端维护的 conv映射唯一性
     */
    private final ConcurrentHashMap<Integer, KcpSession> sessionMap = new ConcurrentHashMap<>();

    private ScheduledExecutorService scheduledExecutorService;

    public KcpChannel(DatagramChannel channel,Worker worker, IoServerConfig config, BufferPage bufferPage) {
        super(channel,worker, config, bufferPage);
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        Runtime.getRuntime().addShutdownHook(new Thread(() -> scheduledExecutorService.shutdown()));
    }

    public boolean containsSession(int conv) {
        return sessionMap.containsKey(conv);
    }

    /**
     * 创建并缓存与conv的会话信息
     * 如果conv存在，SocketAddress不一致 则重新绑定最新的SocketAddress
     */
    public KcpSession createAndCacheSession(final SocketAddress remote,int conv) {
        return sessionMap.computeIfAbsent(conv, s -> {
            KcpSession kcpSession = connect(remote);
            Ukcp ukcp = kcpSession.getUkcp();
            Logger.info("create kcp session ,conv:" + conv + " remote:" + remote);
            ukcp.setConv(conv);
            return kcpSession;
        });
    }

    public KcpSession removeSession(final int conv) {
        KcpSession kcpSession = sessionMap.remove(conv);
        return kcpSession;
    }

    public void closeSession(final int conv) {
        KcpSession kcpSession = sessionMap.get(conv);
        if(kcpSession != null) kcpSession.close(true);
    }



    /**
     * 建立与远程服务的连接会话,通过AioSession可进行数据传输
     */
    public KcpSession connect(SocketAddress remote) {
        return new KcpSession(this, remote, bufferPage);
    }


//    /**
//     * 不需要创建session ,写出
//     * @param remote
//     * @param buffer
//     */
//    public void write(final SocketAddress remote, ByteBuffer buffer) {
//        try {
//            write(VirtualBuffer.wrap(buffer),remote);
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public void writeToAll(Object obj) {
//        for (KcpSession session: sessionMap.values()) {
////            session.sendResponse(obj);  todo
//        }
//    }

}
