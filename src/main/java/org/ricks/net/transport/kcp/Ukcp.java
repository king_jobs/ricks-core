package org.ricks.net.transport.kcp;

import org.ricks.common.actor.Actor;
import org.ricks.common.lang.Logger;
import org.ricks.net.buffer.WriteBuffer;
import org.ricks.common.TimeUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class Ukcp extends Kcp{

    private final LinkedTransferQueue<ByteBuffer> writeBuffer;

    private final Actor actor;

    private boolean active;

    private final AtomicBoolean readProcessing = new AtomicBoolean(false);

    private final ScheduleTask scheduleTask = new ScheduleTask(this);

    private final KcpChannel channel;

    private final KcpSession session;

    public Ukcp(KcpChannel channel, KcpSession session, Actor actor) {
        this.channel = channel;
        this.session = session;
        this.actor = actor;
        this.active = true;
        this.writeBuffer = new LinkedTransferQueue<>();
    }

    public KcpSession reset() {
        InetSocketAddress socketAddress = session.getRemoteAddress();
        session.close(true);
        return channel.createAndCacheSession(socketAddress,getConv());
    }

    @Override
    public void notify_kcp_reset() {
        actor.execute("kcp_reset",() -> super.notify_kcp_reset());
    }
    @Override
    protected void out(ByteBuffer data) {
        int conv = data.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_CONV_OFFSET);
        int cmd = data.order(ByteOrder.LITTLE_ENDIAN).get(Kcp.IKCP_CMD_OFFSET);
        int frg = data.order(ByteOrder.LITTLE_ENDIAN).get(Kcp.IKCP_FRG_OFFSET);
        int wnd = data.order(ByteOrder.LITTLE_ENDIAN).getShort(Kcp.IKCP_WND_OFFSET);
        int ts = data.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_TS_OFFSET);
        int sn = data.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_SN_OFFSET);
        int una = data.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_UNA_OFFSET);
        int kcp_len = data.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_LEN_OFFSET);
        Logger.info(Thread.currentThread().getName() + "真正写出数据 ，conv:" + conv + " , cmd:" + cmd + " , sn:" + sn  + " ," + !session.isInvalid());
        if(!session.isInvalid()) {
            WriteBuffer buffer = session.writeBuffer();
            buffer.write(data);
            buffer.flush();
        }
    }

//    @Override
//    public void ackPush(long sn, long ts) {
//        actor.execute("kcp-ack" ,() -> super.ackPush(sn, ts));
//    }

    public void update() {
        actor.execute("kcp-update" ,() -> super.update(TimeUtils.currentTimeMillis()));
    }

    public LinkedTransferQueue<ByteBuffer> getWriteBuffer() {
        return writeBuffer;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Actor getActor() {
        return actor;
    }

    public KcpChannel getChannel() {
        return channel;
    }

    public KcpSession getSession() {
        return session;
    }

    public void notifyUpdateEvent() {
        this.actor.execute("read",scheduleTask);
    }

    public void kcp_input(ByteBuffer data) throws IOException {
        int ret = super.input(data);
        switch (ret) {
            case -1:
                throw new IOException("No enough bytes of head");
            case -2:
                throw new IOException("No enough bytes of data");
            case -3:
                throw new IOException("Mismatch cmd");
            case -4:
                throw new IOException("Conv inconsistency ");
            default:
                break;
        }
    }
}
