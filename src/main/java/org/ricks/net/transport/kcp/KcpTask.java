package org.ricks.net.transport.kcp;

import org.ricks.common.lang.Logger;
import org.ricks.net.buffer.BufferPagePool;
import org.ricks.net.buffer.VirtualBuffer;
import org.ricks.net.transport.UdpTask;
import org.ricks.common.TimeUtils;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class KcpTask extends UdpTask {
    public KcpTask(KcpChannel channel, SocketAddress remote, BufferPagePool bufferPool, ByteBuffer buffer, VirtualBuffer readyBuffer) {
        super(channel, remote, bufferPool, buffer, readyBuffer);
    }

    @Override
    public void run() {
        //解码
        int conv = buffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_CONV_OFFSET);
        int cmd = buffer.order(ByteOrder.LITTLE_ENDIAN).get(Kcp.IKCP_CMD_OFFSET);
        int frg = buffer.order(ByteOrder.LITTLE_ENDIAN).get(Kcp.IKCP_FRG_OFFSET);
        int wnd = buffer.order(ByteOrder.LITTLE_ENDIAN).getShort(Kcp.IKCP_WND_OFFSET);
        int ts = buffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_TS_OFFSET);
        int sn = buffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_SN_OFFSET);
        int una = buffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_UNA_OFFSET);
        int kcp_len = buffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_LEN_OFFSET);
        KcpChannel kcpChannel = (KcpChannel) channel;
        if(!kcpChannel.containsSession(conv) && sn != 0) {
            Logger.warn(Thread.currentThread().getName() +" conv[" + conv + "] 会话并未绑定session,第一次创建session的会话，但是sn[" + sn + "] 不是初始化状态，丢弃消息 ");
            //正常机制KCP每次会话conv都不一样，所以新的会话sn必然是0。如果sn不是0并且是新的会话说明服务器重启了。丢弃等待对端心跳超时创建新的会话
            return;
        }
        byte[] bytes = new byte[buffer.remaining()];
        buffer.get(bytes);
        buffer.flip();
        Logger.info(Thread.currentThread().getName() +"KCP包 序号 ：" + sn + " cmd :" + cmd + " conv:" + conv + " frg:" + frg + " wnd:" + wnd + " ts:" + ts + " una:" + una + " len:" + kcp_len + " buff size:" + buffer.remaining() + " ," + Arrays.toString(bytes));
        KcpSession kcpSession = kcpChannel.createAndCacheSession(remote,conv);
        Ukcp ukcp = kcpSession.getUkcp();
        ukcp.getActor().execute("kcp-task",() -> {
            try {
                ukcp.kcp_input(buffer);
                ukcp.update(TimeUtils.currentTimeMillis());
                buffer.clear();
                while (ukcp.canRecv()) {
                    int len = ukcp.peekSize();
                    ByteBuffer bb = ByteBuffer.allocate(len);
                    int n = ukcp.recv(bb);
                    if (n > 0) {
                        bb.flip();
                        super.run(kcpSession, bb); //kcp 包的字节流，这个时候应该是解析完后的KCP包，抛却了kcp 24字节消息头
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                kcpSession.writeBuffer().flush();
                readyBuffer.clean();
            }
        });
    }
}
