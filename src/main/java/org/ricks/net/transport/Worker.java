package org.ricks.net.transport;

import org.ricks.common.lang.Logger;
import org.ricks.net.buffer.BufferPagePool;
import org.ricks.net.buffer.VirtualBuffer;
import org.ricks.net.handler.IoServerConfig;
import org.ricks.net.transport.kcp.Kcp;
import org.ricks.net.transport.kcp.KcpChannel;
import org.ricks.net.transport.kcp.KcpTask;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class Worker implements Runnable{

    private final static int MAX_READ_TIMES = 16;
    private static final Runnable SELECTOR_CHANNEL = () -> {
    };
    private static final Runnable SHUTDOWN_CHANNEL = () -> {
    };
    /**
     * 当前Worker绑定的Selector
     */
    private final Selector selector;
    /**
     * 内存池
     */
    private final BufferPagePool bufferPool;
    private final BlockingQueue<Runnable> requestQueue = new ArrayBlockingQueue<>(256);

    /**
     * 待注册的事件
     */
    private final ConcurrentLinkedQueue<Consumer<Selector>> registers = new ConcurrentLinkedQueue<>();

    private VirtualBuffer standbyBuffer;
    private final ExecutorService executorService;

    public Worker(BufferPagePool bufferPool, int threadNum) throws IOException {
        this.bufferPool = bufferPool;
        this.selector = Selector.open();
        try {
            this.requestQueue.put(SELECTOR_CHANNEL);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //启动worker线程组
        executorService = new ThreadPoolExecutor(threadNum, threadNum,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>(), new ThreadFactory() {
            int i = 0;

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "smart-socket:udp-" + Worker.this.hashCode() + "-" + (++i));
            }
        });
        for (int i = 0; i < threadNum; i++) {
            executorService.execute(this);
        }
    }

    /**
     * 注册事件
     */
    void addRegister(Consumer<Selector> register) {
        registers.offer(register);
        selector.wakeup();
    }

    @Override
    public void run() {
        try {
            while (true) {
                Runnable runnable = requestQueue.take();
                //服务终止
                if (runnable == SHUTDOWN_CHANNEL) {
                    requestQueue.put(SHUTDOWN_CHANNEL);
                    selector.wakeup();
                    break;
                } else if (runnable == SELECTOR_CHANNEL) {
                    try {
                        doSelector();
                    } finally {
                        requestQueue.put(SELECTOR_CHANNEL);
                    }
                } else {
                    runnable.run();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doSelector() throws IOException {
        Consumer<Selector> register;
        while ((register = registers.poll()) != null) {
            register.accept(selector);
        }
        Set<SelectionKey> keySet = selector.selectedKeys();
        if (keySet.isEmpty()) {
            selector.select();
        }
        Iterator<SelectionKey> keyIterator = keySet.iterator();
        // 执行本次已触发待处理的事件
        while (keyIterator.hasNext()) {
            SelectionKey key = keyIterator.next();
            UdpChannel udpChannel = (UdpChannel) key.attachment();
            if (!key.isValid()) {
                keyIterator.remove();
                udpChannel.close();
                continue;
            }
            if (key.isWritable()) {
                udpChannel.doWrite();
            }
            if (key.isReadable() && !doRead(udpChannel)) {
                break;
            }
            keyIterator.remove();
        }
    }

    private boolean doRead(UdpChannel channel) throws IOException {
        int count = MAX_READ_TIMES;
        IoServerConfig config = channel.config;
        while (count-- > 0) {
            if (standbyBuffer == null) {
                standbyBuffer = channel.getBufferPage().allocate(config.getReadBufferSize());
            }
            ByteBuffer buffer = standbyBuffer.buffer();
            SocketAddress remote = channel.getChannel().receive(buffer);
            int conv = buffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_CONV_OFFSET);
            int cmd = buffer.order(ByteOrder.LITTLE_ENDIAN).get(Kcp.IKCP_CMD_OFFSET);
            int sn = buffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_SN_OFFSET);
            Logger.info(Thread.currentThread().getName() + "读取数据 ，conv:" + conv + " , cmd:" + cmd + " , sn:" + sn  + " ," );
            if (remote == null) {
                buffer.clear();
                return true;
            }
            VirtualBuffer readyBuffer = standbyBuffer;
            standbyBuffer = channel.getBufferPage().allocate(config.getReadBufferSize());
            buffer.flip();
            Runnable runnable;
            if(channel instanceof KcpChannel kcpChannel) {
                runnable = new KcpTask(kcpChannel,remote,bufferPool,buffer,readyBuffer);
            } else {
                runnable = new UdpTask(channel, remote, bufferPool, buffer, readyBuffer);
            }
            if (!requestQueue.offer(runnable)) {
                return false;
            }
        }
        return true;
    }


    void shutdown() {
        try {
            requestQueue.put(SHUTDOWN_CHANNEL);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        selector.wakeup();
        executorService.shutdown();
        try {
            selector.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
