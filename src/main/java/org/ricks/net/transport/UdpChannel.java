/*******************************************************************************
 * Copyright (c) 2017-2019, org.smartboot. All rights reserved.
 * project name: smart-socket
 * file name: UdpChannel.java
 * Date: 2019-12-31
 * Author: sandao (zhengjunweimail@163.com)
 *
 ******************************************************************************/

package org.ricks.net.transport;

import org.ricks.common.lang.Logger;
import org.ricks.net.buffer.BufferPage;
import org.ricks.net.buffer.VirtualBuffer;
import org.ricks.net.handler.IoServerConfig;
import org.ricks.net.AioSession;
import org.ricks.net.transport.kcp.Kcp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

/**
 * 封装UDP底层真实渠道对象,并提供通信及会话管理
 *
 * @author 三刀
 * @version V1.0 , 2019/8/18
 */
public class UdpChannel {
    protected final BufferPage bufferPage;

    /**
     * 待输出消息
     */
    private ConcurrentLinkedQueue<ResponseUnit> responseTasks;
    private final Semaphore writeSemaphore = new Semaphore(1);
    private Worker worker;
    public final IoServerConfig config;
    /**
     * 真实的UDP通道
     */
    private final DatagramChannel channel;
    private SelectionKey selectionKey;
    //发送失败的
    private ResponseUnit failResponseUnit;

    /**
     * 与当前UDP通道对接的会话
     */
    private final ConcurrentHashMap<SocketAddress, AioSession> sessionMap = new ConcurrentHashMap<>();

    private UdpChannel(final DatagramChannel channel, IoServerConfig config, BufferPage bufferPage) {
        this.channel = channel;
        this.bufferPage = bufferPage;
        this.config = config;
    }

    public UdpChannel(final DatagramChannel channel, Worker worker, IoServerConfig config, BufferPage bufferPage) {
        this(channel, config, bufferPage);
        responseTasks = new ConcurrentLinkedQueue<>();
        this.worker = worker;
        worker.addRegister(selector -> {
            try {
                UdpChannel.this.selectionKey = channel.register(selector, SelectionKey.OP_READ, UdpChannel.this);
            } catch (ClosedChannelException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 创建并缓存与指定地址的会话信息
     */
    public AioSession createAndCacheSession(final SocketAddress remote) {
        return sessionMap.computeIfAbsent(remote, s -> connect(remote));
    }

    void write(VirtualBuffer virtualBuffer, UdpAioSession session) {
        if (writeSemaphore.tryAcquire() && responseTasks.isEmpty() && send(virtualBuffer.buffer(), session) > 0) {
            virtualBuffer.clean();
            writeSemaphore.release();
            session.writeBuffer().flush();
            return;
        }
        responseTasks.offer(new ResponseUnit(session, virtualBuffer));
        if (selectionKey == null) {
            worker.addRegister(selector -> selectionKey.interestOps(selectionKey.interestOps() | SelectionKey.OP_WRITE));
        } else {
            if ((selectionKey.interestOps() & SelectionKey.OP_WRITE) == 0) {
                selectionKey.interestOps(selectionKey.interestOps() | SelectionKey.OP_WRITE);
            }
        }
    }

    void doWrite() {
        while (true) {
            ResponseUnit responseUnit;
            if (failResponseUnit == null) {
                responseUnit = responseTasks.poll();
            } else {
                responseUnit = failResponseUnit;
                failResponseUnit = null;
            }
            if (responseUnit == null) {
                writeSemaphore.release();
                if (responseTasks.isEmpty()) {
                    selectionKey.interestOps(selectionKey.interestOps() & ~SelectionKey.OP_WRITE);
                    if (!responseTasks.isEmpty()) {
                        selectionKey.interestOps(selectionKey.interestOps() | SelectionKey.OP_WRITE);
                    }
                }
                return;
            }
            if (send(responseUnit.response.buffer(), responseUnit.session) > 0) {
                responseUnit.response.clean();
                responseUnit.session.writeBuffer().flush();
            } else {
                failResponseUnit = responseUnit;
                Logger.warn("send fail,will retry...");
                break;
            }
        }
    }

    private int send(ByteBuffer byteBuffer, UdpAioSession session) {
        if (config.getMonitor() != null) {
            config.getMonitor().beforeWrite(session);
        }
        int size = 0;
        try {
            int conv = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_CONV_OFFSET);
            int cmd = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).get(Kcp.IKCP_CMD_OFFSET);
            int sn = byteBuffer.order(ByteOrder.LITTLE_ENDIAN).getInt(Kcp.IKCP_SN_OFFSET);
            Logger.info(Thread.currentThread().getName() + "DatagramChannel 最最最真正写出数据 ，conv:" + conv + " , cmd:" + cmd + " , sn:" + sn  + " ," + !session.isInvalid());
            size = channel.send(byteBuffer, session.getRemoteAddress());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        if (config.getMonitor() != null) {
            config.getMonitor().afterWrite(session, size);
        }
        return size;
    }

    /**
     * 建立与远程服务的连接会话,通过AioSession可进行数据传输
     */
    public AioSession connect(SocketAddress remote) {
        return new UdpAioSession(this, remote, bufferPage);
    }

    public AioSession connect(String host, int port) {
        return connect(new InetSocketAddress(host, port));
    }

    /**
     * 关闭当前连接
     */
    public void close() {
        Logger.info("close channel...");
        if (selectionKey != null) {
            Selector selector = selectionKey.selector();
            selectionKey.cancel();
            selector.wakeup();
            selectionKey = null;
        }
        try {
            if (channel != null) {
                channel.close();
            }
        } catch (IOException e) {
            Logger.error("", e);
        }
        //内存回收
        ResponseUnit task;
        while ((task = responseTasks.poll()) != null) {
            task.response.clean();
        }
        if (failResponseUnit != null) {
            failResponseUnit.response.clean();
        }
    }

    BufferPage getBufferPage() {
        return bufferPage;
    }


    DatagramChannel getChannel() {
        return channel;
    }

    static final class ResponseUnit {
        /**
         * 待输出数据的接受地址
         */
        private final UdpAioSession session;
        /**
         * 待输出数据
         */
        private final VirtualBuffer response;

        public ResponseUnit(UdpAioSession session, VirtualBuffer response) {
            this.session = session;
            this.response = response;
        }

    }
}
