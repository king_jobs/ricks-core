package org.ricks.net.balancer;

import org.ricks.common.lang.Assert;
import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
import org.ricks.net.HeadPacket;
import org.ricks.net.LogicSessionManager;
import org.ricks.protocol.IPacket;
import java.util.*;

/**
 * 游戏服定制策略，玩家登录 游戏服缓存玩家数据，直至玩家退出游戏 销毁内存数据。这期间玩家都再此游戏服节点处理业务逻辑
 *
 * 以玩家在线人数 分配节点
 *
 * 其实简单 可以使用hash一致性算法，也能保障节点分配均匀
 */
public class UserOnlineCountLoadBalancer extends AbstractConsumerLoadBalancer{

    private static final UserOnlineCountLoadBalancer INSTANCE = new UserOnlineCountLoadBalancer();

    private UserOnlineCountLoadBalancer() {
    }

    public static UserOnlineCountLoadBalancer getInstance() {
        return INSTANCE;
    }

    /**
     * 每个游戏节点 玩家在线总人数
     */
    private Map<Short,Integer> userOnlineCounts = new HashMap<>();


    /**
     * game节点 总在线人数同步
     * 一般用于定时同步
     * @param gameId
     * @param userOnlineCount
     */
    public synchronized void changeUserOnlineCount(short gameId, int userOnlineCount) {
        userOnlineCounts.put(gameId,userOnlineCount);
    }

    /**
     * 玩家进入游戏，改变游戏服在线人数统计
     * @param gameId
     */
    public synchronized void userIntoGame (short gameId) {
        int gameUserOnlineCount = userOnlineCounts.computeIfAbsent(gameId,k-> 0);
        userOnlineCounts.put(gameId,++gameUserOnlineCount);
    }

    /**
     * 玩家退出游戏，改变游戏服在线人数统计
     * @param gameId
     */
    public synchronized void userQuitGame(short gameId) {
        int gameUserOnlineCount = userOnlineCounts.getOrDefault(gameId,0);
        if(gameUserOnlineCount <= 0) {
            Logger.warn("GameId:" + gameId + " 在线人数统计异常，请检测 ");
            return;
        }
        userOnlineCounts.put(gameId, --gameUserOnlineCount);
    }

    /**
     * 游戏服关闭
     * @param gameId
     */
    public synchronized void removeUserOnlineCount(short gameId) {
        userOnlineCounts.remove(gameId);
    }

    /**
     * 游戏服启动
     * @param gameId
     */
    public synchronized void initUserOnlineCount(short gameId) {
        userOnlineCounts.put(gameId, 0);
    }

    @Override
    public AioSession loadBalancer(HeadPacket head, IPacket packet, Object argument) {
        //第一步 查看是否绑定
        AioSession session = ModuleBindConsumerLoadBalancer.getInstance().loadBalancer(head,argument);
        if (Objects.isNull(session) || session.isInvalid()) {
            Assert.isTrue(!userOnlineCounts.isEmpty(), "Game server 启动未初始化userOnlineCounts ！");
            //节点未进行强绑定，或则节点故障。开始重新分配节点
            List<Map.Entry<Short,Integer>> list = new ArrayList<Map.Entry<Short,Integer>>(userOnlineCounts.entrySet());
            Collections.sort(list, new Comparator<Map.Entry<Short, Integer>>() {
                public int compare(Map.Entry<Short, Integer> o1, Map.Entry<Short, Integer> o2) {
                    return (o1.getValue() - o2.getValue());
                }
            });
            //在线人数最少的游戏服节点
            short gameId =  list.get(0).getKey();
            session =LogicSessionManager.me().getLogicSession(head.getModule(), gameId);
        }
        return session;
    }
}
