package org.ricks.net.balancer;

import org.ricks.common.exception.RunException;
import org.ricks.common.lang.Pair;
import org.ricks.net.AioSession;
import org.ricks.net.HeadPacket;
import org.ricks.net.LogicSessionManager;
import org.ricks.net.SessionManager;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;
import org.ricks.protocol.collection.CollectionUtils;
import org.ricks.common.ConsistentHash;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.stream.Collectors;

public class ConsistentHashConsumerLoadBalancer extends AbstractConsumerLoadBalancer{


    public static final ConsistentHashConsumerLoadBalancer INSTANCE = new ConsistentHashConsumerLoadBalancer();

    private volatile int lastClientSessionChangeId = 0;
    private static final AtomicReferenceArray<ConsistentHash<String, Long>> consistentHashMap = new AtomicReferenceArray<>(ProtocolManager.MAX_MODULE_NUM);
    private static final int VIRTUAL_NODE_NUMS = 200;

    private ConsistentHashConsumerLoadBalancer() {
    }

    public static ConsistentHashConsumerLoadBalancer getInstance() {
        return INSTANCE;
    }

    /**
     * 通过argument的toString计算一致性hash，所以传入的argument一般要能代表唯一性，比如用户的id
     *
     * @param packet   请求包
     * @param argument 参数，一般要能代表唯一性，比如用户的id
     * @return 调用的session
     */
    @Override
    public AioSession loadBalancer(HeadPacket head, IPacket packet, Object argument) {
        if (argument == null) {
            return RandomConsumerLoadBalancer.getInstance().loadBalancer(head,packet, argument);
        }

        // 如果更新时间不匹配，则更新到最新的服务提供者
        var currentClientSessionChangeId = LogicSessionManager.me().getLogicSessionChangeId();
        if (currentClientSessionChangeId != lastClientSessionChangeId) {
            for (byte i = 0; i < ProtocolManager.MAX_MODULE_NUM; i++) {
                var consistentHash = consistentHashMap.get(i);
                if (consistentHash == null) {
                    continue;
                }
                var module = ProtocolManager.moduleByModuleId(i);
                updateModuleToConsistentHash(module.getId());
            }
            lastClientSessionChangeId = currentClientSessionChangeId;
        }

        var consistentHash = consistentHashMap.get(head.getModule());
        if (consistentHash == null) {
            consistentHash = updateModuleToConsistentHash(head.getModule());
        }
        if (consistentHash == null) {
            throw new RunException("ConsistentHashLoadBalancer [protocolId:{}][argument:{}], no service provides the [module:{}]", packet.protocolId(), argument, head.getModule());
        }
        var sid = consistentHash.getRealNode(argument).getValue();
        return SessionManager.get().getClientSession(sid);
    }


    /**
     * todo hask一致性算法 地址看看怎么存储
     * @param module
     * @return
     */
    private ConsistentHash<String, Long> updateModuleToConsistentHash(Byte module) {
        var sessionStringList = LogicSessionManager.me().getGroupList(module).stream()
                .map(session -> {
                    try {
                        return new Pair<>(session.getRemoteAddress().getHostString(), session.sid());
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                })
                .sorted((a, b) -> a.getKey().compareTo(b.getKey()))
                .collect(Collectors.toList());

        var consistentHash = CollectionUtils.isNotEmpty(sessionStringList) ? new ConsistentHash<>(sessionStringList, VIRTUAL_NODE_NUMS) : null;
        consistentHashMap.set(module, consistentHash);
        return consistentHash;
    }

    public static void main(String[] args) {
//        var consistentHash = CollectionUtils.isNotEmpty(sessionStringList) ? new ConsistentHash<>(sessionStringList, VIRTUAL_NODE_NUMS) : null;
//        consistentHashMap.set(module.getId(), consistentHash);
    }

}
