package org.ricks.net.balancer;

import org.ricks.common.lang.Assert;
import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
import org.ricks.net.HeadPacket;
import org.ricks.protocol.IPacket;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * uid - module - session 绑定load session 策略
 *
 * 重点：随着UserSession生命周期结束而结束，如果UserSession销毁得时候， 未解除节点绑定关系，则存在内存泄漏风险。
 */
public class ModuleBindConsumerLoadBalancer extends AbstractConsumerLoadBalancer{


    public static final ModuleBindConsumerLoadBalancer INSTANCE = new ModuleBindConsumerLoadBalancer();

    /**
     * key: UID 玩家唯一标识
     * value: UserSession
     */
    private static final Map<String,GroupUnit> userGroupIdMap = new ConcurrentHashMap<>();

    private ModuleBindConsumerLoadBalancer() {
    }

    public static ModuleBindConsumerLoadBalancer getInstance() {
        return INSTANCE;
    }

    /**
     * 通过argument的toString计算一致性hash，所以传入的argument一般要能代表唯一性，比如用户的id
     *
     * @param packet   请求包
     * @param argument 参数，一般要能代表唯一性，比如用户的id
     * @return 调用的session
     */
    @Override
    public AioSession loadBalancer(HeadPacket head, IPacket packet, Object argument) {
        Assert.isTrue(argument instanceof String && Objects.nonNull(argument), " ModuleBindConsumerLoadBalancer 无法获取内部Session分配策略，请检测 argument 类型 或则 值 ");
        String uid = (String) argument;
        GroupUnit groupUnit = userGroupIdMap.computeIfAbsent(uid, key -> new GroupUnit());;
        AioSession session = groupUnit.groupMap.get(head.getModule());
        if(Objects.isNull(session) || session.isInvalid()) {
            Logger.error("玩家uid:" + uid + " & 逻辑模块moduleId:" + head.getModule() + "未绑定节点，开始根据hash一致性算法分配节点并进行关系绑定！");
            session = ConsistentHashConsumerLoadBalancer.getInstance().loadBalancer(head,packet,argument);
            join(uid,head.getModule(),session); //uid - module - session 关系维护起来
        }
        return session;
    }

    public AioSession loadBalancer(HeadPacket head, Object argument) {
        Assert.isTrue(argument instanceof String && Objects.nonNull(argument), " ModuleBindConsumerLoadBalancer 无法获取内部Session分配策略，请检测 argument 类型 或则 值 ");
        String uid = (String) argument;
        GroupUnit groupUnit = userGroupIdMap.computeIfAbsent(uid, key -> new GroupUnit());;
        return groupUnit.groupMap.get(head.getModule());
    }


    /**
     * 将AioSession加入群组group
     *
     * @param uid
     * @param session
     */
    public final synchronized void join(String uid,byte module, AioSession session) {
       GroupUnit groupUnit = userGroupIdMap.computeIfAbsent(uid, key -> new GroupUnit());
       groupUnit.groupMap.put(module,session);
    }

    public final synchronized void remove(String uid, byte module) {
       GroupUnit groupUnit = userGroupIdMap.get(uid);
        if (groupUnit == null) {
            return;
        }
        groupUnit.groupMap.remove(module);
        if (groupUnit.groupMap.isEmpty()) {
            remove(uid);
        }
    }

    public final void remove(String uid) {
        userGroupIdMap.remove(uid);
    }

    public void writeToGroup(String uid, IPacket gamePack) {
       GroupUnit groupUnit = userGroupIdMap.get(uid);
        for (AioSession session : groupUnit.groupMap.values()) {
            session.send(gamePack);
        }
    }


    private class GroupUnit {
        Map<Byte,AioSession> groupMap = new HashMap<>(Byte.MAX_VALUE);
    }

}
