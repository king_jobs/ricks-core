package org.ricks.net.balancer;

import org.ricks.net.AioSession;
import org.ricks.net.HeadPacket;
import org.ricks.protocol.IPacket;

/**
 * 制定获取session规则
 */
public interface IConsumerLoadBalancer {

    /**
     * 只有一致性hash会使用这个argument参数，如果在一致性hash没有传入argument默认使用随机负载均衡
     *
     * @param packet   请求包
     * @param argument 计算参数
     * @return 一个服务提供者的session
     */
    AioSession loadBalancer(HeadPacket head, IPacket packet, Object argument);

    default void beforeLoadBalancer(AioSession session, IPacket packet) {
    }

    default void afterLoadBalancer(AioSession session, IPacket packet) {
    }

}
