package org.ricks.net.balancer;

import org.ricks.common.exception.RunException;
import org.ricks.net.AioSession;
import org.ricks.net.HeadPacket;
import org.ricks.net.LogicSessionManager;
import org.ricks.protocol.IPacket;
import org.ricks.common.RandomUtil;

public class RandomConsumerLoadBalancer extends AbstractConsumerLoadBalancer{


    private static final RandomConsumerLoadBalancer INSTANCE = new RandomConsumerLoadBalancer();

    private RandomConsumerLoadBalancer() {
    }

    public static RandomConsumerLoadBalancer getInstance() {
        return INSTANCE;
    }

    @Override
    public AioSession loadBalancer(HeadPacket head, IPacket packet, Object argument) {
        var sessions = LogicSessionManager.me().getGroupList(head.getModule());

        if (sessions.isEmpty()) {
            throw new RunException("RandomConsumerLoadBalancer [protocolId:{}][argument:{}], no service provides the [module:{}]", packet.protocolId(), argument, head.getModule());
        }

        return RandomUtil.randomEle(sessions.stream().toList());
    }
}
