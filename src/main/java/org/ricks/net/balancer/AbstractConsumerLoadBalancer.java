package org.ricks.net.balancer;

import org.ricks.common.StrUtil;

/**
 * 如果tag分类session维护在 MessageProcessor ，就意味只能在接收网关消息那块处理
 * 如果类似UserSessions 进行封装，单例模式 那就可以哪块需求随时使用
 */
public abstract class AbstractConsumerLoadBalancer implements IConsumerLoadBalancer{

    public static AbstractConsumerLoadBalancer valueOf(String loadBalancer) {
        AbstractConsumerLoadBalancer balancer;
        switch (loadBalancer) {
            case "random":
                balancer = RandomConsumerLoadBalancer.getInstance();
                break;
            case "consistent-hash":
                balancer = ConsistentHashConsumerLoadBalancer.getInstance();
                break;
            case "module-bind":
                balancer = ModuleBindConsumerLoadBalancer.getInstance();
            case "user-online":
                balancer = UserOnlineCountLoadBalancer.getInstance();
            default:
                throw new RuntimeException(StrUtil.format("Load balancer is not recognized[{}]", loadBalancer));
        }
        return balancer;
    }


}
