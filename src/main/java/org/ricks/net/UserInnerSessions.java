package org.ricks.net;

import org.ricks.protocol.IPacket;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户内部Session管理
 *
 * uid - module - session
 *
 * session 如果是null 或则 session close
 *
 * 维护在UserSession中，随着UserSession销毁内部级联关系而销毁
 */
public class UserInnerSessions {

    /**
     * key: UID 玩家唯一标识
     * value: UserSession
     */
    private final Map<String,GroupUnit> userGroupIdMap = new ConcurrentHashMap<>();


    /**
     * 将AioSession加入群组group
     *
     * @param uid
     * @param session
     */
    public final synchronized void join(String uid,byte module, AioSession session) {
        GroupUnit groupUnit = userGroupIdMap.computeIfAbsent(uid,key -> new GroupUnit());
        groupUnit.groupMap.put(module,session);
    }

    public final synchronized void remove(String uid, byte module) {
        GroupUnit groupUnit = userGroupIdMap.get(uid);
        if (groupUnit == null) {
            return;
        }
        groupUnit.groupMap.remove(module);
        if (groupUnit.groupMap.isEmpty()) {
           remove(uid);
        }
    }

    public final void remove(String uid) {
        userGroupIdMap.remove(uid);
    }

    public void writeToGroup(String uid, IPacket gamePack) {
        GroupUnit groupUnit = userGroupIdMap.get(uid);
        for (AioSession session : groupUnit.groupMap.values()) {
            session.send(gamePack);
        }
    }


    private class GroupUnit {
        Map<Byte,AioSession> groupMap = new HashMap<>(Byte.MAX_VALUE);
    }
}
