package org.ricks.net;


import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 逻辑节点 分组
 */
public class LogicSessionManager {

    /**
     * 对内 对外 其实是两个对象
     * 那就意味其实也可以对玩家得session进行分组
     * 也可以对逻辑节点进行分组
     */
    private Map<Byte, LogicSessionManager.GroupUnit> sessionGroup = new ConcurrentHashMap<>();

    /**
     * session 所在的二维数组位置
     * 以便销毁业务节点
     */
    private Map<AioSession,Integer> SESSION_INDEX = new ConcurrentHashMap<>();
    private static final AtomicInteger CLIENT_ATOMIC = new AtomicInteger(0);
    private volatile int logicSessionChangeId = CLIENT_ATOMIC.incrementAndGet();

    /**
     * 将AioSession加入群组group
     *
     * @param module
     * @param session
     */
    public final synchronized void join(byte module,short sid, AioSession session) {
        LogicSessionManager.GroupUnit groupUnit = sessionGroup.get(module);
        if (groupUnit == null) {
            groupUnit = new LogicSessionManager.GroupUnit();
            sessionGroup.put(module, groupUnit);
        }
        groupUnit.groupMap.put(sid, session);
        int index = merge(module,sid);
        SESSION_INDEX.put(session,index);
        logicSessionChangeId = CLIENT_ATOMIC.incrementAndGet();
    }


    /**
     * 如果逻辑节点故障，销毁
     * @param session
     */
    public final void remove(AioSession session) {
        Integer index = SESSION_INDEX.remove(session);
        if(index != null) {
            byte moduleId = (byte) getModuleId(index);
            int sid = getSid(index);
            GroupUnit groupUnit = sessionGroup.get(moduleId);
            if(Objects.nonNull(groupUnit)) {
                groupUnit.groupMap.remove(sid);
            }
        }
    }

    public final Collection<AioSession> getGroupList(byte tag) {
        return sessionGroup.get(tag).groupMap.values();
    }

    public final AioSession getLogicSession(byte moduleId,short sid) {
        return sessionGroup.get(moduleId).groupMap.get(sid);
    }

    public void writeToGroup(IPacket packet) {
        ByteBuf buf = new ByteBuf(1024);
        ProtocolManager.write(buf, packet);
        byte[] data = buf.toArray();
        sessionGroup.values().forEach(groupUnit -> {
            for (AioSession aioSession : groupUnit.groupMap.values()) {
                try {
                    aioSession.writeBuffer().write(data);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void writeToGroup(byte group, IPacket packet) {
        LogicSessionManager.GroupUnit groupUnit = sessionGroup.get(group);
        ByteBuf buf = new ByteBuf(1024);
        ProtocolManager.write(buf, packet);
        byte[] data = buf.toArray();
        for (AioSession aioSession : groupUnit.groupMap.values()) {
            try {
                aioSession.writeBuffer().write(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public int getLogicSessionChangeId() {
        return logicSessionChangeId;
    }

    /**
     * 判断节点是否存活
     * @param moduleId
     * @return
     */
    public boolean isDead(byte moduleId,short sid) {
        GroupUnit groupUnit = sessionGroup.get(moduleId);
        if(Objects.isNull(groupUnit)) {
            return true;
        }
        AioSession session = groupUnit.groupMap.get(sid);
        return Objects.isNull(session) || session.isInvalid();
    }

    private class GroupUnit {
        /**
         * k=sid 节点唯一标识
         * v=内部Session
         */
        Map<Short,AioSession> groupMap = new HashMap<>();
    }

    private LogicSessionManager() {}
    public static LogicSessionManager me() {
        return LogicSessionManager.Holder.ME;
    }

    /** 通过 JVM 的类加载机制, 保证只加载一次 (singleton) */
    private static class Holder {
        static final LogicSessionManager ME = new LogicSessionManager();
    }


    /**
     * 合并两个参数,分别存放在 [高16 和 低16]
     * <pre>
     *     moduleId - 高16
     *     serverId - 低16
     *     例如 moduleId = 1; serverId = 1;
     *     mergeConv 的结果: 65537
     *     那么 mergeConv 对应的二进制是: [0000 0000 0000 0001] [0000 0000 0000 0001]
     * </pre>
     *
     * @param moduleId    模块ID由存放于合并结果的高16位, 该参数不得大于 32767
     * @param serverId  节点ID存放于合并结果的低16位, 该参数不得大于 65535
     * @return 合并的结果
     */
    public int merge(int moduleId, int serverId) {
        return (moduleId << 16) + serverId;
    }


    /**
     * 得到模块ID
     * 从 cmdMerge 中获取 [高16位] 的数值
     *
     * @param cmdMerge 合并路由 cmdMerge
     * @return [高16位] 的数值
     */
    public static int getModuleId(int cmdMerge) {
        return cmdMerge >> 16;
    }

    /**
     * 得到节点ID
     * 从 cmdMerge 中获取 [低16位] 的数值
     *
     * @param cmdMerge 合并路由 cmdMerge
     * @return [低16位] 的数值
     */
    public static int getSid(int cmdMerge) {
        return cmdMerge & 0xFFFF;
    }
}
