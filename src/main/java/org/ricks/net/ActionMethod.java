package org.ricks.net;

import org.ricks.ioc.RicksMethod;
import org.ricks.net.anno.Task;

import java.lang.annotation.*;

/**
 * @author ：demon-chen
 * @date ：Created in 2019/12/17 下午1:30
 * @description：业务指令
 */
@Documented
@RicksMethod
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ActionMethod {
    short messageId() default 0;

    Task value() default Task.TaskBus;
}
