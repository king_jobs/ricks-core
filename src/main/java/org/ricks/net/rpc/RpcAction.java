package org.ricks.net.rpc;

import org.ricks.ioc.Bean;

import java.lang.annotation.*;

/**
 * 声明 RPC 类
 */
@Bean
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RpcAction {

}
