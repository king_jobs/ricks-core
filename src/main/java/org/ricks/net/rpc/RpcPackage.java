package org.ricks.net.rpc;

import org.ricks.protocol.IPacket;
import org.ricks.protocol.Protocol;

/**
 * 网关和节点之间通信，如果返回的结果是字节流。就意味可以动态定制序列化方式。比如 外部通信Luban 内部通信protocol.RPC属于内存通信，业务节点返回结果是Luban
 * 缺点就是序列化了两次，
 *
 * /**
 *  * 请求参数 返回结果
 *  * byte[] 就意味RPC需要两次序列化
 *  * gateway服务消费者 business服务提供者，基本上gateway仅仅只是做转发 那就意味byte[]并不需要decode ，收到business消息 只需要写给客户端
 *  * decode只有特殊场景需要，比如玩家登录B节点，上次在A节点。通知A节点落地数据 才允许登录B节点。这时候需要decode拿到结果
 *  *
 *  * Object 是最优方案 方式需要序列化支持
 *  * IPacket 不支持基础数据类型，int  string 都需要封装成IPacket对象，而且RPC服务提供者 也得返回IPacket
 *
 *  Rpc请求 响应模式，对象复用。valueOf(byte[] data) 必然是构建响应数据
 */
@Protocol(id = 1)
public class RpcPackage implements IPacket {

    public static final transient short PROTOCOL_ID = 1000;

    /**
     * 请求ID
     */
    private int reqId;

    /**
     * 请求 响应标识
     */
    private boolean request = true;

    /**
     * 路由ID
     */
    private short cmd;

    /**
     * 数据
     */
    private byte[] data;

    public RpcPackage() {
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public RpcPackage(int reqId, short cmd, byte[] data) {
        this.reqId = reqId;
        this.cmd = cmd;
        this.data = data;
    }

    public int getReqId() {
        return reqId;
    }

    public void setReqId(int reqId) {
        this.reqId = reqId;
    }

    public short getCmd() {
        return cmd;
    }

    public void setCmd(short cmd) {
        this.cmd = cmd;
    }

    public boolean isRequest() {
        return request;
    }

    public void setRequest(boolean request) {
        this.request = request;
    }

    /**
     * @param data 可以使用跨平台protocol 进行序列化，也可以使用内部protocol
     * @return
     */
    public RpcPackage valueOf(byte[] data) {
        this.data = data;
        this.request = false;
        return this;
    }
}
