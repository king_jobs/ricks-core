package org.ricks.net.rpc;

import java.util.List;
import java.util.Objects;

public class ConsumerConfig {


    private List<ConsumerModule> consumers;

    public static ConsumerConfig valueOf(List<ConsumerModule> modules) {
        ConsumerConfig config = new ConsumerConfig();
        config.consumers = modules;
        return config;
    }

    public List<ConsumerModule> getConsumers() {
        return consumers;
    }

    public void setConsumers(List<ConsumerModule> consumers) {
        this.consumers = consumers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConsumerConfig that = (ConsumerConfig) o;
        return Objects.equals(consumers, that.consumers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(consumers);
    }
}
