package org.ricks.net.rpc;

import org.ricks.common.lang.Logger;
import org.ricks.net.AioSession;
import org.ricks.net.rpc.stub.RpcStub;
import org.ricks.net.rpc.stub.RpcSyncStub;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.ByteBufUtils;
import org.ricks.common.MapUtils;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * RPC连接
 */
public class RpcConnector {
    private static final AtomicInteger reqId = new AtomicInteger(0);
    private final AioSession session;

    public RpcConnector( AioSession session) {
        this.session = session;
    }

    private int nextReqId() {
        return reqId.incrementAndGet();
    }

    private final ConcurrentMap<Integer, RpcSyncStub> stubMap = MapUtils.newConcurrentHashMap(512);

    public RpcStub getRpcStub(Integer reqId) {
        return stubMap.get(reqId);
    }

    public byte[] syncCall(short cmd, Object req) {
        ByteBuf buf = new ByteBuf(1024);
        ByteBufUtils.writeObject(buf,req); //参数序列化字节流
        long startTime = System.nanoTime();
        // 1.生成请求唯一ID
        final Integer reqId = this.nextReqId();
        RpcSyncStub stub = new RpcSyncStub(reqId);
        try {
            // 2.绑定存根
            stubMap.put(reqId, stub);

            // 3.发消息
            this.send(new RpcPackage(reqId, cmd, buf.toArray()));

            // 4.等待直到拿到结果
            return stub.waitTillResult();
        }
        // 5.解绑回调
        finally {
            stubMap.remove(reqId);
            float exec = (System.nanoTime() - startTime) / 1000_000 ;
            Logger.debug("rpc sync call. reqId="+reqId+", exec="+exec+" ms");
        }
    }

    public void send(RpcPackage rpcPackage) {
        // 使用封包编码器去编码
        session.send(rpcPackage);
    }
    public boolean isConnected() {
        return session != null && !session.isInvalid();
    }

    public AioSession getSession() {
        return session;
    }


}
