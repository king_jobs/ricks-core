package org.ricks.net.rpc.stub;


import org.ricks.net.rpc.RpcPackage;

/**
 * RPC存根
 */
public abstract class RpcStub {

    protected RpcStub() {
    }

    public abstract boolean isSync();

    public abstract void submit(RpcPackage rpcPackage);

    public void callback(RpcPackage rpcPackage) {

    }
}
