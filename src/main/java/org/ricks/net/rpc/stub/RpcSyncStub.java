package org.ricks.net.rpc.stub;

import org.ricks.common.exception.RpcTimeoutException;
import org.ricks.net.rpc.RpcPackage;

import java.lang.reflect.Method;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * RPC同步请求的存根.
 * 问答模式。请求响应
 */
public class RpcSyncStub extends RpcStub {
    private static final ConcurrentHashMap<Class<?>, Method> CACHES = new ConcurrentHashMap<>(1024);
    private final Integer reqId;
    private final ArrayBlockingQueue<RpcPackage> awaitQueue;

    public RpcSyncStub(Integer reqId) {
        this.reqId = reqId;
        this.awaitQueue = new ArrayBlockingQueue<>(1);
    }

    @Override
    public boolean isSync() {
        return true;
    }

    @Override
    public void submit(RpcPackage rpcPackage) {
        awaitQueue.add(rpcPackage);
    }

    /**
     * 调用 rpc client to server ,server响应的结果
     * @return
     */
    public byte[] waitTillResult() {
        try {
            RpcPackage response = awaitQueue.poll(3, TimeUnit.SECONDS);
            if (response == null) {
                throw new RpcTimeoutException("Rpc超时 reqId=" + reqId);
            }

            return response.getData();
        } catch (Exception e) {
            throw new RpcTimeoutException("Rpc超时 reqId=" + reqId, e);
        }
    }
}
