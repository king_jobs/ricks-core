package org.ricks.net.rpc;

import org.ricks.ioc.RicksMethod;

import java.lang.annotation.*;

/**
 * 声明 RPC 方法
 */
@Documented
@RicksMethod
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RpcMethod {
    short rpcCmd() default 0;
}
