package org.ricks.net.rpc;

import java.util.Objects;

public class ConsumerModule {


    private ProtocolModule protocolModule;

    // 负载均衡方式
    private String loadBalancer;

    // 消费哪个provider
    private String consumer;

    public ConsumerModule(ProtocolModule protocolModule, String loadBalancer, String consumer) {
        this.protocolModule = protocolModule;
        this.consumer = consumer;
        this.loadBalancer = loadBalancer;
    }

    public ConsumerModule(String protocolModule, String loadBalancer, String consumer) {
        this.protocolModule = new ProtocolModule((byte) 0, protocolModule);
        this.consumer = consumer;
        this.loadBalancer = loadBalancer;
    }

    public String getConsumer() {
        return consumer;
    }

    public void setConsumer(String consumer) {
        this.consumer = consumer;
    }

    public String getLoadBalancer() {
        return loadBalancer;
    }

    public void setLoadBalancer(String loadBalancer) {
        this.loadBalancer = loadBalancer;
    }

    public ProtocolModule getProtocolModule() {
        return protocolModule;
    }

    public void setProtocolModule(ProtocolModule protocolModule) {
        this.protocolModule = protocolModule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ConsumerModule that = (ConsumerModule) o;
        return Objects.equals(protocolModule, that.protocolModule) && Objects.equals(consumer, that.consumer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(protocolModule, loadBalancer, consumer);
    }
}
