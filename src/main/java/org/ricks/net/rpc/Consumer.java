package org.ricks.net.rpc;

import org.ricks.common.lang.Logger;
import org.ricks.net.HeadPacket;
import org.ricks.net.IConsumer;
import org.ricks.net.NetContext;
import org.ricks.net.balancer.AbstractConsumerLoadBalancer;
import org.ricks.net.balancer.IConsumerLoadBalancer;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.IPacket;
import org.ricks.protocol.ProtocolManager;
import org.ricks.protocol.collection.CollectionUtils;
import org.ricks.common.RandomUtil;
import java.util.HashMap;
import java.util.Map;

/**
 * 服务消费者 ，以网关节点 和 游戏节点 举例。 网关启动tcp server  游戏节点 tcp client 连接网关 tcp server
 * 网关tcp 连接触发，就是tcp client
 *
 * 以实际项目为基础进行考虑，客户端消息头 只会发送两个字节得cmd 作为消息请求指令。如果考虑网关通用性 搭建网关即可用。
 * 就需要客户端请求头 发一个字节标识 路由模块 比如:聊天集群 游戏集群 邮件集群 工会集群
 * iogame 使用得cmd + subcmd 以一个int 4个字节进行标识。cmd处于高位 subcmd处于低位
 * 齐衡三方案 消息头用了 Map k=tag v=玩家登录分配得节点ID 然后根据bitmap映射 RPC
 *
 * 如果能说服客户端还好，如果说服不了 那就意味网关层面 增加一个模块集群 就得 switch case 根据cmd自己判断所属模块
 *
 *  rpc核心
 *
 *  Consumer 是多节点情况。需要依赖规则拿到session  随机算法 hash一致性算法
 *
 *  以IOC 配置Bean模式进行注入
 */
public class Consumer implements IConsumer {

    /**
     * k= moduleId v=路由策略
     */
    private final Map<Byte, IConsumerLoadBalancer> consumerLoadBalancerMap = new HashMap<>();

    public Consumer() {
        init();
    }


    /**
     * 初始化模块路由策略，比如 game强绑定策略 其它模块可以使用hash一致性策略
     */
    @Override
    public void init() {
        var consumerConfig = NetContext.consumerConfig();
        if (consumerConfig == null || CollectionUtils.isEmpty(consumerConfig.getConsumers())) {
            return;
        }
        var consumers = consumerConfig.getConsumers();
        for (var consumer : consumers) {
            consumerLoadBalancerMap.put(consumer.getProtocolModule().getId(), AbstractConsumerLoadBalancer.valueOf(consumer.getLoadBalancer()));
        }
    }

    /**
     * cmd 拿到 moduleId  , moduleId 拿到获取Session规则。 获取Session规则拿到写出得Session
     * 然后拿到路由规则
     *
     * cmd 与 module 之间映射关系 ,单例模式进行维护。为了保障内网环境 一个网关节点  多个不同版本得游戏节点 情况。就意味除了 module - set<Session> 映射关系
     * Session - set<cmd> 映射关系 。考虑断线重连Session会变化 可以使用address进行映射
     *
     * 考虑得复杂度飙升，该做减法了。就使用switch case 来。增加模块 就case模块指令
     * @param packet
     * @param argument
     */
    public void send(HeadPacket head, IPacket packet, Object argument) {
        try {
            //第一步，cmd拿到module,如果使用switch case cmd 就不用考虑cmd获取module问题
            //第二步，模块拿到session 获取策略
            var loadBalancer = loadBalancer(head.getModule());
            //第三步，策略拿到session,session 获取策略是接口，就意味可以自定义规则
            var session = loadBalancer.loadBalancer(head, packet, argument);
            //hash id 准备分配任务执行线程
            var taskExecutorHash = calTaskExecutorHash(argument);
            //开始发送消息
            NetContext.getRouter().send(session, packet);
        } catch (Throwable t) {
            Logger.error("consumer发送未知异常", t);
        }
    }

    /**
     * 同步问答模式
     *             ByteBuf buf = new ByteBuf(1024);
     *             ProtocolManager.write(buf,packet);
     *             参数形式待定，
     * @param head
     * @param packet
     * @param argument
     * @return
     */
    public byte[] syncAsk(HeadPacket head, IPacket packet, Object argument) {
        try {
            //第一步，cmd拿到module,如果使用switch case cmd 就不用考虑cmd获取module问题
            //第二步，模块拿到session 获取策略
            var loadBalancer = loadBalancer(head.getModule());
            //第三步，策略拿到session,session 获取策略是接口，就意味可以自定义规则
            var session = loadBalancer.loadBalancer(head, packet, argument);
            //hash id 准备分配任务执行线程
            var taskExecutorHash = calTaskExecutorHash(argument);
            ByteBuf buf = new ByteBuf(1024);
            ProtocolManager.write(buf,packet);
            //开始发送消息
            return NetContext.getRouter().syncAsk(session, head.getCmd(), buf.toArray() );
        } catch (Throwable t) {
            Logger.error("consumer发送未知异常", t);
            throw new RuntimeException(t);
        }
    }

    public IConsumerLoadBalancer loadBalancer(byte moduleId) {
        return consumerLoadBalancerMap.get(moduleId);
    }

    /**
     * @param argument uid or session
     * @return
     */
    public static int calTaskExecutorHash(Object argument) {
        var hash = 0;
        if (argument == null) {
            hash = RandomUtil.randomInt();
        } else if (argument instanceof Number) {
            hash = ((Number) argument).intValue();
        } else {
            hash = argument.hashCode();
        }
        return calTaskExecutorHash(hash);
    }
}
