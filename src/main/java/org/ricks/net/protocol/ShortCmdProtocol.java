package org.ricks.net.protocol;

import org.ricks.ioc.Message;
import org.ricks.net.handler.Protocol;
import org.ricks.net.AioSession;

import java.nio.ByteBuffer;

/**
 * @author ricks
 * @Description: 到这一步时，已经被kcp 解析了
 * @date 2022/10/1718:47
 */


public class ShortCmdProtocol<T> implements Protocol {

    @Override
    public Object decode(ByteBuffer data, AioSession session) {
        int length = data.getInt();
        short cmd = data.getShort();
        byte[] bytes = new byte[length - 2];
        data.get(bytes);
        return new Message(cmd, bytes);
    }
}
