package org.ricks.net;

import org.ricks.net.handler.MessageProcessor;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户分组，对比 UserSessions 哪个使用得方便 选择哪个方案
 * 但是逻辑节点 分组可以使用这种模式，
 * @param <T>
 */
abstract class GroupMessageProcessor<T> implements MessageProcessor<T>, GroupIo {

    /**
     * 对内 对外 其实是两个对象
     * 那就意味其实也可以对玩家得session进行分组
     * 也可以对逻辑节点进行分组
     */
    private Map<String, GroupUnit> sessionGroup = new ConcurrentHashMap<>();

    /**
     * 将AioSession加入群组group
     *
     * @param group
     * @param session
     */
    @Override
    public final synchronized void join(String group, AioSession session) {
        GroupUnit groupUnit = sessionGroup.get(group);
        if (groupUnit == null) {
            groupUnit = new GroupUnit();
            sessionGroup.put(group, groupUnit);
        }
        groupUnit.groupList.add(session);
    }

    @Override
    public final synchronized void remove(String group, AioSession session) {
        GroupUnit groupUnit = sessionGroup.get(group);
        if (groupUnit == null) {
            return;
        }
        groupUnit.groupList.remove(session);
        if (groupUnit.groupList.isEmpty()) {
            sessionGroup.remove(group);
        }
    }

    @Override
    public final void remove(AioSession session) {
        for (String group : sessionGroup.keySet()) {
            remove(group, session);
        }
    }

    @Override
    public void writeToGroup(String group, byte[] t) {
        GroupUnit groupUnit = sessionGroup.get(group);
        for (AioSession aioSession : groupUnit.groupList) {
            try {
                aioSession.writeBuffer().write(t);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class GroupUnit {
        Set<AioSession> groupList = new HashSet<>();
    }
}
