package org.ricks.agent;

import org.ricks.common.lang.Logger;
import org.ricks.common.watch.SimpleWatcher;

import java.io.File;
import java.io.FileInputStream;
import java.lang.instrument.ClassDefinition;
import java.lang.instrument.Instrumentation;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.Arrays;

/**
 * class热更
 */
public class JavaAgent extends SimpleWatcher {

    private static Instrumentation inst;

    public static void premain(String args, Instrumentation inst){
        JavaAgent.inst = inst;
//        System.out.println("Hi, I'm premain!" + inst.toString());
    }

    public static void agentmain(String args, Instrumentation inst){
        JavaAgent.inst = inst;
//        System.out.println("Hi, I'm agentmain!" + inst.toString());
    }

//    @Override
//    public void onCreate(Path file) {
//        loop(file.toFile());
//    }
//
//    @Override
//    public void onModify(Path file) {
//        loop(file.toFile());
//    }
//
//    @Override
//    public void onDelete(Path path) {
//        String absolutePath = path.toFile().getAbsolutePath();
//        if (!absolutePath.endsWith(".class") && !absolutePath.endsWith(".dat")) path.toFile().mkdirs();
//    }

    @Override
    public void onCreate(WatchEvent<?> event, Path currentPath) {
        Object obj = event.context();
        Logger.info("创建："+currentPath+"-> "+ obj);
//        loop(currentPath.toFile());
    }

    @Override
    public void onModify(WatchEvent<?> event, Path currentPath) {
        Object obj = event.context();
        String s = currentPath.toString() + "/" + obj.toString();
        Logger.info("修改："+currentPath+"-> " + obj + " -> " + s);
        File file = new File(s);
        if(s.startsWith("test/"))  s = s.replaceAll("test\\/","");
        loop(file,s);
    }

    @Override
    public void onDelete(WatchEvent<?> event, Path currentPath) {
        Object obj = event.context();
        final String s = currentPath.toString() + "\\" + obj.toString();
        Logger.info("删除："+currentPath+"-> " + obj + " -> " + s);
    }

    @Override
    public void onOverflow(WatchEvent<?> event, Path currentPath) {
        Object obj = event.context();
        final String s = currentPath.toString() + "\\" + obj.toString();
        Logger.info("Overflow："+currentPath+"-> " + obj + " -> " + s);
    }

    private void loop(File file,String s){
//        // 获取类加载器
//        ClassLoader classLoader = JavaAgent.class.getClassLoader();
//        // 获取文件路径
//        URI uri = null;
//        try {
//            uri = classLoader.getResource( currentPath.toString()).toURI();
//        } catch (URISyntaxException e) {
//            throw new RuntimeException(e);
//        }
        // 变成一个文件file映射
        if(file.isDirectory()){
            Logger.info("111111111111111111111111");
//            Arrays.stream(file.listFiles()).forEach(f -> loop(f));
        } else {
            Logger.info("2222222222222222222222222");
            redefineClasses(file,s);
        }
    }

    private void redefineClasses(File classFile,String s) {
        Logger.info("start load class file: " + classFile.getAbsolutePath() + " & s=" + s);
//        Arrays.stream(inst.getAllLoadedClasses()).forEach(aClass -> {
//            System.err.println("JVM加载class name:" + aClass.getName() + " SimpleName:" +aClass.getSimpleName() + aClass.getSimpleName() + " CanonicalName" + aClass.getCanonicalName() + "  packname" + aClass.getPackageName());
//        });
        Arrays.stream(inst.getAllLoadedClasses()).filter(c -> {
            //.replaceAll("\\.","\\"+File.separator)+".class"
            return s.endsWith(c.getName().replaceAll("\\.","\\"+File.separator)+".class");
        }).forEach(c -> {
            try {
                byte[] bytes = fileToBytes(classFile);
                ClassDefinition classDefinition = new ClassDefinition(c,bytes);
                inst.redefineClasses(classDefinition);
            } catch (Exception e) {
                Logger.error("error redefined: "+ classFile.getAbsolutePath());
                e.printStackTrace();
            }
            Logger.info("class load success:  "+ classFile.getAbsolutePath());
        });
    }

    private byte[] fileToBytes(File file) {
        byte[] bytes = new byte[(int) file.length()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(bytes);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }
}
