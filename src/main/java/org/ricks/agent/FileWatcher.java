package org.ricks.agent;

import org.ricks.common.lang.ExpiringMap;
import org.ricks.common.lang.ThreadNameFactory;
import org.ricks.common.lang.Logger;
import org.ricks.common.PathUtil;
import org.ricks.common.TimeUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 文件监听
 */
public class FileWatcher {

    private static final String TABLE_DATA = "data.dat";

    private static final String BASE_CLASS_PATH = "ricks//classes";
    private static final String DATA_PATH = "ricks//data";

    private static final ExpiringMap expiringMap =  ExpiringMap.builder().expiration(2, TimeUnit.SECONDS).build();

    /**
     * 一个线程统一监听文件
     */
    private static final ExecutorService executorService  = Executors.newSingleThreadExecutor(new ThreadNameFactory(1,"watcher"));;
    /**
     * 监视的文件目录
     */
    private Path watchDirectory;
    /**
     * 监视的文件类型正则表达式
     */
    private FilenameFilter filenameFilter;
    /**
     * 监视到的文件监听器
     */
    private FileListener dataListener;
//    /**
//     * 监视到的文件监听器
//     */
//    private FileListener classListener = new JavaAgent();

    private volatile boolean isShutdown= false;

    private static WatchService watchService;

    static {
        try {
            watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    ;


    public FileWatcher(String watchDirectory, FilenameFilter filenameFilter, FileListener fileListener) {
        this.watchDirectory = Paths.get(PathUtil.currPath() + "//" + watchDirectory);
        this.filenameFilter = filenameFilter;
        this.dataListener = fileListener;
        Runtime.getRuntime().addShutdownHook(new Thread(() -> stop()));
    }

    public FileWatcher(String watchDirectory, FileListener fileListener) {
        this(watchDirectory, new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                String fileName = name.toLowerCase();
                boolean check = dir.getAbsolutePath().contains(BASE_CLASS_PATH.replace("//","/")) || dir.getAbsolutePath().contains(BASE_CLASS_PATH.replace("//","\\"));
                return check && fileName.endsWith(".class");
            }
        }, fileListener);
    }


//    /**
//     * 监听目录，根据文件的变化处理文件
//     */
//    public void start() {
//        // 监听新文件
//        try {
//            Path classPath = watchDirectory.resolve(BASE_CLASS_PATH);
//            Path dataPath = watchDirectory.resolve(DATA_PATH);
//            File file = classPath.toFile();
//            if(!file.exists()) file.mkdirs();
//            file = dataPath.toFile();
//            if(!file.exists()) file.mkdirs();
//            register(dataPath);
//            register(classPath);
//            Files.walkFileTree(classPath, new SimpleFileVisitor<Path>() {
//
//                @Override
//                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)  {
//                    register(dir);
//                    return FileVisitResult.CONTINUE;
//                }
//            });
//            executorService.execute(() -> watchTask(watchService));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    private void register(Path path) {
        try {
            if(path.toFile().isDirectory()) {
                path.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private void watchTask(WatchService watchService) {
//        while (!isShutdown) {
//            try {
//                WatchKey watchKey = watchService.take();
//                for (WatchEvent event : watchKey.pollEvents()) {
//                    WatchEvent.Kind eventKind = event.kind();
//                    if (eventKind == StandardWatchEventKinds.OVERFLOW) {
//                        continue;
//                    }
//                    String fileName = event.context().toString();
//                    Path file = (Path)watchKey.watchable();
//                    file = file.resolve(fileName);
//                    String key = file.toFile().getAbsolutePath();
//                    if(expiringMap.containsKey(key)) continue;
//                    Path dataPath = watchDirectory.resolve( DATA_PATH).resolve(TABLE_DATA);
//                    if(key.equals(dataPath.toFile().getAbsolutePath())) {
//                        doFileChange(dataListener,dataPath,key);
//                        continue;
//                    }
//                    //文件名不匹配
//                    if (this.filenameFilter != null && !this.filenameFilter.accept(file.toFile(), fileName)) {
//                        continue;
//                    }
//                    if (file.toFile().getCanonicalFile().isDirectory()) {
//                        register(file);
//                        File[] files = file.toFile().listFiles();
//                        Arrays.stream(files).forEach(f -> register(f.toPath()));
//                        continue;
//                    }
//                    if (eventKind == StandardWatchEventKinds.ENTRY_CREATE || eventKind == StandardWatchEventKinds.ENTRY_MODIFY || eventKind == StandardWatchEventKinds.ENTRY_DELETE) {
//                        doFileChange(classListener,file,key);
//                    }
//                }
//                boolean isKeyValid = watchKey.reset();
//                if (!isKeyValid) {
//                    Path path = (Path)watchKey.watchable();
//                    watchValid(path);
//                }
//            } catch (IOException | InterruptedException e) {
//                e.printStackTrace();
//            } catch (ClosedWatchServiceException e) {
//                break;
//            }
//        }
//    }

    private void watchValid(Path watchPath) {
        Path classPath = watchDirectory.resolve(BASE_CLASS_PATH);
        File classFile = classPath.toFile();
        Path dataPath = watchDirectory.resolve(DATA_PATH);
        File dataFile = classPath.toFile();
        if(watchPath.toFile().getAbsolutePath().equals(classFile.getAbsolutePath())) {
            checkFile(classFile,classPath);
        } else if(watchPath.toFile().getAbsolutePath().equals(dataFile.getAbsolutePath())) {
            checkFile(dataFile,dataPath);
        }
    }

    private void checkFile(File file,Path path) {
        if(!file.exists()) {
            file.mkdirs();
            register(path);
        }
    }
    private void doFileChange(FileListener fileListener,Path path,String key) throws InterruptedException {
        while (true) {
            try {
                fileListener.onModify(path);
                expiringMap.put(key, TimeUtils.currentTimeMillis());
                break;
            } catch (FileNotFoundException  | ClassFormatError e) {
                Thread.sleep(100);
            }
        }
    }

    public void stop() {
        try {
            Logger.info("开始销毁文件监听器 [watcher]........");
            isShutdown = true;
            watchService.close();
            executorService.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
