package org.ricks.agent;

import java.io.FileNotFoundException;
import java.nio.file.Path;

/**
 * 文件变化接口只对内 不对外
 */
public interface FileListener {

    /**
     * 在监视目录中新增文件时的处理操作
     * @param file
     */
    void onCreate(Path file);

    /**
     * 在监视目录中修改文件时的处理操作
     * @param file
     */
    void onModify(Path file) throws FileNotFoundException;

    /**
     * 在监视目录中删除文件时的处理操作
     * @param file
     */
    void onDelete(Path file);
}
