package org.ricks.agent;

import org.ricks.common.exception.IORuntimeException;
import org.ricks.common.exception.ServerBootstrapException;
import org.ricks.ioc.Init;
import org.ricks.ioc.IocHolder;
import org.ricks.common.lang.Logger;
import org.ricks.protocol.ByteBuf;
import org.ricks.protocol.ProtocolManager;
import org.ricks.common.PathUtil;
import org.ricks.common.ReflectUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ricks
 * @date 2022/9/111:36
 */
public class DataAgent implements FileListener {
    private static Path path = Paths.get(PathUtil.currPath() + "/data.dat");
    public static Map<String,Map<Integer,Map<String,Object>>> context = new HashMap<>();

    public DataAgent() {
        try {
            reloadData(path);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Path file) {
        try {
            reloadData(path);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //重新加载data ，然后调用init 重新初始化内存数据
    @Override
    public void onModify(Path path) {
        File dataFile = path.toFile();
        if(dataFile.getName().endsWith("data.dat")){
            try {
                Logger.info("==========================  data change  ========================");
                Logger.info("==========================  "+dataFile.length()+" byte  ========================");
                reloadData(path);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            reloadInit();
        }
    }

    @Override
    public void onDelete(Path file) {
        System.err.println("data delete ........");
    }

    public void reloadData(Path path) throws InterruptedException, IOException {
        byte[] bytes = null;
        try {
            if (path.toFile().exists()) {
                FileInputStream inputStream = new FileInputStream(path.toFile());
                bytes = inputStream.readAllBytes();
                ByteBuf buf = new ByteBuf(bytes);
                context = (Map<String, Map<Integer, Map<String, Object>>>) ProtocolManager.read(buf);
                inputStream.close();
            } else throw new ServerBootstrapException(" data not found !");
        } catch (IORuntimeException e) {
            if(e.getMessage().contains("EOFException")) {
                int len =bytes != null ? bytes.length:0;
                Logger.warn(" data eof start reload data, curr data byte size：" + len);
                Thread.sleep(100);
                reloadData(path);
            }
        }
    }

    public void reloadInit() {
        List<Object> beans = IocHolder.getIoc().allBeans();
        beans.stream().filter(b -> b instanceof Init).forEach(bean -> ReflectUtil.invoke(bean,"init"));
    }

//    public static <T> List<T> getBeanList(Class<T> clazz) {
//        String name = clazz.getSimpleName().replaceAll("Data","");
//        Map<Integer,Map<String,Object>> beanMap = context.get(name);
//        return beanMap.values().stream().map(data -> BeanUtil.toBean(data,clazz)).collect(Collectors.toList());
//    }
//
//    public static <T> Map<Integer,T> getBeanMap(Class<T> clazz) {
//        Map<Integer,T> map = new HashMap<>();
//        String name = clazz.getSimpleName().replaceAll("Data","");
//        if(context.containsKey(name)) {
//            Map<Integer, Map<String, Object>> beanMap = context.get(name);
//            beanMap.forEach((k, v) -> map.putIfAbsent(k, BeanUtil.toBean(v, clazz)));
//        }
//        return map;
//    }
}
