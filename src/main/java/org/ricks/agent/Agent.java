package org.ricks.agent;

import org.ricks.common.watch.SimpleWatcher;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ricks
 * @Description: class热更开启注解 默认都开启
 * @date 2022/12/3013:56
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Agent {
    Class<? extends SimpleWatcher> dataLoad() default JavaAgent.class;
}
